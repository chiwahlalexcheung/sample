import { Action } from 'redux'
import APIManager from '@lib/apiManager';

export enum ServiceActionTypes {
  UPLOAD_DOCUMENT = 'UPLOAD_DOCUMENT',
  UPLOAD_DOCUMENT_SUCCEED = 'UPLOAD_DOCUMENT_SUCCEED',
  UPLOAD_DOCUMENT_FAILED = 'UPLOAD_DOCUMENT_FAILED',
  RESET_DOCUMENT = 'RESET_DOCUMENT',
  FETCH_CONTACT = 'FETCH_CONTACT',
  FETCH_CONTACT_SUCCEED = 'FETCH_CONTACT_SUCCEED',
  FETCH_CONTACT_FAILED = 'FETCH_CONTACT_FAILED',
  FETCH_GENERAL_CONTACT = 'FETCH_GENERAL_CONTACT',
  FETCH_GENERAL_CONTACT_SUCCEED = 'FETCH_GENERAL_CONTACT_SUCCEED',
  FETCH_GENERAL_CONTACT_FAILED = 'FETCH_GENERAL_CONTACT_FAILED'
}

export type ServiceAction =
  | Action<ServiceActionTypes.UPLOAD_DOCUMENT>
  | Action<ServiceActionTypes.UPLOAD_DOCUMENT_SUCCEED>
  | Action<ServiceActionTypes.UPLOAD_DOCUMENT_FAILED>
  | Action<ServiceActionTypes.RESET_DOCUMENT>
  | Action<ServiceActionTypes.FETCH_CONTACT>
  | Action<ServiceActionTypes.FETCH_CONTACT_SUCCEED>
  | Action<ServiceActionTypes.FETCH_CONTACT_FAILED>
  | Action<ServiceActionTypes.FETCH_GENERAL_CONTACT>
  | Action<ServiceActionTypes.FETCH_GENERAL_CONTACT_SUCCEED>
  | Action<ServiceActionTypes.FETCH_GENERAL_CONTACT_FAILED>

export const resetUploadDocument = () => ({ type: ServiceActionTypes.RESET_DOCUMENT });
export const requestUploadDocument = (index) => ({ type: ServiceActionTypes.UPLOAD_DOCUMENT, index });
export const requestUploadDocumentSucceed = (index, payload) => ({ type: ServiceActionTypes.UPLOAD_DOCUMENT_SUCCEED, index, payload });
export const requestUploadDocumentError = (index, payload) => ({ type: ServiceActionTypes.UPLOAD_DOCUMENT_FAILED, index, payload });
export const uploadDocument = (index: any, payload: any) => async (dispatch) => {
    dispatch(requestUploadDocument(index));

    // dispatch(requestUploadDocumentSucceed(index, {}));
    const { data, error } = await APIManager.post({
        dispatch,
        url: 'uploadDocument',
        data: payload,
        underAuth: true,
    });
    if (error) {
        dispatch(requestUploadDocumentError(index, error));
        return null;
    }
    dispatch(requestUploadDocumentSucceed(index, data));
    return data;
};

export const requestContact = () => ({ type: ServiceActionTypes.FETCH_CONTACT });
export const requestContactSucceed = (payload) => ({ type: ServiceActionTypes.FETCH_CONTACT_SUCCEED, payload });
export const requestContactError = (payload) => ({ type: ServiceActionTypes.FETCH_CONTACT_FAILED, payload });
export const fetchContact = (isServer: boolean) => async (dispatch) => {
    dispatch(requestContact());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'cscenterContacts',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestContactError(error));
    } else {
      dispatch(requestContactSucceed(data));
    }
};

export const requestGeneralContact = () => ({ type: ServiceActionTypes.FETCH_GENERAL_CONTACT });
export const requestGeneralContactSucceed = (payload) => ({ type: ServiceActionTypes.FETCH_GENERAL_CONTACT_SUCCEED, payload });
export const requestGeneralContactError = (payload) => ({ type: ServiceActionTypes.FETCH_GENERAL_CONTACT_FAILED, payload });
export const fetchGeneralContact = (isServer: boolean) => async (dispatch) => {
    dispatch(requestGeneralContact());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'generalContacts',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestGeneralContactError(error));
    } else {
      dispatch(requestGeneralContactSucceed(data));
    }
};