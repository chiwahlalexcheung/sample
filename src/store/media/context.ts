import { useSelector, useDispatch } from 'react-redux'
import { fetchMainBanners } from './actions'
// import { MediaState } from './reducers'
import { AppState } from './../store'

export const useMainBanner = () => {
  const { list, loading, error } = useSelector(({ media }: AppState) => media.banner);
  const dispatch = useDispatch();
  const onClick = () => {
    dispatch(fetchMainBanners(false));
  }
  return {
    fetch: onClick,
    loading,
    list,
    error,
  };
}
