import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'white !important',
    background: '#FFFFFF 0% 0% no-repeat padding-box',
  },
}));

const useBoxStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        padding: 67,
    },
}))

const useAccordionStyles = makeStyles(theme => ({
    root: {
        paddingLeft: '46px',
    },
    summary: {
        height: '56px',
    }

}))
export {
  useStyles,
  useBoxStyles,
  useAccordionStyles
};
