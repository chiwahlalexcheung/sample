import { ApplicationAction, ApplicationActionTypes } from './actions'
import { BasicInfoStateProps, BasicInfoWithSuccessStateProps } from '@interfaces/state'
export interface ApplicationState {
  readonly form: BasicInfoStateProps;
  readonly otp: BasicInfoStateProps;
  readonly verifyOtp: BasicInfoWithSuccessStateProps;
  readonly evaluation: BasicInfoStateProps;
}

const initialApplicationState = {
  form: {
    info: null,
    loading: false,
    error: null,
  },
  otp: {
    info: null,
    loading: false,
    error: null,
  },
  verifyOtp: {
    info: null,
    loading: false,
    error: null,
    succeed: false,
  },
  evaluation: {
    info: null,
    loading: false,
    error: null,
  },
}

/**
 * Application Reducer
 */
export const applicationReducer = (
  state: ApplicationState = initialApplicationState,
  action: any
): ApplicationState => {
  const { form, otp, verifyOtp, evaluation } = state;
  switch (action.type) {
    case ApplicationActionTypes.VERIFY_OTP:
      return {
        ...state,
        verifyOtp: {
          ...verifyOtp,
          loading: true,
          succeed: false,
        },
      }
    case ApplicationActionTypes.VERIFY_OTP_SUCCEED:
      return {
        ...state,
        verifyOtp: {
          ...verifyOtp,
          loading: false,
          succeed: true,
          info: action.payload,
        },
      }
    case ApplicationActionTypes.VERIFY_OTP_FAILED:
      return {
        ...state,
        verifyOtp: {
          ...verifyOtp,
          loading: false,
          error: action.payload,
        },
      }
    case ApplicationActionTypes.SEND_OTP:
      return {
        ...state,
        otp: {
          ...otp,
          loading: true,
        },
      }
    case ApplicationActionTypes.SEND_OTP_SUCCEED:
      return {
        ...state,
        otp: {
          ...otp,
          loading: false,
          info: action.payload,
        },
      }
    case ApplicationActionTypes.SEND_OTP_FAILED:
      return {
        ...state,
        otp: {
          ...otp,
          loading: false,
          error: action.payload,
        },
      }
    case ApplicationActionTypes.SUBMIT_APPLICATION:
      return {
        ...state,
        form: {
          ...form,
          loading: true,
        },
      }
    case ApplicationActionTypes.SUBMIT_APPLICATION_SUCCEED:
      return {
        ...state,
        form: {
          ...form,
          loading: false,
          info: action.payload,
        },
      }
    case ApplicationActionTypes.SUBMIT_APPLICATION_FAILED:
      return {
        ...state,
        form: {
          ...form,
          loading: false,
          error: action.payload,
        },
      }
      case ApplicationActionTypes.FETCH_EVALUATION:
        return {
          ...state,
          evaluation: {
            ...evaluation,
            loading: true,
          },
        }
      case ApplicationActionTypes.FETCH_EVALUATION_SUCCEED:
        console.log("!!!!!!!newData", {
            ...evaluation.info,
            [action.infoType]: action.payload
        })
        return {
          ...state,
          evaluation: {
            ...evaluation,
            loading: false,
            info: {
                ...evaluation.info,
                [action.infoType]: action.payload
            },
          }
        }
      case ApplicationActionTypes.FETCH_EVALUATION_FAILED:
        return {
          ...state,
          evaluation: {
            ...evaluation,
            loading: false,
            error: action.payload,
          },
        }
    default:
      return state
  }
}
