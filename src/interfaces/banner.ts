import {
  TypographyProps,
} from '@interfaces/text'

export interface BannerListItem {
  label: string;
  thumbnail: string;
  banner?: string;
  labelProps?: TypographyProps;
  title: string;
  subTitle?: string;
  buttonLabel?: string;
  buttonLink?: string;
};

// declare module "*.banner" {
//   interface BannerListItem {
//     label: string;
//     thumbnail: string;
//     banner?: string;
//     labelProps?: TypographyProps;
//     title: string;
//     subTitle?: string;
//     buttonLabel?: string;
//     buttonLink?: string;
//   };
  
//   export {
//     BannerListItem,
//   } 
// }
