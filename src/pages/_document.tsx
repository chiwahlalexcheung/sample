import Document, {
  Html,
  Head,
  Main,
  NextScript,
  DocumentContext,
  DocumentInitialProps,
} from 'next/document'
import sprite from 'svg-sprite-loader/runtime/sprite.build'
import { ServerStyleSheets } from "@material-ui/styles";

interface CustomDocumentProps {
  spriteContent: string
}

export default class CustomDocument extends Document<CustomDocumentProps> {
  public static async getInitialProps(
    ctx: DocumentContext
  ): Promise<CustomDocumentProps & DocumentInitialProps> {
    const sheet = new ServerStyleSheets();
    const originalRenderPage = ctx.renderPage;
    const spriteContent = sprite.stringify()
    try {
       // @ts-ignore
      ctx.renderPage = () =>
        originalRenderPage({
          enhanceApp: App => props => sheet.collect(<App {...props} />)
        })

      const documentProps = await Document.getInitialProps(ctx)
      // TODO fix it future, do not skip it
      // @ts-ignore
      return {
        ...documentProps,
        styles: (
          <>
            {documentProps.styles}
            {sheet.getStyleElement()}
          </>
        )
      }
    } finally {
      // @ts-ignore
      ctx.renderPage(sheet);
    }
  }
  public render(): JSX.Element {
    return (
      <Html>
        <Head></Head>
        <body>
          <div dangerouslySetInnerHTML={{ __html: this.props.spriteContent }} />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}
