import React, { FC, useMemo } from 'react'
import { map } from 'lodash'
import {
  Box,
} from '@material-ui/core'
import { useTranslation } from 'next-i18next'
import MenuListItemProps from '@interfaces/menu'
import {
  TypographyProps,
} from '@interfaces/text'
import Link from '@components/common/Link'
import RouteSources from '@config/routes';
import {
  useRootStyles,
} from './styles'

export interface MainBannerProps {
  items?: MenuListItemProps[];
}

export const MenuItem: FC<MenuListItemProps> = ({ labelProps, label, link, isActive }) => (
  <Link href={link} label={label} labelProps={labelProps} rel={'contents'} />
);

export const MenuList: FC<MainBannerProps> = ({ items=[] }) => {
  const classes = useRootStyles();
  return (
    <Box display={'flex'} className={classes.root}>
      {
        map(items, (item: MenuListItemProps) => (
          <Box display={'flex'} key={item.key}>
            <MenuItem {...item} />
          </Box>
        ))
      }
    </Box>
  );
}

const MainMenu: FC<{}> = () => {
  const rootClasses = useRootStyles();
  const { t } = useTranslation();
  const labelProps: TypographyProps = {
    component: 'h1',
    className: rootClasses.item,
  };
  const mainMenuItems: MenuListItemProps[] = useMemo(() => [
    {
      label: t('menu.first-mortgage'),
      key: 'first',
      link: RouteSources.firstMortgage,
      labelProps,
    },
    {
      label: t('menu.second-mortgage'),
      key: 'second',
      link: RouteSources.secondMortgage,
      labelProps,
    },
    {
      label: t('menu.loan-calculator'),
      key: 'loan',
      link: RouteSources.loanCalculator,
      labelProps,
    },
    {
      label: t('menu.online-property'),
      key: 'online',
      link: RouteSources.onlineProperty,
      labelProps,
    },
  ], [t]);
  return (
    <MenuList items={mainMenuItems} />
  );
}

export default MainMenu;
