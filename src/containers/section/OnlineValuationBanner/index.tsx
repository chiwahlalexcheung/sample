import React, { FC, useCallback } from 'react'
import css from 'clsx'
import { map } from 'lodash'
import { useRouter } from 'next/router'
import Routes from '@config/routes'
import StaticSource from '@config/sources'

import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import Link from '@components/common/Link'
import Fade from 'react-reveal/Fade'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Box,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStyles,
  useBoxStyles,
  useBigToolStyles,
  useSmallToolStyles,
} from './styles';

import {
    BannerBoxInfoProps,
    BannerBoxProps,
    BannerBox,
    InnerToolItemProps,
    InnerToolItem
} from '@containers/section/MortgageBanner'

export const OnlineValuationBanner: FC = () => {
  const { t } = useTranslation('section');
  const { t: Bt } = useTranslation('button');
  const classes = useStyles();
  const info: BannerBoxInfoProps = {
    title: 'Free Online Property Valuation',
    description: 'With Sun Hung Kai Credit Second Mortgage, you can utilize your property’s equity even if you already have an existing mortgage to help paying off your credit cards or other outstanding debts. Application is fast and easy! Simply provide simple income documentations and property ownership proof. Our speedy approval can satisfy any urgent cash needs.',
    applyLabel: 'Apply Now',
    applyLink: '/',
    contactLabel: 'Contact Us',
    contactLink: '/',
    label: '',
    labelLink: '/',
    banner: StaticSource.banner.onlineValuationBanner,
  };
  // valuateNow
  const tools: InnerToolItemProps[] = [
    {
      key: 'mortgage',
      icon: StaticSource.banner.loanCalculatorBorder,
      title: t('useful-tools.mortgageLine'),
      label: Bt('checkNow'),
      link: Routes.loanCalculator,
      big: true,
      dim: true,
    },
    {
      key: 'firstMortgage',
      icon: StaticSource.banner.onlineValuationBorder,
      title: t('useful-tools.firstMortgage'),
      label: Bt('applyNow'),
      link: Routes.firstMortgage,
      big: false,
    },
    {
      key: 'secondMortgage',
      icon: StaticSource.valuationService,
      title: t('useful-tools.secondMortgage'),
      label: Bt('applyNow'),
      link: Routes.secondMortgage,
      big: false,
    },
  ];
  // InnerToolItem
  return (
    <div className={classes.root}>
        <InnerPage>
          <Fade bottom>
            <BannerBox info={info}>
              {
                map(tools, ({ key, ...t }: InnerToolItemProps) => (
                  <InnerToolItem key={key} {...t} />
                ))
              }
            </BannerBox>
          </Fade>
        </InnerPage>
    </div>
  );
};

export default OnlineValuationBanner;
