import axios from 'axios';
import { forOwn, get as getFrom, includes, set as toSet, pick, replace } from 'lodash';
// Local
import Config from '@config/service';
import Cookie from '@lib/cookies';
/*
 * Singleton remote API Manger object, and encapsulate all of remote api call behaviors
 * Terry Chan
 * 30/12/2020
 */
const { debug, cookies, domain, apis } = Config;
const Request = axios.create({
    withCredentials: true,
    baseURL: domain,
})

interface PayloadProps {
    dispatch: Function;
	url: string;
	data?: any;
	underAuth?: boolean;
	skipError?: boolean;
	additional?: any;
    params?: any;
    isServer?: boolean;
    noForceAuth?: boolean;
    method?: string;
    errorKey?: string;
    noFormData?: boolean;
};

interface ResponseProps {
    status: any;
    data?: any;
    message?: any;
}

interface ErrorResponseProps {
    response: ResponseProps;
}

class APIManager {
    // member's loginToken
    loginToken: string = '';
    language: string;
    localStoragesLookup = {
    	loginToken: {field: 'loginToken', storage: 'localToken', initial: null},
        language: {field: 'language', storage: 'language', initial: Config.locale},
    };
    // constructor() {
    // 	this.prepareLocalStoraged();
    // }

    prepareLocalStoraged() {
    	forOwn(this.localStoragesLookup, ({ field, storage, initial }: any) => {
            if (!this[field]) {
                this[field] = Cookie.get(cookies[storage]) || initial;
            }
    		// if (typeof window !== "undefined") {
    		// 	this[field] = localStorage.getItem(cookies[storage], initial);
    		// }
    	});
    }

    clearToken() {
    	const {
    		loginToken: { storage },
    	} = this.localStoragesLookup;
        this.loginToken = null;
        Cookie.remove(cookies[storage]);
    }

    setToken(token) {
        const {
    		loginToken: { storage },
    	} = this.localStoragesLookup;
        Cookie.set(cookies[storage], token);
        this.loginToken = token;
    }

    /*
	 * ================================================================================================================
	 * start of core remote method calls
	 *
	 * Payload Parameters
	 * @Param: dispatch (Function) - Store Dispatching Function
	 * @Param: getState (Function) - Store State Function
	 * @Param: url (String) - URL key to be called from API config
	 * @Param: data (Object) - Data payload to be sent to Remote Endpoint
	 * @Param: underAuth (Boolean) - Indicates the remote call is under auth with LoginToken
	 * @Param: skipError (Boolean) - Indicates throwing out the error as global error
	 * @Param: additional (Object) - Addon option for Axios calls
     * @Param: params (Object) - parameters for the url (e.g /api/v1/member/:userId)
     * @Param: isServer (Boolean) - is request from server rendering
     * @Param: noForceAuth (Boolean) - force check for loginToken empty
     * @Param: noFormData (Boolean) - ignore form data conversion
	 * ================================================================================================================
	 */
    async get(payload: PayloadProps) {
    	return this.executeRequest({ ...payload, method: 'GET' });
    }
    async post(payload: PayloadProps) {
    	return this.executeRequest({ ...payload, method: 'POST' });
    }
    async put(payload: PayloadProps) {
    	return this.executeRequest({ ...payload, method: 'PUT' });
    }
    async del(payload: PayloadProps) {
    	return this.executeRequest({ ...payload, method: 'DELETE' });
    }
    async executeRequest(argu: PayloadProps) {
        const {
            noFormData, dispatch, noForceAuth, method='GET', isServer, url, data={}, params, errorKey, additional={}, underAuth=true, skipError=true,
        } = argu;
        this.prepareLocalStoraged();
        const body = !isServer && typeof window !== 'undefined' ? (data instanceof FormData ? data : {...data}) : { ...data };
        const payloadAddOn = {...additional};
        if (!url) throw new Error('No url key provided.');
        let endpoint = getFrom(apis, url);
        if (!endpoint) throw new Error(`No endpoint mapping with given key ${url}.`);
        console.log('> ', body);
        try {
            if (underAuth && !this.loginToken && !noForceAuth) {
            	if (debug) {
                    console.log('***** APIManager Debug *****');
                    console.log('No LoginToken found');
                }
                if (!skipError) {
                    // dispatch({
                    //     type: GLOBAL_UNAUTH,
                    // });
                    this.clearToken();
                }
                return {error: true};
            }
            if (params) {
                // @ts-ignore
                forOwn(params, (value, key) => {
                    endpoint = replace(endpoint, `:${key}`, value);
                });
            }

            const payload = {
            	url: endpoint,
                method,
                headers: {
                    'Content-Type': 'application/json',
                    language: this.language,    // use language to request
                },
                ...payloadAddOn,
            };
            if (underAuth) {
                toSet(payload, 'headers.Authorization', `${this.loginToken || ''}`);
            }
            if (includes(['POST', 'PUT'], method)) {
                if (!noFormData && body instanceof FormData) {
                    // no need to stringify the target payload, file object will be broken
                    toSet(payload, 'data', body);
                    toSet(payload, 'headers.Content-Type', 'multipart/form-data');

                    if (debug) {
                        console.log('***** APIManager POST/PUT Method Debug *****');
                        console.log('content-type: multipart/form-data');
                        console.log('endpoint: ', endpoint);
                        console.log('data: ');
                        // @ts-ignore
                        for (let pair of body.entries()) {
                            console.log(pair[0]+ ': ' + pair[1]); 
                        }
                    }
                } else {

                    if (debug) {
                        console.log('***** APIManager POST/PUT Method Debug *****');
                        console.log('content-type: application/json');
                        console.log('endpoint: ', endpoint);
                        console.log('data: ', body);
                    }
                    toSet(payload, 'data', body);
                }

            } else {
                if (debug) {
                    console.log('***** APIManager Other Method Debug *****');
                    console.log('content-type: application/json');
                    console.log('endpoint: ', endpoint);
                    console.log('params: ', body);
                }
                toSet(payload, 'params', body);
            }
            if (debug) {
                console.log('target payload: ', payload);
            }
            const { data } = await Request.request(payload);
            // console.log(response);
            // if (additional.responseType === 'blob') {
            //     return { data };
            // }
            return { data };
            
        } catch (error) {
        	const {
        		status,
        		data,
                message,
        	}: ResponseProps = pick(error.response, ['status', 'data', 'message']);
            const errorMessage: any = getFrom(data, 'data', message);
            if (debug) {
                console.log('***** APIManager Debug *****');
                console.log('Error API response with code: ', status);
                console.log('Error API Call: ', errorMessage);
            }
            switch(status) {
                case 403:
                    if (!skipError) {
                        // dispatch({
                        //     type: GLOBAL_UNAUTH,
                        // });
                        this.clearToken();
                    }
                    break;
                default:
                    break;
            }
            if (!skipError) {
                // dispatch({
                //     type: GLOBAL_TARGET_ERROR,
                //     data: errorMessage,
                //     type: 'error',
                //     key: errorKey,
                // });
            }
            return { error: errorMessage || error.message }
        }
    }
};

export default new APIManager();