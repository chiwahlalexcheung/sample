import React, { FC, useCallback, useEffect } from 'react'
import css from 'clsx'
import { map, get } from 'lodash';
import Numeral from 'numeral'
import { useRouter } from 'next/router'
import StaticRoutes from '@config/routes';
import StaticSources from '@config/sources';
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import CommonPopup from '@components/common/Popup'
import FormElement from '@components/form'
import {
  // BasicInfoItemProps,
  // BasicInfo,
  ContactBasicEmailBox,
  ContactBasicPhoneBox,
} from './../ContactUs';
import Service from '@config/service'
import {
  Agreements,
} from './form'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Box,
  Grid,
} from '@material-ui/core';
// import {
//   LabelValueProps,
// } from '@interfaces/list'
// import {
//   useToggle,
//   ToggleProps,
// } from '@hooks/common'

import {
  // useStatementStyles,
  // useFormStyles,
  // useReviewFormStyles,
  // useAgreementStyle,
  // useEnjoyStyle,
  useFormStyles,
  useCompletionTableStyles,
  useCompletionStyles,
} from './styles';
import { useApplicationForm } from '@store/application/context'
// import {
//   useStepSolution,
//   StepSolutionContext,
// } from '@containers/section/StepSolution'
// import {
//   // LoanTypeSelectionBoxProps,
//   LoanTypeSelectionBox,
// } from '@containers/section/Calculator'
// import {
//   useProgressStyles
// } from '@containers/section/StepSolution/styles'
export interface CompletionMemberBannerProps {
  type?: 'first' | 'second';
  conditions: string;
};
export const CompletionMemberBanner: FC<CompletionMemberBannerProps> = ({
  type='first',
  conditions,
}) => {
  const { t } = useTranslation('form');
  const classes = useCompletionStyles();
  const rate: number = 10;
  const amount: number = 7000000;
  
  // if (!isAuth) {
  //   return null;
  // }
  return (
    <div>
      <div className={classes.banner}>
        <div className={classes.bannerTips}>
          {t('note.thank')}
        </div>
        <div className={classes.bannerTitle}>
          {
            t(`note.theFollowingLoan.${type}`, {
              amount: `${Service.currency}${Numeral(amount).format('0,0')}`,
              rate: `${rate}%`,
            })
          }
        </div>
        <div className={classes.bannerHints}>
          {t('note.ourClient')}
        </div>
      </div>
      <div className={classes.tc}>
          <span>{t('label.subjectTo')}</span>
          <Agreements
            labelUnderline={false}
            title={
              <React.Fragment>
                <div className={classes.title}>
                  {t('label.tc')}
                </div>
                <div className={classes.subTitle}>
                  {t(`label.loanOfferType.${type}`)}
                </div>
              </React.Fragment>
            }
            label={
              <span className={classes.subjectTo}>
                {t('label.tc')}
              </span>
            }
          >
            <div className={classes.tcContent} dangerouslySetInnerHTML={{ __html: conditions }} />
          </Agreements>
      </div>
    </div>
  );
}

export const CompletionUploadDocumentButton: FC = () => {
  const { t:Bt } = useTranslation('button');
  const classes = useCompletionTableStyles();
  const router = useRouter();
  const { form } = useApplicationForm();
  const no = get(form, 'appl_no');
  const goUpload = useCallback(() => {
    router.push(`${StaticRoutes.account.documentsUpload}/${no}`);
  }, [no]);
  return (
    <React.Fragment>
      <CommonButton
        label={Bt('uploadDocumentNow')}
        className={classes.btn}
        onClick={goUpload}
      />
    </React.Fragment>
  );
};

export interface CompletionContactInformationProps {
  touchMessage?: boolean;
};

export const CompletionContactInformation: FC<CompletionContactInformationProps> = ({ touchMessage=true }) => {
  const { t } = useTranslation('form');
  const { t: St } = useTranslation('section');
  const classes = useCompletionTableStyles();
  const info = 'Monday to Friday : 9:00 am - 6:00 pm';
  return (
    <React.Fragment>
      {
        touchMessage && <div className={classes.hint}>
          <div className={classes.hintTitle}>
            {t('note.gettingTouch')}
          </div>
          <div className={classes.hintSubTitle}>
            {t('note.warnPhoneAsLogin')}
          </div>
        </div>
      }
      <div className={classes.office}>
        <div className={classes.officeQuestion}>
          {t('note.moreQuestionAskUs')}
        </div>
        <div className={classes.hour}>
          {t('label.businessHour', { info })}
        </div>
      </div>
      <Box
        display={'flex'}
        justifyContent={'space-between'}
      >
        <ContactBasicPhoneBox />
        <ContactBasicEmailBox />
      </Box>
    </React.Fragment>
  );
};

export const CompletionTable: FC = () => {
  const { t } = useTranslation('form');
  const router = useRouter();
  const { t:Bt } = useTranslation('button');
  const classes = useCompletionTableStyles();
  const { form } = useApplicationForm();
  const no = get(form, 'appl_no');
  const goLanding = useCallback(() => {
    router.push(StaticRoutes.landing);
  }, []);
  
  return (
    <React.Fragment>
      <div className={classes.title}>
        {t('title.applicationCompletion')}
      </div>
      <div className={classes.subTitle}>
        {t('note.thanksCustomer')}<br />
        {t('note.ourClient')}
      </div>
      <div className={classes.box}>
        <div className={classes.result}>
          {t('note.preliminaryLoanNo', { no })}
          <span className={classes.resultRemind}>
            {t('note.enquiryHints')}
          </span>
        </div>
        <Box
          display={'flex'}
          justifyContent={'space-between'}
          className={classes.control}
        >
          <CompletionUploadDocumentButton />
          <CommonButton
            label={Bt('uploadDocumentLater')}
            className={css(classes.btn, classes.later)}
            onClick={goLanding}
          />
        </Box>
        <CompletionContactInformation />
      </div>
    </React.Fragment>
  );
};

export const CompletionInformation: FC = () => {
  const { t } = useTranslation('form');
  const { t:Bt } = useTranslation('button');
  const classes = useCompletionStyles();
  const formClasses = useFormStyles();
  // const {
  //   goNextStep,
  // }: StepSolutionContext = useStepSolution();
  const conditions: string = 'Terms & Conditions for First Mortgage Preliminary Offer: - Offer will be valid for customers who can provide the necessary supporting documents - Offer will be valid for customers who can pass the credit checks - *** more terms for first mortgage preliminary offer 1. Sun Hung Kai Credit Limited (“Company”) will receive applications for the“First Mortgage Preliminary Offer” starting from today onwards. 2. The plan is only available to eligible customers. Eligible customers are (subject to the Company’s right of final determination) those whose tenure for repayment of the Mortgage loan are 24 months or less. 3. Eligible customers of 12-month tenure may apply for“First Mortgage Preliminary Offer”for any two of the instalments between the 2nd and 11th instalments. 4. Eligible customers of 24-month tenure may apply for“First Mortgage Preliminary Offer”for any two of the instalments for each of the 12-month periods between the 2nd and 23rd instalments (thus there will be four instalments that can enjoy “First Mortgage Preliminary Offer”during the entire tenure). Eligible customers can call the customer service hotline 2996 2688 or make your request via our SHK Credit website 3 days after payment of the first instalment and full payment of the succeeding instalments to submit an application and should complete the signing of all documentations 5 working days before the due date of the next instalment.';
  return (
    <Box display={'flex'} flexDirection={'column'} className={formClasses.root}>
      <Typography component={'h1'} className={formClasses.title}>
        {t('label.completion')}
      </Typography>
      <CompletionMemberBanner conditions={conditions} />
      <div className={formClasses.form}>
        <CompletionTable />
      </div>
    </Box>
  );
}
