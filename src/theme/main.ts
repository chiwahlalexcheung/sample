import green from "@material-ui/core/colors/green"
import grey from "@material-ui/core/colors/grey"
import { createMuiTheme } from "@material-ui/core/styles"


declare module '@material-ui/core/styles/createMuiTheme' {
  interface ThemeOptions {    
      themeName?: string  // optional
  }
}
/**
 * material-ui theme color pallete
 * @see https://material-ui.com/style/color/
 */
export default createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1470,
      xl: 1920,
    },
    // typography: {
    //   fontWeight: 'bold',
    //   body1: {
    //     fontSize: '13px',
    //   },
    // },
  },
  palette: {
    text: {
      primary: '#333333',
      secondary: '#c2c1c1',
      disabled: '#ebebeb',
    },
    primary: {
      light: '#EE1C25',
      main: '#780D0E',
      dark: '#910e12',
    },
    secondary: {
      light: green[300],
      main: green[500],
      dark: green[700],
    },
  },
})
