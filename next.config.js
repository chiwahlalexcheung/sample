// const { ASSET_HOST } = process.env
const { i18n } = require('./next-i18next.config')
// const { nextI18NextRewrites } = require('next-i18next/rewrites')

const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require('next/constants')

// for those who using CDN
// const assetPrefix = ASSET_HOST || ''

const getBuildConfig = () => {
  const path = require('path')
  const postcssPresetEnv = require('postcss-preset-env')
  const postcssPresetEnvOptions = {
    features: {
      'custom-media-queries': true,
      'custom-selectors': true,
    },
  }

  const cssOptions = {
    postcssLoaderOptions: {
      plugins: [postcssPresetEnv(postcssPresetEnvOptions)],
    },
    sassOptions: {
      includePaths: [path.join(process.cwd(), 'src', 'common', 'css')],
    },
  }
  const nextConfig = {
    ...cssOptions,
    webpack(config) {
      config.module.rules.push({
        test: /\.svg$/,
        include: path.join(process.cwd(), 'src', 'components', 'icon', 'icons'),
        use: [
          'svg-sprite-loader',
          {
            loader: 'svgo-loader',
            options: {
              plugins: [
                { removeAttrs: { attrs: '(fill)' } },
                { removeTitle: true },
                { cleanupIDs: true },
                { removeStyleElement: true },
              ],
            },
          },
        ],
      })
      return config
    },
    i18n,
    // rewrites: async () => nextI18NextRewrites(localeSubPaths),
    // publicRuntimeConfig: {
    //   localeSubPaths,
    // },
  }
  return nextConfig
}

// module.exports = (phase) => {
//   console.log(phase);
//   const shouldAddBuildConfig =
//     phase === PHASE_DEVELOPMENT_SERVER || phase === PHASE_PRODUCTION_BUILD
//   return shouldAddBuildConfig ? getBuildConfig() : {}
// }

module.exports = getBuildConfig();
