import useSWR from "swr";
import APIManager from '@lib/apiManager';

const fetcher = async (url: string) => {
  var {data} = await APIManager.get({
    dispatch: null,
    url,
    underAuth: false,
  })
  return data;
}

const useGetUser = () => {
  const { data, error, ...rest } = useSWR("userInfo", fetcher);
  return { user: data ? data.user : null, error, loading: !data && !error, ...rest };
};

export {
  useGetUser,
}