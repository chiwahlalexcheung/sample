import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  select: {
    width: '100%',
    padding: 0,
    border: 0,
    background: '#F00'
  },
  inputRoot: {
    flex: 1,
  },
  control: {
    boxShadow: "none",
    borderRadius: 0,
    border: 0,
  },
  root: {
    width: '100%',
    color: 'white',
    border: '0 !important',
    fontSize: '14px',
    fontWeight: 'bold',
    paddingLeft: '15px',
    '&::before': {
      border: 0,
      display: 'none !important',
    },
    '&::after': {
      border: 0,
      display: 'none !important',
    },
    '&:hover': {
      '&::before': {
        border: '0 !important',
      },
    },
    '&:focus': {
      outline: 'none',
      '&::before': {
        border: '0 !important',
      },
    }
  },
}));

export {
  useStyles,
};
