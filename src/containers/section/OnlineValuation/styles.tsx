import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
	// position: 'relative',
	padding: '10px 0 60px 0',
	backgroundImage: `url(${StaticSources.bg.valuationFormBg})`,
	...FullCoverBgStyles,
  },
  leftCover: {
	  background: '#fff',
	  width: '30%',
	  height: '100%',
  },
  heading: {
	marginBottom: '70px',
  },
}));

const useFormStyles = makeStyles(theme => ({
	root: {
		background: '#FFFFFF 0% 0% no-repeat padding-box',
		boxShadow: '0px 6px 20px #00000033',
		borderRadius: '9px',
		overflow: 'initial',
	},
	form: {
		width: '75%',
	},
	grid: {
		paddingTop: '10px',
		paddingBottom: '10px'
	},
	contact: {
	  borderRadius: '5px',
	  margin: '0 10px',
	  fontWeight: 'bolder',
	  padding: '10px 30px',
	  background: 'transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box',
	  '&:hover': {
		background: 'transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box',
	  },
	},
	title: {
		width: '100%',
		marginLeft: '-95px',
		fontSize: '23px',
		fontWeight: 'bolder',
		lineHeight: '60px',
		paddingBottom: '16px',
		paddingTop: '30px',
		// color: '#f00'
		color: theme.palette.primary.light
	},
	label: {
		// height: '60px',
		fontStyle: 'Roboto',
		// fontSize: '22px',
		// lineHeight: '60px',
		// fontFamily: 'Roboto'
	},
	inputTips: {
		width: '60%',
		paddingTop: '17px',
		paddingBottom: '17px',
		fontSize: '15px',
		wordSpacing: '0.38px',
		color: theme.palette.text.primary,
		lineHeight: '22px'
	},
	buttons: {
		paddingTop: '35px',
		paddingBottom: '35px',
		gap: '50px',
		width: '100%',
	},
	button: {
		borderRadius: '5px',
		// flex: 1,
		width: '70%',
		fontWeight: 700
	},
	keyFactsBox: {
		flex: 1,
		padding: '10px'
	},
	keyFactsTitleBorder: {
		position: 'relative',
		paddingBottom: '15px',
		marginBottom: '10px',
		'&:before': {
			content: "''",
			display: 'block',
			position: 'absolute',
			bottom: '-3px',
			width: '30%',
			height: '2px',
			backgroundColor: theme.palette.text.secondary,
		},
	},
	keyFactsTitle: {
		color: theme.palette.text.secondary,
		fontWeight: 'bolder',
		fontSize: '16px',
		wordSpacing: '0.4px'

	},
	keyFacts: {
		
	}
}));

const usePopupStyles = makeStyles(theme => ({
	root: {
		padding: '15px'
	},
	title: {
		fontSize: '26px',
		// wordSpacing: '-0.72px',
		color: theme.palette.primary.light,
		marginBottom: '40px',
	},
	content: {
		fontSize: '22px',
		// wordSpacing: '0.22px',
		marginBottom: '40px'
	},
	marketingTitle: {
		background: 'transparent linear-gradient(90deg, #E5413F 0%, #FCFDFE 100%) 0% 0% no-repeat padding-box',
		paddingLeft: '19px',
		color: '#FFFFFF',
		fontSize: '18px',
		fontWeight: 'bolder',
		width: '100%',
		textAlign: 'left',
		marginBottom: '40px',
	},
	block: {
	  marginBottom: '40px',
	},
	marketingCheckboxes: {
		width: '100%',
		padding: '10px',
		marginBottom: '40px',
	},
	tips: {
		width: '100%',
		marginBottom: '40px',
		align: 'flex-start'
	}
}));

export {
  useStyles,
  useFormStyles,
  usePopupStyles
};