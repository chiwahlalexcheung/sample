import React, { FC, useCallback } from 'react'
import css from 'clsx'
import { map } from 'lodash'
import { useRouter } from 'next/router'
import Routes from '@config/routes'
import StaticSource from '@config/sources'

import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import Link from '@components/common/Link'
import Fade from 'react-reveal/Fade'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Box,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStyles,
  useBoxStyles,
  useBigToolStyles,
  useSmallToolStyles,
} from './styles';

export interface InnerToolItemProps {
  icon: string;
  key: string;
  title: string;
  label: string;
  link: string;
  className?: string;
  big?: boolean;
  dim?: boolean;
}

export const InnerToolItem:FC<any> = ({
  icon,
  title,
  label,
  link,
  className='',
  big,
  dim,
}) => {
  const router = useRouter();
  const bigClasses = useBigToolStyles();
  const smallClasses = useSmallToolStyles();
  let classes = big ? bigClasses : smallClasses;
  let rootStyles = classes.wrapper;
  if (dim) {
    rootStyles = css(rootStyles, classes.dim);
  }
  const onPress = useCallback(() => {
    if (link) {
      router.push(link);
    }
  }, [link]);
  return (
    <Box display={'flex'} alignItems={'center'} justifyContent={'center'} className={css(rootStyles, className)}>
      <Box
        display={'flex'}
        flexDirection={big ? 'column' : 'row-reverse'}
        alignItems={big ? 'flex-start' : 'center'}
        className={classes.root}
      >
        <Box display={'flex'} justifyContent={'flex-end'} className={classes.icons}>
          <div className={classes.icon}>
            <LocalImage src={icon} alt={title} />
          </div>
        </Box>
        <div className={classes.block}>
          <Typography component={'h1'} className={classes.title}>
            {title}
          </Typography>
          {
            !!link && label && <CommonButton label={label} className={classes.button} onClick={onPress} />
          }
        </div>
      </Box>
    </Box>
  );
}

export interface BannerBoxInfoProps {
  title: string;
  description?: string;
  applyLabel?: string;
  applyLink?: string;
  contactLabel?: string;
  contactLink?: string;
  label?: string;
  labelLink?: string;
  banner: string;
}
export interface BannerBoxProps {
  info: BannerBoxInfoProps;
  children?: JSX.Element | JSX.Element[];
}

export const BannerBox: FC<BannerBoxProps> = ({ info, children }) => {
  const classes = useBoxStyles();
  // const { t } = useTranslation('button');
  const router = useRouter();
  const {
    title,
    description,
    applyLabel,
    applyLink,
    contactLabel,
    contactLink,
    label,
    labelLink,
    banner,
  }: BannerBoxInfoProps = info;
  const onApply = useCallback(() => {
    if (!!applyLink) {
      router.push(applyLink);
    }
  }, [applyLink]);
  const onContact = useCallback(() => {
    if (!!contactLink) {
      router.push(contactLink);
    }
  }, [contactLink]);
  const bgStyle = {
    backgroundImage: `url(${banner})`,
  };
  return (
    <Box display={'flex'}>
      <Box className={classes.left} display={'flex'} flexDirection={'column'} alignItems={'flex-start'} justifyContent={'center'}>
        
        <Typography component={'h1'} className={classes.title}>
          {title}
        </Typography>
        {
          !!description && <div dangerouslySetInnerHTML={{
            __html: description
          }} className={classes.description} />
        }
        {
          (!!applyLabel || !!contactLabel) && <Box display={'flex'} className={classes.buttons} justifyContent={'space-between'}>
            {
              !!applyLabel && <CommonButton className={classes.button} label={applyLabel} onClick={onApply} />
            }
            {
              !!contactLabel && <CommonButton className={classes.button} secondary label={contactLabel} onClick={onContact} />
            }
          </Box>
        }
        {
          !!label && !!labelLink && <Link className={classes.label} label={label} href={labelLink} rel={'chapter'} />
        }
      </Box>
      <Box className={classes.banner} display={'flex'} flexDirection={'column'} alignItems={'flex-end'} style={bgStyle} />
      {
        !!children && <Box display={'flex'} flexDirection={'column'} className={classes.tool}>
          {children}
        </Box>
      }
    </Box>
  );
}

export const MortgageBanner: FC<BannerBoxProps> = ({ info }) => {
  const { t } = useTranslation('section');
  const { t: Bt } = useTranslation('button');
  const classes = useStyles();
//   const info: BannerBoxInfoProps = {
//     title: 'Second Mortgage Loan',
//     description: 'With Sun Hung Kai Credit Second Mortgage, you can utilize your property’s equity even if you already have an existing mortgage to help paying off your credit cards or other outstanding debts. Application is fast and easy! Simply provide simple income documentations and property ownership proof. Our speedy approval can satisfy any urgent cash needs.',
//     applyLabel: 'Apply Now',
//     applyLink: '/',
//     contactLabel: 'Contact Now',
//     contactLink: '/',
//     label: 'Payment Holiday Highlights',
//     labelLink: '/',
//     banner: StaticSource.banner.firstMortgageBanner,
//   };
  // valuateNow
  const tools: InnerToolItemProps[] = [
    {
      key: 'mortgage',
      icon: StaticSource.banner.loanCalculatorBorder,
      title: t('useful-tools.mortgageLine'),
      label: Bt('checkNow'),
      link: Routes.loanCalculator,
      big: true,
      dim: true,
    },
    {
      key: 'valuation',
      icon: StaticSource.banner.onlineValuationBorder,
      title: t('useful-tools.valuationServiceLine'),
      label: Bt('checkNow'),
      link: Routes.valuation,
      big: true,
    },
  ];
  // InnerToolItem
  return (
    <div className={classes.root}>
        <InnerPage>
          <Fade bottom>
            <BannerBox info={info}>
              <React.Fragment>
                {
                  map(tools, ({ key, ...t }: any) => (
                    <InnerToolItem key={key} {...t} />
                  ))
                }
              </React.Fragment>
            </BannerBox>
          </Fade>
        </InnerPage>
    </div>
  );
};

export default MortgageBanner;
