import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";
import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useStatementStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import {
  apiGet,
  checkUserInfo,
  checkApplicationOwner,
  signOut,
  goProfile,
  formatDate,
} from "../AccountCommon/context";
import { Document, Page } from "react-pdf";

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface StatementProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const StatementContext = createContext<Partial<ContextProps>>({});
export const useStatement = () => useContext(StatementContext) as ContextProps;
export const StatementProvider: FC</*LogonProps*/ any> = ({
  values = {},
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  return (
    <StatementContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </StatementContext.Provider>
  );
};

export interface StatementItemProps {
  key: string;
  component: JSX.Element;
}
export interface StatementBarProps {
  list: StatementItemProps[];
}

export const StatementDetail = ({ applicationNo }) => {
  const classes = useStatementStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useStatement();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
    my code start here 
  */
  const router = useRouter();
  const [options, setOptions] = useState([]);

  const getStatementDates = useCallback(async () => {
    const data = await apiGet("statementDate", {}, { acct_no: applicationNo });
    if (data) {
      const dates = data["data"]["LOS"]["BODY"]["dates"];
      if (dates) {
        const options_tmp = dates.map((d) => ({
          value: d,
          label: formatDate(d),
          link: d,
          key: d,
        }));
        options_tmp.unshift({
          value: "",
          label: ft("placeholder.pleaseSelect"),
          link: "",
          key: "",
        });
        setOptions(options_tmp);
      }
    }
  }, [applicationNo]);
  useEffect(() => {
    getStatementDates();
  }, [applicationNo]);

  const onClickView = async () => {
    const date = watch()["selectedDate"];
    if (date) {
      let path = ServiceConfig.domain + ServiceConfig.apis.statementFile;
      path = path.replace(":loan_no", applicationNo);
      path = path.replace(":stm_date", date);
      window.open(path);
    }
  };

  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.selectEStamentMonth")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {ft("label.selectEStamentMonth")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormElement.NativeSelect
                name={"selectedDate"}
                control={control}
                options={options}
                specialClass={"documentUpload"}
              />
            </Grid>
          </Grid>

          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("back")}
              onClick={() => goProfile(router, applicationNo)}
            />

            <CommonButton
              className={classes.bottomButtonOrange}
              label={bt("viewNow")}
              onClick={onClickView}
            />
          </Box>
        </Box>
      </Box>
      {/* <UploadSuccessPopup
        shown={shown}
        setShown={setShown}
        pdfString={pdfString}
      /> */}
    </Box>
  );
};

export const StatementForm = ({ applicationNo }) => {
  const { t } = useTranslation("section");
  const classes = useStatementStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <StatementProvider values={{}}>
        <StatementDetail applicationNo={applicationNo} />
      </StatementProvider>
    </GoogleReCaptchaProvider>
  );
};
