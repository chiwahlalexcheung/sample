import React, {
  FC,
  forwardRef,
  useRef,
  createRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, times, get, size, has } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import { AppState } from "@store/store";
import css from "clsx";
import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
  IconButton,
  Button,
  LinearProgress,
  Snackbar,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useFormStyles, useUploadStyles } from "./styles";
import { LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import ServiceConfig from "@config/service";
const {
  uploadFile: { maximumSize: maxUploadSize },
} = ServiceConfig;
import { LabelValueProps } from "@interfaces/list";

import { useRouter } from "next/router";
import StaticRoutes from "@config/routes";
export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
  applicationNo?: string;
}
export interface DocumentsUploadProps {
  children: JSX.Element;
  values: KeyPairProps;
}
import { useUploadDocument, uploadDocumentProps } from "@store/service/context";
import DocumentTypeData from "./documentType.json";
import { useSelector } from "react-redux";
// import Link from 'next/link'
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import apiManager from "@lib/apiManager";

import {
  apiGet,
  checkUserInfo,
  checkApplicationOwner,
  goProfile
} from "@containers/section/AccountCommon/context";

export const DocumentsUploadContext = createContext<Partial<ContextProps>>({});
export const useDocumentsUpload = () =>
  useContext(DocumentsUploadContext) as ContextProps;
export const DocumentsUploadProvider: FC<any> = ({
  values = {},
  applicationNo,
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);

  return (
    <DocumentsUploadContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        applicationNo,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </DocumentsUploadContext.Provider>
  );
};

export interface LogonItemProps {
  key: string;
  component: JSX.Element;
}
export interface LogonBarProps {
  list: LogonItemProps[];
}

export interface DocumentsUploadBlockProps {
  register: Function;
  control: any;
  totalLength?: number;
}

export const DocumentsUploadBlock: FC<DocumentsUploadBlockProps> = ({
  totalLength = 5,
  control,
  register,
}) => {
  const classes = useUploadStyles();
  // const classes = useFormStyles();

  const [refs, setRefs] = useState<any[]>([]);
  const { t: bt } = useTranslation("button");
  const { t } = useTranslation("form");
  const { watch, reset, setValue }: ContextProps = useDocumentsUpload();
  const v = watch();
  useEffect(() => {
    const allRefs = map(times(totalLength), () => createRef());
    setRefs(allRefs);
  }, []);
  const documentType: LabelValueProps[] = DocumentTypeData.documentType.map(
    (data) => ({
      value: data.value,
      label: data.label,
      link: "",
      key: data.value,
    })
  );
  const uploadState = useSelector(
    ({ service }: AppState) => service.uploadDocument
  );
  return (
    <div className={classes.root}>
      {map(refs, (ref, index) => {
        let rightIcon: any = (
          <>
            <label htmlFor={`document[${index}].file`}>
              <Button component="span" className={classes.browseButton}>
                {bt("browse")}
              </Button>
            </label>
            <IconButton
              className={classes.icon}
              onClick={() => {
                setValue(`document[${index}].file`, null);
              }}
            >
              <LocalImage src={StaticSources.icon.delete} />
            </IconButton>
          </>
        );
        if (get(uploadState, `list[${index}].LOS.BODY.rtn_cde`) == "000") {
          rightIcon = (
            <LocalImage
              className={classes.icon}
              src={StaticSources.icon.advantagesCheck}
            />
          );
        }
        return (
          <Grid container spacing={5} className={classes.row} key={index}>
            <Grid item xs={6} sm={5}>
              <FormElement.NativeSelect
                name={`document[${index}].type`}
                control={control}
                options={documentType}
                specialClass={"documentUpload"}
              />
            </Grid>
            <Grid item xs={12} sm={7}>
              <Box
                display={"flex"}
                height={1}
                flexDirection={"row"}
                justifyContent={"center"}
                alignItems={"center"}
              >
                <Typography className={classes.fileNameLabel} noWrap>
                  {t("label.fileName")} :{" "}
                  {get(v, `document[${index}].file[0].name`, "-")}
                </Typography>
                <input
                  type="file"
                  ref={register()}
                  style={{ display: "none" }}
                  name={`document[${index}].file`}
                  id={`document[${index}].file`}
                />

                {rightIcon}
              </Box>
            </Grid>
          </Grid>
        );
      })}
      <Grid container spacing={5} className={classes.row}>
        <Grid item xs={6} sm={5} />
        <Grid item xs={12} sm={7}>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"center"}
            alignItems={"center"}
          >
            <Typography className={classes.fileSizeLabel} noWrap>
              {t("label.maximumFileSize", { size: maxUploadSize })}
            </Typography>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export const BottomButton: FC = () => {
  const classes = useFormStyles();
  const { t: bt } = useTranslation("button");
  const router = useRouter();
  const {
    register,
    control,
    watch,
    applicationNo,
  }: ContextProps = useDocumentsUpload();
  const {
    multipleUpload,
    list,
    loading,
    error,
    submitted,
  } = useUploadDocument();
  const uploadState = useSelector(
    ({ service }: AppState) => service.uploadDocument
  );

  console.log(applicationNo)

  const submit = useCallback(() => {
    // toggle();
    if (loading) {
      return;
    }
    const v = watch();
    let files: any[] = [];
    get(v, "document").map((item, index) => {
      if (size(get(item, "file")) > 0 && get(item, "type", "") != "") {
        const data = new FormData();
        data.append("appl_no", applicationNo);
        data.append("doc_cde", get(item, "type"));
        data.append("doc_name", get(item, "file[0].name"));
        data.append("file", get(item, "file[0]"));
        files.push({
          index,
          payload: data,
        });
      }
    });
    multipleUpload(files);
  }, [multipleUpload, watch, applicationNo]);

  return (
    <Box
      display={"flex"}
      width={1}
      flexDirection={"row"}
      justifyContent={"space-around"}
    >
  
        <CommonButton className={classes.bottomButtonRed} label={bt("back")}
        onClick={() => goProfile(router, applicationNo)}
        />


      <CommonButton
        className={classes.bottomButtonOrange}
        label={bt("uploadNow")}
        onClick={(r: any) => submit()}
      />
      <UploadSuccessPopup />
    </Box>
  );
};

const UploadSuccessPopup: FC = () => {
  const classes = useFormStyles();
  const { t } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const router = useRouter();
  const { reset, applicationNo }: ContextProps = useDocumentsUpload();
  const { resetUpload, submitted, list } = useUploadDocument();
  const resetDocs = useCallback(() => {
    resetUpload();
    //reset();
    // seems that this one is about resitering reset.
    router.reload();
  }, [reset, resetUpload]);
  console.log(applicationNo)
  return (
    <CommonPopup
      show={submitted}
      afterClose={() => {}}
      maxWidth={"md"}
      noCloseBtn
      otherProps={{
        disableBackdropClick: true,
      }}
    >
      <Box
        display={"flex"}
        flexDirection={"column"}
        alignContent={"center"}
        alignItems={"center"}
        className={classes.popup}
      >
        <Typography className={classes.popupTitle}>
          {t("label.thanksUploadDocsTitle")}
        </Typography>
        <Typography className={classes.popupContent}>
          {t("label.thanksUploadDocsContent")}
        </Typography>
        <Box
          display={"flex"}
          width={1}
          justifyContent={"space-around"}
          className={classes.popupButtons}
        >
          <CommonButton
            className={classes.popupButton}
            label={bt("uploadMoreDocs")}
            onClick={(r: any) => resetDocs()}
          />
   
            <CommonButton
              className={classes.popupButton}
              label={bt("backToHome")}
              onClick={() => goProfile(router, applicationNo)}
              secondary
            />
        </Box>
      </Box>
    </CommonPopup>
  );
};

export const ErrorMessage: FC = () => {
  const { t: ft } = useTranslation("form");
  const uploadState = useSelector(
    ({ service }: AppState) => service.uploadDocument
  );
  return (
    <Box>
      {map(uploadState.list, (item: any, index: any) => {
        const errorMsg: string = get(item, "LOS.BODY.rtn_msg");
        if (errorMsg) {
          return (
            <Snackbar
              key={index}
              message={ft("label.submitFail", { errorMessage: errorMsg })}
              open={true}
              //   onRequestClose={() => this.setState({ open: false })}
              autoHideDuration={5000}
            />
          );
        }
      })}
    </Box>
  );
};

export const DocumentsUploadContent: FC = () => {
  const classes = useFormStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    register,
    control,
    watch,
    applicationNo,
  }: ContextProps = useDocumentsUpload();
  const router = useRouter();
  const onClickLogout = async () => {
    await apiGet("signOut");
    router.push(RouteSources.accountLogon, RouteSources.accountLogon);
  };

  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel="start">
          <CommonButton className={classes.TopButtonHome} label={bt("home")} />
        </Link>

        <CommonButton
          className={classes.TopButtonLogout}
          label={bt("logout")}
          onClick={onClickLogout}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.selectEnterInformation")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container className={classes.row}>
            <Grid item xs={12} sm={5}>
              <Typography component={"h4"} className={classes.label}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Typography
                component={"h4"}
                className={css(classes.label, classes.labelRed)}
              >
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container className={classes.row}>
            <Grid item xs={12} sm={5}>
              <Typography component={"h4"} className={classes.label}>
                {ft("label.documentType")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Typography component={"h4"} className={classes.label}>
                {ft("label.file")}*
              </Typography>
            </Grid>
          </Grid>
          <DocumentsUploadBlock control={control} register={register} />
          <BottomButton />
          <ErrorMessage />
        </Box>
        <Typography className={classes.remark}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const DocumentsUploadForm: FC<any> = ({ applicationNo }) => {
  return (
    <DocumentsUploadProvider values={{}} applicationNo={applicationNo}>
      <DocumentsUploadContent />
    </DocumentsUploadProvider>
  );
};
