import React, { FC, forwardRef, useMemo, useContext, useEffect, cloneElement, useCallback, createContext } from 'react'
import { map, times, find, findIndex, pick, isArray, has, get } from 'lodash'
import css from 'clsx'
import Zoom from 'react-reveal/Zoom'
import QueryString from 'query-string'
import Fade from 'react-reveal/Fade'
import Shake from 'react-reveal/Shake'
import StaticSources from '@config/sources';
import StaticRoutes from '@config/routes';
import { useRouter } from 'next/router'
import LocalImage from '@components/image/LocalImage'
import CommonPopup from '@components/common/Popup'
// import Slide from 'react-reveal/Slide'
import CommonButton from '@components/common/Button'
import LinearProgress from '@material-ui/core/LinearProgress'
import ContactUs from '@containers/section/ContactUs'
import { useTranslation } from 'next-i18next'
import { useSolutionRule, Solution3StepProps } from '@store/common/context'
import { useLocale, LocaleContextProps } from '@components/common/Locale'
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  CompletionContactInformation,
} from '@containers/section/FullStepSolution/complete'
import {
  Card,
  CardContent,
  Typography,
  Container,
  Radio,
  RadioGroup,
  FormControlLabel,
  Box,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStepper,

  KeyPairProps,
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairFormHandleProps,
  useKeyPairFormValue,
  StepHandleProps,
  useToggle,
  ToggleProps,
} from '@hooks/common';
import {
  useCardStyles,
  useRootStyles,
  useProgressStyles,
  useYesNoStyles,
  useCheckListStyles,
} from './styles';
import {
  LabelValueProps,
} from '@interfaces/list'

export interface StepSolutionContext extends KeyPairFormHandleProps,  StepHandleProps {
  onSubmit: Function;
  slideTo?: Function;
  onStepSelected: Function;
};
export interface StepSolutionProps {
  children: JSX.Element;
  maxStep?: number;
  index?: number;
  values?: KeyPairProps;
  onSubmit: Function;
  slide?: Function;
}

export const StepSolContext = createContext<Partial<StepSolutionContext>>({});
export const useStepSolution = () => useContext(StepSolContext) as StepSolutionContext;
export const StepSolution: FC<StepSolutionProps> = ({ slide, onSubmit, maxStep=1, index=0, values={}, children }) => {
  const {
    goNextStep,
    goPrevStep,
    goStep,
    step,
    stepValue,
    isLastStep,
  }: StepHandleProps = useStepper({ maxStep, step: index });
  const {
    value,
    changeValue,
    setValue,
    getValue,
    register,
    control,
    watch,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  const onStepSelected = useCallback((answer: any) => {
    setValue('answer', answer);
  }, [setValue]);
  useEffect(() => {
    register('answer');
  }, []);
  return (
    <StepSolContext.Provider value={{
      goNextStep,
      goPrevStep,
      step,
      goStep,
      onStepSelected,
      changeValue,
      setValue,
      maxStep,
      isLastStep,
      getValue,
      value,
      onSubmit,
      register,
      control,
      watch,
      stepValue,
      slideTo: slide,
    }}>
      {children}
    </StepSolContext.Provider>
  );
}

export interface CardProps {
  active?: boolean;
  title?: string;
  subTitle?: string;
  target: string;
  control?: JSX.Element;
  list: any[];
}
export interface SolutionCardProps extends CardProps {
  offset: number;
}
export const SolutionCard: FC<SolutionCardProps> = ({
  active=false,
  title,
  subTitle,
  offset=1,
  target,
  // list,
  control,
}) => {
  const classes = useCardStyles();
  const { t } = useTranslation('section');
  const {
    goStep,
    step,
  }: StepSolutionContext = useStepSolution();
  let styles = classes.root;
  let offsetStyles = classes.offset;
  const onPress = useCallback(() => {
    goStep(offset);
  }, [goStep, offset]);
  let addOn = {};
  if (active) {
    styles = css(styles, classes.active);
    offsetStyles = css(offsetStyles, classes.offsetActive);
  }
  
  // if the current card has been clicked, then can directly click and
  // switch to this card step
  if (offset <= step && offset !== step) {
    addOn = { onClick: onPress };
  }
  return (
    <Card className={styles} {...addOn}>
      {
        active && <div className={classes.activeConner} />
      }
      <CardContent className={classes.content}>
        <Box
          display={'flex'}
          alignItems={'center'}
          justifyContent={'space-between'}
        >
          <Typography component={'h2' } className={classes.heading}>
            {t('step-solution.questionTitle')}
          </Typography>
          <Typography component={'h2'} className={offsetStyles}>
            {`0${offset+1}`}
          </Typography>
        </Box>
        <Box
          display={'flex'}
          alignItems={'flex-start'}
          flexDirection={'column'}
          justifyContent={'center'}
          className={classes.question}
        >
          {
            !!title && <Typography component={'h3' } className={classes.title}>
              {title}
            </Typography>
          }
          {
            !!subTitle && <Typography component={'h3' } className={classes.subTitle}>
              {subTitle}
            </Typography>
          }
        </Box>
        {!!control && cloneElement(control, {
          active,
          target,
        })}
      </CardContent>
    </Card>
  );
};

export interface CardsProps {
  cards: any[];
}
export const SolutionCards: FC<CardsProps> = ({ cards }) => {
  const {
    step,
    watch,
  }: StepSolutionContext = useStepSolution();
  const values = watch();
  const { inspector }: LocaleContextProps = useLocale();
  const classes = useRootStyles();
  const listLookup: string[] = ['owner', 'mortgage', 'loan'];
  // TODO: handle in server-side?
  let target: any;
  let control: JSX.Element | undefined;
  let optionsList: CheckListItem[] = [];
  let prevChildren: any;
  let wasSelected: string;
  let idx = -1;
  const questions: CardProps[] = map(listLookup, (lookup: string, index: number) => {
    if (index === 0) {
      target = get(cards, '[0]'); // get first element from server payload
    } else if (prevChildren && prevChildren.length) {
      if (!wasSelected) { // pick first children if not selected any value from the current value
        target = prevChildren[0];
      } else {
        idx = findIndex(prevChildren, ({ parentAnswers } : any) => {
          const item = find(parentAnswers, ({ id }: any) => 
            !!wasSelected && id === wasSelected
          );
          return !!item;
        });
  
        if (idx !== -1) {
          target = prevChildren[idx]; 
        } else {
          target = prevChildren[0];
        }
      }
    }
    wasSelected = get(values, lookup); // get current selected value, for the next time checking
    idx = -1;
    if (target) {
      optionsList = map(target.answers, ({ id, ...answer }: any) => ({
        value: id,
        link: '',
        key: id,
        label: inspector({
          target: answer, field: 'label', defaultValue: ''
        }),
        ...(pick(answer, ['product', 'productType']))
      }));
      control = (
        <CheckList
          key={lookup}
          target={lookup}
          list={optionsList}
          className={classes.largeBar}
        />
      );
      // for any isBool answer, use YesNoControl
      if (!!get(target, 'answers.[0].isBool', false)) {
        control = (
          <YesNoControl
            key={lookup}
            target={lookup}
            list={optionsList}
            className={classes.smallBar}
          />
        );
      }
      prevChildren = get(target, 'children'); // get the possible children list

      return {
	      target: lookup,
	      title: inspector({
	        target, field: 'title', defaultValue: ''
	      }),
	      subTitle: inspector({
	        target, field: 'subtitle', defaultValue: ''
	      }),
        control,
        list: optionsList,
	    };
    }
    return { target: lookup, title: '', subTitle: '', list: [] };
  });
  return (
    <Box display={'flex'} alignItems={'flex-end'} justifyContent={'center'}>
      {
        map(questions, (card: CardProps, index) => (
          <SolutionCard key={card.target} active={index === step} {...card} offset={index} />
        ))
      }
    </Box>
  );
};

export const ProgressBar: FC = () => {
  const { t } = useTranslation('section');
  const { t: Bt } = useTranslation('button');
  const classes = useProgressStyles();
  const {
    maxStep,
    step,
    goStep,
    value,
    onSubmit,
    stepValue,
    watch,
  }: StepSolutionContext = useStepSolution();
  const { owner, mortgage, loan, answer } = watch();
  const onPress = useCallback((offset: number) => {
    goStep(offset);
  }, [goStep]);
  // const confirm = useCallback(() => {
  //   onSubmit(value);
  // }, [onSubmit, value]);
  // const lookups = ['owner', 'mortgage', 'loan'];
  const canSubmit = step >= maxStep - 1 
  && owner !== undefined 
  && mortgage !== undefined
  && loan !== undefined
  // console.log('> ', answer);
  // console.log(owner, mortgage, loan, step, maxStep - 1);
  // reduce(lookups, (valid: boolean, target: string) => 
  //   valid && has(value, target), true) && step >= maxStep - 1;
  // console.log(value);
  return (
    <Box display={'flex'} alignItems={'center'} justifyContent={'center'} className={classes.root}>
      <Typography component={'h3'} className={classes.label}>
        {t('label.progress')}
      </Typography>
      <Box display={'flex'} className={classes.bar}>
        <LinearProgress
          variant="determinate"
          classes={{
            bar: classes.barContent,
            colorPrimary: classes.mainBarContent,
            barColorPrimary: classes.barBufferContent,
          }}
          value={stepValue}
        />
        {
          times(maxStep, (s: number) => {
            let styles = css(classes.step, classes[`step${s + 1}`]);
            let addOn = {};
            if (s <= step) {
              styles = css(styles, classes.activeStep);
            }
            if (s <= step && s !== step) {
              addOn = { onClick: () => onPress(s) };
              styles = css(styles, classes.clickableStep);
            }
            return (
              <Box display={'flex'}
                key={s}
                {...addOn}
                className={styles}>
                <div className={classes.wrapper}>
                  {t('label.step', { step: s + 1 })}
                </div>
              </Box>
            );
          })
        }
      </Box>
      {
        canSubmit && !!onSubmit && <BeforeLoan />
      }
    </Box>
  );
}

interface ControlProps {
  target: string;
  active?: boolean;
  className: string;
  list: CheckListItem[];
  // onNext?: Function;
}
export const YesNoControl: FC<ControlProps> = ({ list, active, target, className }) => {
  const { t } = useTranslation('button');
  const classes = useYesNoStyles();
  const {
    changeValue,
    goNextStep,
    value,
    register,
    onStepSelected,
  }: StepSolutionContext = useStepSolution();
  const onPress = useCallback((v: any) => {
    changeValue(target, v);
    onStepSelected(find(list, ({ value }: any) => value === v));
    goNextStep();
  }, [onStepSelected, list, changeValue, goNextStep, target]);
  useEffect(() => {
    register(target);
  }, []);

  return (
    <Box
      display={'flex'}
      alignItems={'flex-end'}
      flexDirection={'row'}
      justifyContent={'flex-start'}  
      className={className}
    >
      <Box className={css(classes.root)}
        display={'flex'}
        justifyContent={'space-between'}
        alignItems={'center'}>
        {
          map(list, ({ label, value: v }: CheckListItem, index: number) => {
            let yesStyles = css(classes.button, classes.yes);
            let noStyles = css(classes.button, classes.no);
            
            if (has(value, target)) {
              yesStyles = css(yesStyles, value[target] === v ? classes.buttonSelected : classes.buttonNonSelected);
              noStyles = css(noStyles, value[target] === v ? classes.buttonSelected : classes.buttonNonSelected);
            }
            return (
              <CommonButton
                buttonProps={{
                  disabled: !active,
                }}
                label={label}
                onClick={() => onPress(v)}
                className={index === 0 ? yesStyles : noStyles}
              />
            );
          })
        }
      </Box>
    </Box>
  );
}

export interface CheckListItem extends LabelValueProps {
  // TODO
}
export interface CheckListControlProps extends ControlProps {
  list: CheckListItem[];
}
export const CheckList: FC<ControlProps> = ({ className, list, active, target }) => {
  const classes = useCheckListStyles();
  const {
    value,
    changeValue,
    goNextStep,
    onStepSelected,
    register,
  }: StepSolutionContext = useStepSolution();
  const handleChange = useCallback(({ target: e }: any) => {
    const { value: v } = e;
    changeValue(target, v);
    onStepSelected(find(list, ({ value }: any) => value === v));
    goNextStep();
  }, [onStepSelected, list, goNextStep, target, changeValue]);
  useEffect(() => {
    register(target);
  }, []);
  const selected = get(value, target);
    // get(list, '[0].value'));
  return (
    <Box
      display={'flex'}
      alignItems={'flex-end'}
      flexDirection={'row'}
      justifyContent={'flex-start'}  
      className={className}
    >
      <RadioGroup
        className={css(classes.root, className)}
        name={target}
        // value={selected}
        onChange={(e: any) => handleChange(e)}
      >
        {
          map(list, ({ label, value }: CheckListItem) => (
            <FormControlLabel
              key={value}
              // className={classes.option}
              classes={{
                root: classes.optionRoot,
                label: classes.optionLabel,
              }}
              disabled={!active}
              value={value}
              control={<Radio classes={{ root: classes.radio }} color={'primary'} />}
              label={label}
            />
          ))
        }
      </RadioGroup>
    </Box>
  );
};

export interface HeadingWithSubTitleProps {
  title?: string;
  subTitle: string;
};

export const HeadingWithSubTitle: FC<HeadingWithSubTitleProps> = ({
  title,
  subTitle,
}) => {
  const classes = useRootStyles();
  return (
    <Box className={classes.heading}>
      {
        !!title && <Typography component={'h1'} className={classes.title}>
          {title}
        </Typography>
      }
        <Typography component={'h1'} className={classes.subTitle}>
          {subTitle}
        </Typography>
    </Box>
  );
};

export interface BeforeLoanPopupProps {
  enabled?: boolean;
  onClose: Function;
  type: string;
  subType?: string;
  targetElement: string;
}

export const BeforeLoanPopup: FC<BeforeLoanPopupProps> = ({ onClose, subType, type, targetElement, enabled }) => {
  const classes = useRootStyles();
  const router = useRouter();
  const { t } = useTranslation('section');
  const { t: Bt } = useTranslation('button');
  const query = { t: type }
  const learnMore = useCallback(() => {
    const applyQuery = { ...query, r: 'learn' };
    router.push(`${StaticRoutes.mortgage[type]}?${QueryString.stringify(applyQuery)}`);
  }, [type, router]);
  const applyNow = useCallback(() => {
    const applyQuery = { ...query, r: 'form' };
    router.push(`${StaticRoutes.mortgage[type]}?${QueryString.stringify(applyQuery)}`);
  }, [type, router]);
  const list: any[] = [
    {
      _id: 1,
      content: 'Intent 1',
    },
    {
      _id: 2,
      content: 'Intent 2',
    },
    {
      _id: 3,
      content: 'Intent 3',
    },
  ];
  const Mortgage = (
    <div className={classes.popRoot}>
      <div className={classes.popContainer}>
        <div>
          <div className={classes.popMessage}>
            {t(`step-solution.message`)}
          </div>
          <Box display={'flex'} alignItems={'center'} justifyContent={'flex-start'} className={classes.popHeader}>
            <LocalImage src={StaticSources.icon.buildingWhite} alt={t(`step-solution.title.${type}`)} className={classes.popHeaderIcon} />
            <span>{t(`step-solution.title.${type}`, { 
              type: subType ? t(`step-solution.type.${subType}`) : ''
            })}</span>
          </Box>
        </div>
        <div className={classes.popList}>
          <div className={classes.popListBox}>
            {
              map(list, ({ _id, content }) => (
                <Box key={_id} display={'flex'} alignItems={'center'} justifyContent={'flex-start'} className={classes.listItem}>
                  <LocalImage
                    src={StaticSources.icon.payLogo}
                    alt={content}
                    className={classes.listIcon}
                  />
                  <span>{content}</span>
                </Box>
              ))
            }
            <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'} className={classes.listControl}>
              <CommonButton
                label={Bt('applyNow')}
                className={css(classes.btn)}
                onClick={applyNow}
              />
              <CommonButton
                label={Bt('learnMore')}
                className={css(classes.btn, classes.learn)}
                onClick={learnMore}
              />
            </Box>
          </div>
        </div>
        <CompletionContactInformation touchMessage={false} />
      </div>
    </div>
  );
  const lookups = {
    first: Mortgage,
    second: Mortgage,
    // contact: ContactUsMainContent,
  }
  // console.log(targetElement);
  let Target;
  if (has(lookups, targetElement)) {
    Target = lookups[targetElement];
  }
  return (
    <CommonPopup
      show={enabled}
      noCloseBtn={!!Target}
      maxWidth={'xl'}
      afterClose={onClose}
      blockContentScroll={true}
      otherProps={{
        scroll: 'body',
        onBackdropClick: onClose,
        onEscapeKeyDown: onClose,
      }}
    >
      {
        !!Target ? Target : <ContactUs />
      }
    </CommonPopup>
  );
}
export const BeforeLoan: FC = () => {
  const { enabled: show, toggle } = useToggle(false);
  const classes = useProgressStyles();
  const {
    appliedProduct: type='first',
    appliedProductType: subType='instalment',
    apply,
    applying,
    beingApplied,
  } = useSolutionRule();
  const {
    // value,
    watch,
  }: StepSolutionContext = useStepSolution();
  const { t } = useTranslation('button');
  // const type = 'first';
  const { answer } = watch();
  // const targetElement = 'first';
  const confirm = useCallback(() => {
    // toggle();
    apply(answer);
  }, [apply, answer]);
  useEffect(() => {
    if (beingApplied && !show) {
      toggle();
    }
  }, [beingApplied]);
  return (
    <React.Fragment>
      <BeforeLoanPopup
        targetElement={type}
        type={type}
        subType={subType}
        enabled={show}
        onClose={toggle}
      />
      <Shake>
        <CommonButton
          label={t('viewNow')}
          onClick={(r: any) => confirm()}
          className={classes.submitBtn}
          buttonProps={{
            disabled: applying,
          }}
        />
      </Shake>
    </React.Fragment>
  );
}

export const MainStepSolution: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const { list: questions } = useSolutionRule();
  const classes = useRootStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);
  
  return (
    <div ref={r as React.RefObject<HTMLDivElement>}>
      <StepSolution maxStep={3} values={{
        answer: {},   // default empty find selection
      }} onSubmit={onSubmit}>
        <div className={classes.root}>
            <InnerPage>
              <Fade left>
                <div className={classes.topHeading}>
                  <HeadingWithSubTitle
                    title={t('title.stepHeader')}
                    subTitle={t('title.stepSubTitle')}
                  />
                </div>
              </Fade>
              <Zoom right>
                <SolutionCards cards={questions} />
              </Zoom>
              <Shake>
                <ProgressBar />
              </Shake>
            </InnerPage>
        </div>
      </StepSolution>
    </div>
  );
});

export default MainStepSolution;
