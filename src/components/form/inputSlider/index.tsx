import React, { FC, useCallback } from 'react'
// import { Controller } from "react-hook-form";
// import { map } from 'lodash'
// import css from 'clsx'
// import LocalImage from '@components/image/LocalImage'
// import StaticSources from '@config/sources'
import FormElement from '@components/form'
import {
  Slider,
} from '@material-ui/core'
import {
  FormRenderWithControlElementProps,
  FormNumberProps,
} from '@interfaces/form'
// import {
//   LabelValueProps,
// } from '@interfaces/list'
import {
  useStyles,
} from './styles'

export interface FormSliderProps extends FormRenderWithControlElementProps{
  specialClass?: string;
  step?: number;
  min?: number;
  max?: number;
  value?: number;
  unit?: string;
  setValue: Function;
}

export interface NativeSliderProps {
  onChange: Function;
  value?: any;
  inputProps?: any;
  specialClass?: string;
  step?: number;
  min?: number;
  max?: number;
};

const NativeSlider: FC<NativeSliderProps> = ({
  onChange,
  value=0,
  step=1,
  min=1,
  max=100,
  specialClass='interest',
}) => {
  const classes = useStyles();
  const targetValue = !isNaN(value) ? +value : +min;
  return (
    <Slider
      onChange={(r: any, value: number) => onChange(value)}
      classes={{
        root: classes.sliderRoot,
        rail: classes.sliderRail,
        thumb: classes[specialClass],
      }}
      value={targetValue}
      step={step}
      min={min}
      max={max}
      aria-labelledby="non-linear-slider"
    />
  );
}

const FormSlider: FC<FormSliderProps> = ({
  control,
  name,
  unit='%',
  specialClass='interest',
  render,
  min=1,
  setValue,
  value,
  max=100,
  inputProps, ...props
}) => {
  const classes = useStyles();
  const handleChange = useCallback((value: number) => {
    setValue(name, value);
  }, [setValue, name]);
  // let addOn = {
  //   render: ({ onChange, value }: any) => (
  //     <NativeSlider 
  //       onChange={onChange}
  //       // value={value}
  //       specialClass={specialClass}
  //       min={min}
  //       max={max}
  //       {...props}
  //     />
  //   ),
  // };
  // if (!!render) {
  //   addOn = { render: (payload: any) => render(payload) };
  // }
  return (
    <div>
      <div className={classes.input}>
        <FormElement.SuffixNumericInput
          inputProps={inputProps}
          control={control}
          name={name}
          unit={unit}
          specialClass={specialClass}
        />
      </div>
      <NativeSlider 
        onChange={handleChange}
        specialClass={specialClass}
        min={min}
        value={value}
        max={max}
      />
      {/* <Controller
        name={name}
        control={control}
        {...addOn}
      /> */}
    </div>
  );
}

export default FormSlider;