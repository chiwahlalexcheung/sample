import { makeStyles } from '@material-ui/core/styles';

const useRootStyles = makeStyles(theme => ({
  root: {
  },
  item: {
    fontSize: '16px',
    fontWeight: 'bold',
    marginLeft: '53px',
  },
}));

export {
  useRootStyles,
};
