import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    width: '100%',
    height: '253px',
    padding: '30px 145px'
  },
}));

export {
  useStyles,
};
