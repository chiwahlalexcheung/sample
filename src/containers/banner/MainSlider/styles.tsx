import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';

const useStyles = makeStyles({
  image: {
    position: 'relative',
    width: '100%',
    height: '100%',
  },
  source: {
    maxWidth: '100%',
    maxHeight: '100%',
  }
});
const useSlideStyles = makeStyles({
  root: {
    position: 'relative',
    marginRight: '26px',
    height: '334px',
    minWidth: '272px',
  },
  thumbnail: {
    width: '272px',
    height: '334px',
    cursor: 'pointer',
  },
  title: {
    color: 'white',
    fontSize: '20px',
    fontWeight: 'bolder',
    position: 'absolute',
    bottom: '25px',
    left: '25px',
    width: '60%',
  },
  skeleton: {
    boxShadow: '0px 5px 10px #00000033',
    width: '100%',
  },
});
const useBackgroundStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    width: '100%',
    paddingTop: '46.4563%',
  },
  overlay: {
    position: 'absolute',
    zIndex: 7,
    width: '100%',
    height: '40%',
    bottom: '-1px',
    background: `transparent linear-gradient(180deg, #FFFFFF00 1%, #FFFFFF 26%) 0% 0% no-repeat padding-box`,
  },
  overlayOut: {
    opacity: 0,
  },
  sliderWrapper: {
    width: '65%',
  },
  wrapper: {
    position: 'absolute',
    top: '150px',
    left: 0,
    right: 0,
    margin: '0 auto',
    height: '334px',
    zIndex: 9,
  },
  bg: {
    width: '100%',
    height: '100%',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    position: 'absolute',
    top: '1px',
    left: 0,
    zIndex: 0,
  },
  info: {
    width: '320px',
    height: '169px',
    marginLeft: '60px',
  },
  actionBtn: {
    textTransform: 'uppercase',
    width: '166px',
  },
  title: {
    marginBottom: '10px',
    color: theme.palette.primary.dark,
    textTransform: 'uppercase',
    fontSize: '35px',
    fontWeight: 'bolder',
    lineHeight: 'normal',
  },
  subTitle: {
    fontSize: '22px',
    fontWeight: 'bolder',
    marginBottom: '10px',
    color: theme.palette.text.primary,
  },
}));
const useSliderStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    flex: 1,
  },
  prevBtn: {
    position: 'absolute',
    boxShadow: '0px 5px 10px #00000033',
    backgroundImage: `url("${StaticSources.icon.arrowLeft}")`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    width: '40px',
    height: '40px',
    display: 'block',
    left: '-60px',
    backgroundColor: 'white',
    borderRadius: '50%',
    cursor: 'pointer',
    '&::before': {
      display: 'none !important',
    },
    '&:hover': {
      backgroundImage: `url("${StaticSources.icon.arrowLeftActive}")`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundColor: theme.palette.primary.light,
    },
  },
  nextBtn: {
    position: 'absolute',
    backgroundImage: `url("${StaticSources.icon.arrowRight}")`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    boxShadow: '0px 5px 10px #00000033',
    display: 'block',
    cursor: 'pointer',
    width: '40px',
    right: '10px',
    height: '40px',
    backgroundColor: 'white',
    borderRadius: '50%',
    '&::before': {
      display: 'none !important',
    },
    '&:hover': {
      backgroundImage: `url("${StaticSources.icon.arrowRightActive}")`,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundColor: theme.palette.primary.light,
    },
  },
}));
export {
  useStyles,
  useSlideStyles,
  useSliderStyles,
  useBackgroundStyles,
};
