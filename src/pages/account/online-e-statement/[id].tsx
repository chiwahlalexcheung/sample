import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { storeWrapper } from '@store/store'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetServerSideProps } from 'next'
import OnlineEstatement from '@containers/section/OnlineEStatement'

const OnlineEStatementPage: React.FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation('common');
  const solutionRef = useRef<any>();
  const refs = {
    solutionRef,
  };
  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.online-e-statement')}</title>
        </Head>
        <OnlineEstatement applicationNo={applicationNo} />
    </HeadingLayout>
  );
}

export const getServerSideProps: GetServerSideProps = storeWrapper.getServerSideProps(
  async ({ locale, params={} }: any) => {
    return {
      props: {
        applicationNo: params.id,
        ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
      }
    };
  }
)
export default OnlineEStatementPage
