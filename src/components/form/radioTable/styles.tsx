import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';
import {
  Colors,
} from '@theme/global';
const{
  lightGrey
} = Colors;

export const useTableStyles = makeStyles(theme => ({
  table: {
    minWidth: 650,
  },
  title: {
    color: theme.palette.text.primary,
    fontSize: '19px',
    fontWeight: 'bolder',
  },
  content: {
    fontSize: '17px',
    fontWeight: 'normal',
    color: theme.palette.text.primary
  },
  radio: {
    fontSize: '18px'
  }
}))


export default useTableStyles