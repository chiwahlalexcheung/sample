import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '80px 75px',
    backgroundImage: `url(${StaticSources.banner.revolvingHighlightBanner})`,
    ...FullCoverBgStyles,
  },
}));
const useBoxStyles = makeStyles(theme => ({
    root: {
        paddingBottom: '47px'
    },
    title: {
      fontSize: '26px',
      color: theme.palette.primary.light,
      letterSpacing: '0px',
      fontWeight: 'bolder',
    }
}))


const useSlideStyles = makeStyles(theme => ({
    root: {
    //   position: 'relative',
      margin: '70px 10px',
      marginRight: '0',
      height: '235px',
      width: '175px',
      overflow: 'hidden',
      boxShadow: '0px 3px 6px #00000029',
      transition: "transform 0.15s ease-in-out",
      background: '#fff'
    },
    block: {
        position: 'absolute',
        margin: '0 auto',
        left: 0,
        right: 0,
        bottom: '60px',
        zIndex: 2,
        textAlign: 'center',
        height: '100%',
    },
    overlay: {
      background: 'transparent linear-gradient(180deg, #FFFFFF00 0%, #FAFAFA 36%, #FFFFFF 100%) 0% 0% no-repeat padding-box',
      opacity: 0.89,
      position: 'absolute',
      bottom: 0,
      height: '250px',
      width: '100%',
      zIndex: 1,
    },
    active: {
      transform: "scale3d(1.3, 1.3, 1)",
      zIndex: 888,
    },
    inActive: {
      zIndex: (props: any) => props.zIndex,
    },
    skeleton: {
      boxShadow: '0px 5px 10px #00000033',
    },
    cardContent: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        height: '100%'
    },
    title: {
        fontSize: '20px',
        lineHeight: '20px',
        fontWeight: 'bolder',
        letterSpacing: '0px',
        color: theme.palette.primary.light,
    },
    subTitle: {
        fontSize: '13px',
        fontWeight: 'bolder',
        letterSpacing: '0px',
        color: theme.palette.primary.light,
    },
    activeSubTitle: {
        fontSize: '22px',
        lineHeight: '22px',

    },
    image: {
        display: 'flex',
        flex: 1,
    },
    img: {
        width: '80px',
        height: '80px'
    }

  }));

export {
  useStyles,
  useBoxStyles,
  useSlideStyles
};
