import React, { FC, forwardRef, useCallback, useState, useEffect, useContext ,createContext } from 'react'
import { map, get, size, findIndex, isUndefined } from 'lodash'
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import CommonPopup from '@components/common/Popup'
import StaticSources from '@config/sources'
import FormElement from '@components/form';
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  // Typography,
  Grid,
//   Container,
  Box,
//   List,
//   ListItem,
//   ListItemText,
//   ListItemIcon,
//   Card,
//   CardHeader,
//   CardContent,
  Typography,
} from '@material-ui/core';
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from '@hooks/common';

import {
    ReCaptchaBlock,
} from '@containers/section/ContactUs'
import {
  useStyles,
  useFormStyles,
  usePopupStyles
} from './styles';
import { LabelImportantTwoTone } from '@material-ui/icons';
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
import {
    Agreements,
    StatementBlock,
    StatementBlockProps
} from '@containers/section/FullStepSolution/form'
import ServiceConfig from '@config/service'
const {
  googleReCaptcha: {
    siteKey: reCaptchaKey,
  },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from 'react-google-recaptcha-v3'
import { useEvaluation } from '@store/application/context'

export interface PropertyProps {
    children: JSX.Element;
    values: KeyPairProps;
  }

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export const PropertyContext = createContext<Partial<ContextProps>>({});
export const useProperty = () => useContext(PropertyContext) as ContextProps;
export const PropertyProvider: FC<PropertyProps> = ({ values={}, children }) => {
  const { control, register, changeValue, setValue, watch, getValue, getValues, value, reset }: KeyPairFormHandleProps = useKeyPairFormValue(values);
//   useEffect(() => {
//     register("loanType");
//   }, []);
//   const {
//     value: info,
//     changeValue,
//     getValue,
//     setValue,
//   }: KeyPairHandleProps = useKeyPairValue(values);
  return (
    <PropertyContext.Provider value={{
      info: value,
      changeValue,
      getValue,
      setValue,
      onChange: changeValue,
      register,
      control,
      getValues,
      watch,
      reset
    }}>
      <form>
        {children}
      </form>
    </PropertyContext.Provider>
  );
}
export interface propertySelectProps {
    label: string;
}
const PropertySelectCell: FC<any> = ({type, options}) => {
    const { t } = useTranslation('form');
    const classes = useFormStyles();
    const {
        control,
        watch,
        setValue,
        register
    }: ContextProps = useProperty();
    const { info, onValueChange } = useEvaluation();
    const {[type]: formValue} = watch();
    useEffect(() => {
        onValueChange(type, formValue, watch());
    }, [formValue, type, watch])
    
    const selectOpts = get(info, type, []);
    const isDisabled = (size(selectOpts)==0);
    return (
        <Grid container spacing={3} className={classes.grid} key={type}>
            <Grid item xs={12} sm={4}>
                <Typography component={'h4'} className={classes.label}>
                    {t(`label.${type}`)}
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                <FormElement.SearchSelect
                    setValue={setValue}
                    name={type}
                    control={control}
                    options={selectOpts}
                    value={formValue}
                    register={register}
                    disable={isDisabled}
                />
            </Grid>
        </Grid>
    )
}

export interface SubmitButtonProps{
    children: JSX.Element;
    label: string;
}
export const ValuationTerms: FC = () =>{
    const classes = usePopupStyles();
    const { t:Bt } = useTranslation('button');
    const {
      goNextStep,
    }: StepSolutionContext = useStepSolution();
    const title: string = 'Free Online Property Valuation Service'
    const content: string = `Please read, understand and agree to the Terms and Conditions<br/>for Free Online Property Valuation Service and Privacy Policy.`
    return (   
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'} className={classes.root}>
            <Typography component={'h4'}>
                {title}
            </Typography>
            <div dangerouslySetInnerHTML={{ __html: content}} className={classes.content} />
            
            <CommonButton
                label={Bt('ok')}
                onClick={goNextStep}
            />
        </Box>
    )
}

export const ValuationTermsDetail: FC = () => {
    const classes = usePopupStyles();
    const { t:Bt } = useTranslation('button');
    const {
      goNextStep,
    }: StepSolutionContext = useStepSolution();
    const terms: string = `Free Online Property Valuation Service<br /><br />
    Sun Hung Kai Credit always provides you professional loan services. Free Online Property Valuation Service is available for you to obtain the latest property value anywhere, anytime. Please read and accept the below terms and conditions to enjoy this service. After valuation, you can apply loan through online to enjoy the cash-out services right away.<br /><br />
    Terms and Conditions for Free Online Property Valuation Service<br /><br />
    1. By submitting a request for a Sun Hung Kai Credit Limited (“Sun Hung Kai Credit”) Free Online Property Valuation Service, you shall be deemed to agree to be bound by the terms and conditions set out below.<br />
    2. Sun Hung Kai Credit may require visitors of our Free Online Property Valuation Service to provide the Personal Data in the course of their browsing of our website. You may receive from us or our affiliated companies’ telephone calls, email and direct mailings containing promotional and/or premium message from time to time. If you do not want to receive them, please write to our Data Protection Officer (For details, please refer to the “Disclaimer” and “Privacy Policy”).<br /><br />
    3. Sun Hung Kai Credit does not warrant the accuracy, timeliness or completeness of any information shown.<br /><br />
    4. The Free Online Property Valuation Service is indicatory and for reference purpose only and is not binding on Sun Hung Kai Credit. Sun Hung Kai Credit reserves the right to re-value the subject property at its full discretion at anytime.<br /><br />
    5. Sun Hung Kai Credit shall not be liable to you for any damage incurred or loss suffered by you either directly or indirectly in connection with the purchase or proposed purchase of the property in respect of which a Free Online Property Valuation Service is made, whether as a result of you had relied on the Free Online Property Valuation Service in deciding whether to proceed with the purchase of the property or otherwise, and you hereby acknowledge that if you so rely Free Online Property Valuation Service, you should so entirely at your own risk. 6. In case of any discrepancy between English and Chinese version, the English version shall prevail.`
    return (   
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <div dangerouslySetInnerHTML={{ __html: terms}} />
            <CommonButton
                label={Bt('ok')}
                onClick={goNextStep}
            />
        </Box>
    )

}
export const MarketingReceivingForm: FC = () => {
    const classes = usePopupStyles();
    const { t:Bt } = useTranslation('button');
    const { t } = useTranslation('form')
    const {
        control,
        watch,
        setValue,
        register
    }: ContextProps = useProperty();
    const {
      goNextStep,
    }: StepSolutionContext = useStepSolution();
    const tips: string = 'I do not wish to receive any promotional material from Sun Hung Kai Credit Limited via the following channel(s)*: <br />(Please tick (“✓”) the box(es) below where applicable) ';
    const remark: string = '* If you return without (“✓”) any of the above boxes, it means that you do not wish to opt-out from any form of Sun Hung Kai Credit Limited (“the Company”)’s direct marketing.<br/><br/>The above options represent your present choice regarding whether or not to receive direct marketing materials. This replaces any choice communicated by you to the Company prior to this application.<br/><br/>Please note that your above choice applies to the direct marketing of the classes of products, services and/or subjects as set out in the Company’s “Notice to Customers and Other Individuals Relating to the Personal Data (Privacy) Ordinance and the Code of Practice on Consumer Credit Data”. Please also refer to the said Notice on the kinds of personal data which may be used in direct marketing and the classes of persons to which your personal data may be provided for them to use in direct marketing.';
    return (
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <Typography component={'h4'}>
                {t('title.directMarketingForm')}
            </Typography>
            <div dangerouslySetInnerHTML={{__html: tips}}/>
            <Box display={'flex'} flexDirection={'row'} justifyContent={'space-between'} className={classes.marketingCheckboxes}>
                <FormElement.NativeCheckbox
                    label={t('label.directMailing')}
                    name="directMailing"
                    control={control}
                    className="form"
                />
                <FormElement.NativeCheckbox
                    label={t('label.mobileSms')}
                    name="mobileSms"
                    control={control}
                    className="form"
                />
                <FormElement.NativeCheckbox
                    label={t('label.promotionEmail')}
                    name="promotionEmail"
                    control={control}
                    className="form"
                />
                <FormElement.NativeCheckbox
                    label={t('label.telephoneCall')}
                    name="telephoneCall"
                    control={control}
                    className="form"
                />
            </Box>
            <div dangerouslySetInnerHTML={{__html: remark}} />
            <CommonButton
                label={Bt('confirm')}
                onClick={goNextStep}

            />
        </Box>
    )
}

export const Thankyou: FC = () =>{
    const classes = usePopupStyles();
    const { t:Bt } = useTranslation('button');
    const { t } = useTranslation('form');
    const {
        watch,
    }: ContextProps = useProperty();

    const {
      goNextStep,
      onSubmit
    }: StepSolutionContext = useStepSolution();
    const content: string = 'Thank you for using our free property valuation service. We have sent the property result to you via SMS. Should you have any enquiries, please call our Customer Service Hotline at 2966 2688 or click here to contact our staff.'
    return (   
        <Box display={'flex'} flexDirection={'column'} alignItems={'center'}>
            <Typography component={'h4'}>
                {t('title.valuationService')}
            </Typography>
            <Typography component={'h4'}>
                {content}
            </Typography>

            <CommonButton
                label={Bt('ok')}
                // onClick={goNextStep}
                onClick={()=>{
                    const v = watch();
                    console.log(v);
                    // alert(JSON.stringify(v))
                }} 
            />
        </Box>
    )
}

export interface PopupItemProps {
    key: string;
    component: JSX.Element;
}
export interface PopupBarProps {
  list: PopupItemProps[];
};

export const PopupContent: FC<PopupBarProps> = ({ list }) => {
    const {
      step,
    }: StepSolutionContext = useStepSolution();
    // console.log("step", step);
    return (
      <div>
        {
          map(list, ({ key, ...l }: PopupItemProps, index: number) => {
            let styles = { display: 'none' };
            if (index === step) {
              styles = { display: 'block' };
            }
            return (
              <div key={key} style={styles}>
                {l.component}
              </div>
            );
          })
        }
      </div>
    );
  }
  
export const SubmitButton: FC<any> = ({ children, label}) => {
    const { enabled, toggle }: ToggleProps = useToggle(false);
    const classes = useFormStyles();
    const onCloseAgreement = useCallback(() => {
      toggle();
    }, [toggle]);
    // const {
    //   setStep,
    //   step,
    // }: StepHandleProps = useStepper({ maxStep: list.length, step: 0 });
    // const onChanged = useCallback((index: number) => {
    //   setStep(index);
    // }, [setStep]);
    const onSubmit = useCallback((v: any) => {
        console.log(v);
    }, []);
    const steps: PopupItemProps[] = [
        {
            key: 'vt',
            component: <ValuationTerms />
        },
        {
            key: 'vtd',
            component: <ValuationTermsDetail />
        },
        {
            key: 'mrf',
            component: <MarketingReceivingForm />
        },
        {
            key: 'ty',
            component: <Thankyou />
        },
    ];
    return(
        <React.Fragment>
            <CommonButton 
                label={label} 
                className={classes.button} 
                onClick={() => toggle()}
                // onClick={()=>{
                //     const v = watch();
                //     console.log(v);
                //     alert(JSON.stringify(v))
                // }} 
            />
            <CommonPopup
              show={enabled}
              afterClose={onCloseAgreement}
              maxWidth={'lg'}
            //   title={title}
            >
                <StepSolution maxStep={steps.length} onSubmit={onSubmit}>
                    <PopupContent list={steps} />
                </StepSolution>
            </CommonPopup>
        </React.Fragment>
    )
}

export const PropertyInformation: FC = () => {
    const [reCaptcha, setReCaptcha] = useState('');
    const { t } = useTranslation('form');
    const { t: bt } = useTranslation('button')
    const {
        control ,
        watch,
        setValue,
        register,
        reset,
        info
    }: ContextProps = useProperty();
    const {
        contactNo,
        name
    } = watch();
    const classes = useFormStyles();

    const privacyPolicy: string = `To preserve the confidentiality of all information provided by you through internet, we maintain the following privacy principles: <br /><br />1. We only collect personal information that we believe to be relevant and required to understand your financial needs and to conduct our business. <br /><br />2. We use your personal information to provide you with better customer services and products. <br /><br />3. We may pass your personal information to our group’s companies or agents, as permitted by law. <br /><br />4. We will not disclose your personal information to any external organisation unless we have obtained your consent. <br /><br />5. We may be required from time to time to disclose your personal information to Governmental or judicial bodies or agencies or our regulators, but we will only do so under proper authority. <br />6. We aim to keep your personal information on our records accurate and up-to-date. <br />7. We maintain strict internet security systems designed to prevent unauthorised access to your personal information by anyone.`;
  
    const firstMortgageKeyFacts: string = "1.The minimum and maximum repayment period is 6 months and 84 months respectively.<br />2.The annual percentage rate ranges from 14% to 24%.<br />3.A representative example of total cost for a loan below is for reference only:<br /> - Loan amount: HK$1,000,000<br /> - Monthly instalment of HK$89,803 for 12 months<br /> - Annual percentage rate: 14%<br /> - Total repayment amount: HK$1,077,631";
    const secondMortgageKeyFacts: string = "1.The minimum and maximum repayment period is 6 months and 84 months respectively.<br />2.The annual percentage rate ranges from 14% to 24%.<br />3.A representative example of total cost for a loan below is for reference only:<br /> - Loan amount: HK$1,000,000<br /> - Monthly instalment of HK$89,803 for 12 months<br /> - Annual percentage rate: 14%<br /> - Total repayment amount: HK$1,077,631";
    return (
      <Box display={'flex'} alignItems={'center'} justifyContent={'center'} flexDirection={'column'} className={classes.form}>
        <Typography className={classes.title}>{t('title.propertyInformation')}</Typography>   
        {map([
            'area', 
            'district', 
            'estate',
            'streetName',
            'streetNo',
            'block',
            'floor',
            'flatRoom',
            'carpark'
        ], (type)=>(
            <PropertySelectCell type={type} />
        ))}

        <Typography className={classes.title}>{t('title.contactInformation')}</Typography>
    
        <Grid container spacing={3} className={classes.grid}>
            <Grid item xs={12} sm={4}>
                <Typography component={'h4'} className={classes.label}>
                    {t('label.name')}
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                <FormElement.TextInput
                    control={control}
                    name="name"
                    value={name}
                    type="text"
                    inputProps={{
                        placeholder: t('placeholder.pleaseType'),
                    }}
                />
            </Grid>
        </Grid>
        <Grid container spacing={3} className={classes.grid}>
            <Grid item xs={12} sm={4}>
                <Typography component={'h4'} className={classes.label}>
                    {t('label.contactNo')}
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                <FormElement.TextInput
                    control={control}
                    name="contactNo"
                    type="text"
                    value={contactNo}
                    inputProps={{
                        placeholder: t('placeholder.pleaseType'),
                    }}
                />
                <Typography className={classes.inputTips}>{t('note.validMobileSms')}</Typography>

            </Grid>
        </Grid>
        <Grid container spacing={3} className={classes.grid}>
            <Grid item xs={12} sm={4}>
                <Typography component={'h4'} className={classes.label}>
                    {t('label.ownProperty')}
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                {/* <FormElement.NativeCheckbox
                    label={bt('yes')}
                    name="ownProperty"
                    control={control}
                    className="form"
                />
                <FormElement.NativeCheckbox
                    label={bt('no')}
                    name="ownProperty"
                    control={control}
                    className="form"
                /> */}
                {/* <input name="test123" ref={register} /> */}
                <FormElement.NativeRadioGroup
                    name="property"
                    control={control}
                    // selectedValue={property}
                    setValue={setValue}
                    options={[
                        {label: t('yes'), link: '', key: 'yes', value: 'yes'},
                        {label: t('no'), link: '', key: 'yes', value: 'no'}
                    ]}
                />
            </Grid>
        </Grid>

        <Grid container spacing={3} className={classes.grid}>
            <Grid item xs={12} sm={4}>
                <Typography component={'h4'} className={classes.label}>
                    {t('label.recaptcha')}
                </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
                <ReCaptchaBlock token={reCaptcha} page={"valuation_form"} onToken={setReCaptcha} />
            </Grid>
        </Grid>

        <Box display={'flex'} flexDirection={'row'} justifyContent={'center'}>
            
            <FormElement.NativeCheckbox
                name="agreement"
                control={control}
                // value={'1'}
            />
            <div style={{flex: 1, }}>
                <Typography component={'span'} >I confirm that I have read, understood and agreed to the</Typography>
                <Agreements
                    label={t('label.onlineValuationTerms')}
                    title={t('label.onlineValuationTerms')}
                >
                    <Box>
                        <Typography component={'span'} >online Valuation Terms</Typography>
                    </Box>
                </Agreements>
                
                <Typography component={'span'} > and </Typography>
                
                <Agreements
                    label={t('label.privacyPolicy')}
                    title={t('label.privacyPolicy')}
                >
                    <div dangerouslySetInnerHTML={{ __html: privacyPolicy }} />
                </Agreements>
                
                <Typography component={'span'} >. If you do not wish Sun Hung Kai Credit Limited to use your personal data in direct marketing, please click </Typography>
                
                <Agreements label={t('label.here')} >
                    <div dangerouslySetInnerHTML={{ __html: privacyPolicy }} />
                </Agreements>

                <Typography component={'span'} >.</Typography>
            </div>
        </Box> 

        <Box display={'flex'} flexDirection={'row'} justifyContent={'space-between'} className={classes.buttons}>
            {/* <CommonButton label={bt('getValuation')} className={classes.button} onClick={()=>{
                const v = watch();
                console.log(v);
                alert(JSON.stringify(v))
            }} /> */}
            <SubmitButton label={bt('getValuation')} />
            <CommonButton label={bt('reset')} className={classes.button} onClick={()=>{
                // reset(info);
                reset();
                // setValue("contactNo", "123")
            }} />
            <CommonButton label={bt('noApplicableChoice')} className={classes.button}/>
        </Box>
        <Box display={'flex'} flexDirection={'row'} >
            <Box display={'flex'} flexDirection={'column'} className={classes.keyFactsBox}>
                <div className={classes.keyFactsTitleBorder}>
                    <Typography className={classes.keyFactsTitle} >{t('label.firstMortgageKeyFacts')}</Typography>
                </div>
                <StatementBlock content={firstMortgageKeyFacts} />
            </Box>
            <Box display={'flex'} flexDirection={'column'} className={classes.keyFactsBox}>
                <div className={classes.keyFactsTitleBorder}>
                    <Typography className={classes.keyFactsTitle}>{t('label.secondMortgageKeyFacts')}</Typography>
                </div>
                <StatementBlock content={secondMortgageKeyFacts} />
            </Box>
        </Box>

      </Box>
    );
  }

  

export const PropertyInformationForm: FC<any> = () => {
    const classes = useFormStyles();
    return (
      <PropertyProvider 
        values={{
            area: '',
            district: '',
            estate: '',
            streetName: '',
            streetNo: '',
            block: '',
            floor: '',
            flatRoom: '',
            carpark: '',

            contactNo: '',
            name: '',

            // property: '',
            // agreement: '',

            // directMailing: '1',
            // mobileSms: '1',
        }} >
          <Box display={'flex'} alignItems={'center'} justifyContent={'center'} >
            <PropertyInformation />
            {/* <Typography className={classes.title}>test</Typography> */}
          </Box>
        </PropertyProvider>
    );
    return (
        <Box className={classes.root}>
            {/* <Typography className={classes.title}>test</Typography> */}
        </Box>
    )
}


export const ValuationForm: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  // const { t: Bt } = useTranslation('button');
  const classes = useStyles();

    return (
        <div>
            <InnerPage>
                <div className={classes.root} >
                    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
                        <PropertyInformationForm />
                    </GoogleReCaptchaProvider>
                </div>
                {/* <CardCalculator /> */}
                {/* <Box display={'flex'} style={{background: '#fff', width: '100%'}}>
                    <Typography component={'h4'} className={classes.label}>tetest
                        {t('label.recaptcha')}
                    </Typography>

                </Box> */}
            </InnerPage>
        </div>
    );
});

export const OnlineValuationForm: FC = () => {
    const { t } = useTranslation('section');
    const classes = useStyles();
    return (
        <InnerPage>
            <div className={classes.root} >
                <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
                    <PropertyInformationForm />
                </GoogleReCaptchaProvider>
            </div>
            {/* <CardCalculator /> */}
            {/* <Box display={'flex'} style={{background: '#fff', width: '100%'}}>
                <Typography component={'h4'} className={classes.label}>tetest
                    {t('label.recaptcha')}
                </Typography>

            </Box> */}
        </InnerPage>
    )
}
// export default ValuationForm;
