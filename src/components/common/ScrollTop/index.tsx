import { FC, useCallback } from 'react'
import css from 'clsx'
import {
  useStyles,
} from './styles'
import {
  useEventListener,
} from '@hooks/native'
import { useTranslation } from 'next-i18next'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'

import {
  useToggle,
  ToggleProps,
} from '@hooks/common'

interface BackToTopProps {
  offset?: number;
}

const BackToTop: FC<BackToTopProps> = ({
  offset=220,
}) => {
  const { t } = useTranslation('common');
  const { enabled: show, toSwitch }: ToggleProps = useToggle();
  const classes = useStyles();
  const goToTop = useCallback(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  }, []);
  const handler = useCallback(() => {
    const { pageYOffset } = window;
    if (pageYOffset >= offset) {
      toSwitch(true);
    } else {
      toSwitch(false);
    }
  }, [offset]);
  useEventListener('scroll', handler);
  let styles = classes.root;
  if (show === true) {
    styles = css(styles, classes.show);
  } else if (show === false) {
    styles = css(styles, classes.hide);
  }
  return (
    <div className={styles}>
      <div className={classes.wrapper} onClick={goToTop}>
        <LocalImage src={StaticSources.icon.scrollToTop} alt={t('label.backToTop')} />
      </div>
    </div>
  );
}

export default BackToTop;
