import React, { FC, forwardRef, useCallback } from 'react'
import css from 'clsx'
import { map } from 'lodash'
import { useRouter } from 'next/router'
import Routes from '@config/routes'
import StaticSource from '@config/sources'

import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import StaticSources from '@config/sources';
import Link from '@components/common/Link'
import Fade from 'react-reveal/Fade'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Box,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStyles,
  useBoxStyles,
  useFootnotesStyles,
  useHighLightStyles
} from './styles';

import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
import {
    Agreements,
} from '@containers/section/FullStepSolution/form'

export const BannerBox: FC = () => {
    const { t } = useTranslation('section');
    const { t: Bt } = useTranslation('button');
    const classes = useBoxStyles();
    const description: string = '<span style="font-size: 22px; color: #000000; ">We have launched the first*</span><br/><span style="font-size: 26px; color: #D21818; font-weight: bold; ">PAYMENT HOLIDAY# in Hong Kong!</span><br /><br /><span style="font-size: 20px; color: #000000; ">Enjoy a 1-year free home comprehensive insurance with coverage up to HK$500,000 for new customers upon successful application. This offer is subject to terms and conditions. For details, please contact our Customer Service Officer.</span>'
  

    return (
        <Box display={'flex'} className={classes.root} justifyContent={'space-between'}>
            <Box display={'flex'} flexDirection={'column'} className={classes.left}>
                <div className={classes.heading}>
                    <HeadingWithSubTitle
                        title={t('title.whyChooseUsTitle')}
                        subTitle={t('title.whyChooseUsSubTitle')}
                    />
                </div>
                
                <div className={classes.description}>
                    <div dangerouslySetInnerHTML={{__html: description}}  />
                </div> 
            </Box>
            <Box display={'flex'} className={classes.right}>
                <Box display={'flex'} className={classes.banner} justifyContent={'flex-end'} alignContent={'flex-end'}>
                    <Box display={'flex'} flexDirection={'column'} className={classes.bannerBottomCover} justifyContent={'center'} alignContent={'center'}>
                        <div className={classes.affordHeader}>
                            <div className={classes.icon}>
                                <LocalImage
                                    src={StaticSources.icon.paymentHoliday}
                                    alt={t('title.paymentHoliday')}
                                />
                            </div>
                            <Typography component={'h3'} className={classes.title}>
                            {t('title.paymentHoliday')}
                            </Typography>
                        </div>
                        <Box className={classes.buttons} display={'flex'} flexDirection={'row'} justifyContent={'space-between'} >
                            <CommonButton
                                className={classes.button}
                                label={Bt('learnMore')}
                            />
                            <CommonButton
                                className={classes.button}
                                label={Bt('contactUs')}
                                secondary
                            />
                        </Box>
                    </Box>

                </Box>
            </Box>
        </Box>
    );
}

export const PaymentHolidayHighlights : FC = () => {
    const { t } = useTranslation('section');
    const classes = useHighLightStyles();
    
    return(
        <Box display={'flex'} className={classes.root}>
            <Box display={'flex'} className={classes.highLights}>
                <Box display={'flex'} className={classes.highLight} justifyContent={'center'} alignItems={'center'}>
                    <LocalImage
                        className={classes.highLightIcon}
                        src={StaticSources.icon.whyChooseUs.roundNoLimitation}
                        alt={t('paymentHoliday.noLimitation')}
                    />
                    <Typography component={'h3'} className={classes.highLightLabel}>
                        {t('paymentHoliday.noLimitation')}
                    </Typography>
                </Box>
                <Box display={'flex'} className={classes.highLight} justifyContent={'center'} alignItems={'center'}>
                    <LocalImage
                        className={classes.highLightIcon}
                        src={StaticSources.icon.whyChooseUs.round24Hours}
                        alt={t('paymentHoliday.approvalNextDay')}
                    />
                    <Typography component={'h3'} className={classes.highLightLabel}>
                        {t('paymentHoliday.approvalNextDay')}
                    </Typography>
                </Box>
                <Box display={'flex'} className={classes.highLight} justifyContent={'center'} alignItems={'center'}>
                    <LocalImage
                        className={classes.highLightIcon}
                        src={StaticSources.icon.whyChooseUs.roundPenalty}
                        alt={t('paymentHoliday.noPenaltyFee')}
                    />
                    <Typography component={'h3'} className={classes.highLightLabel}>
                        {t('paymentHoliday.noPenaltyFee')}
                    </Typography>
                </Box>
                <Box display={'flex'} className={classes.highLight} justifyContent={'center'} alignItems={'center'}>
                    <LocalImage
                        className={classes.highLightIcon}
                        src={StaticSources.icon.whyChooseUs.roundFlexible}
                        alt={t('paymentHoliday.flexibleTerms')}
                    />
                    <Typography component={'h3'} className={classes.highLightLabel}>
                        {t('paymentHoliday.flexibleTerms')}
                    </Typography>
                </Box>
            </Box>
            <div className={classes.phoneIcon}>
                <LocalImage
                    src={StaticSources.icon.whyChooseUs.phone}
                />
            </div>
        </Box>
    )
}

export const Footnotes: FC = () => {
    const { t } = useTranslation('section');
    const classes = useFootnotesStyles();
    
    // const 
    return(
        <Box display={'flex'} className={classes.root} flexDirection={'column'}>
            {/* <BannerBox */}
            <Box display={'flex'} className={classes.footnote}>
                <Typography component={'span'} >{t('paymentHoliday.firstPaymentHoliday')}</Typography>
            </Box>
            <Box display={'flex'} className={classes.footnote}>
                <div style={{ flex: 1, }}>
                    <Typography component={'span'} >{t('paymentHoliday.newCustomer1')}</Typography>
                    
                    <Agreements
                        label={t('paymentHoliday.newCustomer2')}
                        title={t('paymentHoliday.newCustomer2')}
                    >
                        <Box>
                            <Typography component={'span'} >{t('paymentHoliday.newCustomer2')}</Typography>
                        </Box>
                    </Agreements>
                    
                    <Typography component={'span'} >{t('paymentHoliday.newCustomer3')}</Typography>
                </div>

            </Box>
        </Box>

    )
}

export const WhyChooseUs: FC<any> = forwardRef((props, r) => {
  const classes = useBoxStyles();
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
        <InnerPage>
            <BannerBox />
            <PaymentHolidayHighlights />
            <Footnotes  />
        </InnerPage>
    </div>
  );
});

export default WhyChooseUs;
