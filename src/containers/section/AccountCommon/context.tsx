/*
  several Account sections share some common functions.
*/
import apiManager from "@lib/apiManager";
import RouteSources from "@config/routes";

const formatMoney = (s) => {
  if (!s) {
    return s;
  }
  if (Number(s) === NaN) {
    return s;
  }
  return "HK$" + Number(s).toLocaleString();
};

const formatDate = (s) => {
  // s is "20210408", result is "08 / 04 / 2012"
  if (!s) {
    return s;
  }
  if (s.length !== 8) {
    return s;
  }
  return s.substring(6) + " / " + s.substring(4, 6) + " / " + s.substring(0, 4);
};

/*
  get method, by apiManager.
*/
const apiGet = (url, data = {}, params = {}) => {
  const payload = {
    dispatch: () => {},
    url: url,
    data: data,
    underAuth: null,
    skipError: null,
    additional: null,
    params: params,
    isServer: null,
    noForceAuth: null,
    method: null,
    errorKey: null,
    noFormData: null,
  };
  return apiManager.get(payload);
};

/*
  post method, by apiManager.
*/
const apiPost = (url, data = {}, params = {}) => {
  const payload = {
    dispatch: () => {},
    url: url,
    data: data,
    underAuth: null,
    skipError: null,
    additional: null,
    params: params,
    isServer: null,
    noForceAuth: null,
    method: null,
    errorKey: null,
    noFormData: null,
  };
  return apiManager.post(payload);
};

/*
  if no user, go to logon page.
*/
const checkUserInfo = async (router) => {
  const userInfo = await apiGet("userInfo");
  if (userInfo !== undefined) {
    if (!("user" in userInfo["data"])) {
      router.push(RouteSources.accountLogon, RouteSources.accountLogon);
    }
  }
};

/*
  get loanList and return it
*/
const getLoanList = async () => {
  const result = await apiGet("custAppLoan");
  if (result && !("error" in result)) {
    return result["data"]["LOS"]["BODY"]["appl_loan_list"];
  }
  return null;
};

/*
  if user does not have the application, go to profile page.
  for Documents Upload
*/
const checkApplicationOwner = async (router, applicationNumber) => {
  const loanList = await getLoanList();
  let found = false;
  loanList.forEach((loan) => {
    if (loan["acct_no"] === applicationNumber) {
      found = true;
    }
  });
  if (!found) {
    router.push(RouteSources.accountProfile, RouteSources.accountProfile);
  }
};

/*
  clear cookie, go to logon page.
*/
const signOut = async (router) => {
  await apiGet("signOut");
  router.push(RouteSources.accountLogon, RouteSources.accountLogon);
};

/*
  go back to the profile page, with the app. info.
 */
const goProfile = (router, applicationNo) => {
  router.push({
    pathname: RouteSources.accountProfile,
    query: { number: applicationNo },
  });
};

export {
  formatDate,
  formatMoney,
  apiGet,
  apiPost,
  getLoanList,
  checkUserInfo,
  checkApplicationOwner,
  signOut,
  goProfile,
};
