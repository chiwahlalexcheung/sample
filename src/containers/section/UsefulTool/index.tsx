import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import Fade from 'react-reveal/Fade'
import { useRouter } from 'next/router'
import StaticSources from '@config/sources';
import Routes from '@config/routes';
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
// import Slide from 'react-reveal/Slide'
import { useTranslation } from 'next-i18next'
import {
  InnerPage,
} from '@containers/layout/Heading'

import {
  // Typography,
  Container,
  Box,
  Card,
  CardContent,
} from '@material-ui/core';
import {
  RefControlProps,
} from '@interfaces/common'
import {
  useStyles,
  useCardStyles,
} from './styles';

export interface ToolProps {
  label: string;
  icon: string;
  link: Function;
  buttonLabel: string;
  id: string;
};

export interface UseFulCardProps extends ToolProps {
  // todo
};
export interface UseFulCardsProps {
  tools: UseFulCardProps[];
};
export const UseFulCard: FC<UseFulCardProps> = ({ label, icon, link, buttonLabel }) => {
  const classes = useCardStyles();
  const router = useRouter();
  const onDetail = useCallback(() => {
    link();
  }, [link]);
  return (
    <Card className={classes.root}>
      <CardContent className={classes.content}>
        <Box display={'flex'} flexDirection={'column'} className={classes.wrapper}>
          <h3 className={classes.title}>
            <div dangerouslySetInnerHTML={{ __html: label }} />
          </h3>
          <Box
            className={classes.image}
            display={'flex'}
            alignItems={'center'}
            justifyContent={'center'}
          >
            <LocalImage src={icon} alt={label} />
          </Box>
          <Box
            className={classes.button}
            display={'flex'}
            alignItems={'center'}
            justifyContent={'center'}
          >
            <CommonButton
              label={buttonLabel}
              onClick={() => onDetail()}
              className={classes.btn}
            />
          </Box>
        </Box>
      </CardContent>
    </Card>
  );
}

export const UseFulCards: FC<UseFulCardsProps> = ({ tools }) => {
  const classes = useStyles();
  return (
    <Box display={'flex'} className={classes.panel}>
      {
        map(tools, (tool: UseFulCardProps) => (
          <UseFulCard key={tool.id} {...tool} />
        ))
      }
    </Box>
  );
}

export const UseFulTooler: FC<any> = forwardRef(({ slide }, r) => {
  const { t } = useTranslation('section');
  const { t: Bt } = useTranslation('button');
  const classes = useStyles();
  const router = useRouter();
  const tools: ToolProps[] = [
    {
      id: 'mortgage',
      label: t('useful-tools.mortgage'),
      link: (): void => {
        slide('loadCalculatorRef');  // slide to the ref div
      },
      icon: StaticSources.calculator,
      buttonLabel: Bt('checkNow'),
    },
    {
      id: 'valuationService',
      label: t('useful-tools.valuationService'),
      icon: StaticSources.valuationService,
      link: (): void => {
        router.push(Routes.onlineProperty);
      },
      buttonLabel: Bt('valuateNow'),
    },
  ];

  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <Box display={'flex'} justifyContent={'space-between'}>
          <Box display={'flex'} flexDirection={'column'} className={classes.left} justifyContent={'flex-start'} >
              <Fade top>
                <div>
                  <HeadingWithSubTitle
                    title={t('title.usefulHeader')}
                    subTitle={t('title.usefulSubTitle')}
                  />
                </div>
              </Fade>
              <Fade right>
                <div className={classes.sentence} dangerouslySetInnerHTML={{ __html: t('title.usefulSentence') }} />
              </Fade>
              <Fade bottom>
                <ul className={classes.list}>
                  <li>{t('label.easyApplication')}</li>
                  <li>{t('label.instantValuation')}</li>
                </ul>
              </Fade>
          </Box>
          <div className={classes.right}>
            <Fade right>
              <UseFulCards tools={tools} />
            </Fade>
          </div>
        </Box>
      </InnerPage>
    </div>
  );
});

export default UseFulTooler;
