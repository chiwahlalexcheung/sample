import React, { FC, useMemo } from 'react'
import Link from 'next/link'
import css from 'clsx'
import {
  Grid,
  Typography,
} from '@material-ui/core'
import {
  TypographyProps,
} from '@interfaces/text'
import {
  useStyles,
} from './styles'

interface LinkProps {
  href: string;
  rel: string;
  label?: string;
  // alt?: string;
  children?: JSX.Element | React.ReactNode;
  className?: string;
  labelProps?: any;
}

const CommonHyperLink: FC<LinkProps> = ({
  href,
  rel,
  label,
  labelProps={},
  children,
  className='',
}) => {
  const classes = useStyles();
  return useMemo(() => (
    <Link href={href}>
      <a rel={rel} className={css(classes.root, className)}>
        {
          !label ? children : <Typography {...labelProps}>
            {label}
          </Typography>
        }
      </a>
    </Link>
  ), [
    href
    ,
    rel,
    label,
    classes,
    className,
    labelProps,
    children,
  ]);
}

export default CommonHyperLink;
