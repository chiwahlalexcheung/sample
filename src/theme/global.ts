
const FullCoverBgStyles = {
  backgroundRepeat: 'no-repeat',
  backgroundSize: 'cover',
  backgroundPosition: 'center center',
};

const RelativeBefore = {
  position: 'relative',
  '&::before': {
    content: "''",
    display: 'block',
    position: 'absolute',
    top: 0,
  }
};

const Colors = {
  lightGrey: '#F5F6F3',
  linkBlue: '#77a1ff',
  titleGold: '#CC7D20',
}

export {
  FullCoverBgStyles,
  RelativeBefore,
  Colors,
};
