import React, { FC, forwardRef, useCallback, useEffect, useContext ,createContext, useMemo } from 'react'
import { map, pick, includes, debounce, isUndefined } from 'lodash'
// import { useForm } from "react-hook-form"
import css from 'clsx';
import Numeral from 'numeral';
import Fade from 'react-reveal/Fade'
import ServiceConfig from '@config/service'
import { useRouter } from 'next/router'
// import StaticSources from '@config/sources';
import Routes from '@config/routes';
import LocalImage from '@components/image/LocalImage'
import { PopupContactUs } from '@containers/section/ContactUs'
import { useCalculatorConfig, useCalculate } from '@store/content/context'
import { useAppliedCalculationInfo } from '@store/common/context'
import { useLocale, LocaleContextProps } from '@components/common/Locale'
import CommonButton from '@components/common/Button'
import StaticSources from '@config/sources'
import FormElement from '@components/form';
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  HeadingWithSubTitle,
  CheckListItem,
} from '@containers/section/StepSolution'
// import Slide from 'react-reveal/Slide'
import { useTranslation } from 'next-i18next'
import {
  // Typography,
  Grid,
  Container,
  Box,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  Card,
  CardHeader,
  CardContent,
  Typography,
} from '@material-ui/core';
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useToggle,
  ToggleProps,
  useCountDown,
  CountdownItemsProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useDebounce,
} from '@hooks/common';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStyles,
  useCalculatorStyles,
  useCalculatorTabStyles,
  useCalculatorPlanStyles,
  useCalculatorPlanHighlightsStyles,
  useCalculatorSelectionStyles,
  useCalculatorFormStyles,
} from './styles';

export interface CalculatorTabItemProps extends CheckListItem {
  highlights: any[];
  subTitle: string;
  icon: string;
  checkIcon: string;
}

export interface CalculatorTabProps extends CalculatorTabItemProps{
  name: string;
  active: boolean;
}

export interface CalculatorPlanProps extends CalculatorTabProps{
  // amount?: number;
  // TODO
}

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  openContactUs?: Function;
}
export interface CalculatorProps {
  children: JSX.Element;
  values: KeyPairProps;
  openContactUs?: Function;
}

export const CalculatorContext = createContext<Partial<ContextProps>>({});
export const useCalculator = () => useContext(CalculatorContext) as ContextProps;
export const CalculatorProvider: FC<CalculatorProps> = ({ openContactUs, values={}, children }) => {
  const { control, register, changeValue, setValue, watch, getValue, getValues, value }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  useEffect(() => {
    register("loanType");
  }, []);
  // const {
  //   value: info,
  //   changeValue,
  //   getValue,
  //   setValue,
  // }: KeyPairHandleProps = useKeyPairValue(values);
  return (
    <CalculatorContext.Provider value={{
      info: value,
      changeValue,
      getValue,
      setValue,
      onChange: changeValue,
      register,
      control,
      getValues,
      openContactUs,
      watch,
    }}>
      <form>
        {children}
      </form>
    </CalculatorContext.Provider>
  );
}

export const CalculatorTab: FC<CalculatorTabProps> = ({ active, name, value, label }) => {
  const classes = useCalculatorTabStyles();
  const {
    onChange,
  }: ContextProps = useCalculator();
  const onTabChanged = useCallback(() => {
    onChange(name, value);
  }, [onChange, name, value]);
  let addOn = {};
  let styles = classes.root;
  if (!active) {
    addOn = { onClick: onTabChanged };
  } else {
    styles = css(styles, classes.active, classes[value]);
  }
  return (
    <div className={styles} {...addOn}>
      {label}
    </div>
  );
}

export interface LoanTypeSelectionBoxProps {
  watch: Function;
  changeValue: Function;
  hideSelected?: boolean;
}

export const LoanTypeSelectionBox: FC<LoanTypeSelectionBoxProps> = ({ hideSelected, watch, changeValue }) => {
  const classes = useCalculatorSelectionStyles();
  const formClasses = useCalculatorFormStyles();
  const { loanType } = watch();
  const { t } = useTranslation('form');
  const types: CheckListItem[] = [
    {
      label: t('options.firstMortgage'),
      value: 'first',
      link: '',
      key: 'first',
    },
    {
      label: t('options.secondMortgage'),
      value: 'second',
      link: '',
      key: 'second',
    },
  ];
  return (
    <Grid container spacing={3}>
      <Grid item xs={12} sm={4}>
        <Typography component={'h4'} className={formClasses.label}>
          {t('label.loanType')}
        </Typography>
      </Grid>
      <Grid item xs={12} sm={8}>
        <List className={classes.root}>
          {
            map(types, ({ label, value }: CheckListItem) => {
              const active = value === loanType;
              let addOn = {};
              let textStyles = classes.text;
              if (!active) {
                if (hideSelected) {
                  return null;
                } else {
                  addOn = { onClick: () => changeValue('loanType', value) };
                  textStyles = css(textStyles, classes.normalText);
                }
              }
              return (
                <ListItem
                  key={value}
                  button
                  selected={active}
                  classes={{
                    root: classes.item,
                    selected: classes.selected
                  }}
                  {...addOn}
                >
                  <ListItemText primary={label} classes={{ primary: textStyles }} />
                  {
                    active && <ListItemIcon className={classes.icon}>
                      <LocalImage src={StaticSources.icon.calculator.checked} alt={label} />
                    </ListItemIcon>
                  }
                </ListItem>
              );
            })
          }
        </List>
      </Grid>
    </Grid>
  );
}

export const LoanTypeSelection: FC = () => {
  // const { t } = useTranslation('form');
  const {
    changeValue,
    watch,
  }: ContextProps = useCalculator();
  return (
    <LoanTypeSelectionBox watch={watch} changeValue={changeValue} />
  );
}

export const CalculatorPlan: FC<CalculatorPlanProps> = ({ checkIcon, highlights, icon, active, name, value, subTitle, label }) => {
  const classes = useCalculatorPlanStyles();
  const { amount } = useCalculate(value);
  const highlightsClasses = useCalculatorPlanHighlightsStyles({ checkIcon });
  const {
    onChange,
  }: ContextProps = useCalculator();
  const onTabChanged = useCallback(() => {
    onChange(name, value);
  }, [onChange, name, value]);
  let addOn = {};
  let styles = css(classes.root, classes[value]);
  if (!active) {
    addOn = { onClick: onTabChanged };
  } else {
    styles = css(styles, classes.active);
  }
  return (
    <Box display={'flex'}>
      <Box display={'flex'} className={classes.left}>
        <Card className={styles} {...addOn}>
          <CardHeader
            avatar={
              <div className={classes.icon}>
                <LocalImage src={icon} alt={label} />
              </div>
            }
            classes={{
              root: classes.header,
              title: classes.title,
              subheader: classes.subHeader,
            }}
            title={label}
            subheader={subTitle}
          />
          <CardContent className={classes.content}>
            <Typography variant="h3" className={classes.amount}>
              {
                !isUndefined(amount) ? `$${Numeral(amount).format('0,0')}` : '$0'
              }
              </Typography>
          </CardContent>
        </Card>
      </Box>
      <Box display={'flex'} className={classes.right}>
        {
          highlights.length && <ul className={highlightsClasses.ul}>
            {
              active && <Fade bottom>
                {
                  map(highlights, (h: string, index: number) => (
                    <li key={index} className={css(highlightsClasses.li, highlightsClasses[value])}>{h}</li>
                  ))
                }
              </Fade>
            }
          </ul>
        }
      </Box>
    </Box>
  );
}

export interface CalculatorFormProps {
  tabs: CalculatorTabItemProps[];
};

export const CalculatorForm: FC<CalculatorFormProps> = ({ tabs }) => {
  const classes = useCalculatorFormStyles();
  const { t } = useTranslation('form');
  const {
    control,
    watch,
    setValue,
  }: ContextProps = useCalculator();
  const { paymentMode, repaymentTerms, interestRate } = watch();
  return (
    <div className={classes.root}>
      <LoanTypeSelection />
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.loanAmount')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.PrefixNumericInput
            name="loanAmount"
            control={control}
            unit={'HKD'}
            inputProps={{
              min: 1,
              max: 9999999999,
            }}
          />
        </Grid>
      </Grid>
      <div className={classes.affordHeader}>
        <div className={classes.icon}>
          <LocalImage
            src={StaticSources.icon.calculator.building}
            alt={t('title.mortgageAffordability')}
          />
        </div>
        <Typography component={'h3'} className={classes.title}>
          {t('title.mortgageAffordability')}
        </Typography>
      </div>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.paymentMode')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.NativeSelect
            name="paymentMode"
            control={control}
            options={tabs}
            specialClass={paymentMode}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.interestRate')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.InputSlider
            name="interestRate"
            control={control}
            value={interestRate}
            setValue={setValue}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
              min: 1,
              max: 100,
            }}
            unit={t('label.percent')}
            specialClass={paymentMode}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.repaymentTerms')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
        <FormElement.InputSlider
            name="repaymentTerms"
            control={control}
            value={repaymentTerms}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
              min: 1,
              max: 100,
            }}
            max={100}
            min={1}
            setValue={setValue}
            unit={t('label.yearPercent')}
            specialClass={paymentMode}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export const CalculatorButtons: FC = () => {
  const {
    watch,
    openContactUs,
  }: ContextProps = useCalculator();
  const classes = useCalculatorStyles();
  const { t } = useTranslation('button');
  const { execute, currentAmount, loading } = useCalculate('interest');
  const { apply } = useAppliedCalculationInfo();
  const router = useRouter();
  const {
    loanAmount,
    paymentMode,
    repaymentTerms,
    loanType,
    interestRate,
  } = watch();
  const onSubmit = useCallback(() => {
    // apply the info and redirect to mortgage page
    apply({
      loanAmount: currentAmount,
      loanType,
      calculateType: paymentMode,
    });
    router.push(`${Routes.mortgage[loanType]}?r=form`);
  }, [
    router,
    loanType,
    currentAmount,
    paymentMode,
  ]);
  
  useEffect(() => {
    const amount = Number(loanAmount);
    const term = Number(repaymentTerms);
    const rate = Number(interestRate);
    let valid = amount > 0 && term > 0 && rate > 0;
    valid = valid && includes([
      'principal',
      'interest',
      'revolving',
    ], paymentMode);
    // console.log(amount, term, rate, valid, loading);
    // Make sure we have a value (user has entered something in input)
    if (valid && !loading) {
      // console.log('>');
      execute({
        loanAmount: amount,
        repaymentTerms: term,
        interestRate: rate,
      }, paymentMode);
    }
  }, [
    loanAmount,
    paymentMode,
    repaymentTerms,
    interestRate,
  ]);
  const valid: boolean = !!+interestRate && !!loanType && !!+loanAmount && !!paymentMode && !!+repaymentTerms
  return valid && (
    <Fade bottom>
      <Box display={'flex'} alignItems={'center'} justifyContent={'center'} className={classes.submit}>
        <CommonButton 
          label={t('applyNow')}
          className={classes.apply}
          onClick={(e: any) => onSubmit()}
        />
        <CommonButton
          label={t('contactUs')}
          className={classes.contact}
          onClick={(e: any) => openContactUs()}
        />
      </Box>
    </Fade>
  );
}

export const Calculator: FC = () => {
  const { t } = useTranslation('section');
  const {
    // info,
    watch,
  }: ContextProps = useCalculator();
  const classes = useCalculatorStyles();
  const { paymentMode } = watch();
  const { config } = useCalculatorConfig();
  const { inspector }: LocaleContextProps = useLocale();
  // console.log(inspector);
  const tabs: CalculatorTabItemProps[] = useMemo(() => [
    { 
      label: t('label.interestOnly'), 
      value: 'interest',
      link: '',
      key: 'interest',
      icon: StaticSources.icon.calculator.interestOnly,
      checkIcon: StaticSources.icon.calculator.interestOnlyCheck,
      subTitle: t('title.interestOnlySubTitle'),
      highlights: map(config?.interestOnly, (item: any) => 
        inspector({ target: item, field: 'label' })
      ),
      // highlights: [
      //   t('remarks.revolving1'),
      //   t('remarks.revolving2'),
      //   t('remarks.revolving3'),
      // ],
    },
    { 
      label: t('label.principalAndInterest'),
      value: 'principal',
      link: '',
      key: 'principal',
      subTitle: t('title.principalAndInterestSubTitle'),
      icon: StaticSources.icon.calculator.principalInterest,
      checkIcon: StaticSources.icon.calculator.principalInterestCheck,
      highlights: map(config?.principalAndInterest, (item: any) => 
        inspector({ target: item, field: 'label' })
      ),
      // highlights: [
      //   t('remarks.revolving1'),
      //   t('remarks.revolving2'),
      //   t('remarks.revolving3'),
      // ],
    },
    {
      label: t('label.revolvingLoan'),
      value: 'revolving',
      link: '',
      key: 'revolving',
      subTitle: t('title.revolvingLoanSubTitle'),
      icon: StaticSources.icon.calculator.revolvingLoan,
      checkIcon: StaticSources.icon.calculator.revolvingLoanCheck,
      highlights: map(config?.revolving, (item: any) => 
        inspector({ target: item, field: 'label' })
      ),
      // highlights: [
      //   t('remarks.revolving1'),
      //   t('remarks.revolving2'),
      //   t('remarks.revolving3'),
      // ],
    },
  ], [inspector, t, config]);
  return (
    <Box display={'flex'} className={classes.form}>
      <Box display={'flex'} justifyContent={'space-between'} className={classes.tab}>
        {
          map(tabs, (tab: CalculatorTabItemProps) => {
            const active = paymentMode === tab.value;
            return (
              <CalculatorTab
                key={tab.value}
                active={active}
                name={'paymentMode'}
                {...tab}
              />
            );
          })
        }
      </Box>
      <Box display={'flex'} className={classes.left}>
        <CalculatorForm tabs={tabs} />
      </Box>
      <Box display={'flex'} flexDirection={'column'} className={classes.right}>
        <Box display={'flex'} className={classes.summary} alignItems={'center'} justifyContent={'flex-start'}>
          <Typography component={'h2'} className={classes.summaryTitle}>
            {t('label.resultSummary')}
          </Typography>
          <div className={classes.summaryIcon}>
            <LocalImage src={StaticSources.icon.calculator.summary} alt={t('label.resultSummary')} />
          </div>
        </Box>
        {
          map(tabs, (tab: CalculatorTabItemProps) => {
            const active = paymentMode === tab.value;
            return (
              <CalculatorPlan
                key={tab.value}
                active={active}
                name={'paymentMode'}
                {...tab}
              />
            );
          })
        }
        <CalculatorButtons />
      </Box>
    </Box>
  );
}

export interface CardCalculatorProps {
  openContactUs?: Function;
}

export const CardCalculator: FC<CardCalculatorProps> = ({ openContactUs }) => {
  const classes = useCalculatorStyles();
  return (
    <CalculatorProvider values={{
      loanType: 'first',
      loanAmount: 0,
      paymentMode: 'interest',
    }} openContactUs={openContactUs}>
      <Card className={classes.root}>
        <CardContent className={classes.content}>
          <Calculator />
        </CardContent>
      </Card>
    </CalculatorProvider>
  );
}

export interface LoanCalculatorProps {
  popContact?: boolean;
  slide: Function;
}

export const LoanCalculator: FC<any> = forwardRef(({ popContact }, r) => {
  const { t } = useTranslation('section');
  const { enabled: show, toggle }: ToggleProps = useToggle(false);
  // when reach the timer (e.g. 30 seconds) popup contact us form
  const {
    left: timeLeft,
  }: CountdownItemsProps = useCountDown({
    total: ServiceConfig.awaitContactUsInterval,
    start: popContact,
  });
  useEffect(() => {
    if (popContact && timeLeft === 0) {
      toggle();
    }
  }, [timeLeft]);
  const classes = useStyles();
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <PopupContactUs enabled={show} onClose={toggle} />
        <Fade bottom>
          <div className={classes.heading}>
            <HeadingWithSubTitle
              title={t('title.loanCalculatorHeader')}
              subTitle={t('title.loanCalculatorSubTitle')}
            />
          </div>
          <CardCalculator openContactUs={toggle} />
        </Fade>
      </InnerPage>
    </div>
  );
});

export default LoanCalculator;
