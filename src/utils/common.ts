// import Cookies from 'universal-cookie';
// import Config from '@config/service';
import { get } from 'lodash';

class CommonHelper {
    pageSlide (refs: any, target: string) {
        const ref = get(refs, target, {});
        if (ref.current) {
            ref.current.scrollIntoView({
                behavior: 'smooth',
                block: 'start'
            });
        }
    }
}

export default new CommonHelper();