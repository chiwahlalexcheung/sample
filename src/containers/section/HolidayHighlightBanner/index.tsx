import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Zoom from 'react-reveal/Zoom'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Card,
  CardContent,
  // CardContent,
} from '@material-ui/core';
import {
  useStyles,
  useBoxStyles,
  useSlideStyles
} from './styles';
import {
  useStepper,
  StepHandleProps,
} from '@hooks/common';

import {
    BannerListItem,
} from '@interfaces/banner'

import {
    MainSlideProps,
} from '@containers/banner/MainSlider'

  
export interface HighlightSlideProps extends MainSlideProps {
    active?: boolean;
    zIndex: number;
}
export interface HighlightSliderListItem extends BannerListItem {
    icon: string;
}

export interface HighlightSliderProps {
  list: HighlightSliderListItem[];
};

export const PersonaSlide: FC<HighlightSlideProps> = ({ item, zIndex, onChanged, offset, active }) => {
    const classes = useSlideStyles({ zIndex });
    const { t } = useTranslation('button');
    const { icon, title, subTitle }: any = item;
    let styles = classes.root;
    let titleStyles = classes.title;
    let subTitleStyles = classes.subTitle;
    if (active) {
      styles = css(styles, classes.active);
    //   subTitleStyles = css(subTitleStyles, classes.activeSubTitle);
    //   titleStyles = css(titleStyles, classes.activeTitle);
    } else {
      styles = css(styles, classes.inActive);
    }
    return (

        <Card className={styles}  onClick={() => onChanged(offset)}>
            <CardContent className={classes.cardContent}>
                <Box
                    className={classes.image}
                    display={'flex'}
                    alignItems={'center'}
                    justifyContent={'center'}
                >
                    <LocalImage src={icon}  />
                </Box>
                <Box>
                    <Typography className={titleStyles} >{title}</Typography>
                    <Typography className={subTitleStyles} >{subTitle}</Typography>
                </Box>
            </CardContent>
        </Card>
    )
  };

  
export const HighlightSlider: FC<HighlightSliderProps> = ({ list }) => {
    const classes = useStyles();
    const {
      setStep,
      step,
    }: StepHandleProps = useStepper({ maxStep: list.length, step: 0 });
    const onChanged = useCallback((index: number) => {
      setStep(index);
  }, [setStep]);
    const targetList: HighlightSliderListItem[] = list.slice(0, 3);
    const totalLength: number = targetList.length;
    return (
      <Box display={'flex'} alignItems={'center'} justifyContent={'flex-start'}>
        {
          map(targetList, (l: HighlightSliderListItem, offset: number) => (
            <PersonaSlide
              zIndex={totalLength - offset}
              onChanged={onChanged}
              item={l}
              key={offset}
              offset={offset}
              active={step === offset}
            />
          ))
        }
      </Box>
    );
  }


export const HolidayHighlightBanner: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const classes = useStyles();
  const boxClasses = useBoxStyles();
  
  const bannerLists: HighlightSliderListItem[] = [
    {
      icon: StaticSources.icon.calendar,
      title: 'Self-select',
      subTitle: 'payment Holiday Period',
      label: '',
      thumbnail: ''
    },
    {
      icon: StaticSources.icon.zero,
      title: '$0',
      subTitle: 'Handling Fee',
      label: '',
      thumbnail: ''
    },
    {
      icon: StaticSources.icon.timeCount,
      title: 'Any 2 months',
      subTitle: 'in a year to postpone your payment',
      label: '',
      thumbnail: ''
    }
];
  
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
          <Box display={'flex'} justifyContent={'center'} >
            <Typography className={boxClasses.title} >{t('title.applyPaymentHolidayTitle')}</Typography>
          </Box> 
          <Box display={'flex'}  alignItems={'center'} justifyContent={'center'} >
            <HighlightSlider list={bannerLists} />
          </Box> 
      </InnerPage>
    </div>
  );
});

export default HolidayHighlightBanner;
