import { ServiceAction, ServiceActionTypes } from './actions'
import { BasicListStateProps, BasicInfoStateProps } from '@interfaces/state'
import { get } from 'lodash'
export interface ServiceState {
    readonly uploadDocument: BasicListStateProps
    readonly csContact: BasicListStateProps
    readonly generalContact: BasicInfoStateProps
}

const initialServiceState = {
    uploadDocument: {
        list: [],
        loading: false,
        error: null,
    },
    csContact: {
        list: [],
        loading: false,
        error: null,
    },
    generalContact: {
        info: {},
        loading: false,
        error: null
    }
}

/**
 * Service Reducer
 */
export const serviceReducer = (
  state: ServiceState = initialServiceState,
  action: any
): ServiceState => {
  const { uploadDocument, csContact, generalContact } = state;
  switch (action.type) {
    case ServiceActionTypes.RESET_DOCUMENT:
        return {
        ...state,
        uploadDocument: {
            ...initialServiceState.uploadDocument
        }
    };

    case ServiceActionTypes.UPLOAD_DOCUMENT:
      return {
        ...state,
        uploadDocument: {
            loading: true,
            list:{
                ...uploadDocument.list,
            }
        },
    }
    case ServiceActionTypes.UPLOAD_DOCUMENT_SUCCEED:
      return {
        ...state,
        uploadDocument: {
          ...uploadDocument,
          loading: false,
          list:{
              ...uploadDocument.list,
              [action.index]: action.payload,
          }
        },
      }
    case ServiceActionTypes.UPLOAD_DOCUMENT_FAILED:
      return {
        ...state,
        uploadDocument: {
          ...uploadDocument,
          loading: false,
          error: action.payload,
        }
      }
    case ServiceActionTypes.FETCH_CONTACT:
        return {
          ...state,
          csContact: {
              loading: true,
              list:[]
          },
    }
    case ServiceActionTypes.FETCH_CONTACT_SUCCEED:
        return {
            ...state,
            csContact: {
                ...csContact,
                loading: false,
                list:action.payload,
            },
        }
    case ServiceActionTypes.FETCH_CONTACT_FAILED:
        return {
            ...state,
            csContact: {
                ...csContact,
                loading: false,
                error: action.payload,
            }
        }
    case ServiceActionTypes.FETCH_GENERAL_CONTACT:
        return {
            ...state,
            generalContact: {
                loading: true,
                info:{}
            },
    }
    case ServiceActionTypes.FETCH_GENERAL_CONTACT_SUCCEED:
        return {
            ...state,
            generalContact: {
                ...generalContact,
                loading: false,
                info: get(action.payload, '[0]', {}),
            },
        }
    case ServiceActionTypes.FETCH_GENERAL_CONTACT_FAILED:
        return {
            ...state,
            generalContact: {
                ...generalContact,
                loading: false,
                error: action.payload,
            }
        }
    default:
      return state
  }
}
