import React, { FC, useCallback } from 'react'
import { Controller } from "react-hook-form";
import { map } from 'lodash'
import css from 'clsx'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'
// import {
//   Select,
// } from '@material-ui/core'
import Select from "react-select";
import {
  FormRenderElementProps,
  FormInputProps,
} from '@interfaces/form'
import {
  LabelValueProps,
} from '@interfaces/list'
import {
  useStyles,
} from './styles'
import {
  useStyles as useInputStyles,
} from './../text/styles'

import {
	FormRenderWithControlElementProps,
	FormNumberProps,
  } from '@interfaces/form'

export interface SearchSelectProps extends FormRenderElementProps{
  // backgroundColor?: string;
  name: string;
  options: LabelValueProps[];
  setValue: Function;
  value: any;
  onChange?: Function;
  register: Function;
  disable?: Boolean
}

export interface SearchSelectOptionProps {
  onChange: Function;
  options: LabelValueProps[];
  value: any;
  inputProps?: FormInputProps;
};

const SearchSelectOption: FC<any> = ({ 
	inputProps, 
	onChange, 
	options, 
	value, 
	name,
	setValue,
    disable
}) => {
  const classes = useStyles();
  const customStyles = {
	control: (base) => ({
	  ...base,
	  boxShadow: "none",
	  borderRadius: 0,
	  border: 0,
	}),
	valueContainer: (provided, state) => ({
		...provided,
		boxShadow: "none",
		border: 0,
	}),
	}
  return (
	<Select
        isDisabled={disable}
        value={options.filter(option => option.value === value )}
		styles={customStyles}
		onChange={(r: any) => onChange(r)}
		components={{
			IndicatorSeparator: () => null
		}}
		options={options}
	/>
  );
}

const SearchSelector: FC<any> = ({
  control,
  name,
  setValue,
  value,
  render,
  register,
  options=[],
  inputProps,
  disable=false
}) => {
	const classes = useStyles();
	const inputClasses = useInputStyles();

	const handleChange = ({value}) => {
        setValue(name, value);
    }

	let addOn = {
		render: ({ onChange }: any) => (
		<SearchSelectOption 
			onChange={(r: any) => handleChange(r)}
			value={value}
			inputProps={{
				...inputProps,
				name,
			}}
			options={options}
            register={register}
            disable={disable}
		/>
		),
	};
	if (!!render) {
		addOn = { render: (payload: any) => render(payload) };
	}
	return (
		<div >
			<div className={classes.inputRoot}>
				<Controller
					name={name}
					control={control}
					{...addOn}
				/>
			</div>
		</div>
	);
}

export default SearchSelector;