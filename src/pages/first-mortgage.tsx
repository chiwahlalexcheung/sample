import { useRef, useEffect, useCallback } from 'react';
import Head from 'next/head'
import { get } from 'lodash'
import { GetServerSideProps } from 'next'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { storeWrapper } from '@store/store'
import { fetchGeneralContact } from '@store/service/actions'
import FullStepSolution from '@containers/section/FullStepSolution'
import {
    BannerBoxInfoProps,
    MortgageBanner
} from '@containers/section/MortgageBanner'
import StaticSource from '@config/sources'
import WhyChooseUs from '@containers/section/WhyChooseUs'
import ContactUs from '@containers/section/ContactUs'

const FirstMortgagePage: React.FC<any> = ({ initialRef, ...props }) => {
  const { t } = useTranslation('common');
  const formRef = useRef<any>();
  const learnRef = useRef<any>();
  const refs = {
    form: formRef,
    learn: learnRef,
  };
  // TODO: use Context Provider to do so? all ref slide pull to the Context Props
  const slide = useCallback((target: string) => {
    const ref = get(refs, target, {});
    if (ref.current) {
      ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }, []);
  useEffect(() => {
    if (initialRef) {
      slide(initialRef);
    }
  }, []);
  const info: BannerBoxInfoProps = {
    title: 'First\n Mortgage Loan',
    description: 'Sun Hung Kai Credit provides refinancing cash out on paid up property, transfer mortgage as well as high loan to value ratio property purchase mortgage. There is no limit on the loan amount and can loan up to 75% of the property value. Application process is easy and simple to help satisfying your different financial needs.',
    applyLabel: 'Apply Now',
    applyLink: '/',
    contactLabel: 'Contact Now',
    contactLink: '/',
    label: 'Payment Holiday Highlights',
    labelLink: '/',
    banner: StaticSource.banner.firstMortgageBanner,
  };
  return (
    <HeadingLayout>
      <Head>
          <title>{t('menu.first-mortgage')}</title>
      </Head>
      <MortgageBanner info={info}/>
      <FullStepSolution slide={slide} initialType={props.initialType || 'first'} ref={formRef} />
      <WhyChooseUs ref={learnRef}  />
      <ContactUs />
    </HeadingLayout>
  );
}


export const getServerSideProps: GetServerSideProps = storeWrapper.getServerSideProps(
  async ({ locale, store, query={} }: any) => {
    const isServer = true;
    await store.dispatch(fetchGeneralContact(isServer));
    return {
      props: {
        initialRef: query.r || '',
        initialType: query.t || '',
        ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
      }
    };
  }
)
// export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
//   props: {
//     ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
//   }
// })
export default FirstMortgagePage
