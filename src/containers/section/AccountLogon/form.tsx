import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useSelector, useDispatch } from "react-redux";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";

import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useLoginStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import apiManager from "@lib/apiManager";

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface LogonProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const LogonContext = createContext<Partial<ContextProps>>({});
export const useLogon = () => useContext(LogonContext) as ContextProps;
export const LogonProvider: FC</*LogonProps*/ any> = ({
  values = {},
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  return (
    <LogonContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </LogonContext.Provider>
  );
};

export interface LogonItemProps {
  key: string;
  component: JSX.Element;
}
export interface LogonBarProps {
  list: LogonItemProps[];
}

export const OTP: FC = () => {
  const classes = useLoginStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const [mountCount, setMountCount] = useState<number>(0);
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useLogon();
  const {
    //   register,
    //   control,
    // watch,
    step,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* start here */
  const router = useRouter();

  console.log(watch());
  const { mobile, hkid, sms } = watch();
  const { sendCode, verifyCode } = useOTP();
  const onSendOtp = useCallback(() => {
    var reCaptchaToken = watch();
    sendCode({
      mobileNo: mobile,
      hkId: hkid,
      reCaptchaToken,
    });
  }, [sendCode, mobile, hkid]);

  const verify = useCallback(async () => {
    const v = watch();
    console.log(v);
    const { mobile: mobileNo, hkid: hkId, sms } = watch();
    console.log(mobile, hkid, sms.join(""));
    var result: any = await verifyCode({
      mobileNo,
      hkId,
      sms,
    });
    console.log(result);
    if (result && result.success) {
      APIManager.setToken("Bearer " + result.accessToken);
      router.push(RouteSources.accountProfile, RouteSources.accountProfile);
    } else {
      alert("Verification failed!");
    }
  }, [verifyCode, mobile, hkid]);
  const onClickVerify = () => {
    let number = sms.join("");
    if (!/^\d{6}$/.test(number)) {
      console.log("SMS: " + sms);
      alert("Please input the OTP!.");
      return;
    }
    verify();
  };
  /* end here */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton className={classes.TopButton} label={t("label.home")} />
        </Link>
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.enterOTP")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          {/* <Grid container spacing={3} >
                        <Grid item xs={12} sm={3}>
                            <Typography component={'h4'}>
                                {ft('note.willSendOTP')}
                            </Typography>
                        </Grid>
                    </Grid> */}
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <Typography component={"h4"} className={classes.otpTitle}>
                {ft("title.smsOtp")}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <Typography component={"h4"} className={classes.otpSubtitle}>
                {ft("note.willSendOTP")}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            spacing={3}
            justify="center"
            alignItems="center"
            className={classes.otpSMSBlock}
          >
            <Grid item xs={12} sm={12}>
              <SMSBlock
                control={control}
                register={register}
                flexDirection={"column"}
                onSend={onSendOtp}
                pageType="logon"
              />
            </Grid>
          </Grid>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.otpButton}
              label={bt("back")}
              onClick={goPrevStep}
            />
            <CommonButton
              className={classes.otpButton}
              label={bt("verifyAndNext")}
              onClick={onClickVerify}
            />
          </Box>
        </Box>
        <Typography className={classes.remark}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const AccountInformation = () => {
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
    getValues,
  }: ContextProps = useLogon();
  const { goNextStep }: StepSolutionContext = useStepSolution();
  const classes = useLoginStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const [token, setToken] = useState("");
  /* P145 the onChange here may override the onChange in the TextInput */
  const post = (url, data) => {
    const payload = {
      dispatch: () => {},
      url: url,
      data: data,
      underAuth: null,
      skipError: null,
      additional: null,
      params: null,
      isServer: null,
      noForceAuth: null,
      method: null,
      errorKey: null,
      noFormData: null,
    };
    return apiManager.post(payload);
  };

  const onClickLogin = async () => {
    const { hkid, mobile } = watch();
    if (!/^[A-Z]\d{6}[A-Z0-9]$/.test(hkid)) {
      alert("Invalid HKID NO.!");
      return;
    }
    if (!/^\d{8}$/.test(mobile)) {
      alert("Invalid Mobile No.!");
      return;
    }
    var result: any = await post("verifyAccount", {
      id_no: hkid,
      mobile: mobile,
    });
    if (result["data"]["success"] === true) {
      goNextStep();
    } else {
      alert("Account does not exist!");
    }
  };
  /*****/

  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton className={classes.TopButton} label={t("label.home")} />
        </Link>
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.enterAccountInfo")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={4}>
              <Typography
                component={"h4"}
                className={css(classes.label, classes.required)}
              >
                {ft("label.hkidNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
              <FormElement.TextInput
                control={control}
                name="hkid"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>

          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={4}>
              <Typography
                component={"h4"}
                className={css(classes.label, classes.required)}
              >
                {ft("label.mobileNo")}
                {/* <span className={classes.require}>*</span> */}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
              <FormElement.TextInput
                control={control}
                name="mobile"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={4}>
              <Typography component={"h4"} className={classes.label}>
                {ft("label.enterCaptcha")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={8}>
              <ReCaptchaBlock
                token={token}
                page={"valuation_form"}
                onToken={(token) => {
                  setValue("reCaptchaToken", token);
                }}
              />
              <FormElement.TextInput
                control={control}
                name="reCaptchaToken"
                type="hidden"
              />
            </Grid>
          </Grid>

          <CommonButton
            className={classes.button}
            icon={StaticSources.icon.logon}
            label={ft("label.login")}
            onClick={onClickLogin}
          />
        </Box>
        <Typography className={classes.remark}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const LogonContent: FC<LogonBarProps> = ({ list }) => {
  const { step }: StepSolutionContext = useStepSolution();
  // console.log("step", step);
  return (
    <div>
      {map(list, ({ key, ...l }: LogonItemProps, index: number) => {
        let styles = { display: "none" };
        if (index === step) {
          styles = { display: "block" };
        }
        return (
          <div key={key} style={styles}>
            {l.component}
          </div>
        );
      })}
    </div>
  );
};

export const LoginForm: FC = () => {
  const { t } = useTranslation("section");
  const classes = useLoginStyles();

  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);

  const steps: LogonItemProps[] = [
    {
      key: "accInfo",
      component: <AccountInformation />,
    },
    {
      key: "otp",
      component: <OTP />,
    },
  ];

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <LogonProvider
        values={{
          hkid: "",
          mobile: "",
        }}
      >
        <StepSolution maxStep={steps.length} onSubmit={onSubmit}>
          <LogonContent list={steps} />
        </StepSolution>
        {/* <OTP />
                <AccountInformation /> */}
      </LogonProvider>
    </GoogleReCaptchaProvider>
  );
};
