import React, { FC, forwardRef, ReactNode, useCallback, useMemo, useState, useContext, createContext } from 'react'
import css from 'clsx';
import dynamic from 'next/dynamic'
import Pulse from 'react-reveal/Pulse'
// import Slider from 'react-slick';
import {
  useStyles
} from './styles';
import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
import { useTranslation } from 'next-i18next'
import LazyImage from '@components/image/LazyImage';
import StaticSources from '@config/sources';
import {
  Box,
  Typography,
//   Fade,
  Container,
} from '@material-ui/core'
import Fade from 'react-reveal/Fade'

export interface TitleBannerProps {
    banner: string;
    title: string;
}
const TitleBanner: FC<TitleBannerProps> = ({banner, title}) => {
    const { t } = useTranslation('section');
    const classes = useStyles();
    const bgStyle = {
        backgroundImage: `url(${banner})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover'
    };
    return (
        <div className={classes.root} style={bgStyle}>
            <Box display={'flex'} height={1} alignItems={'center'}>    
                <Fade left>
                    <HeadingWithSubTitle
                        subTitle={title}
                    />
                </Fade>
            </Box>
        </div>
    );
}
export default TitleBanner;
