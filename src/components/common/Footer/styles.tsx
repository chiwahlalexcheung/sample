import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '35px 0',
    backgroundColor: theme.palette.primary.light,
  },
  menu: {
    marginBottom: '25px',
  },
  fitMenu: {
    '@global': {
      '> a': {
        marginRight: '42px',
      }
    }
  },
  copyright: {
    borderBottom: `2px solid white`,
    paddingBottom: '10px',
    color: 'white',
  },
  warning: {
    fontSize: '30px',
    fontWeight: 600,
    color: 'white',
    margin: '25px 0 15px 0',
  },
  hotLine: {
    fontSize: '18px',
    color: 'white',
  },
}));

const useMenuStyles = makeStyles(theme => ({
  root: {
    flexWrap: 'wrap',
  },
  item: {
    color: 'white',
    fontSize: '12px',
  },
}));

export {
  useStyles,
  useMenuStyles,
};
