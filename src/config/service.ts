const env: string = 'localhost';
let config = {
  googleReCaptcha: {
    siteKey: '6LfXxH8aAAAAAEPTNMwuzE0-YIl9KyzMh4Kh_zvu',
  },
  smsLive: true,
  awaitContactUsInterval: 30, // seconds for popup contact us when enter loan calculator page
  currency: 'HK$',
  debug: true,
  cookies: {
    localToken: '_LOGINTOKEN_',
    language: 'next-i18next',
  },
  locale: 'en',
  domain: 'https://shkc.4d.com.hk',
  apis: {
    'rules': '/api/v1/product-finder',
    'applyRule': '/api/v1/product-rule',
    'banners': '/api/v1/site/banners',
    'createApplication': '/api/v1/los/createApplication',
    'sendOtp': '/api/v1/user/sendOTP',
    'verifyOtp': '/api/v1/user/signin',
    'uploadDocument': 'api/v1/los/uploadDocument',
    'verifyAccount': '/api/v1/user/verify',
    'userInfo': '/api/v1/user/info',
    'custAppLoan': '/api/v1/los/custApplLoanList',
    'signOut': '/api/v1/user/signout',
    'perona': '/api/v1/site/personas',
    'calculatorConfig': '/api/v1/loan-cal/config',
    'calculate': '/api/v1/loan-cal/:paymentType',
    'faq': 'api/v1/site/faqs',
    'statementDate': "/api/v1/los/internetStmDate?acct_no=:acct_no",
    "statementFile": "/api/v1/los/enquiryStatement?loan_no=:loan_no&stm_date=:stm_date",
    "internetInformationUpload": "/api/v1/los/internetInfoUpd",
    "paymentHoliday": "/api/v1/los/paymHoliday",
    'cscenterContacts': '/api/v1/site/contacts/cscenter',
    "increaseCreditLimit":"/api/v1/los/increaseCreditLimit",
    "fundTransfer": "/api/v1/los/internetWithdrawal",
    'generalContacts': '/api/v1/site/contacts/gen',
    'evaluation': {
        'area': '/api/v1/evaluation/areas',
        'district': '/api/v1/evaluation/districts',
        'buildings': '/api/v1/evaluation/buildings',
        'estate': '/api/v1/evaluation/estates',
        'streetName': '/api/v1/evaluation/streets',
        'streetNo': '/api/v1/evaluation/streetno',
        'block': '/api/v1/evaluation/blocks',
        'floor': '/api/v1/evaluation/floors',
        'flatRoom': '/api/v1/evaluation/rooms',
        'carpark': '/api/v1/evaluation/carparks'
    }
  },
  cookieOptions: {

  },
  momentLocalization: {
    hk: 'zh-hk',
    cn: 'zh-cn',
    en: 'en-gb',
  },
  socialLanguageCode: {
    hk: 'zh_tw',
    cn: 'zh_cn',
    en: 'en',
  },
  timezone: {
    hk: 'Asia/Hong_Kong',
    en: 'Asia/Hong_Kong'
  },
  timeFormat: {
    hk: {
      date: 'YYYY-MM-DD',
      day: 'LL',
      time: 'LTS',
    },
    cn: {
      date: 'YYYY-MM-DD',
      day: 'LL',
      time: 'LTS',
    },
    en: {
      date: 'YYYY-MM-DD',
      day: 'LL',
      time: 'LTS',
    },
  },
  uploadFile: {
    maximumSize: 20, //Mb
    format: [] 
  },
};

if (env === 'development') {
  config = {
    ...config,
    domain: 'https://shkc.4d.com.hk',
    debug: true,
  }
} else if (env === 'uat') {
  config = {
    ...config,
    domain: 'https://shkc.4d.com.hk',
    debug: true,
  }
} else if (env === 'production') {
  config = {
    ...config,
    domain: 'http://shkcapi.4d.com.hk',
    debug: false,
  }
} 

export default config;
