import { useState, useRef, useCallback, useEffect, useLayoutEffect } from 'react';
import { get } from 'lodash';
import { useForm } from "react-hook-form"
// const useIsomorphicLayoutEffect =
//   typeof window !== 'undefined' ? useLayoutEffect : useEffect

export interface CountdownItemProps {
  total: number;
  intervalTime?: number;
  start: boolean;
};
export interface CountdownItemsProps {
  left: number;
  counting?: boolean;
  setTimeLeft: Function;
  onStart: Function;
  startCounting: boolean;
};
export const useCountDown = ({
  total,
  intervalTime=1,
  start=false,
}: CountdownItemProps): CountdownItemsProps => {
  const [timeLeft, setTimeLeft] = useState<number>(total);
  // const { enabled, toSwitch }: ToggleProps = useToggle(false);
  const { enabled: startCounting, toSwitch: onStart }: ToggleProps = useToggle(start);
  useEffect(() => {
    const interval = setInterval(() => {    
      if (startCounting) {
        setTimeLeft((current) => {
          // toSwitch(true);
          if (current <= 0) {
            clearInterval(interval);
            // toSwitch(false);
            return 0;
          }
          return current - intervalTime;
        });
      }
    }, intervalTime * 1000);
    return () => {
      clearInterval(interval);
      // toSwitch(false);
    }
  }, [intervalTime, timeLeft, startCounting]);
  // useEffect(() => {
  //   setTimeLeft();
  // }, [total]);
  return {
    left: timeLeft || 0,
    counting: timeLeft > 0 && startCounting,
    setTimeLeft,
    startCounting,
    onStart,
  };
}

export interface ToggleProps {
  enabled: boolean;
  toggle: Function;
  setOn: Function;
  setOff: Function;
  toSwitch: Function;
};
// control boolean toggle behevior
const useToggle = (props: boolean=false): ToggleProps => {
  const [enabled, onToggle] = useState<boolean>(props);
  const setOn = (): void => onToggle(true);
  const setOff = (): void => onToggle(false);
  const toggle = (): void => onToggle(!enabled);
  const toSwitch = (status: boolean=false): void => onToggle(status);
  return {
    enabled: enabled || false,
    toggle,
    setOn,
    setOff,
    toSwitch,
  };
};

// hook for the target prop previous state changes
const usePrevious = (value: any): any => {
  const ref = useRef();
  // Store current value in ref
  useEffect(() => {
    ref.current = value;
  }, [value]); // Only re-run if value changes
  
  // Return previous value (happens before update in useEffect above)
  return ref.current;
}

// hook for useEffect does not call onComponentWillMount
const useAfterEffect = ((fn: Function, deps: any[]): void => {
  const willMount = useRef(false);
  const useIsomorphicLayoutEffect = typeof window !== 'undefined' ? useLayoutEffect : useEffect;
  useIsomorphicLayoutEffect(() => {
    if (willMount.current) {
      fn();
    }
    willMount.current = true;
  }, deps);
});


export interface StepHandleProps {
  step: number;
  maxStep: number;
  setStep: Function;
  goStep: Function;
  goFirstStep: Function;
  goNextStep: Function;
  goPrevStep: Function;
  goLastStep: Function;
  isLastStep: boolean;
  stepValue: number;
};
const useStepper = ({
  step=0,
  maxStep=1,
}: {
  step: number;
  maxStep: number;
}): StepHandleProps => {
  const [currentStep, setStep] = useState<number>(step);
  const goNextStep = useCallback((): void => {
    let next = currentStep + 1;
    if (next > maxStep - 1) {
      next = maxStep - 1;
    }
    setStep(next);
  }, [maxStep, setStep, currentStep]);
  const goPrevStep = useCallback(() => {
    let prev = currentStep - 1;
    if (prev < 0) {
      prev = 0;
    }
    setStep(prev);
  }, [maxStep, setStep, currentStep]);
  const goFirstStep = useCallback(() => {
    setStep(0);
  }, [setStep]);
  const goLastStep = useCallback(() => {
    setStep(maxStep - 1);
  }, [maxStep, setStep]);
  const goStep = useCallback((step: number) => {
    if (step >= 0 && step <= maxStep - 1) {
      setStep(step);
    }
  }, [maxStep, setStep]);
  // useIsomorphicLayoutEffect(() => {
  //   setStep(step);
  // }, [step]);
  // console.log(currentStep, maxStep - 1);
  return {
    setStep,
    step: currentStep,
    maxStep,
    goStep,
    goFirstStep,
    isLastStep: currentStep === maxStep - 1,
    stepValue: Math.round((currentStep + 1) / maxStep * 100),
    goNextStep,
    goPrevStep,
    goLastStep,
  };
};

export interface KeyPairProps { 
  [name: string]: any
};
export interface KeyPairHandleProps {
  setValue: Function;
  changeValue: Function;
  getValue: Function;
  value: KeyPairProps;
};
const useKeyPairValue = (v: KeyPairProps={}): KeyPairHandleProps => {
  const [value, setValue] = useState<KeyPairProps>(v);
  const getValue = useCallback((key: string): any => {
    return get(value, key);
  }, [value]);
  const changeValue = useCallback((payload: KeyPairProps) => {
    setValue({
      ...value,
      ...payload,
    });
  }, [value, setValue]);
  return {
    changeValue,
    value,
    setValue,
    getValue,
  };
};

export interface KeyPairFormHandleProps extends KeyPairHandleProps {
  register: any;
  control: any;
  handleSubmit: Function;
  getValues: Function;
  watch: Function;
  reset: Function;
};
const useKeyPairFormValue = (defaultValues: KeyPairProps={}): KeyPairFormHandleProps => {
  const { control, handleSubmit, reset, register, setValue, getValues, watch } = useForm({
    defaultValues,
  });
  const getValue = useCallback((key: string): any => {
    const value = getValues();
    return get(value, key);
  }, [getValues]);
  const changeValue = useCallback((name: string, value: any) => {
    setValue(name, value);
  }, [setValue]);
  return {
    changeValue,
    value: getValues(),
    getValues,
    setValue,
    getValue,
    reset,
    handleSubmit,
    register,
    control,
    watch,
  };
};

const useDebounce = (value: any, delay: number=500) => {
  // State and setters for debounced value
  const [debouncedValue, setDebouncedValue] = useState(value);
  useEffect(() => {
    // Update debounced value after delay
    const handler = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);
    // Cancel the timeout if value changes (also on delay change or unmount)
    // This is how we prevent debounced value from updating if value is changed ...
    // .. within the delay period. Timeout gets cleared and restarted.
    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return debouncedValue;
}
// const useDebounce = (
//   handler: Function,
//   watchedValue: any, 
//   delay: number = 500,
// ) => {
//   useEffect(() => {
//     const timeoutHandler = setTimeout(() => {
//       handler();
//     }, delay);
//     return () => {
//       clearTimeout(timeoutHandler);
//     };
//   }, [watchedValue, delay]);
// }

export {
  useAfterEffect,
  useToggle,
  usePrevious,
  useStepper,
  useKeyPairValue,
  useKeyPairFormValue,
  useDebounce,
};
