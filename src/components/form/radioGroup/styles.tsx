import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  
}));

const useCheckboxStyles = makeStyles(theme => ({
  checkbox: {
    borderRadius: '3px',
    width: '22px',
    height: '22px',
    padding: 0,
    background: '#fff'
  },
  icon: {
    width: '20px',
    height: '20px',
    padding: 0,
    margin: 'auto',
  }
}));
export {
  useStyles,
  useCheckboxStyles,
};
