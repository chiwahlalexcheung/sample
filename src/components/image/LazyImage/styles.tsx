import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  image: {
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
    height: '100%',
  },
  skeleton: {
    backgroundColor: theme.palette.text.disabled,
    position: 'absolute',
    top: 0,
  },
  error: {
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  postImage: {
    objectFit: (props: any) => props.fitImage ? 'cover' : 'contain',
    width: (props: any) => props.fitImage ? '100%' : 'inherit',
  },
  errorImage: {
    width: '60%',
  },
  process: {
    display: 'none',
  },
}));

export {
  useStyles,
};
