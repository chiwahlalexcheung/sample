import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useSelector, useDispatch } from "react-redux";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";

import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useCreditStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import apiManager from "@lib/apiManager";
import {
  getLoanList,
  formatMoney,
  apiPost,
  signOut,
  goProfile,
} from "@containers/section/AccountCommon/context";
import { classNames } from "react-select/src/utils";

export interface ContextProps extends KeyPairFormHandleProps {
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface CreditProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const CreditContext = createContext<Partial<ContextProps>>({});
export const useCredit = () => useContext(CreditContext) as ContextProps;
export const CreditProvider: FC<any> = ({ values = {}, children }) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  //   useEffect(() => {
  //     register("loanType");
  //   }, []);
  //   const {
  //     value: info,
  //     changeValue,
  //     getValue,
  //     setValue,
  //   }: KeyPairHandleProps = useKeyPairValue(values);
  return (
    <CreditContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </CreditContext.Provider>
  );
};

export interface CreditItemProps {
  key: string;
  component: JSX.Element;
}
export interface CreditBarProps {
  list: CreditItemProps[];
}

export const CreditRequest = ({
  applicationNo,
  limit,
  setLimit,
  balance,
  setBalance,
}) => {
  const classes = useCreditStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useCredit();
  const {
    // register,
    // control,
    // watch,
    step,
    goNextStep,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
      my code start here 
    */
  const router = useRouter();
  const lowerBound = 1000;
  const upperBound = 10000000;

  const getBalance = async () => {
    const loanList = await getLoanList();
    loanList.forEach((l) => {
      if (l["acct_no"] === applicationNo) {
        setLimit(formatMoney(l["avl_cr_limit"]));
        setBalance(formatMoney(l["loan_ledg_bal"]));
      }
    });
  };
  useEffect(() => {
    getBalance();
  }, []);
  const onClickNext = () => {
    const increase = watch()["increase"];
    if (increase && !isNaN(increase)) {
      if (increase < lowerBound) {
        alert(ft("increaseLimitTooLow") + formatMoney(upperBound));
      } else if (increase > upperBound) {
        alert(ft("increaseLimitTooHigh") + formatMoney(upperBound));
      } else {
        goNextStep();
      }
    } else {
      alert(ft("message.inputValidCreditLimit"));
    }
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.enterRequestedCredit")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.existingCreditLimit")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {limit}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.availableBalance")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {balance}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelBlack, classes.required)}
              >
                {ft("label.requestedCreditLimitIncrease")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormElement.TextInput
                control={control}
                name="increase"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>

          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("back")}
              onClick={() => goProfile(router, applicationNo)}
            />

            <CommonButton
              className={classes.bottomButtonOrange}
              label={bt("next")}
              onClick={onClickNext}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const CreditReview = ({ applicationNo, limit, balance }) => {
  const classes = useCreditStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useCredit();
  const {
    // register,
    // control,
    // watch,
    step,
    goNextStep,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
      my code start here 
    */
  const router = useRouter();

  const onClickReset = () => {
    goPrevStep();
  };

  const onClickConfirm = async () => {
    const data = {
      acct_no: applicationNo,
      req_limit: watch()["increase"],
    };
    const result = await apiPost("increaseCreditLimit", data, {});
    if (result["data"]["LOS"]["BODY"]["rtn_cde"] === "000") {
      goNextStep();
    }
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.reviewConfirmRequestedCredit")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.existingCreditLimit")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {limit}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.availableBalance")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {balance}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelBlack, classes.required)}
              >
                {ft("label.requestedCreditLimitIncrease")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {formatMoney(watch()["increase"])}
              </Typography>
            </Grid>
          </Grid>

          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("reset")}
              onClick={onClickReset}
            />

            <CommonButton
              className={classes.bottomButtonOrange}
              label={bt("confirm")}
              onClick={onClickConfirm}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const CreditAcknowledgement = ({ applicationNo, limit, balance }) => {
  const classes = useCreditStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useCredit();
  const {
    // register,
    // control,
    // watch,
    step,
    goNextStep,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
      my code start here 
    */
  const router = useRouter();
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.requestBeingProcessed")}
        </Typography>
        <Box display="flex" justifyContent="flex-start">
          <Typography className={css(classes.title, classes.title_margin)}>
            {ft("label.enquiresCall")}
          </Typography>
          <Typography
            className={css(
              classes.title,
              classes.title_margin,
              classes.title_red_underline
            )}
          >
            {ft("label.enquiresNumber")}
          </Typography>

          <Typography className={classes.title}>
            {ft("label.duringOfficeHours")}
          </Typography>
        </Box>

        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.topLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.existingCreditLimit")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {limit}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.availableBalance")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {balance}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelBlack, classes.required)}
              >
                {ft("label.requestedCreditLimitIncrease")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {formatMoney(watch()["increase"])}
              </Typography>
            </Grid>
          </Grid>

          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"center"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonOrange}
              label={bt("backToHome")}
              onClick={() => goProfile(router, applicationNo)}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const CreditContent: FC<CreditBarProps> = ({ list }) => {
  const { step }: StepSolutionContext = useStepSolution();
  // console.log("step", step);
  return (
    <div>
      {map(list, ({ key, ...l }: CreditItemProps, index: number) => {
        let styles = { display: "none" };
        if (index === step) {
          styles = { display: "block" };
        }
        return (
          <div key={key} style={styles}>
            {l.component}
          </div>
        );
      })}
    </div>
  );
};

export const CreditForm: FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation("section");
  const classes = useCreditStyles();

  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);
  /* my code here */
  const [limit, setLimit] = useState("");
  const [balance, setBalance] = useState("");
  const steps: CreditItemProps[] = [
    {
      key: "creditRequest",
      component: (
        <CreditRequest
          applicationNo={applicationNo}
          limit={limit}
          setLimit={setLimit}
          balance={balance}
          setBalance={setBalance}
        />
      ),
    },
    {
      key: "creditReview",
      component: (
        <CreditReview
          applicationNo={applicationNo}
          limit={limit}
          balance={balance}
        />
      ),
    },
    {
      key: "creditAcknowledgement",
      component: (
        <CreditAcknowledgement
          applicationNo={applicationNo}
          limit={limit}
          balance={balance}
        />
      ),
    },
  ];

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <CreditProvider
        values={{
          hkid: "",
          mobile: "",
        }}
      >
        <StepSolution maxStep={steps.length} onSubmit={onSubmit}>
          <CreditContent list={steps} />
        </StepSolution>
        {/* <OTP />
                <AccountInformation /> */}
      </CreditProvider>
    </GoogleReCaptchaProvider>
  );
};
