import React, { FC } from 'react'
import { Controller } from "react-hook-form";
// import Link from 'next/link'
import css from 'clsx'
// import {
//   Typography,
// } from '@material-ui/core'
import {
  FormRenderElementProps,
} from '@interfaces/form'
import {
  useStyles,
} from './styles'

export interface InputProps extends FormRenderElementProps{
  // TODO
  type: 'text' | 'password' | 'hidden' | 'date' | 'email' | 'tel' | 'url'
  value?: string;
}

const FormInput: FC<InputProps> = ({
  // register,
  type,
  name,
  control,
  value,
  inputProps={},
}) => {
  const classes = useStyles();
  let styles = classes.root;
  if (type === 'hidden') {
    styles = css(styles, classes.hide);
  }
  // console.log(inputProps);
  return (
    <div className={styles}>
      {/* <input
        className={classes.input}
        type={type}
        {...inputProps}
        ref={register()}
      /> */}
      <Controller
        name={name}
        control={control}
        render={({ onChange }: any) => (
          <input
            className={classes.input}
            type={type}
            onChange={(r: any) => onChange(r)}
            value={value}
            {...inputProps}
          />
        )}
      />
    </div>
  );
}

export default FormInput;
