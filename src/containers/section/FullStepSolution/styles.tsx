import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  Colors,
} from '@theme/global';
const {
  linkBlue,
  titleGold,
} = Colors;

const useBarItemStyles = makeStyles(theme => {
  const {
    palette: {
      text: {
        primary,
      },
      primary: {
        light,
      },
    },
  }: any = theme;
  return {
    root: {
      padding: '14px 35px',
      borderRadius: '5px',
      flex: 1,
      userSelect: 'none',
    },
    icon: {
      width: '15px',
      height: '15px',
      marginRight: '25px',
    },
    title: {
      display: 'flex',
      flex: 1,
      textAlign: 'left',
      fontWeight: 600,
      fontSize: '15px',
    },

    inActive: {
      border: `1px solid ${primary}`,
      cursor: 'pointer',
      background: 'transparent',
    },
    inActiveIcon: {
      color: primary,
    },
    inActiveTitle: {
      color: primary,
    },
    active: {
      background: 'white',
      boxShadow: '0px 10px 20px #0000001A',
    },
    activeIcon: {
      color: light,
    },
    activeTitle: {
      color: light,
    },
  };
});

const useBarStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
  },
  step: {
    margin: '0 -32px',
    // gap: '64px',
    '@global': {
      '> div': {
        flex: '0 1 calc(33% - 67px)',
        margin: '0 32px',
      },
    },
  },
}));

const useStyles = makeStyles(theme => ({
  root: {
    backgroundImage: `url(${StaticSources.bg.fullStep})`,
    backgroundSize: 'cover',
    padding: '49px 0 37px 0',
  },
  switch: {
    textAlign: 'right',
    marginTop: '10px',
  },
  form: {
    marginTop: '50px',
  },
  wrapper: {
    position: 'relative',
  },
  content: {
    marginTop: '53px',
  },
  tool: {
    width: 622,
    margin: '0 -5px',
    '@global': {
      '> button': {
        margin: '0 5px',
      },
    },
  },
  tools: {
    position: 'absolute',
    top: '-49px',
    right: 0,
  },
}));
const useProgressStyles = makeStyles(theme => ({
  root: {
    marginTop: '22px',
    position: 'relative',
  },
  step: {
    position: 'absolute',
    top: '-10px',
    minWidth: '50px',
  },
  step1: {
    left: '30%',
  },
  step2: {
    left: '63.6%',
  },
  step3: {
    left: '96%',
  },
  activeStep: {
    fontWeight: 'bold',
  },
  clickableStep: {
    cursor: 'pointer',
  },
  wrapper: {
    fontSize: '11px',
    textTransform: 'uppercase',
    boxShadow: '0px 3px 6px #00000029',
    padding: '2px 5px',
    borderRadius: '8px',
    background: 'white',
    textAlign: 'center',
  },
  finished: {
    color: 'white',
    backgroundColor: `${theme.palette.primary.light} !important`,
  },
  bar: {
    position: 'relative',
    flex: 1,
  },
  mainBarContent: {
    width: '100%',
    background: theme.palette.text.disabled,
  },
  barContent: {
    
  },
  barBufferContent: {
    background: `transparent linear-gradient(270deg, #EF191B 0%, #EF191B 0%, #780D0E 100%) 0% 0% no-repeat padding-box`,
  },
  label: {
    fontWeight: 'bolder',
    textTransform: 'uppercase',
    position: 'absolute',
    left: '-130px',
  },
}));
const useButtonStyles = makeStyles(theme => ({
  root: {
    padding: '36px 27px 16px 27px',
    boxShadow: '0px 20px 40px #0000001A',
    borderRadius: '10px',
    fontSize: '18px',
    textAlign: 'left',
  },
  switchLabel: {
    fontSize: '12px',
  },
  yellow: {
    background: 'transparent linear-gradient(180deg, #EB9812 0%, #802900 100%) 0% 0% no-repeat padding-box !important',
    '&:hover': {
      background: 'transparent linear-gradient(180deg, #EB9812 0%, #802900 100%) 0% 0% no-repeat padding-box !important',
    }
  },
}));
const useStatementStyles = makeStyles(theme => ({
  root: {
    color: theme.palette.text.secondary,
    fontSize: '15px',
  },
  
}));

const useFormStyles = makeStyles(theme => ({
  alert: {
    marginBottom: '10px',
  },
  root: {
    marginBottom: '5px',
  },
  statementBlock: {
    padding: '10px 0 30px 0',
  },
  auth: {
    marginTop: '48px',
  },
  hints: {

  },
  fact: {
    position: 'relative',
    fontSize: '16px',
    fontWeight: 700,
    color: 'black',
    '&:before':{
      content: "''",
      display: 'block',
      position: 'absolute',
      width: '136px',
      height: '2px',
      background: 'black',
      bottom: '-15px',
      left: 0,
    },
  },
  factContent: {
    fontSize: '15px',
    color: theme.palette.text.secondary,
    marginTop: '20px',
  },
  statement: {
    marginTop: '10px',
    marginLeft: '11px',
  },
  statementWithBorder: {
    position: 'relative',
    '&:before': {
      content: "''",
      display: 'block',
      width: '136px',
      height: '2px',
      background: theme.palette.primary.light,
      position: 'absolute',
      bottom: '-3px',
      left: 0,
    },
  },
  step: {
    margin: '100px 0 50px 0',
  },
  footer: {
    padding: '0 100px',
  },
  stepButton: {
    width: '401px',
    height: '40px',
    fontSize: '15px',
    textTransform: 'uppercase',
  },
  inlineForm: {
    margin: '0 -15px',
    marginBottom: '80px',
    '@global': {
      '> div': {
        margin: '0 15px',
        width: '50%',
        height: '100%',
      },
    }
  },
  label: {
    fontSize: '14px',
    fontWeight: 600,
    height: '43px',
    display: 'flex',
    alignItems: 'center',
    // lineHeight: '40px'
    color: theme.palette.text.primary,
  },
  underBlock: {
    position: 'relative',
    width: 'fit-content',
    '&:before': {
      content: "''",
      display: 'block',
      position: 'absolute',
      top: '24px',
      width: '100%',
      height: '2px',
      backgroundColor: theme.palette.primary.light,
    },
  },
  break: {
    padding: '30px 0 10px 0',
  },
  reviewValue: {
    color: theme.palette.primary.light,
    fontWeight: 700,
  },
  inlineIcon: {
    marginRight: '5px',
    width: '11px',
    height: '14px',
    textTransform: 'uppercase',
    display: 'inline-block',
    verticalAlign: 'middle',
  },
  inline: {
    display: 'inline-block',
  },
  reviewTable: {
    flex: 1,
    width: '100%',
  },
  title: {
    fontSize: '18px',
    fontWeight: 700,
    color: theme.palette.primary.light,
  },
  remarks: {
    fontSize: '14px',
    color: theme.palette.primary.light,
  },
  form: {
    background: 'white',
    boxShadow: '0px 20px 40px #0000001A',
    padding: '30px',
  },
}));

const useReviewFormStyles = makeStyles(theme => ({
  root: {
    margin: '0 24px',
  },
  title: {
    position: 'relative',
    marginBottom: '55px',
    width: 'fit-content',
    '&:before': {
      position: 'absolute',
      content: "''",
      width: '100%',
      height: '2px',
      backgroundColor: theme.palette.primary.light,
      bottom: '-5px',
    },
  },
  step: {
    padding: '0 20px',
    // gap: '57px',
    margin: '0 auto 81px auto',
    '@global': {
      '> button': {
        margin: '0 12px',
      },
    }
  },
  left: {
    flex: 1,
    width: '100%',
    margin: '0 -12px',
  },
  right: {
    flex: 1,
    width: '100%',
    margin: '0 12px',
  },
  row: {
    marginBottom: '25px',
  },
  footer: {
    marginTop: '51px',
  },
}));

const useAgreementStyle = makeStyles(theme => ({
  root: {
    '@global': {
      '> span': {
        marginRight: '5px !important',
      },
    },
  },
  note: {
    color: theme.palette.text.secondary,
    fontSize: '15px',
  },
  agreement: {
    color: linkBlue,
    textDecoration: (props: any) => props.labelUnderline ? 'underline' : 'none',
    cursor: 'pointer',
  }
}));

const useEnjoyStyle = makeStyles(theme => ({
  root: {
  },
  paper: {
    maxWidth: '1260px !important',
  },
  content: {
    margin: '20px -16px 0 -16px,'
    // marginTop: '20px',
  },
  left: {
    flex: 1,
    textAlign: 'left',
    margin: '0 16px',
  },
  right: {
    minWidth: '600px',
    position: 'relative',
    margin: '0 16px',
  },
  title: {
    marginBottom: '5px',
    fontSize: '32px',
    color: 'black',
    textTransform: 'uppercase',
    fontWeight: 'normal',
  },
  subTitle: {
    marginBottom: '50px',
    fontWeight: 900,
    fontSize: '32px',
    textTransform: 'uppercase',
    color: titleGold,
  },
  description: {
    fontSize: '22px',
    color: 'black',
    marginBottom: '50px',
    opacity: 0.5,
  },
  control: {
    gap: '39px',
    width: '100%',
  },
  banner: {
    width: '600px',
    height: '421px',
  },
  icon: {
    width: '61px',
    height: '61px',
  },
  slogan: {
    top: '116px',
    left: '56px',
    position: 'absolute',
    zIndex: 2,
  },
  sloganText: {
    color: 'white',
    textTransform: 'uppercase',
    fontWeight: 700,
    fontSize: '31px',
  },
  subSloganText: {
    color: 'white',
    textTransform: 'uppercase',
    fontWeight: 'bolder',
    fontSize: '31px',
  },
  btn: {
    display: 'flex',
    flex: 1,
    width: '100%',
    borderRadius: '9px',
  },
  nowBtn: {
    color: 'white',
    fontWeight: 900,
    fontSize: '18px',
    background: 'transparent linear-gradient(180deg, #E18E24 0%, #713312 100%) 0% 0% no-repeat padding-box',
    boxShadow: '0px 0px 48px #0001010A',
    '&:hover': {
      background: 'transparent linear-gradient(180deg, #E18E24 0%, #713312 100%) 0% 0% no-repeat padding-box',
    },
  },
  laterBtn: {
    color: titleGold,
    fontWeight: 900,
    fontSize: '18px',
    paddingTop: '21px',
    paddingBottom: '21px',
    background: 'white',
    boxShadow: '0px 0px 48px #0001010A',
    border: `1px solid ${titleGold}`,
    '&:hover': {
      background: 'white',
    },
  },
}));

const useCompletionStyles = makeStyles(theme => ({
  banner: {
    backgroundImage: `url(${StaticSources.bg.thankyouCongrat})`,
    backgroundSize: '120%',
    backgroundPosition: 'center center',
    width: '100%',
    height: '266px',
    padding: '32px 66px 50px 66px',
  },
  bannerTips: {
    color: 'white',
    fontSize: '20px',
    fontWeight: 700,
    marginBottom: '30px',
  },
  bannerTitle: {
    color: 'white',
    fontSize: '26px',
    fontWeight: 700,
    marginBottom: '30px',
    textAlign: 'center',
    margin: '0 70px',
  },
  bannerHints: {
    color: 'white',
    fontSize: '20px',
    textAlign: 'center',
  },
  tc: {
    textAlign: 'right',
    marginTop: '12px',
    fontSize: '12px',
    color: theme.palette.text.primary,
    marginBottom: '21px',
  },
  subjectTo: {
    color: theme.palette.primary.light,
    cursor: 'pointer',
    marginLeft: '5px',
    fontSize: '12px',
    textDecoration: 'none',
    // textTransform: 'uppercase',
  },
  title: {
    fontSize: '26px',
    textTransform: 'uppercase',
    fontWeight: 700,
    color: theme.palette.primary.main,
    marginTop: '30px',
  },
  subTitle: {
    fontSize: '26px',
    textTransform: 'uppercase',
    fontWeight: 700,
    marginBottom: '5px',
  },
  tcContent: {
    overflowY: 'auto',
    maxHeight: '300px',
  },
}));

const useSMSStyles = makeStyles(theme => ({
  root: {
    gap: '22px',
  },
  resend: {
    color: theme.palette.primary.light,
    marginTop: '5px',
    fontSize: '13px',
  },
  resendBtn: {
    textTransform: 'uppercase',
    cursor: 'pointer',
    display: 'inline-block',
  },
  btn: {
    width: '133px',
    height: '27px',
    fontSize: '13px',
    textTransform: 'uppercase',
    color: 'white',
    borderRadius: '5px',
    fontWeight: 700,
    '&:disabled': {
      border: `1px solid ${theme.palette.primary.light}`,
      background: 'white',
      color: theme.palette.primary.light,
      cursor: 'not-allowed',
    },
  },
  // active: {
  //   border: `1px solid ${theme.palette.primary.light}`,
  //   background: 'white',
  //   color: theme.palette.primary.light,
  // },
  input: {
    width: '26px',
    background: '#F9F9F9 0% 0% no-repeat padding-box',
    borderBottom: `2px solid ${theme.palette.primary.light}`,
    margin: '0 3px',
  },
  inputBox: {
    width: '26px',
    border: 0,
    fontSize: '15px',
    padding: '3px 0',
    margin: '0',
    background: 'transparent',
    outline: 'none',
    textAlign: 'center',
    '&focus': {
      outline: 'none',
    },
  },
  content: {
    margin: '0 -3px',
  },
}));

const useCompletionTableStyles = makeStyles(theme => ({
  box: {
    width: '652px',
    margin: '0 auto',
    textAlign: 'center',
  },
  resultRemind: {
    color: theme.palette.text.primary,
    fontSize: '26px',
    display: 'block',
  },
  subTitle: {
    fontSize: '20px',
    color: theme.palette.text.primary,
    margin: '37px 0',
    textAlign: 'center',
  },
  title: {
    fontSize: '18px',
    position: 'relative',
    fontWeight: 'bold',
    width: 'fit-content',
    marginBottom: '37px',
    color: theme.palette.primary.light,
    '&:before': {
      content: "' '",
      display: 'block',
      position: 'absolute',
      width: '98%',
      height: '9px',
      left: 0,
      bottom: '-10px',
      background: theme.palette.primary.light,
    },
  },
  result: {
    color: theme.palette.primary.light,
    fontSize: '26px',
    marginBottom: '21px',
  },
  control: {
    margin: '0 -5px 62px -5px',
    '& > button': {
      display: 'flex',
      flex: 1,
      margin: '0 5px',
    },
  },
  btn: {
    height: '50px',
    fontSize: '18px',
    borderRadius: '6px',
    textTransform: 'none',
  },
  later: {
    background: 'transparent linear-gradient(0deg, #434040 0%, #878585 70%, #D6D6D6 100%) 0% 0% no-repeat padding-box',
    '&:hover': {
      background: 'transparent linear-gradient(0deg, #434040 0%, #878585 70%, #D6D6D6 100%) 0% 0% no-repeat padding-box',
    }
  },
  hint: {
    marginBottom: '30px',
  },
  hintTitle: {
    color: theme.palette.text.primary,
    fontSize: '17px',
    fontWeight: 600,
    textDecoration: 'underline',
  },
  hintSubTitle: {
    color: theme.palette.text.primary,
    fontSize: '17px',
  },
  office: {
    marginBottom: '22px',
  },
  officeQuestion: {
    fontWeight: 'bold',
    color: theme.palette.text.primary,
    fontSize: '16px',
  },
  hour: {
    color: theme.palette.text.primary,
    fontSize: '16px',
  },
}));

export {
  useBarItemStyles,
  useStyles,
  useProgressStyles,
  useCompletionTableStyles,
  useBarStyles,
  useButtonStyles,
  useFormStyles,
  useStatementStyles,
  useAgreementStyle,
  useEnjoyStyle,
  useReviewFormStyles,
  useSMSStyles,
  useCompletionStyles,
};
