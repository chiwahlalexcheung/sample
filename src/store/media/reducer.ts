import { MediaAction, MediaActionTypes } from './actions'
import { BasicListStateProps } from '@interfaces/state'
export interface MediaState {
  readonly banner: BasicListStateProps
}

const initialMediaState = {
  banner: {
    list: [],
    loading: false,
    error: null,
  },
}

/**
 * Media Reducer
 */
export const mediaReducer = (
  state: MediaState = initialMediaState,
  action: any
): MediaState => {
  const { banner } = state;
  switch (action.type) {
    case MediaActionTypes.FETCH_BANNERS:
      return {
        ...state,
        banner: {
          ...banner,
          loading: true,
        },
      }
    case MediaActionTypes.FETCH_BANNERS_SUCCEED:
      return {
        ...state,
        banner: {
          ...banner,
          loading: false,
          list: action.payload,
        },
      }
    case MediaActionTypes.FETCH_BANNERS_FAILED:
      return {
        ...state,
        banner: {
          ...banner,
          loading: false,
          error: action.payload,
        },
      }
    default:
      return state
  }
}
