import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  select: {
    width: '100%',
    padding: 0,
    border: 0,
  },
  interest: {
    background: 'transparent linear-gradient(270deg, #DC1718 0%, #7F0D0E 100%) 0% 0% no-repeat padding-box',
    border: '0 !important',
  },
  principal: {
    background: 'transparent linear-gradient(270deg, #3DAAE8 0%, #073F8C 100%) 0% 0% no-repeat padding-box',
    border: '0 !important',
  },
  revolving: {
    background: 'transparent linear-gradient(270deg, #DD9932 0%, #A7894F 100%) 0% 0% no-repeat padding-box',
    border: '0 !important',
  },
  documentUpload: {
      background: '#fff',
      color: theme.palette.text.primary,
  },
  root: {
    width: '100%',
    color: 'white',
    // border: '0 !important',
    fontSize: '14px',
    fontWeight: 'bold',
    paddingLeft: '15px',
    '&::before': {
      border: 0,
      display: 'none !important',
    },
    '&::after': {
      border: 0,
      display: 'none !important',
    },
    '&:hover': {
      '&::before': {
        border: '0 !important',
      },
    },
    '&:focus': {
      outline: 'none',
      '&::before': {
        border: '0 !important',
      },
    }
  },
}));

export {
  useStyles,
};
