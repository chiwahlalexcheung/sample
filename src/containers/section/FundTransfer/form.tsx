import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useSelector, useDispatch } from "react-redux";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";

import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useTransferStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import apiManager from "@lib/apiManager";

import {
  getLoanList,
  formatMoney,
  apiPost,
  signOut,
  goProfile,
} from "@containers/section/AccountCommon/context";
import { requestCalculation } from "@store/content/actions";

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface TransferProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const TransferContext = createContext<Partial<ContextProps>>({});
export const useTransfer = () => useContext(TransferContext) as ContextProps;
export const TransferProvider: FC<any> = ({ values = {}, children }) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  //   useEffect(() => {
  //     register("loanType");
  //   }, []);
  //   const {
  //     value: info,
  //     changeValue,
  //     getValue,
  //     setValue,
  //   }: KeyPairHandleProps = useKeyPairValue(values);
  return (
    <TransferContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </TransferContext.Provider>
  );
};

export interface TransferItemProps {
  key: string;
  component: JSX.Element;
}
export interface TransferBarProps {
  list: TransferItemProps[];
}

const TransferSuccessPopup: FC<any> = ({ shown, applicationNo }) => {
  const classes = useTransferStyles();
  const { t } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const router = useRouter();
  return (
    <CommonPopup
      show={shown}
      afterClose={() => {}}
      maxWidth={"md"}
      noCloseBtn
      otherProps={{
        disableBackdropClick: true,
      }}
    >
      <Box
        display={"flex"}
        flexDirection={"column"}
        alignContent={"center"}
        alignItems={"center"}
        className={classes.popup}
      >
        <Typography className={classes.popupTitle}>
          {t("message.fundTransferSucceed")}
        </Typography>
        <Box
          display={"flex"}
          width={1}
          justifyContent={"space-around"}
          className={classes.popupButtons}
        >
          <CommonButton
            className={classes.bottomButtonOrange}
            label={bt("backToHome")}
            onClick={() => goProfile(router, applicationNo)}
          />
        </Box>
      </Box>
    </CommonPopup>
  );
};

export const FundRequest = ({ applicationNo, limit, setLimit }) => {
  const classes = useTransferStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useTransfer();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
    goNextStep,
  }: StepSolutionContext = useStepSolution();
  /* my code start here */
  const router = useRouter();
  const [shown, setShown] = useState(false);

  const getLimit = async () => {
    const loanList = await getLoanList();
    loanList.forEach((l) => {
      if (l["acct_no"] === applicationNo) {
        setLimit(l["avl_cr_limit"]);
      }
    });
  };
  useEffect(() => {
    getLimit();
  }, []);

  const onClickNext = () => {
    const amount = watch()["amount"];
    if (!amount || isNaN(amount)) {
      alert(ft("message.inputValidFundTransfer"));
      return;
    }
    const valueAmount = Number(amount);
    const upperBound = Number(limit);
    const lowerBound = 1000; // TODO edit. maybe
    if (valueAmount < 0) {
      alert(ft("message.inputValidFundTransfer"));
      return;
    }
    if (valueAmount < lowerBound) {
      alert(ft("fundTransferTooLow") + formatMoney(lowerBound));
      return;
    }
    if (valueAmount > upperBound) {
      alert(ft("fundTransferTooHigh"));
      return;
    }
    setShown(true);
    goNextStep();
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography component={"h4"} className={classes.title}>
          {ft("label.enterFundTransfer")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.availableCreditLimit")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {formatMoney(limit)}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelBlack, classes.required)}
              >
                {ft("label.fundTransferAmount")}
                {/* <span className={classes.require}>*</span> */}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormElement.TextInput
                control={control}
                name="amount"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("back")}
              onClick={() => goProfile(router, applicationNo)}
            />

            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("next")}
              onClick={onClickNext}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
      <TransferSuccessPopup shown={shown} applicationNo={applicationNo} />
    </Box>
  );
};

export const FundReview = ({ applicationNo, limit }) => {
  const classes = useTransferStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useTransfer();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
    goNextStep,
  }: StepSolutionContext = useStepSolution();
  /* my code start here */
  const router = useRouter();
  const [shown, setShown] = useState(false);

  const onClickReset = () => {
    goPrevStep();
  };

  const onClickConfirm = async () => {
    const amount = watch()["amount"];
    const todayIso = new Date().toISOString().slice(0, 10);
    const today =
      todayIso.slice(0, 4) + todayIso.slice(5, 7) + todayIso.slice(8, 10);
    const data = {
      acct_no: applicationNo,
      drw_amt: amount,
      trx_date: today,
    };
    const result = await apiPost("fundTransfer", data, {});

    if (result["data"]["LOS"]["BODY"]["rtn_cde"] === "000") {
      setShown(true);
    }
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography component={"h4"} className={classes.title}>
          {ft("label.enterFundTransfer")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.availableCreditLimit")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {formatMoney(limit)}
              </Typography>
            </Grid>
          </Grid>
          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelBlack, classes.required)}
              >
                {ft("label.fundTransferAmount")}
                {/* <span className={classes.require}>*</span> */}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {formatMoney(watch()["amount"])}
              </Typography>
            </Grid>
          </Grid>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("reset")}
              onClick={onClickReset}
            />

            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("confirm")}
              onClick={onClickConfirm}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
      <TransferSuccessPopup applicationNo={applicationNo} shown={shown} />
    </Box>
  );
};

export const TransferContent: FC<TransferBarProps> = ({ list }) => {
  const { step }: StepSolutionContext = useStepSolution();
  // console.log("step", step);
  return (
    <div>
      {map(list, ({ key, ...l }: TransferItemProps, index: number) => {
        let styles = { display: "none" };
        if (index === step) {
          styles = { display: "block" };
        }
        return (
          <div key={key} style={styles}>
            {l.component}
          </div>
        );
      })}
    </div>
  );
};

export const TransferForm: FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation("section");
  const classes = useTransferStyles();

  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);

  /* my code here */
  const [limit, setLimit] = useState("");

  const steps: TransferItemProps[] = [
    {
      key: "fundRequest",
      component: (
        <FundRequest
          applicationNo={applicationNo}
          limit={limit}
          setLimit={setLimit}
        />
      ),
    },
    {
      key: "fundReview",
      component: <FundReview applicationNo={applicationNo} limit={limit} />,
    },
  ];

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <TransferProvider
        values={{
          hkid: "",
          mobile: "",
        }}
      >
        <StepSolution maxStep={steps.length} onSubmit={onSubmit}>
          <TransferContent list={steps} />
        </StepSolution>
        {/* <OTP />
                <AccountInformation /> */}
      </TransferProvider>
    </GoogleReCaptchaProvider>
  );
};
