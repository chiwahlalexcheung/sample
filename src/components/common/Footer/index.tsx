import React, { FC } from 'react'
import { map } from 'lodash'
import css from 'clsx'
import {
  Box, Container, Typography,
} from '@material-ui/core'
import { useTranslation } from 'next-i18next'
import Link from '@components/common/Link'
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  useStyles,
  useMenuStyles,
} from './styles'
import MenuListItemProps from '@interfaces/menu'
import StaticRoutes from '@config/routes'

export interface FooterMenuItemProps extends MenuListItemProps { 
  // TODO
  newline?: boolean;
};

export interface FooterMenuProps { 
  items: FooterMenuItemProps[];
  className?: string;
  justifyContent?: 'flex-end' | 'center' | 'flex-start' | 'space-between';
};

export const FooterMenuItem: FC<FooterMenuItemProps> = ({ link, label }) => {
  const classes = useMenuStyles();
  return (
    <Link href={link} rel={'contents'} label={label} className={classes.item} />
  );
}
export const FooterMenu: FC<FooterMenuProps> = ({ className='', items, justifyContent='space-between' }) => {
  const classes = useMenuStyles();
  return (
    <Box className={css(classes.root, className)} display={'flex'} alignItems={'center'} justifyContent={justifyContent}>
      {
        map(items, (i: FooterMenuItemProps, index: number) => (
          <FooterMenuItem key={index} {...i} />
        ))
      }
    </Box>
  );
}

export const FooterCopyright: FC = () => {
  const { t } = useTranslation('common');
  const licenceNo = '0921/2020';
  return (
    <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
      <Typography>{t('sentence.copyright')}</Typography>
      <Typography>
        {t('sentence.licenceNo', { licenceNo })}
      </Typography>
    </Box>
  );
};

const MainFooter: FC = () => {
  const { t } = useTranslation('footer');
  const { t: Ct } = useTranslation('common');
  const classes = useStyles();
  const hotLine = '2996 2600';
  const menuItems: FooterMenuItemProps[] = [
    {
      label: t('menu.career'),
      link: StaticRoutes.career,
      key: 'career',
    },
    {
      label: t('menu.faq'),
      link: StaticRoutes.faq,
      key: 'faq',
    },
    {
      label: t('menu.tc'),
      link: StaticRoutes.tc,
      key: 'tc',
    },
    {
      label: t('menu.privacy'),
      link: StaticRoutes.privacy,
      key: 'privacy',
    },
    {
      label: t('menu.hyperlink'),
      link: StaticRoutes.hyperlink,
      key: 'hyperlink',
    },
    {
      label: t('menu.important-declaration'),
      link: StaticRoutes.important,
      key: 'important',
    },
    {
      label: t('menu.noteToIntendingBorrowers'),
      link: StaticRoutes.noteToIntending,
      key: 'noteToIntending',
    },
  ];
  const menuSecondItems: FooterMenuItemProps[] = [
    {
      label: t('menu.beware'),
      link: StaticRoutes.beware,
      key: 'beware',
    },
    {
      label: t('menu.sitemap'),
      link: StaticRoutes.sitemap,
      key: 'sitemap',
    }
  ];
  return (
    <div className={classes.root}>
      <InnerPage>
        <div className={classes.menu}>
          <FooterMenu items={menuItems} />
        </div>
        <div className={classes.menu}>
          <FooterMenu items={menuSecondItems} className={classes.fitMenu} justifyContent={'flex-start'} />
        </div>
        <div className={classes.copyright}>
          <FooterCopyright />
        </div>
        <Typography className={classes.warning} component={'h1'}>
          {Ct('sentence.warning')}
        </Typography>
        <Typography className={classes.hotLine}>
          {Ct('sentence.complaintHotline', { hotLine })}
        </Typography>
      </InnerPage>
    </div>
  );
}

export default MainFooter;
