import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  
}));

const useCheckboxStyles = makeStyles(theme => ({
  root: {
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: '3px',
    width: '22px',
    height: '22px',
    padding: 0,
  },
  input: {

    
  },
  icon: {
    width: '20px',
    height: '20px',
    padding: 0,
    margin: 'auto',
  }
}));
export {
  useStyles,
  useCheckboxStyles,
};
