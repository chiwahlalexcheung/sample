import React, { FC, forwardRef, createRef, useState, useCallback, useEffect } from 'react'
import css from 'clsx'
import { map, times } from 'lodash';
// import moment from 'moment';
import CommonButton from '@components/common/Button'
// import FormElement from '@components/form'
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from 'react-google-recaptcha-v3'
import { ReCaptchaBlock } from '@containers/section/ContactUs'
import ServiceConfig from '@config/service'
import { useTranslation } from 'next-i18next'
import { useOTP } from '@store/application/context';
import {
  Typography,
  Box,
  Grid,
} from '@material-ui/core';
// import {
//   LabelValueProps,
// } from '@interfaces/list'
import {
  // useToggle,
  // ToggleProps,
  useCountDown,
  CountdownItemsProps,
  // usePrevious,
} from '@hooks/common'

import {
  useSMSStyles,
  useFormStyles,
  useReviewFormStyles,
} from './styles';
const {
  googleReCaptcha: {
    siteKey: reCaptchaKey,
  },
} = ServiceConfig;
import {
  useStepSolution,
  StepSolutionContext,
} from '@containers/section/StepSolution'


export interface SMSBlockInputProps {
  register: Function;
  name: string;
  offset: number;
  onNext: Function;
  disabled: boolean;
}

export interface SMSBlockProps {
  register: Function;
  control: any;
  totalLength?: number;
  flexDirection?: string;
  onSend?: Function;
  pageType: string;
};

export const SMSBlockInput = forwardRef<HTMLInputElement, SMSBlockInputProps>(({
  name,
  register,
  offset,
  onNext,
  disabled,
}, ref: React.Ref<HTMLInputElement>) => {
  const classes = useSMSStyles();
  const onkeyUp = useCallback(() => {
    if ((ref as React.RefObject<HTMLInputElement>).current.value) {
      onNext(offset);
    }
  }, [offset, onNext]);
  return (
    <div className={classes.input}>
      <input
        type='text'
        name={name}
        disabled={disabled}
        className={classes.inputBox}
        ref={r => {
          register(r);
          (ref as React.MutableRefObject<HTMLInputElement>).current = r;
        }}
        maxLength={1}
        onKeyUp={onkeyUp}
      />
    </div>
  );
});

export interface SMSCountdownButtonProps {
  counting: boolean;
  onStart: Function;
  timeLeft: number;
}
export const SMSCountdownButton: FC<SMSCountdownButtonProps> = ({
  counting, // in seconds
  timeLeft,
  onStart,
}) => {
  const classes = useSMSStyles();
  const { t } = useTranslation('form');
  let styles = classes.btn;
  const onStartCountdown = useCallback(() => {
    onStart(true);
  }, [onStart]);
  return (
    <CommonButton
      className={styles}
      buttonProps={{
        disabled: !!counting,
      }}
      onClick={onStartCountdown}
      label={
        !counting ? t('label.startCountDown') : t('label.countDown', { timeLeft })}
    />
  );
}

export const SMSBlock: FC<SMSBlockProps> = ({
  totalLength=6,
  flexDirection='row',
  pageType,
  // control,
  onSend,
  register,
}) => {
  const [reCaptcha, setReCaptcha] = useState('');
  const classes = useSMSStyles();
  const {
    left: timeLeft,
    counting,
    setTimeLeft,
    onStart,
  }: CountdownItemsProps = useCountDown({
    total: 60,
    start: false,
  });
  const [refs, setRefs] = useState<any[]>([]);
  const { t: Bt } = useTranslation('button');
  const { t } = useTranslation('form');
  const [mountCount, setMountCount] = useState<number>(0);
  const onNext = useCallback(index => {
    const maxLength = refs.length;
    let next = index + 1;
    if (next < maxLength) {
      refs[next].current.focus();
    }
  }, [refs]);
  const afterDown = useCallback(() => {
    // console.log('>>');
  }, []);
  const onRestart = useCallback(() => {
    if (!!reCaptcha) {
      onStart(true);
      setMountCount(1);
      setTimeLeft(60);
      if (onSend) {
        onSend(reCaptcha);
      }
    }
  }, [reCaptcha, onSend, onStart, setMountCount, setTimeLeft]);
  useEffect(() => {
    if (timeLeft <= 0) {
      afterDown();
    }
  }, [timeLeft]);
  useEffect(() => {
    const allRefs =  map(times(totalLength), () => createRef());
    setRefs(allRefs);
  }, []);
  // console.log(reCaptcha);
  return (
    <div>
      <Box display={'flex'} flexDirection={flexDirection} alignItems={'center'} justifyContent={'flex-start'} className={classes.root}>
        <Box display={'flex'} alignItems={'center'} justifyContent={'space-between'} className={classes.content}>
          {
            map(refs, (ref, index) => (
              <SMSBlockInput
                register={register}
                key={index}
                ref={ref}
                disabled={false}
                offset={index}
                onNext={onNext}
                name={`sms[${index}]`}
              />
            ))
          }
        </Box>
        <div>
          {
            (mountCount === 0 || counting) && <SMSCountdownButton
              timeLeft={timeLeft}
              counting={counting}
              onStart={onRestart}
            />
          }
        </div>
      </Box>
      {
        mountCount !== 0 && !counting && <div className={classes.resend}>
          {t('label.sendRetry')} <div className={classes.resendBtn} onClick={onRestart}>
            {Bt('resend')}
          </div>
        </div>
      }
      {
        !counting && <ReCaptchaBlock
          token={reCaptcha}
          page={pageType}
          onToken={setReCaptcha}
        />
      }
    </div>
  );
}
export interface OTPAuthenticationProps {
  pageType: string;
}
export const OTPAuthentication: FC<OTPAuthenticationProps> = ({ pageType }) => {
  const { t } = useTranslation('form');
  const {
    register,
    control,
    watch,
    step,
  }: StepSolutionContext = useStepSolution();
  const reviewClasses = useReviewFormStyles();
  const formClasses = useFormStyles();
  const { sendCode } = useOTP();
  const { mobileNo, hkId } = watch();
  const onSendOtp = useCallback((verifyToken: string) => {
    sendCode({
      mobileNo,
      hkId,
      verifyToken,
    });
  }, [sendCode, mobileNo, hkId]);
  return (
    <div className={formClasses.form}>
      <Box display={'flex'} flexDirection={'column'} className={reviewClasses.left}>
        <Typography component={'h1'} className={css(formClasses.title, reviewClasses.title)}>
          {t('title.smsOtp')}
        </Typography>
        <Box display={'flex'}>
          <Grid container spacing={3} className={reviewClasses.row}>
            <Grid item xs={12} sm={3}>
              <Typography component={'h4'} className={formClasses.label}>
                {t('note.willSendOTP')}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={9} className={formClasses.reviewValue}>
              {
                step === 1 && <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
                  <SMSBlock
                    control={control}
                    register={register}
                    onSend={onSendOtp}
                    pageType={pageType}
                  />
                </GoogleReCaptchaProvider>
              }
            </Grid>
          </Grid>
          
        </Box>
      </Box>
    </div>
  );
}

