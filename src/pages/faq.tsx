import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import Faq from '@containers/section/Faq'
import TitleBanner, { TitleBannerProps } from '@containers/banner/TitleBanner'
import StaticSources from '@config/sources';
import { fetchFaq } from '@store/content/actions';

import { storeWrapper } from '@store/store'
import { fetchSolutionRules } from '@store/common/actions'

const FaqPage: React.FC = () => {
    const { t } = useTranslation('common');
    const { t: st } = useTranslation('section');
    const bannerProps: TitleBannerProps = {
        title: st('title.faq'),
        banner: StaticSources.banner.faq
    }
    return (
        <HeadingLayout>
            <Head>
                <title>{t('menu.faq')}</title>
            </Head>
            <TitleBanner {...bannerProps} />
            <Faq />
        </HeadingLayout>
    );
}

export const getStaticProps: GetStaticProps = storeWrapper.getStaticProps(
    async ({ store, ...props }: any) => {
        const isServer = true;
        await store.dispatch(fetchFaq(isServer));
        return {
          props: {
            ...await serverSideTranslations(props.locale, ['common', 'form', 'button', 'footer', 'section']),
          }
        };
    }
)
  
export default FaqPage
