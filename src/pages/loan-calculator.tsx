import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { GetStaticProps } from 'next'
import { fetchCalculatorConfig } from '@store/content/actions'
import { fetchGeneralContact } from '@store/service/actions'
// import Fade from 'react-reveal/Fade';
import { storeWrapper } from '@store/store'
import ContactUs from '@containers/section/ContactUs'
import LoanCalculator from '@containers/section/Calculator'

const LoanCalculatorPage: React.FC = () => {
  const { t } = useTranslation('common');
  const solutionRef = useRef<any>();
  const refs = {
    solutionRef,
  };
  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.loan-calculator')}</title>
        </Head>
        <LoanCalculator popContact />
        <ContactUs />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = storeWrapper.getStaticProps(
    async ({ store, ...props }: any) => {
        const isServer = true;
        await Promise.all([
            store.dispatch(fetchCalculatorConfig(isServer)),
            store.dispatch(fetchGeneralContact(isServer)),
        ]);
        return {
            props: {
            ...await serverSideTranslations(props.locale, ['common', 'form', 'button', 'footer', 'section']),
            }
        };
    }
)

export default LoanCalculatorPage
