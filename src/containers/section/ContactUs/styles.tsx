import { makeStyles } from '@material-ui/core/styles';
// import StaticSources from '@config/sources';
// import {
//   FullCoverBgStyles,
//   // RelativeBefore,
// } from '@theme/global';
const useFormStyles = makeStyles(theme => ({
  button: {
    padding: '2px 30px',
    borderRadius: '5px',
  },
  label: {
    fontSize: '14px',
    color: theme.palette.text.primary,
    marginBottom: '2px',
  },
  remark: {
    fontSize: '12px',
    color: theme.palette.primary.light,
    textAlign: 'right',
    marginTop: '3px',
    marginBottom: '10px',
  },
  inlineRight: {
    marginLeft: '10px',
  },
  recap: {
    position: 'relative',
    margin: '20px 0',
    width: 'fit-content',
    border: `1px solid ${theme.palette.text.secondary}`,
    borderRadius: '8px',
    padding: '5px',
  },
  done: {
    position: 'absolute',
    left: '50px',
    top: '50%',
    width: '20px',
    height: '20px',
    color: theme.palette.primary.main,
  },
  recapImage: {
    width: '70px',
    height: '70px',
  },
  recapText: {
    color: theme.palette.text.secondary,
    fontSize: '11px',
    fontWeight: 700,
    marginLeft: '10px',
  },
}));
const useFormInfoStyles = makeStyles(theme => ({
  header: {
    marginBottom: '40px',
  },
  title: {
    fontSize: '16px',
    color: theme.palette.text.primary,
    fontWeight: 700,
  },
  remark: {
    fontSize: '16px',
    color: theme.palette.text.primary,
  },
}));

const useStyles = makeStyles(theme => ({
  root: {
    padding: '30px 0 117px 0'
  },
  heading: {

  },
  header: {
    width: '100%',
  },
  left: {
    paddingTop: '80px',
    width: '45%',
  },
  right: {
    paddingTop: '80px',
    width: '55%',
  },
  info: {
    width: 'fit-content',
    paddingRight: '60px',
  }
}));

const useInfosStyles = makeStyles(theme => ({
  block: {
    marginBottom: '40px',
  },
  linkLabel: {
    color: theme.palette.primary.light,
    textDecoration: 'underline',
  },
}));

const useInfoStyles = makeStyles(theme => ({
  icon: {
    width: '67px',
    height: '62px',
    marginRight: '15px',
  },
  address: {
    marginBottom: '10px',
  },
  info: {

  },
  title: {
    textTransform: 'uppercase',
    marginBottom: '10px',
    fontSize: '15px',
    fontWeight: 700,
  },
  inlineTitle: {
    fontSize: '15px',
    marginRight: '5px',
    color: theme.palette.text.primary,
  },
  subTitle: {
    fontSize: '15px',
    color: theme.palette.primary.main,
    textDecoration: 'underline',
  },
  description: {
    color: theme.palette.primary.light,
    fontSize: '15px',
  },
}));

export {
  useStyles,
  useInfoStyles,
  useInfosStyles,
  useFormInfoStyles,
  useFormStyles
};
