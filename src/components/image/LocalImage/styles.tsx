import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  image: {
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
    height: '100%',
  },
  source: {
    maxWidth: '100%',
    maxHeight: '100%',
  }
}));

export {
  useStyles,
};
