export default {
  landing: '/',
  firstMortgage: '/first-mortgage',
  secondMortgage: '/second-mortgage',
  mortgage: {
    first: '/first-mortgage',
    second: '/second-mortgage',
  },
  loanCalculator: '/loan-calculator',
  onlineProperty: '/online-property',
  aboutUs: '/about-us',
  customerCentre: '/customer-centre',
  contactUs: '/contact-us',
  career: '/career',
  faq: '/faq',
  tc: '/terms-conditions',
  privacy: '/privacy-policy',
  hyperlink: '/hyperlink-declaration',
  important: '/important-declaration',
  noteToIntending: '/note-to-intending-borrowers',
  beware: '/beware-of-money-lending-scams',
  sitemap: '/sitemap',
  valuation: '/property-valuation-service',
  revolvingLoan: '/revolving-loan',
  paymentHoliday: '/payment-holiday',

  accountLogon: '/account/logon',
  accountProfile: '/account/profile',
  account: {
    documentsUpload: '/account/documents-upload',
    changeEmail: '/account/change-email',
    onlineEStatement: '/account/online-e-statement',
    fundTransfer: '/account/fund-transfer',
    increaseCreditLimit: '/account/increase-credit-limit',
    paymentHolidayRequest: '/account/payment-holiday-request'
  }  
};
