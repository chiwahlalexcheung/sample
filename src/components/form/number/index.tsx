import React, { FC, useCallback, useRef } from 'react'
import { Controller } from "react-hook-form";
// import Link from 'next/link'
import css from 'clsx'
import {
  InputBase,
  Box,
} from '@material-ui/core'
import {
  FormRenderElementProps,
} from '@interfaces/form'
import {
  useStyles,
  useSpecialStyles,
} from './styles'
import {
  useStyles as useInputStyles,
} from './../text/styles'

export interface NumericPrefixProps extends FormRenderElementProps{
  unit?: string;
  // hasUnit?: boolean;
}

export interface NumericUnitProps extends NumericPrefixProps{
  specialClass?: string;
  // hasUnit?: boolean;
}

export interface NumericProps extends FormRenderElementProps{
  prefix?: JSX.Element;
  suffix?: JSX.Element;
}

const FormNumeric: FC<NumericProps> = ({
  control,
  name,
  render,
  prefix,
  suffix,
  inputProps={},
}) => {
  // const inputRef = useRef<any>();
  const classes = useStyles();
  const inputClasses = useInputStyles();
  const onInputFocus = useCallback((e: any) => {
    const { target: { value } }: any = e;
    if (Number(value) === 0) {
      e.target.select();
      // inputRef.current.value = undefined;
    }
  }, []);
  let addOn = {
    render: ({ onChange, value }: any) => (
      <InputBase
        onChange={(r: any) => onChange(r)}
        onFocus={(r: any) => onInputFocus(r)}
        value={!isNaN(value) ? +value : 0}
        type={'number'}
        // ref={inputRef as React.RefObject<HTMLInputElement>}
        classes={{
          input: classes.input,
        }}
        className={css(inputClasses.input)}
        inputProps={inputProps}
      />
    ),
  };
  if (!!render) {
    addOn = { render: (payload: any) => render(payload) };
  }
  return (
    <Box display={'flex'} className={css(inputClasses.root, classes.root)}>
      {
        !!prefix && prefix
      }
      <div className={classes.inputRoot}>
        <Controller
          name={name}
          control={control}
          {...addOn}
        />
      </div>
      {
        !!suffix && suffix
      }
    </Box>
  );
}

export const FormPrefixNumeric: FC<NumericPrefixProps> = ({
  unit, ...props
}) => {
  const classes = useStyles();
  return (
    <FormNumeric
      {...props}
      prefix={<div className={classes.prefix}>{unit}</div>}
    />
  );
}

export const FormUnitNumeric: FC<NumericUnitProps> = ({
  unit,
  specialClass='', ...props
}) => {
  const clientClasses = useSpecialStyles();
  const classes = useStyles();
  return (
    <FormNumeric
      {...props}
      suffix={<div
        className={css(classes.suffix, clientClasses[specialClass])}
      >{unit}</div>}
    />
  );
}

export default FormNumeric;
