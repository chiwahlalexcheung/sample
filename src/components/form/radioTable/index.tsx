import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Radio, Typography, Box } from "@material-ui/core";
import { useTranslation } from "next-i18next";
import useTableStyles from "./styles";
//import { selectFields } from "express-validator/src/select-fields";

export default function RadioTable({ data, selected, setSelected }) {
  const classes = useTableStyles();
  const { t: ft } = useTranslation("form");
  // empty data , error or no application
  if (!(data && data[0])) {
    return <div>{ft('label.noData')}</div>;
  }
  // translate the keys
  const titles = {
    acct_no: ft("label.loanAccountApplicationNo"),
    loan_typ: ft("label.loanType"),
    status: ft("label.status"),
    acct_date: ft("label.date"),
  };
  //
  const keys = Object.keys(data[0]);
  const firstKey = keys[0];
  // selected row
  const handleChange = (e) => {
    setSelected(e.target.value);
  };
  //
  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            {keys.map((k) => (
              <TableCell align={k === firstKey ? "left" : "right"}>
                <Typography className={classes.title}>{titles[k]}</Typography>
              </TableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map((r) => (
            <TableRow key={JSON.stringify(r)}>
              {keys.map((k) =>
                k === firstKey ? (
                  <TableCell component="th" scope="row">
                    <Box display="flex" alignItems="center">
                      <Radio
                        value={JSON.stringify(r)}
                        onChange={handleChange}
                        checked={JSON.stringify(r) === selected}
                        color={"primary"}
                      />
                      <Typography className={classes.content}>
                        {r[k]}
                      </Typography>
                    </Box>
                  </TableCell>
                ) : (
                  <TableCell align="right">
                    <Typography className={classes.content}>{r[k]}</Typography>
                  </TableCell>
                )
              )}
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
