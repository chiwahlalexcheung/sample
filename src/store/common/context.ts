import { useCallback } from 'react'
import { pick, isBoolean } from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import { fetchSolutionRules, fetchConfig, applyRule } from './actions'
import { AppState } from './../store'
import { useMoment } from '@hooks/locale'
import { usePrevious } from '@hooks/common'
import { CalculationInfoPayloadProps, applyCalculation } from './actions'

export const useMainConfig = () => {
  const dispatch = useDispatch();
  const { info, loading, error } = useSelector(({ common }: AppState) => common.config);
  const { tsCM, ts } = useMoment();
  const isReadyCall = useCallback(() => {
    const {
      start,
      end,
    } = pick(info, [
      'start',
      'end',
    ]);
    const mStart = tsCM(start);
    const mEnd = tsCM(end);
    return ts().isSameOrAfter(mStart) && ts().isBefore(mEnd);
  }, [info]);
  const fetch = useCallback(() => {
    dispatch(fetchConfig());
  }, [dispatch]);
  return {
    fetch,
    loading,
    info,
    error,
    isReadyCall,
  };
}

export interface Solution3StepProps {
  owner: boolean | string | undefined;
  mortgage: boolean | string | undefined;
  loan: string | undefined;
  [key: string]: any;
}

export const useSolutionRule = () => {
  const dispatch = useDispatch();
  const { list, loading, error } = useSelector(({ common }: AppState) => common.ruleQuestions);
  const {
    info,
    loading: applying, 
    error: invalidApply,
  } = useSelector(({ common }: AppState) => common.applyRule);
  const wasApplying = usePrevious(applying);
  
  const normalizeRuleAnswer = useCallback(
    ({ owner, mortgage, loan }: Solution3StepProps) => {
      const answer: string[] = ['', '', ''];
      if (isBoolean(owner)) {
        answer[0] = owner ? '1' : '0';
      } else {
        answer[0] = String(owner);
      }
      if (isBoolean(mortgage)) {
        answer[1] = mortgage ? '1' : '0';
      } else {
        answer[1] = String(mortgage);
      }
      if (isBoolean(loan)) {
        answer[2] = loan ? '1' : '0';
      } else {
        answer[2] = String(loan);
      }
      return answer;
    },
    []
  );
  // normalizeRuleAnswer(payload)))
  const apply = useCallback((payload: any={}) => (
    dispatch(applyRule(payload))
  ), [dispatch]);
  // [dispatch, normalizeRuleAnswer]);
  const fetch = useCallback((isServer: boolean) => (
    dispatch(fetchSolutionRules(isServer))
  ), [dispatch]);
  // const getPopTarget = useCallback((rules: Solution3StepProps) => {
  // console.log(wasApplying, applying, invalidApply);
  // }, [list]);

  const isResolvingLoan = info?.productType === 'resolving';
  // console.log(info);
  return {
    fetch,
    loading,
    list,
    error,
    normalizeRuleAnswer,
    // application
    appliedProduct: info?.product,
    appliedProductType: info?.productType,
    applying,
    invalidApply,
    isResolvingLoan,
    apply,
    beingApplied: wasApplying && !applying && !invalidApply,
    beingFailApplied: wasApplying && !applying && !!invalidApply, 
    // formatRule,
    // getPopTarget,
  };
}

export const useAppliedCalculationInfo = () => {
  const {
    loanAmount=0,
    loanType='first',
    calculateType='interestOnly',
  }: CalculationInfoPayloadProps = useSelector(({ common }: AppState) => common.applyCalculation);
  const dispatch = useDispatch();
  const isResolvingLoan = calculateType === 'resolving';
  const apply = useCallback((info: CalculationInfoPayloadProps) => {
    dispatch(applyCalculation(info));
  }, [dispatch]);
  return {
    apply,
    loanType,
    calculateType,
    isResolvingLoan,
    loanAmount,
  };
}
