import { makeStyles } from "@material-ui/core/styles";
import StaticSources from "@config/sources";
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from "@theme/global";
import { Colors } from "@theme/global";
const { lightGrey } = Colors;

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#F9FBFC",
  },
  heading: {
    marginBottom: "70px",
  },
}));

const useProfileStyles = makeStyles((theme) => ({
  root: {
    margin: "30px 0 30px 0",
    maxWidth: "1153px",
  },
  form: {
    padding: "36px 73px 36px 73px",
    // border: '1px solid #000',
    boxShadow: "0px 6px 20px #00000033",
    background: "#fff",
  },
  TopButtonHome: {
    width: "146px",
    height: "37px",
    background:
      "transparent linear-gradient(180deg, #D49637 0%, #7C4B00 100%) 0% 0% no-repeat padding-box !important",
    borderRadius: "5px",
    fontSize: "13.5px",
    lineHeight: "30px",
    fontWeight: "bolder",
    letterSpacing: "0px",
    textTransform: "uppercase",
    "&:hover": {
      background:
        "transparent linear-gradient(180deg, #D49637 0%, #7C4B00 100%) 0% 0% no-repeat padding-box !important",
    },
  },
  TopButtonLogout: {
    width: "146px",
    height: "37px",
    background:
      "transparent linear-gradient(180deg, #b0b0b0 0%, #585858 100%) 0% 0% no-repeat padding-box !important",
    borderRadius: "5px",
    fontSize: "13.5px",
    lineHeight: "30px",
    fontWeight: "bolder",
    letterSpacing: "0px",
    textTransform: "uppercase",
    "&:hover": {
      background:
        "transparent linear-gradient(180deg, #b0b0b0 0%, #585858 100%) 0% 0% no-repeat padding-box !important",
    },
    marginLeft: "3px",
  },
  title: {
    color: theme.palette.text.primary,
    fontSize: "19px",
    fontWeight: "bolder",
  },
  formBlock: {
    paddingTop: "80px",
    paddingBottom: "80px",
    width: "902px",
    alignSelf: "center",
    // gap: '29px',
  },
  row: {
    marginTop: "8px",
    marginBottom: "8px",
  },
  label: {
    fontSize: "18px",
    fontWeight: "bolder",
    color: theme.palette.text.primary,
  },
  button: {
    width: "386px",
    borderRadius: "5px",
    fontWeight: "bolder",
    marginTop: "35px",
    // fontSize: '20px',
    // lineHeight: '30px'
  },
  bottomLine: {
    paddingBottom: "48px",
    borderBottom: `1px solid ${theme.palette.text.primary}`,
  },
  remarkRequired: {
    color: theme.palette.primary.light,
    wordSpacing: "0.28px",
    fontSize: "11px",
    lineHeight: "26px",
  },
  remarkDate: {
    color: "#b0b0b0",
    wordSpacing: "0.28px",
    fontSize: "11px",
    lineHeight: "26px",
  },
  remarkTransparent: {
    color: "transparent",
    wordSpacing: "0.28px",
    fontSize: "11px",
    lineHeight: "26px",
  },
  required: {
    "&::after": {
      content: "'*'",
      color: theme.palette.primary.light,
    },
  },
  otpBlock: {
    paddingTop: "82px",
    paddingBottom: "119px",
    borderBottom: `1px solid ${theme.palette.primary.light}`,
    width: "100%",
    alignItems: "center",
  },
  otpSMSBlock: {
    paddingTop: "50px",
    paddingBottom: "50px",
    // marginTop: '80px',
    // marginBottom: '80px',
    borderBottom: `1px solid ${theme.palette.primary.light}`,
  },

  optButtons: {
    padding: "30px",
    width: "100%",
    // border: '1px solid #000'
  },
  otpButton: {
    width: "293px",
    borderRadius: "5px",
    fontWeight: "bolder",
  },

  accountInfoColumn: {
    marginLeft: "20px",
    marginRight: "20px",
  },
  accountInfoValue: {
    fontSize: "18px",
    color: theme.palette.primary.light,
    margin: "10px",
  },
  accountInfoKey: {
    fontSize: "18px",
    color: theme.palette.text.primary,
    margin: "10px",
  },
  operationButtons: {
    padding: "30px",
    width: "100%",
  },
  operationButton: {
    width: "293px",
    height: "90px",
    borderRadius: "5px",
    fontWeight: "bolder",
    margin: "5px",
    whiteSpace: "normal",
  },
  operationButtonBig: {
    width: "293px",
    height: "190px",
    borderRadius: "5px",
    fontWeight: "bolder",
    margin: "5px",
    whiteSpace: "normal",
  },
}));

export { useStyles, useProfileStyles };
