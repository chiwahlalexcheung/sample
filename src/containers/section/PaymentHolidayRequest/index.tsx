import { InnerPage } from "@containers/layout/Heading";
import {
  apiPost,
  checkApplicationOwner,
  checkUserInfo,
} from "@containers/section/AccountCommon/context";
import { HeadingWithSubTitle } from "@containers/section/StepSolution";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React, { FC, useEffect } from "react";
import { HolidayForm } from "./form";
// import {
//   RefControlProps,
// } from '@types/common/common.d'
import { useStyles } from "./styles";
import RouteSources from "@config/routes";

export const PaymentHolidayRequest: FC<any> = ({ applicationNo }) => {
  const classes = useStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");

  /* check user and application here, also need to check whether he can apply holiday */
  const router = useRouter();
  const checkHoliday = async () => {
    const data = { acct_no: applicationNo, action: "V" };
    const result = await apiPost("paymentHoliday", data, {});
    if (!result || result["data"]["LOS"]["BODY"]["rtn_cde"] === "003") {
      router.push(RouteSources.accountProfile, RouteSources.accountProfile);
    }
  };

  useEffect(() => {
    checkUserInfo(router);
    checkApplicationOwner(router, applicationNo);
    checkHoliday();
  }, []);
  /* check user and application here */

  return (
    <div className={classes.root}>
      <InnerPage>
        <div className={classes.heading}>
          <HeadingWithSubTitle
            title={t("title.customerService")}
            subTitle={t("title.paymentHoliday")}
          />
        </div>
        <HolidayForm applicationNo={applicationNo} />
      </InnerPage>
    </div>
  );
};

export default PaymentHolidayRequest;
