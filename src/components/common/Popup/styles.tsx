import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    // background: '#FFFFFF 0% 0% no-repeat padding-box',
    // boxShadow: '0px 4px 20px #25265E1A',
    // borderRadius: '4px',
  },
  content: {
    padding: '0 37px 37px 37px',
    maxHeight: '500px',
  },
  title: {
    minHeight: '32px',
    paddingBottom: '10px',
  },
  body: {
    marginTop: '10px',
  },
  close: {
    position: 'absolute',
    right: '10px',
    top: '10px',
  }
}));

export {
  useStyles,
};
