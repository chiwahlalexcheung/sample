import CommonButton from "@components/common/Button";
import Link from "@components/common/Link";
import CommonPopup from "@components/common/Popup";
import FormElement from "@components/form";
import RouteSources from "@config/routes";
import ServiceConfig from "@config/service";
import { Agreements } from "@containers/section/FullStepSolution/form";
import {
  StepSolutionContext,
  useStepSolution,
} from "@containers/section/StepSolution";
import {
  KeyPairFormHandleProps,
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
} from "@hooks/common";
import {
  //   Container,
  Box,
  Grid,
  Typography,
} from "@material-ui/core";
import css from "clsx";
import { useTranslation } from "next-i18next";
import { useRouter } from "next/router";
import React, {
  createContext,
  FC,
  useCallback,
  useContext,
  useEffect,
  useState,
} from "react";
import { GoogleReCaptchaProvider } from "react-google-recaptcha-v3";
import { formatMoney, getLoanList, signOut } from "../AccountCommon/context";
import { apiPost } from "@containers/section/AccountCommon/context";

import { useHolidayStyles } from "./styles";

const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface ProfileProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const HolidayContext = createContext<Partial<ContextProps>>({});
export const useHoliday = () => useContext(HolidayContext) as ContextProps;
export const HolidayProvider: FC</*LogonProps*/ any> = ({
  values = {},
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  return (
    <HolidayContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </HolidayContext.Provider>
  );
};

export interface HolidayItemProps {
  key: string;
  component: JSX.Element;
}
export interface ProfileBarProps {
  list: HolidayItemProps[];
}

const UploadSuccessPopup = ({ shown }) => {
  const classes = useHolidayStyles();
  const { t } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const router = useRouter();
  return (
    <CommonPopup
      show={shown}
      afterClose={() => {}}
      maxWidth={"md"}
      noCloseBtn
      otherProps={{
        disableBackdropClick: true,
      }}
    >
      <Box
        display={"flex"}
        flexDirection={"column"}
        alignContent={"center"}
        alignItems={"center"}
        className={classes.popup}
      >
        <Typography className={classes.popupContent}>
          {t("label.thanksChangeEmailContent")}
        </Typography>
        <Box
          display={"flex"}
          width={1}
          justifyContent={"space-around"}
          className={classes.popupButtons}
        >
          <Link href={RouteSources.accountProfile} rel="start">
            <CommonButton
              className={classes.popupButton}
              label={bt("backToHome")}
              secondary
            />
          </Link>
        </Box>
      </Box>
    </CommonPopup>
  );
};

export const HolidayDetail: FC<any> = ({ applicationNo }) => {
  const classes = useHolidayStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useHoliday();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
    my code start here 
  */
  const router = useRouter();
  const [balance, setBalance] = useState("");
  const [applied, setApplied] = useState(false);

  const getBalance = async () => {
    const loanList = await getLoanList();
    loanList.forEach((l) => {
      if (l["acct_no"] === applicationNo) {
        setBalance(l["loan_ledg_bal"]);
      }
    });
  };
  useEffect(() => {
    getBalance();
  }, []);

  const onClickApply = async () => {
    if (watch()["agree"]) {
      const data = { acct_no: applicationNo, action: "R" };
      const result = await apiPost("paymentHoliday", data, {});
      if (result && result["data"]["LOS"]["BODY"]["rtn_cde"] === "000") {
        setApplied(true);
        alert(ft("message.paymentHolidayApplied"));
      }
    }
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.fillInRequestForm")}
        </Typography>
        <Typography className={classes.subtitle}>
          {ft("label.submittedAppliactionDetails")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={5}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.loanApplicationNo")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {applicationNo}
              </Typography>
            </Grid>
          </Grid>

          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={5}>
              <Typography component={"h4"} className={css(classes.labelBlack)}>
                {ft("label.accountOutstandingBalance")}
                {/* <span className={classes.require}>*</span> */}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={7}>
              <Typography component={"h4"} className={css(classes.labelRed)}>
                {`${formatMoney(balance)} (${ft(
                  "label.outstandingBalanceNotIncluded"
                )})`}
              </Typography>
            </Grid>
          </Grid>

          <Box display="flex" flexDirection="column" alignItems="flex-start">
            <Box
              display="flex"
              justifyContent="flex-start"
              alignItems="center"
              flexWrap="wrap"
            >
              <Typography className={classes.content}>
                {ft("label.pleaseClick")}
              </Typography>
              <Box className={classes.agreements}>
                <Agreements title={ft("label.here")} label={ft("label.here")}>
                  <div>test content</div>
                </Agreements>
              </Box>
              <Typography className={classes.content}>
                {ft("label.forTermsAndConditions") + "."}
              </Typography>
            </Box>

            <Box
              display="flex"
              justifyContent="flex-start"
              alignItems="center"
              flexWrap="wrap"
            >
              <FormElement.NativeCheckbox
                name={"agree"}
                control={control}
                className={classes.checkBox}
                inputProps={{ disabled: applied }}
              />
              <Typography className={classes.content}>
                {ft("note.confirmRead")}
              </Typography>
              <Box className={classes.agreements}>
                <Agreements
                  title={ft("label.customerDeclaration")}
                  label={ft("label.customerDeclaration")}
                >
                  <div>test content</div>
                </Agreements>
              </Box>
              <Typography className={classes.content}>
                {ft("note.and")}
              </Typography>
              <Box className={classes.agreements}>
                <Agreements
                  title={ft("label.privacyPolicy")}
                  label={ft("label.privacyPolicy")}
                >
                  <div>test content</div>
                </Agreements>
              </Box>
            </Box>
          </Box>

          <CommonButton
            className={classes.bottomButtonOrange}
            label={bt("confirmApply")}
            onClick={onClickApply}
            buttonProps={{ disabled: applied }}
          />
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
    </Box>
  );
};

export const HolidayForm: FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation("section");
  const classes = useHolidayStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);
  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <HolidayProvider values={{}}>
        <HolidayDetail applicationNo={applicationNo} />
      </HolidayProvider>
    </GoogleReCaptchaProvider>
  );
};
