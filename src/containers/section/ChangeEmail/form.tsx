import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";

import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";

import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useEmailStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import {
  apiGet,
  apiPost,
  checkUserInfo,
  checkApplicationOwner,
  signOut,
  goProfile,
} from "../AccountCommon/context";
import { requestCalculationError } from "@store/content/actions";

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface ProfileProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const EmailContext = createContext<Partial<ContextProps>>({});
export const useEmail = () => useContext(EmailContext) as ContextProps;
export const EmailProvider: FC</*LogonProps*/ any> = ({
  values = {},
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  return (
    <EmailContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </EmailContext.Provider>
  );
};

export interface EmailItemProps {
  key: string;
  component: JSX.Element;
}
export interface ProfileBarProps {
  list: EmailItemProps[];
}

const UploadSuccessPopup = ({ shown, applicationNo }) => {
  const classes = useEmailStyles();
  const { t } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const router = useRouter();
  return (
    <CommonPopup
      show={shown}
      afterClose={() => {}}
      maxWidth={"md"}
      noCloseBtn
      otherProps={{
        disableBackdropClick: true,
      }}
    >
      <Box
        display={"flex"}
        flexDirection={"column"}
        alignContent={"center"}
        alignItems={"center"}
        className={classes.popup}
      >
        <Typography className={classes.popupContent}>
          {t("label.thanksChangeEmailContent")}
        </Typography>
        <Box
          display={"flex"}
          width={1}
          justifyContent={"space-around"}
          className={classes.popupButtons}
        >
          <CommonButton
            className={classes.popupButton}
            label={bt("backToHome")}
            secondary
            onClick={() => goProfile(router, applicationNo)}
          />
        </Box>
      </Box>
    </CommonPopup>
  );
};

export const EmailDetail: FC<any> = ({ applicationNo }) => {
  const classes = useEmailStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useEmail();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
    my code start here 
  */
  const router = useRouter();
  const [shown, setShown] = useState(false);

  const onClickChange = async () => {
    const newEmail = watch()["newEmail"];
    const confirmedEmail = watch()["confirmedEmail"];
    const reg = /^\S+@\S+\.\S+$/;
    if (!(reg.test(newEmail) && reg.test(confirmedEmail))) {
      alert(ft("message.emailBadFormat"));
      return;
    }
    if (newEmail !== confirmedEmail) {
      alert(ft("message.emailsNotMatch"));
      return;
    }
    // use API to update the email.

    const data = {
      acct_no: applicationNo,
      email: newEmail,
    };
    const result = await apiPost("internetInformationUpload", data, {});

    if (result && "data" in result) {
      const code = result["data"]["LOS"]["BODY"]["rtn_cde"];
      if (code === "004") {
        alert(ft("message.sameEmails"));
        return;
      } else if (code === "000") {
        setShown(true);
      }
    }
  };
  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={() => signOut(router)}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.selectEnterInformation")}
        </Typography>
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <Grid container spacing={3} className={classes.row}>
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelRed, classes.required)}
              >
                {ft("label.newEmailAddress")}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormElement.TextInput
                control={control}
                name="newEmail"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>

          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          >
            <Grid item xs={12} sm={6}>
              <Typography
                component={"h4"}
                className={css(classes.labelRed, classes.required)}
              >
                {ft("label.confirmedEmailAddress")}
                {/* <span className={classes.require}>*</span> */}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <FormElement.TextInput
                control={control}
                name="confirmedEmail"
                type="text"
                inputProps={{
                  placeholder: ft("placeholder.pleaseEnter"),
                }}
              />
            </Grid>
          </Grid>
          <Box
            display={"flex"}
            flexDirection={"row"}
            justifyContent={"space-between"}
            className={classes.optButtons}
          >
            <CommonButton
              className={classes.bottomButtonRed}
              label={bt("back")}
              onClick={() => goProfile(router, applicationNo)}
            />

            <CommonButton
              className={classes.bottomButtonOrange}
              label={bt("changeNow")}
              onClick={onClickChange}
            />
          </Box>
        </Box>

        <Typography className={classes.remarkRequired}>
          {ft("note.required")}
        </Typography>
      </Box>
      <UploadSuccessPopup shown={shown} applicationNo={applicationNo} />
    </Box>
  );
};

export const EmailForm: FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation("section");
  const classes = useEmailStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <EmailProvider values={{}}>
        <EmailDetail applicationNo={applicationNo} />
      </EmailProvider>
    </GoogleReCaptchaProvider>
  );
};
