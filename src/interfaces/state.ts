export interface StateProps {
  loading: boolean;
  error?: any;
}
export interface BasicListStateProps extends StateProps{
  list: any[];
}

export interface BasicInfoStateProps extends StateProps{
  info: any;
}
export interface BasicInfoWithSuccessStateProps extends BasicInfoStateProps{
  succeed: boolean;
}

