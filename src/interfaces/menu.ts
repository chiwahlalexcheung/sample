import {
  TypographyProps,
} from '@interfaces/text'

interface MenuListItem {
  label: string;
  isActive?: boolean;
  link: string;
  key: string;
  labelProps?: TypographyProps;
};
export default MenuListItem 
// declare module "*.menu" {
//   interface MenuListItem {
//     label: string;
//     isActive?: boolean;
//     link: string;
//     key: string;
//     labelProps?: TypographyProps;
//   }
//   export default MenuListItem 
// }

