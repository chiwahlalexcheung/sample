import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: 'white !important',
        background: '#FFFFFF 0% 0% no-repeat padding-box',
    },
    left: {
        width: '50%',
        padding: '54px 73px 65px 133px',
    },
    right: {
        width: '50%',
    },
    heading: {
        marginBottom: '20px',
    },
}));

const useBoxStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        padding: 67,
    },
}))

const useAccordionStyles = makeStyles(theme => ({
    root: {
        paddingLeft: '46px',
    },
    summary: {
        height: '56px',
    }

}))

const useInfoStyles = makeStyles(theme => ({
    root: {
        padding: '20px 0'
    },
    infoIcon:{
        width: '67px',
        height: '62px',
        marginRight: '15px',
    },
    infoTitle: {
        color: theme.palette.primary.light,
        fontSize: '22px',
        textDecorationLine: 'underline'
    },
    infoBody: {
        color: theme.palette.text.primary,
        fontSize: '18px',
        fontWeight: 'bolder',
        lineHeight: '25px',
        wordSpacing: '0.36px'
    },
    infoBlock: {
        margin: '10px 0'
    },
    branchBlock: {

    }

}))
export {
  useStyles,
  useBoxStyles,
  useAccordionStyles,
  useInfoStyles
};
