import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  left: {
    
  },
}));

const useBoxStyles = makeStyles(theme => ({
  left: {
    width: '45%',
    paddingLeft: '143px',
    paddingRight: '20px',
  },
  banner: {
    flex: 1,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
  },
  description: {
    marginTop: '50px',
    fontSize: '18px',
    fontFamily: 'Roboto',
    marginBottom: '20px',
    letterSpacing: '0.9px',

    color: theme.palette.text.secondary
  },
  tips: {
    color: theme.palette.primary.main,
    fontWeight: 'bolder',
    fontSize: '16px',
    marginTop: '20px',
    marginBottom: '30px'
  },
  button: {
    width: '90%',
    marginBottom: '23px',
    // fontSize: '22px',
    fontFamily: 'Roboto',
    // borderRadius: '5px'
  }
}));

const useRootStyles = makeStyles(theme => ({
    root: {
    //   backgroundImage: `url(${StaticSources.banner.revolvingLoanBanner})`,
      padding: '60px 0 121px 0',
      ...FullCoverBgStyles,
    },
    smallBar: {
      height: '52px',
    },
    largeBar: {
      flex: 1,
    },
    topHeading: {
      marginBottom: '130px',
    },
    heading: {
      position: 'relative',
      '&::before': {
        content: "''",
        display: 'block',
        position: 'absolute',
        height: '6px',
        width: '220px',
        bottom: '-15px',
        background: theme.palette.primary.main,
      },
    },
    title: {
      fontSize: '27px',
      fontWeight: 'normal',
      color: 'black',
    },
    subTitle: {
      fontSize: '29px',
      fontWeight: 'bolder',
      color: 'black',
      textTransform: 'uppercase'
    },
  }));

export {
  useStyles,
  useBoxStyles,
  useRootStyles,
};
