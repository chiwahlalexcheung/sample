import {
  createStore,
  applyMiddleware,
  combineReducers,
  AnyAction,
  Reducer,
} from 'redux'
import Immutable from 'immutable'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { createWrapper, MakeStore, HYDRATE } from 'next-redux-wrapper'

import { mediaReducer, MediaState } from './media/reducer'
import { contentReducer, ContentState } from './content/reducer'
import { commonReducer, CommonState } from './common/reducer'
import { applicationReducer, ApplicationState } from './application/reducer'
import { serviceReducer, ServiceState } from './service/reducer'

import Config from '@config/service'

export interface AppState {
  media: MediaState;
  content: ContentState;
  common: CommonState;
  application: ApplicationState;
  service: ServiceState;
}

const combinedReducers = combineReducers({
  media: mediaReducer,
  common: commonReducer,
  content: contentReducer,
  application: applicationReducer,
  service: serviceReducer,
})

const createMiddleware = () => {
  const middleware: any[] = [
    thunkMiddleware
  ]
  // console.log('>>>', typeof window);
  if (typeof window !== 'undefined') {
    
  //  && config.env === 'development') {
    middleware.push(createLogger({
      level: 'info',
      collapsed: true,
      stateTransformer: (state) => {
        const newState = {}

        for (const i of Object.keys(state)) {
          // @ts-ignore
          if (Immutable.Iterable.isIterable(state[i])) {
            newState[i] = state[i].toJS()
          } else {
            newState[i] = state[i]
          }
        }

        return newState
      }
    }));
  }
  return middleware
}

const reducer: Reducer<AppState, AnyAction> = (state, action) => {
  if (action.type === HYDRATE) {
    /* client state will be overwritten
     * by server or static state hydation.
     * Implement state preservation as needed.
     * see: https://github.com/kirill-konshin/next-redux-wrapper#server-and-client-state-separation
     */
    const nextState = {
      ...state,
      ...action.payload,
    }
    return nextState
  }
  return combinedReducers(state, action)
}

/**
 * initStore
 * Initialise and export redux store
 */
const initStore: MakeStore<AppState> = () => {
  const middleware = createMiddleware();
  return createStore(
    reducer,
    composeWithDevTools(applyMiddleware(...middleware))
  )
}

export const storeWrapper = createWrapper<AppState>(initStore)