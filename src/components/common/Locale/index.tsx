import { createContext, useCallback, useContext } from 'react';
import { useTranslation } from 'next-i18next'
import Config from '@config/service';
import { get, capitalize, isNull, isUndefined } from 'lodash';
// import { useUserLoginProfile } from './auth';
export interface LocalizedInspectorProps {
  target: any;
  field: string;
  defaultValue?: any;
};
export interface LocaleContextProps {
  t: Function;
  i18n: any;
  changeLanguage: Function;
  inspector: Function;
}

const LocalizationContext = createContext({} as LocaleContextProps);
export const useLocale = () => {
  const context: LocaleContextProps = useContext(LocalizationContext);
  return context;
}
export const LocalizationProvider = ({ ns, children }) => {
  const { t, i18n } = useTranslation(ns);
  const changeLanguage = useCallback(language => {
    i18n.changeLanguage(language);
  }, [ i18n ]);
  // get the target value against localized structure
  // example: contentHk, contentCn
  const inspector = useCallback(({ target, field, defaultValue }: LocalizedInspectorProps) => {
    let camelLanguage: string = capitalize(i18n.language);
    let value: any = get(target, `${field}${camelLanguage}`);
    if (isUndefined(value) || isNull(value)) {
      camelLanguage = capitalize(Config.locale);
      value = get(target, `${field}${camelLanguage}`);
    }
    return !isUndefined(value) ? value : defaultValue;
  }, [ i18n.language ]);
  return (
    <LocalizationContext.Provider value={{ t, i18n, changeLanguage, inspector }}>
      {children}
    </LocalizationContext.Provider>
  );
};
