import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  input: {
  },
  sliderRoot: {
    color: theme.palette.text.primary,
    height: '4px',
  },
  sliderRail: {
    height: '4px',
  },
  sliderTrack: {
    height: '4px',
  },
  interest: {
    background: 'transparent linear-gradient(1deg, #DC1718 0%, #7F0D0E 100%) 0% 0% no-repeat padding-box',
  },
  principal: {
    background: 'transparent linear-gradient(1deg, #3DAAE8 0%, #073F8C 100%) 0% 0% no-repeat padding-box',
  },
  revolving: {
    background: 'transparent linear-gradient(1deg, #D49636 0%, #B38D47 100%) 0% 0% no-repeat padding-box',
  },
}));

export {
  useStyles,
};
