import { makeStyles } from "@material-ui/core/styles";
import StaticSources from "@config/sources";
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from "@theme/global";
import { Colors } from "@theme/global";
const { lightGrey } = Colors;

const useStyles = makeStyles((theme) => ({
  root: {
    background: "#F9FBFC",
    padding: "53px 0 53px 0",
  },
  heading: {
    marginBottom: "70px",
  },
}));

const useFormStyles = makeStyles((theme) => ({
  root: {
    margin: "30px 0 30px 0",
    maxWidth: "1153px",
  },
  form: {
    padding: "36px 73px 36px 73px",
    // border: '1px solid #000',
    boxShadow: "0px 6px 20px #00000033",
    background: "#fff",
  },
  TopButtonHome: {
    width: "146px",
    height: "37px",
    background:
      "transparent linear-gradient(180deg, #D49637 0%, #7C4B00 100%) 0% 0% no-repeat padding-box !important",
    borderRadius: "5px",
    fontSize: "13.5px",
    lineHeight: "30px",
    fontWeight: "bolder",
    letterSpacing: "0px",
    textTransform: "uppercase",
    "&:hover": {
      background:
        "transparent linear-gradient(180deg, #D49637 0%, #7C4B00 100%) 0% 0% no-repeat padding-box !important",
    },
  },
  TopButtonLogout: {
    width: "146px",
    height: "37px",
    background:
      "transparent linear-gradient(180deg, #b0b0b0 0%, #585858 100%) 0% 0% no-repeat padding-box !important",
    borderRadius: "5px",
    fontSize: "13.5px",
    lineHeight: "30px",
    fontWeight: "bolder",
    letterSpacing: "0px",
    textTransform: "uppercase",
    "&:hover": {
      background:
        "transparent linear-gradient(180deg, #b0b0b0 0%, #585858 100%) 0% 0% no-repeat padding-box !important",
    },
    marginLeft: "3px",
  },
  title: {
    color: theme.palette.text.primary,
    fontSize: "19px",
    fontWeight: "bolder",
  },
  formBlock: {
    paddingTop: "45px",
    paddingBottom: "80px",
    width: "702px",
    alignSelf: "center",
    // gap: '29px',
  },
  row: {
    marginTop: "25px",
    marginBottom: "25px",
  },
  label: {
    fontSize: "18px",
    fontWeight: "bolder",
    color: theme.palette.text.primary,
  },
  labelRed: {
    color: theme.palette.primary.light,
  },
  button: {
    width: "386px",
    borderRadius: "5px",
    fontWeight: "bolder",
    marginTop: "35px",
    // fontSize: '20px',
    // lineHeight: '30px'
  },
  bottomLine: {
    borderBottom: `1px solid ${theme.palette.text.primary}`,
  },
  remark: {
    color: theme.palette.primary.light,
    wordSpacing: "0.28px",
    fontSize: "11px",
    lineHeight: "26px",
  },
  required: {
    "&::after": {
      content: "'*'",
      color: theme.palette.primary.light,
    },
  },
  otpBlock: {
    paddingTop: "82px",
    paddingBottom: "119px",
    borderBottom: `1px solid ${theme.palette.primary.light}`,
    width: "100%",
    alignItems: "center",
  },
  otpSMSBlock: {
    paddingTop: "50px",
    paddingBottom: "50px",
    // marginTop: '80px',
    // marginBottom: '80px',
    borderBottom: `1px solid ${theme.palette.primary.light}`,
  },
  otpTitle: {
    fontSize: "18px",
    fontWeight: "bolder",
    color: theme.palette.primary.light,
  },
  otpSubtitle: {
    fontSize: "18px",
    fontWeight: "bolder",
    color: theme.palette.text.primary,
  },
  optButtons: {
    padding: "30px",
    width: "100%",
    // border: '1px solid #000'
  },
  bottomButtonRed: {
    width: "293px",
    borderRadius: "5px",
    fontWeight: "bolder",
  },
  bottomButtonOrange: {
    width: "293px",
    borderRadius: "5px",
    fontWeight: "bolder",
    background:
      "transparent linear-gradient(180deg, #D49637 0%, #7C4B00 100%) 0% 0% no-repeat padding-box !important",
  },

  popupButtons: {
    width: "500px",
  },
  popupButton: {
    width: "223px",
    height: "63px",
    fontWeight: "bolder",
  },
  popup: {
    padding: "30px 10px",
  },

  popupTitle: {
    padding: "30px 50px",
    fontSize: "20px",
    fontWeight: "bolder",
    textAlign: "center",
  },
  popupContent: {
    padding: "30px 50px",
    fontSize: "20px",
    textAlign: "center",
  },
}));

const useUploadStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flex: 1,
    flexDirection: "column",
    width: "100%",
    paddingBottom: "40px",
    marginBottom: "26px",
    borderBottom: `1px solid ${theme.palette.primary.light}`,
  },
  row: {
    width: "100%",
  },
  fileNameLabel: {
    flex: 1,
    fontSize: "14px",
    fontWeight: "bolder",
    color: theme.palette.primary.light,
    // textOverflow: 'ellipsis',
  },
  fileSizeLabel: {
    flex: 1,
    fontSize: "11px",
    wordSpacing: "0.28px",
    color: theme.palette.primary.light,
  },
  browseButton: {
    background:
      "transparent linear-gradient(180deg, #0F0000 0%, #780D0E 100%) 0% 0% no-repeat padding-box !important",
    color: "#fff",
    height: "25px",
    fontSize: "13px",
    fontWeight: "bolder",
  },
  icon: {
    marginLeft: "6px",
    width: "33px",
    height: "33px",
    background: "#fff",
    padding: "0px",
  },
}));

export { useStyles, useFormStyles, useUploadStyles };
