import { useCallback } from 'react'
import { set, forEach, map, join } from 'lodash'
import { useSelector, useDispatch } from 'react-redux'
import { submitApplication, sendOTP, verifyOtp, fetchEvaluation } from './actions'
import { AppState } from './../store'
import {
  usePrevious,
} from '@hooks/common'

export interface FormNormalizationProp {
  name: string;
  field?: string;
  inspector?: Function;
  defaultValue?: any;
}
const grpCodeLookup: any = {
  first: '11',
  second: '21',
};
const proposalLookup: FormNormalizationProp[] = [
  {
    name: 'loanType',
    inspector: (type: string) => ({
      'prod_cat_cde': 'MG',
      'prod_grp_cde': grpCodeLookup[type] || '11',
    }),
  },
  {
    name: 'appliedAmount',
    inspector: (amount: string) => ({
      'apply_amt': Number(amount) || 0,
    }),
  },
  {
    name: 'creditLimit',
    inspector: (amount: string) => ({
      'credit_lmt': Number(amount) || 0,
    }),
  },
  {
    name: 'loanTenor',
    // field: 'apply_tnr',
    inspector: (amount: string) => ({
      'apply_tnr': Number(amount) || 0,
    }),
  },
  {
    name: 'bestContactTime',
    field: 'remark',
  },
];
const customerLookup: FormNormalizationProp[] = [
  {
    name: 'hkId',
    field: 'id_no',
  },
  {
    name: 'title',
    field: 'title',
  },
  {
    name: 'name',
    field: 'cust_nam',
  },
  {
    name: 'email',
    field: 'email_addr',
  },
];
const collateralLookup: FormNormalizationProp[] = [
  {
    name: 'propertyPrice',
    field: 'val_price',
  },
  {
    name: 'address',
    field: 'address',
  },
];

const normalizationLookups: any = {
  proposal: proposalLookup, 
  customer: customerLookup, 
  collateral: collateralLookup,
};
export const useApplicationForm = () => {
  const dispatch = useDispatch();
  const { info, loading, error } = useSelector(({ application }: AppState) => application.form);
  const wasSubmitting = usePrevious(loading);
  const normalization = useCallback((formValues: any) => {
    let payload: any = {};
    forEach(normalizationLookups, (lookup: any[], setName: string) => {
      forEach(lookup, ({ name, inspector, field, defaultValue }: FormNormalizationProp) => {
        const targetValue = formValues[name] || defaultValue || '';
        const value = inspector ? inspector(targetValue) : field ? { [field]: targetValue } : {};
        payload = {
          ...payload,
          [setName]: {
            ...payload[setName],
            ...value,
          },
        };
      });
    });
    payload = {
      ...payload,
      phone: [
        {
          typ: 'M',
          ctry: '852',
          no: formValues.mobileNo,
        },
      ],
    }
    return payload;
  }, []);
  const submit = useCallback((formValues: any) => {
    const payload: any = normalization(formValues);
    // console.log(payload);
    dispatch(submitApplication(payload));
  }, [dispatch, normalization]);
  return {
    submit,
    submitting: loading,
    submitError: error,
    form: info,
    beingSubmitted: wasSubmitting && !loading,
  };
}
const basicPayloadWithAccount = {
  "iss_ctry":"HKG",
  "id_typ": "I",
  "code": "+852",
}
export const useOTP = () => {
  const dispatch = useDispatch();
  const { info: optInfo, loading: sending, error: sendError } = useSelector(({ application }: AppState) => application.otp);
  const { info: verifyInfo, succeed: verificationSucceed, loading: verifying, error: verifyOtpError } = useSelector(({ application }: AppState) => application.verifyOtp);
  const wasVerified = usePrevious(verifying);
  const sendCode = useCallback((formValues: any) => {
    const payload: any = {
      ...basicPayloadWithAccount,
      "id_no": formValues.hkId,
      "mobile": formValues.mobileNo,
      "reCaptchaToken": formValues.reCaptchaToken,
    };
    dispatch(sendOTP(payload));
  }, [dispatch]);
  const verifyCode = useCallback(async(formValues: any) => {
    const payload: any = {
      ...basicPayloadWithAccount,
      "id_no": formValues.hkId,
      "mobile": formValues.mobileNo,
      "token": optInfo.token,
      "otp": join(formValues.sms, ''),
    };
    // console.log(payload);
    return dispatch(verifyOtp(payload));
  }, [dispatch, optInfo]);
  
  return {
    verifyCode,
    passVerified: wasVerified && !verifying && verificationSucceed,
    failVerified: wasVerified && !verifying && !verificationSucceed,
    sendCode,
    optInfo,
    sending,
    sendError,
    verificationSucceed,
    verifyInfo,
    verifying,
    verifyOtpError,
  };
}



export const useEvaluation = () => {
    const dispatch = useDispatch();
    const { info, loading, error } = useSelector(({ application }: AppState) => application.evaluation);
    
    
    const onValueChange = useCallback(async(type: string, value: string, watch: any) => {
        
        let payload: any = {};
        switch(type)
        {
            case "area":
                payload['area'] = watch['area'];
                return await Promise.all([
                    dispatch(fetchEvaluation(false, 'district', payload )),
                ]);
            case "district":
                payload['district'] = watch['district'];
                return await Promise.all([
                    dispatch(fetchEvaluation(false, 'buildings', payload )),
                    dispatch(fetchEvaluation(false, 'estate', payload )),
                    dispatch(fetchEvaluation(false, 'streetName', payload ))
                ]);
            case "streetName":
                payload['street'] = watch['streetName'];
                return await Promise.all([
                    dispatch(fetchEvaluation(false, 'streetNo', payload )),
                ]);
            case "estate":
                payload['estate'] = watch['estate'];
                return await Promise.all([
                    dispatch(fetchEvaluation(false, 'block', payload )),
                    dispatch(fetchEvaluation(false, 'floor', payload )),
                    dispatch(fetchEvaluation(false, 'flatRoom', payload )),
                    dispatch(fetchEvaluation(false, 'carpark', payload )),
                ]);

            default:
                return;
        }
    }, [dispatch ]);
    return {
        info,
        loading, 
        error,
        onValueChange
    }

}