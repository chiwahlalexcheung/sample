import { makeStyles } from '@material-ui/core/styles';
// import StaticSources from '@config/sources';
// import {
//   FullCoverBgStyles,
//   // RelativeBefore,
// } from '@theme/global';
import {
  Colors,
} from '@theme/global';
const{
  lightGrey
} = Colors;

const useStyles = makeStyles(theme => ({
  root: {
    background: 'white',
  },
}));
const useSmallToolStyles = makeStyles(theme => ({
  wrapper: {
    backgroundColor: lightGrey,
    width: '376px',
    height: '200px',
    padding: '30px',
  },
  root: {
  },
  block: {
    zIndex: 1,
  },
  icons: {
    width: '100%',
  },
  title: {
    width: '158px',
    fontSize: '23px',
    fontWeight: 'bolder',
    textAlign: 'left',
    marginBottom: '10px',
    lineHeight: '30px',
  },
  button: {
    width: '148px',
    height: '35px',
    fontSize: '11px',
  },
  icon: {
    width: '194px',
    height: '194px',
  },
  dim: {
    backgroundColor: `${theme.palette.text.disabled} !important`,
  },
}));
const useBigToolStyles = makeStyles(theme => ({
  root: {
  },
  wrapper: {
    backgroundColor: lightGrey,
    width: '376px',
    height: '371px',
    padding: '30px',
  },
  icons: {
    width: '100%',
    zIndex: 0,
  },
  block: {
    marginTop: '-50px',
    zIndex: 1,
  },
  title: {
    fontSize: '26px',
    fontWeight: 'bolder',
    textAlign: 'left',
    marginBottom: '10px',
    lineHeight: '30px',
  },
  button: {
    width: '148px',
    height: '35px',
    fontSize: '11px',
    fontWeight: 700,
    borderRadius: '5px',
  },
  icon: {
    width: '243px',
    height: '243px',
  },
  dim: {
    backgroundColor: `${theme.palette.text.disabled} !important`,
  },
}));
const useBoxStyles = makeStyles(theme => ({
  left: {
    width: '35%',
    paddingLeft: '0',
    paddingRight: '30px',
  },
  banner: {
    flex: 1,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center center',
  },
  tool: {
    width: '376px',
  },
  title: {
    marginBottom: '30px',
    fontSize: '38px',
    maxWidth: '370px',
    lineHeight: '45px',
    textTransform: 'uppercase',
    fontWeight: 'bolder',
    color: theme.palette.primary.light,
  },
  buttons: {
    gap: '10px',
    width: '100%',
  },
  button: {
    borderRadius: '5px',
    flex: 1,
    fontWeight: 700,
  },
  description: {
    fontSize: '16px',
    marginBottom: '30px',
    color: 'black'
  },
  label: {
    color: theme.palette.primary.main,
    textDecoration: 'underline',
    fontSize: '18px',
    marginTop: '34px',
    textTransform: 'uppercase',
    fontWeight: 700,
  },
}));

export {
  useStyles,
  useBoxStyles,
  useSmallToolStyles,
  useBigToolStyles,
};
