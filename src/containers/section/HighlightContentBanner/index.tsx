import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Card,
  CardContent,
  // CardContent,
} from '@material-ui/core';
import {
  useStyles,
  useBoxStyles,
  useCardStyles
} from './styles';
import {
  useStepper,
  StepHandleProps,
} from '@hooks/common';


export interface AdvantagesCardProps {
    title: string;
    subTitle: string;
    advantages: string[];
}

export interface AdvantagesCardsProps {
  item: AdvantagesCardProps;
}
  
export const AdvantagesCard: FC<AdvantagesCardsProps> = ({ item }) => {
  const classes = useBoxStyles();
  const { t } = useTranslation('button');
  const {
      title,
      subTitle,
      advantages
  } :AdvantagesCardProps = item
  return (
    <Box 
        display={'flex'} 
        flexDirection={'column'} 
        className={classes.advantagesCard} 
        // alignItems={'center'}
        justifyContent={'center'}
    >
        <Typography className={classes.title}>{title}</Typography>
        <Typography className={classes.subTitle}>{subTitle}</Typography>
        <ul className={classes.list}>
            {map(advantages, (item)=>(
                <li className={classes.li}>{item}</li>
            ))}
        </ul>
    </Box>
  );

}
export interface HighlightCardProps {
    title: string;
    subTitle: string;
    icon: string;
    buttonLabel: string;
    buttonLink: string;
}
export interface HighlightCardsProps {
  item: HighlightCardProps;
}

export const HighlightCard: FC<HighlightCardsProps> = ({ item }) => {
    const classes = useCardStyles();
    const {
      title,
      subTitle,
      icon,
      buttonLabel,
      buttonLink
    }: HighlightCardProps = item;
    
    return (
        <Card className={classes.root}>
            <CardContent className={classes.content}>
                <Box display={'flex'} flexDirection={'column'} className={classes.wrapper}>
                    <Typography className={classes.title} >{title}</Typography>
                    <Typography className={classes.subTitle}>{subTitle}</Typography>
                    <Box
                        className={classes.image}
                        display={'flex'}
                        alignItems={'center'}
                        justifyContent={'center'}
                    >
                        <LocalImage className={classes.img} src={icon} alt={buttonLabel} />
                    </Box>
                    <Box
                        className={classes.button}
                        display={'flex'} 
                        alignItems={'center'}
                        justifyContent={'center'}
                    >
                        <CommonButton
                            label={buttonLabel}
                            // onClick={() => onDetail()}
                            className={classes.btn}
                        />
                    </Box>
                </Box>
            </CardContent>
    </Card>
    );
}

export const HighlightContentBanner: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const classes = useStyles();
  const cardLists: HighlightCardProps[] = [
    {
      title: 'Mortgage',
      subTitle: 'Loan Calculator',
      icon: StaticSources.calculator,
      buttonLabel: 'CHECK NOW',
      buttonLink: '/'
    },
    {
      title: 'Free Online',
      subTitle: 'Property Valuation Service',
      icon: StaticSources.valuationService,
      buttonLabel: 'VALUATE NOW',
      buttonLink: '/'
    }
  ];
  const advantagesInfo: AdvantagesCardProps = {
      title: 'Advantages of',
      subTitle: 'Mortgage-In-One Application',
      advantages: [
        'An integrated mortgage account to centralize First, Second and multiple Mortgages, Revolving Loan is also available',
        'Clear all credit card debts and outstanding personal loans',
        'Mortgage loan amount up to 80% of property’s valuation',
        'Simple and fast application procedures with exemption of stress test'
      ]
  }
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <Box display={'flex'} justifyContent={'space-between'}>
          <Box display={'flex'} className={classes.left} alignItems={'center'} justifyContent={'flex-start'} >
            <AdvantagesCard item={advantagesInfo} />
          </Box>   
          <Box display={'flex'} flexDirection={'row'} className={classes.right} alignItems={'center'} justifyContent={'center'} >
            {map(cardLists, (item: HighlightCardProps)=>(
                <HighlightCard item={item} />
            ))}
          </Box> 
        </Box>
      </InnerPage>
    </div>
  );
});

export default HighlightContentBanner;
