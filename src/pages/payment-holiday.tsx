import { useRef, useCallback } from 'react'
import { get } from 'lodash'
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import StaticSources from '@config/sources'
import MainStepSolution from '@containers/section/StepSolution'
import RevolvingBanner from '@containers/section/RevolvingBanner'
import HighlightContentBanner from '@containers/section/HighlightContentBanner'
import Footnotes from '@containers/section/Footnotes'
import HolidayHighlightBanner from '@containers/section/HolidayHighlightBanner'
/**
 * Revolving Loan Page
 */
const PaymentHolidayPage: React.FC = () => {
  const { t } = useTranslation('common');
  const banner = useRef<any>();
  const highlightContentRef = useRef<any>();
  const refs = {
    banner,
    highlightContentRef
  };
  // const {t} = useTranslation();

  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.payment-holiday')}</title>
        </Head>
        <RevolvingBanner bannerInfo={{
            title: 'Sun Hung Kai Mortgage',
            subTitle: 'Payment Holiday',
            description: 'Sun Hung Kai Credit is proud to introduce Hong Kong’s first* Mortgage Payment Holiday#– a scheme which allows you to choose to postpone any two monthly instalments every year during the loan period, so you can enjoy more flexibility and be better prepared for any unforeseen circumstances. What’s more, there’s no limit on the loan amount, no handling fee and no penalty charge! Why wait? Apply a new mortgage or transfer your existing one to Sun Hung Kai Credit today!',
            // tips: '3-STEP SIMPLE APPLICATION, APPLY NOW!',
            banner: StaticSources.banner.paymentHolidayBanner,
            buttons: [
                {
                    label: 'FIRST MORTAGE LOAN',
                    link: '/',
                    width: '70%'
                },
                {
                    label: 'SECOND MORTAGE LOAN',
                    link: '/',
                    width: '70%'
                }
            ]}
            } 
        />
        <HolidayHighlightBanner />
        <Footnotes />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
  props: {
    ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
  }
})
export default PaymentHolidayPage
