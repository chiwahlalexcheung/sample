import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { uploadDocument, resetUploadDocument } from './actions'
import { AppState } from './../store'
import { filter, get, size } from 'lodash'
// import { MediaState } from './reducers'

export interface uploadDocumentProps {
    appl_no: string;
    doc_cde: string;
    doc_name: string;
    file: any;
}

export const useUploadDocument = () => {
    const dispatch = useDispatch();
    const { list, loading, error } = useSelector(({ service }: AppState) => service.uploadDocument);
    
    
    const upload = useCallback((index: any, payload: any) => (
        dispatch(uploadDocument(index, payload))
    ), [dispatch]);
    
    const multipleUpload = useCallback((payloads: any) => {
        let result: any[] = [];
        payloads.map((item: any)=>(
            dispatch(uploadDocument(item.index, item.payload))
        ));
    }, [dispatch]);
    const uploadedSize = size(filter(list, function(o) { return get(o, 'LOS.BODY.rtn_cde') === "000"; }))
    const resetUpload  = useCallback(() => {
        dispatch(resetUploadDocument());
    }, [resetUploadDocument]);
    return {
        upload,
        multipleUpload,
        resetUpload, //: dispatch(()),
        list,
        loading,
        error,
        submitted: !loading && (uploadedSize === size(list)) && size(list)>0
    };
}


export const useContact = () => {
    const { list, loading, error } = useSelector(({ service }: AppState) => service.csContact);
    return {
        list,
        loading,
        error
    };
}

export const useGeneralContact = () => {
    const { info, loading, error } = useSelector(({ service }: AppState) => service.generalContact);
    return {
        info,
        loading,
        error
    };
}