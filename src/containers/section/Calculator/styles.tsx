import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
// import {
//   FullCoverBgStyles,
//   // RelativeBefore,
// } from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '100px 0',
  },
  heading: {
    marginBottom: '70px',
  },
}));

const useCalculatorTabStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    textAlign: 'center',
    color: theme.palette.text.secondary,
    fontWeight: 'normal',
    cursor: 'pointer',
    padding: '7px 0',
    fontSize: '18px',
    userSelect: 'none',
    textTransform: 'uppercase',
    '&:before': {
      content: "''",
      width: '0',
      transition: 'width 0.3s',
      display: 'block',
      position: 'absolute',
      height: '9px',
      bottom: '0',
    }
  },
  active: {
    cursor: 'default',
    color: 'black',
    fontWeight: 'bolder',
    '&::before': {
      width: '100%',
    }
  },
  interest: {
    '&::before': {
      background: '#CB1517 0% 0% no-repeat padding-box',
      
    }
  },
  principal: {
    '&::before': {
      background: '#1D79D0 0% 0% no-repeat padding-box',
    }
  },
  revolving: {
    '&::before': {
      background: '#CD9741 0% 0% no-repeat padding-box',
    }
  }
}));
const useCalculatorPlanHighlightsStyles = makeStyles({
  ul: {
    marginTop: '2px',
  },
  icon: {
    width: '20px',
    height: '20px',
  },
  li: {
    textShadow: '0px 3px 6px #00000029',
    paddingLeft: '20px',
    position: 'relative',
    listStyle: 'none',
    fontSize: '12px',
    fontStyle: 'italic',
    lineHeight: '30px',
    '&::before': {
      content: "''",
      width: '20px',
      height: '20px',
      left: '-20px',
      top: '3px',
      display: 'block',
      position: 'absolute',
      backgroundImage: (props: any) => `url(${props.checkIcon})`,
    },
  },
  interest: {
    color: '#CB1517',
  },
  principal: {
    color: '#1D79D0',
  },
  revolving: {
    color: '#CD9741',
  }
});
const useCalculatorPlanStyles = makeStyles({
  root: {
    marginTop: '-10px',
    opacity: 0.5,
    cursor: 'pointer',
    position: 'relative',
    width: '100%',
    borderRadius: '10px',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '0px 30px 60px #0000001A',
    // backgroundRepeat: 'no-repeat',
    // backgroundPosition: 'center center',
    // backgroundSize: 'contain',
    transform: 'perspective(220px) rotateX(5deg)',
    transition: 'transform 0.2s',
  },
  active: {
    opacity: 1,
    zIndex: 99,
    cursor: 'default',
    transform: 'perspective(220px) rotateX(0deg)',
    transition: 'transform 0.2s',
  },
  left: {
    width: '310px',
  },
  right: {
    flex: 1,
  },
  icon: {
    width: '31px',
    height: '31px',
    background: 'white',
    borderRadius: '50%',
  },
  subHeader: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: '14px',
  },
  title: {
    color: 'white',
    fontSize: '14px',
    fontWeight: 'bold',
    // textShadow: '0px 6px 0px #0000001A',
    textTransform: 'uppercase',
  },
  header: {
    flex: 1,
    alignItems: 'flex-start',
  },
  content: {
    padding: '0 30px',
    height: '55px',
  },
  amount: {
    color: 'white',
    fontSize: '27px',
    fontWeight: 'bold',
    letterSpacing: '0.27px',
    textShadow: '0px 6px 0px #0000001A',
    textAlign: 'right',
  },
  interest: {
    background: 'transparent linear-gradient(0deg, #EF191B 0%, #780D0E 100%) 0% 0% no-repeat padding-box',
    // backgroundImage: `url(${StaticSources.bg.interestOnlyCardBg})`,
  },
  principal: {
    background: 'transparent linear-gradient(0deg, #3FADEA 0%, #053989 100%) 0% 0% no-repeat padding-box',
    // backgroundImage: `url(${StaticSources.bg.principalCardBg})`,
  },
  revolving: {
    background: 'transparent linear-gradient(0deg, #E39B30 0%, #A08853 100%) 0% 0% no-repeat padding-box',
    // backgroundImage: `url(${StaticSources.bg.revolvingLoanCardBg})`,
  },
});

const useCalculatorFormStyles = makeStyles(theme => ({
  root: {
    width: '85%',
  },
  label: {
    fontSize: '14px',
    color: theme.palette.text.primary,
    marginTop: '12px',
    fontWeight: 600,
  },
  title: {
    color: theme.palette.primary.dark,
    fontSize: '16px',
    textTransform: 'uppercase',
    fontWeight: 600,
  },
  affordHeader: {
    margin: '38px 0 32px 0',
    display: 'flex',
    alignItems: 'center',
  },
  icon: {
    width: '27px',
    height: '26px',
    marginRight: '9px',
  },
}));

const useCalculatorSelectionStyles = makeStyles(theme => ({
  root: {
    border: `1px solid ${theme.palette.text.secondary}`,
    padding: 0,
  },
  item: {
    backgroundColor: `${theme.palette.text.disabled} !important`,
  },
  selected: {
    backgroundColor: 'white !important',
    cursor: 'default',
  },
  text: {
    fontSize: '13px',
    fontWeight: 'bold',
  },
  normalText: {
    fontWeight: 'normal',
    color: theme.palette.text.secondary,
  },
  icon: {
    width: '10px',
    height: '7px',
    minWidth: 'initial',
  },
}));

const useCalculatorStyles = makeStyles({
  root: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
    boxShadow: '0px 6px 20px #00000033',
    borderRadius: '9px',
    overflow: 'initial',
  },
  contact: {
    borderRadius: '5px',
    margin: '0 10px',
    fontWeight: 'bolder',
    padding: '10px 30px',
    background: 'transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box',
    '&:hover': {
      background: 'transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box',
    },
  },
  apply: {
    borderRadius: '5px',
    fontWeight: 'bolder',
    margin: '0 10px',
    padding: '10px 30px',
  },
  submit: {
    marginTop: '20px',
  },
  content: {
    padding: '30px 10px 30px 30px',
  },
  form: {
    position: 'relative',
  },
  left: {
    width: '40%',
  },
  right: {
    width: '58%',
  },
  summary: {
    width: 'fit-content',
    marginBottom: '50px',
    marginLeft: '10px',
  },
  tab: {
    position: 'absolute',
    top: '-70px',
    right: '-10px',
    width: '52%',
  },
  summaryTitle: {
    fontSize: '16px',
    fontWeight: 700,
    marginRight: '20px',
    lineHeight: 'normal',
    textTransform: 'uppercase',
  },
  summaryIcon: {
    width: '41px',
    height: '19px',
  },
});

export {
  useStyles,
  useCalculatorFormStyles,
  useCalculatorStyles,
  useCalculatorTabStyles,
  useCalculatorPlanStyles,
  useCalculatorPlanHighlightsStyles,
  useCalculatorSelectionStyles,
};