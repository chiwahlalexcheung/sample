import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    padding: '0 !important',
  },
  input: {
    padding: '12px',
  },
  inputRoot: {
    flex: 1,
  },
  prefix: {
    padding: '12px 10px',
    // position: 'absolute',
    // left: 0,
    // height: '41px',
    // top: 0,

    backgroundColor: theme.palette.text.secondary,
    fontWeight: 'bolder',
    color: 'white',
    fontSize: '13px',
    display: 'flex',
    alignItems: 'center',
  },
  suffix: {
    padding: '12px 10px',
    fontWeight: 600,
    color: 'white',
    background: theme.palette.text.secondary,
    fontSize: '11px',
    display: 'flex',
    alignItems: 'center',
  },
}));

const useSpecialStyles = makeStyles({
  interest: {
    background: 'transparent linear-gradient(1deg, #DC1718 0%, #7F0D0E 100%) 0% 0% no-repeat padding-box !important',
  },
  principal: {
    background: 'transparent linear-gradient(1deg, #3DAAE8 0%, #073F8C 100%) 0% 0% no-repeat padding-box !important',
  },
  revolving: {
    background: 'transparent linear-gradient(1deg, #D49636 0%, #B38D47 100%) 0% 0% no-repeat padding-box !important',
  },
});

export {
  useStyles,
  useSpecialStyles,
};
