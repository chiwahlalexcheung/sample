import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: 'white !important',
    background: '#FFFFFF 0% 0% no-repeat padding-box',
    // padding: '43px 75px',
    // backgroundImage: `url(${StaticSources.banner.revolvingHighlightBanner})`,
    // ...FullCoverBgStyles,
  },
}));
const useBoxStyles = makeStyles(theme => ({
    root: {
        flex: 1,
        // height: '100%',
        padding: 67,
        // background: '##F9FBFC 0% 0% no-repeat padding-box',
        // boxShadow: '2px 10px 6px #00000029',
    },
    footnote: {
        color : theme.palette.text.secondary,
        fontSize: '16px',
        lineHeight: '22px',
        letterSpacing: '0.4px'
    },
    title: {
      fontSize: '26px',
      color: theme.palette.text.primary,
      letterSpacing: '0px',
      fontWeight: 'bolder',
    },
    subTitle: {
      fontSize: '26px',
      textTransform: 'uppercase',
      color: theme.palette.primary.main,
      letterSpacing: '0px',
      fontWeight: 'bolder',
    },
    list: {
      paddingLeft: '20px',
      '@global': {
        'li': {
          marginBottom: '10px',
        },
      },
    },
    li: {
      paddingLeft: '10px',
      color: theme.palette.primary.light,
      position: 'relative',
      listStyle: 'none',
      fontSize: '18px',
      fontStyle: 'italic',
      lineHeight: '21px',
      wordSpacing: '-0.22px',
      '&::before': {
        content: "''",
        width: '20px',
        height: '20px',
        left: '-20px',
        top: '3px',
        display: 'block',
        position: 'absolute',
        backgroundImage: `url(${StaticSources.icon.advantagesCheck})`,
      },
    },
}))

const useCardStyles = makeStyles(theme => ({
    root: {
        // background: '#FFFFFF 0% 0% no-repeat padding-box',
        // boxShadow: '2px 10px 6px #00000029',
        borderRadius: '8px',
        width: 317,
        height: '100%',
        // height: 480,
        // flex: 1,
        // height: '100%',
        margin: 10,
        padding: 15,
        opacity: 1,
        backdropFilter: 'blur(50px)',
        justifyContent: 'center'

    },
    content: {
      padding: '0',
      height: '100%',
        alignSelf: 'center',
    //   background: '#00f'
    },
    title: {
        fontStyle: 'bold',
        fontSize: '22px',
        letterSpacing: '0px',
        color: theme.palette.text.primary,
    },
    subTitle: {
        fontStyle: 'bold',
        fontSize: '22px',
        letterSpacing: '0px',
        color: theme.palette.text.primary,

    },
    image: {
        flex : 1,
        // background: '#000'
    },
    img: {
        // flex: 1,
        // width: '100%'
    },
    button: {
        // background: '#f00'
    },
    btn: {
    },
    wrapper: {
       
        padding: '10px',
        height: '400px',
        // height: '100%', 
        // background: '#0f0'
    }

}))
export {
  useStyles,
  useBoxStyles,
  useCardStyles
};
