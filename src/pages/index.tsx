import { useRef, useCallback } from 'react'
import { get } from 'lodash'
import Head from 'next/head'
import { GetStaticProps } from 'next'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
import { storeWrapper } from '@store/store'
import { fetchSolutionRules } from '@store/common/actions'
import { fetchPersona, fetchCalculatorConfig } from '@store/content/actions'
import { fetchMainBanners } from '@store/media/actions'
import { fetchGeneralContact } from '@store/service/actions'
// import UtilHelper from '@lib/common'
// import Fade from 'react-reveal/Fade';
import MainStepSolution from '@containers/section/StepSolution'
import MainFullBanner from '@containers/banner/MainSlider'
import LoanCalculator from '@containers/section/Calculator'
import UsefulTool from '@containers/section/UsefulTool'
import Persona from '@containers/section/Persona'
import ContactUs from '@containers/section/ContactUs'
/**
 * Homepage
 */
const HomePage: React.FC = () => {
  const { t } = useTranslation('common');
  // TODO: do not use any in anywhere
  const banner = useRef<any>();
  const stepSolutionRef = useRef<any>();
  const usefulToolRef = useRef<any>();
  const loadCalculatorRef = useRef<any>();
  const personaRef = useRef<any>();
  const contactUsRef = useRef<any>();
  const refs = {
    banner,
    stepSolutionRef,
    usefulToolRef,
    loadCalculatorRef,
    contactUsRef,
    personaRef,
  };
  // const {t} = useTranslation();
  const slide = useCallback((target: string) => {
    const ref = get(refs, target, {});
    if (ref.current) {
      ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }, []);
  return (
    <HeadingLayout>
      <Head>
        <title>{t('info.appName')}</title>
      </Head>
      <MainFullBanner ref={banner} slide={slide} />
      <MainStepSolution ref={stepSolutionRef} slide={slide}  />
      <UsefulTool ref={usefulToolRef} slide={slide}  />
      <LoanCalculator ref={loadCalculatorRef} slide={slide}  />
      <Persona ref={personaRef} slide={slide}  />
      <ContactUs ref={contactUsRef} animation slide={slide}  />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = storeWrapper.getStaticProps(
  async ({ store, ...props }: any) => {
      const isServer = true;
      await Promise.all([
        store.dispatch(fetchSolutionRules(isServer)),
        store.dispatch(fetchPersona(isServer)),
        store.dispatch(fetchCalculatorConfig(isServer)),
        store.dispatch(fetchMainBanners(isServer)),
        store.dispatch(fetchGeneralContact(isServer)),
      ]);
      return {
        props: {
          ...await serverSideTranslations(props.locale, ['common', 'form', 'button', 'footer', 'section']),
        }
      };
  }
)

// export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
//   props: {
//     ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
//   }
// })
export default HomePage
