import { useRef } from "react";
import Head from "next/head";
import HeadingLayout from "@containers/layout/Heading";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from "next";
import AccountProfile from "@containers/section/AccountProfile";

const AccountProfilePage: React.FC<any> = ({ query }) => {
  const { t } = useTranslation("common");
  return (
    <HeadingLayout>
      <Head>
        <title>{t("menu.account-profile")}</title>
      </Head>
      <AccountProfile query={query} />
    </HeadingLayout>
  );
};

export const getStaticProps: GetStaticProps = async ({ locale = {} }: any) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        "common",
        "form",
        "button",
        "footer",
        "section",
      ])),
    },
  };
};

export default AccountProfilePage;
