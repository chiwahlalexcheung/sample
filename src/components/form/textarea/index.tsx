import React, { FC } from 'react'
// import Link from 'next/link'
import { Controller } from "react-hook-form";
import css from 'clsx'
// import {
//   Typography,
// } from '@material-ui/core'
import {
  FormRenderElementProps,
} from '@interfaces/form'
import {
  useStyles,
} from './styles'

export interface FormMultilineInputProps extends FormRenderElementProps{
  // TODO
  rows?: number;
}

const FormMultilineInput: FC<FormMultilineInputProps> = ({
  control,
  rows=10,
  name,
  inputProps={},
}) => {
  const classes = useStyles();
  const styles = classes.root;
  return (
    <div className={styles}>
      <Controller
        name={name}
        control={control}
        render={({ onChange }: any) => (
          <textarea
            rows={rows}
            onChange={(r: any) => onChange(r)}
            className={classes.textarea}
            {...inputProps}
          />
        )}
      />
    </div>
  );
}

export default FormMultilineInput;
