import React, { FC, forwardRef, useState, useEffect, useCallback } from 'react'
import { map, get } from 'lodash'
import Fade from 'react-reveal/Fade'
import Flash from 'react-reveal/Flash'
import Link from '@components/common/Link'
import CommonPopup from '@components/common/Popup'
// import { useRouter } from 'next/router'
import StaticSources from '@config/sources'
import { useForm } from "react-hook-form"
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from 'react-google-recaptcha-v3'
import {
  Done as DoneIcon,
} from '@material-ui/icons'
// import Routes from '@config/routes'
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import FormElement from '@components/form'
import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
import ServiceConfig from '@config/service'
// import Slide from 'react-reveal/Slide'
import { useGeneralContact } from '@store/service/context'
import { useLocale, LocaleContextProps } from '@components/common/Locale'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  CircularProgress,
  Grid,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useInfoStyles,
  useStyles,
  useInfosStyles,
  useFormStyles,
  useFormInfoStyles,
} from './styles';
const {
  googleReCaptcha: {
    siteKey: reCaptchaKey,
  },
} = ServiceConfig;

export interface ContactUsFormProps {
  pageType?: string;
  onCompleted?: Function;
}

export interface ReCaptchaBlockProps {
  token?: string;
  onToken: Function;
  page?: string;
  ts?: number;
}

export const ReCaptchaBlock: FC<ReCaptchaBlockProps> = ({ ts, page='contact_us', token, onToken }) => {
  const { executeRecaptcha } = useGoogleReCaptcha();
  const execute = useCallback(async() => {
    const token = await executeRecaptcha(page);
    onToken(token);
  }, [executeRecaptcha, onToken, page]);
  useEffect(() => {
    execute();
  }, [ts]);
  const { t } = useTranslation('section');
  const classes = useFormStyles();
  return (
    <Box display={'flex'} alignItems={'center'} className={classes.recap}>
      <LocalImage className={classes.recapImage} src={StaticSources.icon.reCaptcha} alt={t('remarks.autoReCaptcha')} />
      {
        !!token && <Flash>
          <Typography className={classes.recapText}>{t('remarks.autoReCaptcha')}</Typography>
        </Flash>
      }
      {
        !!token ? <DoneIcon className={classes.done} /> : 
        <CircularProgress size={20} className={classes.done} color={'secondary'} />
      }
    </Box>
  );
}

export const ContactUsForm: FC<ContactUsFormProps> = ({ onCompleted, pageType }) => {
  const [reCaptcha, setReCaptcha] = useState('');
  const { t } = useTranslation('section');
  const { t:Bt } = useTranslation('button');
  const { control } = useForm();
  const classes = useFormStyles()
  return (
    <Grid container spacing={1}>
      <Grid item xs={12} md={5}>
        <Typography component={'h2'} className={classes.label}>
          {t('label.name')}<sup>*</sup>
        </Typography>
        <FormElement.TextInput
          name="name"
          type="text"
          control={control}
        />
      </Grid>
      <Grid item xs={12} md={7}>
        <Typography component={'h2'} className={classes.label}>
          {t('label.email-single')}<sup>*</sup>
        </Typography>
        <FormElement.TextInput
          name="email"
          type="email"
          control={control}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography component={'h2'} className={classes.label}>
          {t('label.contactNumber')}<sup>*</sup>
        </Typography>
        <FormElement.TextInput
          name="contactNumber"
          type="text"
          control={control}
        />
      </Grid>
      <Grid item xs={12}>
        <Typography component={'h2'} className={classes.label}>
          {t('label.message')}<sup>*</sup>
        </Typography>
        <FormElement.TextMultilineInput
          name="message"
          control={control}
        />
      </Grid>
      <Grid item xs={12}>
        <div dangerouslySetInnerHTML={{ __html: t('remarks.required') }} className={classes.remark} />
        <ReCaptchaBlock token={reCaptcha} page={pageType} onToken={setReCaptcha} />
        <CommonButton className={classes.button} label={Bt('sendMessage')} />
      </Grid>
    </Grid>
  );
}

export const ContactUsFormBox: FC<any> = ({ onCompleted }) => {
  const { t } = useTranslation('section');
  const classes = useFormInfoStyles();
  const { inspector }: LocaleContextProps = useLocale();
  const { info } = useGeneralContact();
  const officeHours = inspector({
    target: info,
    field: 'businessHour',
    defaultValue: '',
  })
  const officeHourRemark = inspector({
    target: info,
    field: 'businessHourRemark',
    defaultValue: '',
  })
  return (
    <div>
      <div className={classes.header}>
        <Typography component={'h1'} className={classes.title}>
          {t('label.businessHours')}: {officeHours}
        </Typography>
        <div className={classes.remark}>
          {officeHourRemark}
        </div>
      </div>
      <div>
        <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
          <ContactUsForm onCompleted={onCompleted} />
        </GoogleReCaptchaProvider>
      </div>
    </div>
  );
}

export interface AddressProps {
  location: string;
  address: string;
  id: string; 
}
export interface AddressesProps {
  list: AddressProps[];
  onClick?: Function;
}

export const AddressesInfo: FC<AddressesProps> = ({ onClick, list }) => {
  const { t } = useTranslation('section');
  const classes = useInfoStyles();
  const onPress = useCallback((id: string) => {
    if (onClick) {
      onClick(id);
    }
  }, [onClick]);
  return (
    <Box display={'flex'}>
      <div className={classes.icon}>
        <LocalImage alt={t('label.address')} src={StaticSources.icon.addressInfo} />
      </div>
      <Box display={'flex'} flexDirection={'column'}>
        <Typography component={'h1'} className={classes.title}>
          {t('label.address')}
        </Typography>
        <Box display={'flex'} flexDirection={'column'} alignItems={'flex-start'} justifyContent={'flex-start'} className={classes.info}>
          {
            map(list, ({ location, id, address }: AddressProps) => (
              <div key={id} className={classes.address}>
                <Typography onClick={() => onPress(id)} component={'h2'} className={classes.subTitle}>
                  {location}
                </Typography>
                <div dangerouslySetInnerHTML={{ __html: address }} className={classes.description} />
              </div>
            ))
          }
        </Box>
      </Box>
    </Box>
  );
}

export interface BasicInfoItemProps {
  label?: string;
  value: string;
  id: string; 
  inspector?: Function;
}
export interface BasicInfoProps {
  list: BasicInfoItemProps[];
  onClick?: Function;
  title: string;
  icon: string;
}

export const BasicInfo: FC<BasicInfoProps> = ({ icon, title, onClick, list }) => {
  // const { t } = useTranslation('section');
  const classes = useInfoStyles();
  const onPress = useCallback((value: string, id: string) => {
    if (onClick) {
      onClick(value, id);
    }
  }, [onClick]);
  return (
    <Box display={'flex'}>
      <div className={classes.icon}>
        <LocalImage alt={title} src={icon} />
      </div>
      <Box display={'flex'} flexDirection={'column'} alignItems={'flex-start'} justifyContent={'flex-start'}>
        <Typography component={'h1'} className={classes.title}>
          {title}
        </Typography>
        <Box display={'flex'} flexDirection={'column'} alignItems={'flex-start'} justifyContent={'flex-start'} className={classes.info}>
          {
            map(list, ({ label, id, value, inspector }: BasicInfoItemProps) => (
              <Box key={id} display={'flex'} alignItems={'center'} justifyContent={'flex-start'} className={classes.info}>
                {
                  !!label && <Typography onClick={() => onPress(value, id)} component={'h2'} className={classes.inlineTitle}>
                    {label}:
                  </Typography>
                }
                <span className={classes.description}>
                  {inspector ? inspector(value) : value}
                </span>
              </Box>
            ))
          }
        </Box>
      </Box>
    </Box>
  );
}
export const ContactBasicEmailBox: FC = () => {
  const classes = useInfosStyles();
  const { t } = useTranslation('section');
  const { inspector }: LocaleContextProps = useLocale();
  const { info } = useGeneralContact();
  const emailInfo: BasicInfoItemProps[] = map(info.emails, (email, index)=>({
    value: email.email,
    id: index,
    inspector: (v: any) => (
      <Link
        href={`mailTo:${email.email}`}
        rel={'noopener noreferrer'}
        label={v}
        className={classes.linkLabel}
      />
    )
  }))
  return (
    <BasicInfo
      list={emailInfo}
      title={t('label.email')}
      icon={StaticSources.icon.emailInfo}
    />
  );
}
export const ContactBasicPhoneBox: FC = () => {
  const { t } = useTranslation('section');
  const { inspector }: LocaleContextProps = useLocale();
  const { info } = useGeneralContact();
  const phoneInfo: BasicInfoItemProps[] = map(info.numbers, (number, index)=>({
    id: index,
    label: inspector({
        target: number,
        field: 'name',
        defaultValue: '',
    }),
    value: number.number
  }))
  return (
    <BasicInfo
      list={phoneInfo}
      title={t('label.phoneAndFax')}
      icon={StaticSources.icon.phoneInfo}
    />
  );
}
export const ContactUsInfo: FC = () => {
  const { t } = useTranslation('section');
  const classes = useInfosStyles();
  const { inspector }: LocaleContextProps = useLocale();
  const onAddressClick = useCallback(() => {

  }, []);
  const { info } = useGeneralContact();
  const addressInfo: AddressProps[] =  map(info.addresses, (address, index)=>({
    id: index,
    location: inspector({
        target: address,
        field: 'name',
        defaultValue: '',
    }),
    address: inspector({
      target: address,
      field: 'address',
      defaultValue: '',
    })
  }))
  
  return (
    <Box display={'flex'} flexDirection={'column'}>
      <div className={classes.block}>
        <AddressesInfo list={addressInfo} onClick={onAddressClick} />
      </div>
      <div className={classes.block}>
        <ContactBasicPhoneBox />
      </div>
      <ContactBasicEmailBox />
    </Box>
  );
}
export interface ContactUsMainContentProps {
  animation: boolean;
  onCompleted?: Function;
}
export const ContactUsMainContent: FC<ContactUsMainContentProps> = ({ onCompleted, animation }) => {
  const classes = useStyles();
  return (
    <Box display={'flex'} justifyContent={'space-between'}>
      <Box display={'flex'} flexDirection={'column'} className={classes.left} alignItems={'flex-end'}>
          {
            animation ? <Fade left when={animation}>
              <div className={classes.info}>
                <ContactUsInfo />
              </div>
            </Fade> : <div className={classes.info}>
              <ContactUsInfo />
            </div>
          }
      </Box>
      <div className={classes.right}>
        {
          animation ? <Fade right when={animation}>
            <ContactUsFormBox onCompleted={onCompleted} />
          </Fade> : <ContactUsFormBox onCompleted={onCompleted} />
        }
      </div>
    </Box>
  );
}

export const ContactUs: FC<any> = forwardRef(({ onCompleted, animation=false }, r) => {
  const { t } = useTranslation('section');
  // const { t: Bt } = useTranslation('button');
  const classes = useStyles();
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <Box display={'flex'} className={classes.header}>
          {
            animation ? <Fade top>
              <div className={classes.heading}>
                <HeadingWithSubTitle
                  title={t('title.contactUsHeader')}
                  subTitle={t('title.contactUsSubTitle')}
                />
              </div>
            </Fade> : <div className={classes.heading}>
              <HeadingWithSubTitle
                title={t('title.contactUsHeader')}
                subTitle={t('title.contactUsSubTitle')}
              />
            </div>
          }
        </Box>
        <ContactUsMainContent onCompleted={onCompleted} animation={animation} />
      </InnerPage>
    </div>
  );
});

export const PopupContactUs: FC<any> = ({
  onClose, 
  enabled,
}) => {
  const whenClose = useCallback(() => {
    if (onClose) {
      onClose();
    }
  }, [onClose]);
  return (
    <CommonPopup
      show={enabled}
      maxWidth={'xl'}
      afterClose={whenClose}
      blockContentScroll={true}
      otherProps={{
        scroll: 'body',
        onBackdropClick: whenClose,
        onEscapeKeyDown: whenClose,
      }}
    >
      <ContactUs onCompleted={whenClose} />
    </CommonPopup>
  );
}

export default ContactUs;
