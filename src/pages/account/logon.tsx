import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import AccountLogon from '@containers/section/AccountLogon'

const AccountLogonPage: React.FC = () => {
  const { t } = useTranslation('common');
  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.customer-login')}</title>
        </Head>
        < AccountLogon />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
  props: {
    ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
  }
})
export default AccountLogonPage
