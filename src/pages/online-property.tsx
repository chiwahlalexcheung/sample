import { useRef, useCallback } from 'react'
import { get } from 'lodash'
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import StaticSources from '@config/sources'
import OnlineValuation from '@containers/section/OnlineValuation'
import OnlineValuationBanner from '@containers/section/OnlineValuationBanner'
import ContactUs from '@containers/section/ContactUs'
import { storeWrapper } from '@store/store'
import { fetchGeneralContact } from '@store/service/actions'
import { fetchEvaluation } from '@store/application/actions'

/**
 * Online Property Valuation Page
 */
const OnlinePropertyPage: React.FC = () => {
  const { t } = useTranslation('common');
  const banner = useRef<any>();
  const highlightContentRef = useRef<any>();
  // const {t} = useTranslation();

  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.online-property')}</title>
        </Head>
        <OnlineValuationBanner />
        <OnlineValuation />
        <ContactUs />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = storeWrapper.getStaticProps(
    async ({ store, ...props }: any) => {
        const isServer = true;
        await store.dispatch(fetchGeneralContact(isServer));
        await store.dispatch(fetchEvaluation(isServer, "area", {}));
        return {
          props: {
            ...await serverSideTranslations(props.locale, ['common', 'form', 'button', 'footer', 'section']),
          }
        };
    }
  )
  
// export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
//   props: {
//     ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
//   }
// })
export default OnlinePropertyPage
