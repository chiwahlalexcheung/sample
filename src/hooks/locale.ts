import { createContext, useCallback, useEffect, useContext } from 'react';
import { useTranslation } from '@locale';
import { get } from 'lodash';
import moment from 'moment-timezone';
import Config from '@config/service';
// import { useUserLoginProfile } from './auth';

const useMoment = (props={}) => {
  const { date, type='date' }: any = props;
  const { timezone=Config.timezone.hk, language }: any = {};
  // useUserLoginProfile();
  const getMomentLocale = useCallback(() => {
    return get(Config, `momentLocalization.${language}`, Config.momentLocalization.hk);
  }, [language]);
  useEffect(() => {
    moment.locale(getMomentLocale());
  }, []);
  const tsCM = useCallback((d?: any) => {
    // console.log(timezone);
    if (!date && !d) {
        return moment.tz(timezone).clone();
    }
    // console.log(moment(d || date), timezone, moment(d || date).tz(timezone));
    return moment(d || date).tz(timezone).clone();
  }, [date, timezone]);
  const isValidDate = useCallback((d?: any) => {
    return tsCM(d || date).isValid();
  }, [date, tsCM]);
  const getFormat = useCallback(() => {
    return get(Config, `timeFormat.${language}.${type}`, Config.timeFormat.hk.date);
  }, [language, type]);
  
  const ts = useCallback((d?: any) => {
    return moment().tz(timezone).clone();
  }, [date, timezone]);
  const formatWithDate = useCallback(date => {
    const f = getFormat();
    // console.log(tsCM(date).toISOString());
    if (date && isValidDate(date)) {
      return tsCM(date).format(f);
    }
    return null;
  }, [tsCM, getFormat, type, isValidDate]);
  const format = useCallback((d?: any) => {
    return formatWithDate(d || date);
  }, [formatWithDate, date]);
  const m = useCallback(() => {
    if (!date) {
        return moment().clone();
    }
    return moment(date).clone();
  }, [date]);
  const localeMFromDate = useCallback((d?: any) => {
    moment.locale(getMomentLocale());
    return tsCM(d);
  }, [tsCM, getMomentLocale]);
  const localeM = useCallback(() => {
    moment.locale(getMomentLocale());
    return tsCM(date);
  }, [date, tsCM, getMomentLocale]);
  const currentFormat = useCallback(() => {
    moment.locale(getMomentLocale());
    return format(tsCM());
  }, [format, getMomentLocale, tsCM]);
  const now = useCallback(() => {
    const f = getFormat();
    return tsCM().format(f);
  }, [tsCM, getFormat]);

  return {
    now,
    currentFormat,
    localeM,
    m,
    ts,
    getMomentLocale,
    format,
    tsCM,
    getFormat,
    isValidDate,
    formatWithDate,
    localeMFromDate,
  };
}


export {
  useMoment,
};
