import React, { FC, forwardRef, ReactNode, useCallback, useMemo, useState, useContext, createContext } from 'react'
import css from 'clsx';
import dynamic from 'next/dynamic'
import Pulse from 'react-reveal/Pulse'
// import Slider from 'react-slick';
import {
//   useStyles,
  useSlideStyles,
  useBackgroundStyles,
  useSliderStyles,
} from './styles';
import { useTranslation } from 'next-i18next'
import { get, map } from 'lodash';
import LazyImage from '@components/image/LazyImage';
import StaticSources from '@config/sources';
import CommonButton from '@components/common/Button';
import { useRouter } from 'next/router'
import { useMainBanner as useStoreMainBanner } from '@store/media/context';
import { useLocale, LocaleContextProps } from '@components/common/Locale';
import {
  Box,
  Typography,
//   Fade,
  Container,
} from '@material-ui/core'
import Fade from 'react-reveal/Fade'
import {
    // ToggleProps,
    // useToggle,
    useStepper,
    StepHandleProps,
} from '@hooks/common';
// import {
//     Skeleton,
// } from '@material-ui/lab';
import {
  BannerListItem,
} from '@interfaces/banner'
const Slider = dynamic(() => import('react-slick'), { ssr: false });
export type ContextProps = { 
  list: BannerListItem[],
  // interval?: number;
  index: number;
  setSelected: Function;
  selected?: BannerListItem;
  prevPage: Function;
  nextPage: Function;
};

export interface MainBannerProps{
    children: ReactNode;
    list: BannerListItem[],
    index?: number;
}

export const MainBannerContext = createContext<Partial<ContextProps>>({});
export const useMainBanner = () => useContext(MainBannerContext) as ContextProps;
export const MainBanner: FC<MainBannerProps> = ({ list=[], children, index=0 }) => {
  const {
    goNextStep,
    goPrevStep,
    setStep,
    step,
  }: StepHandleProps = useStepper({ maxStep: list.length, step: index });
//   const [selected, setSelected] = useState<number>(0);
  return (
    <MainBannerContext.Provider value={{
      setSelected: setStep,
      index: step,
      list,
      prevPage: goPrevStep,
      nextPage: goNextStep,
      selected: get(list, `[${step}]`),
    }}>
      {children}
    </MainBannerContext.Provider>
  );
}

export interface MainSlideProps {
  item: BannerListItem;
  onChanged: Function;
  offset: number;
}

export const MainSlide: FC<MainSlideProps> = ({ item, onChanged, offset }) => {
  const classes = useSlideStyles();
  const { label, thumbnail }: BannerListItem = item;
  const {
    index=0,
}: ContextProps = useMainBanner();
let styles = classes.thumbnail;
if (index === offset) {
    styles = css(styles, classes.skeleton);
}
  return (
    <Box className={classes.root} onClick={() => onChanged(offset)}>
        <LazyImage className={styles} src={thumbnail} alt={label} />
        <Typography className={classes.title}>{label}</Typography>
    </Box>
  );
};

export interface ArrowProps {
    className?: string;
    style?: any;
    onClick?: Function;
};
export const SlideNextPageArrow: FC<any> = ({ className='', style={}, onClick }) => {
    const classes = useSliderStyles();
    const onPress = useCallback(() => {
        onClick();
    }, [onClick]);
    return (
        <div style={{...style}} className={css(className, classes.nextBtn)} onClick={onPress} />
    );
}
export const SlidePrevPageArrow: FC<any> = ({ className='', style={}, onClick }) => {
    const classes = useSliderStyles();
    const onPress = useCallback(() => {
        onClick();
    }, [onClick]);
    return (
        <div style={{...style}}  className={css(className, classes.prevBtn)} onClick={onPress} />
    );
}
export interface MainSliderProps {
    settings: any;
}
export const MainSlider: FC<MainSliderProps> = ({ settings }) => {
    const classes = useSliderStyles();
    const {
        list,
        setSelected,
    }: ContextProps = useMainBanner();
    const onChanged = useCallback((index: number) => {
        setSelected(index);
    }, [setSelected]);

    return (
        <Box className={classes.root}>
            <Slider {...settings}>
                {
                    map(list, (l: BannerListItem, offset: number) => (
                        <MainSlide onChanged={onChanged} item={l} key={offset} offset={offset} />
                    ))
                }
            </Slider>
        </Box>
    );
} 

export interface BackgroundImageProps {
    style?: any;
};
export const MainWidthBackgroundBanner: FC = () => {
    const {
      selected,
      index=0,
      setSelected,
    }: ContextProps = useMainBanner();
    const { t } = useTranslation('button');
    const classes = useBackgroundStyles();
    const router = useRouter();
    let overlayClasses = classes.overlay;
    const onChanged = useCallback((index: number) => {
        setSelected(index);
    }, [setSelected]);
    const settings = {
        className: "slider variable-width",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 100,
        afterChange: onChanged,
        dots: false,
        autoplay: true,
        initialSlide: index,
        variableWidth: true,
        autoplaySpeed: 7000,
        prevArrow: (<SlidePrevPageArrow />),
        nextArrow: (<SlideNextPageArrow />),
        // infinite: false,
    };
    let bg: string = '';
    if (selected) {
        bg = selected.banner || '';
    }
    const subTitle = get(selected, 'subTitle');
    const content = (
        <Box display={'flex'}>
            <div className={classes.bg}>
                {
                    !!bg && selected && <Pulse><LazyImage
                        src={bg}
                        alt={selected.title}
                        fitImage
                    /></Pulse>
                }
            </div>
            <Box className={classes.wrapper}>
                <Container maxWidth={'lg'}>
                    <div>
                        {
                            selected && <Box className={classes.info}>
                                <Typography component={'h1'} className={classes.title}>
                                    {selected.title}
                                </Typography>
                                { 
                                    !!subTitle && <Typography component={'h2'} className={classes.subTitle}>
                                        {subTitle}
                                    </Typography>
                                }
                                {
                                    !!selected.buttonLink && <CommonButton
                                        className={classes.actionBtn}
                                        label={selected.buttonLabel || t('learnMore')}
                                        onClick={()=>{ router.push(get(selected, 'buttonLink'))}}
                                    />
                                }
                            </Box>
                        }
                    </div>
                </Container>
                <Fade left>
                    <Box display={'flex'} justifyContent={'flex-end'}>
                        <Box className={classes.sliderWrapper}>
                            <MainSlider settings={settings} />
                        </Box>
                    </Box>
                </Fade>
            </Box>
        </Box>
    );
    return (
        <div className={classes.root}>
            {content}
            <div className={overlayClasses} />
        </div>
    );
}

const MainFullBanner: FC<any> = forwardRef((props, r) => {
    const { list }: any = useStoreMainBanner();
    const { inspector }: LocaleContextProps = useLocale();

    const bannerLists: BannerListItem[] = map(list, (banner)=>({
        label: inspector({
            target: banner, field: 'title', defaultValue: ''
        }),
        thumbnail: banner.thumbnail,
        banner: banner.banner,
        title: inspector({
            target: banner, field: 'title', defaultValue: ''
        }),
        subTitle: inspector({
            target: banner, field: 'subtitle', defaultValue: ''
        }),
        buttonLabel: inspector({
            target: banner, field: 'button', defaultValue: ''
        }),
        buttonLink: banner.url
    }))
    return (
        <div ref={r as React.RefObject<HTMLDivElement>}>
            <Fade left>
                <MainBanner list={bannerLists}>
                    <MainWidthBackgroundBanner />
                </MainBanner>
            </Fade>
        </div>
    );
});

export default MainFullBanner;
