import React, { FC, useCallback, useEffect, useState } from 'react'
import validator from 'validator';
import css from 'clsx'
import { map, join, chain, includes } from 'lodash';
import Numeral from 'numeral'
import StaticRoutes from '@config/routes';
import { useRouter } from 'next/router'
import StaticSources from '@config/sources';
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import CommonPopup from '@components/common/Popup'
import FormElement from '@components/form'
import { useSolutionRule } from '@store/common/context'
import { useOTP, useApplicationForm } from '@store/application/context'
import Service from '@config/service'
import {
  OTPAuthentication,
} from './sms'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Box,
  Grid,
  Snackbar,
} from '@material-ui/core';
import {
  Alert
} from '@material-ui/lab';
// import {
//   LabelValueProps,
// } from '@interfaces/list'
import {
  useToggle,
  ToggleProps,
  useAfterEffect,
} from '@hooks/common'

import {
  useStatementStyles,
  useFormStyles,
  useReviewFormStyles,
  useAgreementStyle,
  useEnjoyStyle,
} from './styles';
import {
  useStepSolution,
  StepSolutionContext,
} from '@containers/section/StepSolution'
import {
  // LoanTypeSelectionBoxProps,
  LoanTypeSelectionBox,
} from '@containers/section/Calculator'
// import {
//   useProgressStyles
// } from '@containers/section/StepSolution/styles'

export const ContactInformationForm: FC = () => {
  const { t } = useTranslation('form');
  const {
    // watch,
    // register,
    control,
  }: StepSolutionContext = useStepSolution();
  const classes = useFormStyles();
  // const values = watch();
  // console.log(values);
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.title')}*
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          {/* <FormElement.PrefixNumericInput
            name="title"
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            control={control}
            unit={'HKD'}
          /> */}
          <FormElement.TextInput
            name="title"
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            control={control}
            type="text"
            // register={register}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.name')}*
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.TextInput
            name="name"
            control={control}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            type="text"
            // register={register}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.mobileNo')}*
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.TextInput
            name="mobileNo"
            control={control}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            type="text"
            // register={register}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.hkidNo')}*
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.TextInput
            name="hkId"
            control={control}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            type="text"
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.emailAddress')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.TextInput
            control={control}
            name="email"
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            type="email"
            // register={register}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.bestContactTime')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.TextInput
            control={control}
            name="bestContactTime"
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            type="text"
            // register={register}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export const PropertyInformationForm: FC = () => {
  const { t } = useTranslation('form');
  const { isResolvingLoan } = useSolutionRule();
  const {
    control,
    register,
    changeValue,
    watch,
  }: StepSolutionContext = useStepSolution();
  const classes = useFormStyles();
  useEffect(() => {
    register("loanType");
    register("propertyPrice");
  }, []);
  return (
    <div>
      <LoanTypeSelectionBox hideSelected watch={watch} changeValue={changeValue} />
      {/* <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.loanType')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.NativeSelect
            name="loanType"
            control={control}
            options={loanTypes}
          />
        </Grid>
      </Grid> */}
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.appliedAmount')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.PrefixNumericInput
            name="appliedAmount"
            control={control}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            unit={'HKD'}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.loanTenor')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.SuffixNumericInput
            control={control}
            name={'loanTenor'}
            unit={t('label.yearPercent')}
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
          />
        </Grid>
      </Grid>
      <Grid container spacing={3} className={classes.break}>
        <Grid item xs={12} sm={4}>
          <div className={classes.underBlock}>
            <span className={classes.inlineIcon}><LocalImage src={StaticSources.icon.note} alt={t('label.note')} />
            </span>
            <Typography component={'h4'} className={css(classes.label, classes.remarks, classes.inline)}>
              {t('label.note')}*
            </Typography>
          </div>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography component={'h5'} className={css(classes.label, classes.remarks)}>
            {t('note.preliminary')}
          </Typography>
        </Grid>
      </Grid>
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={css(classes.label, classes.remarks)}>
            {t('label.address')}*
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <AddressInputWithEnjoyPopup />
        </Grid>
      </Grid>
      {
        isResolvingLoan && <Grid container spacing={3}>
          <Grid item xs={12} sm={4}>
            <Typography component={'h4'} className={classes.label}>
              {t('label.resolvingCreditLimit')}
            </Typography>
          </Grid>
          <Grid item xs={12} sm={8}>
            <FormElement.PrefixNumericInput
              name="creditLimit"
              inputProps={{
                placeholder: t('placeholder.typeHere'),
              }}
              control={control}
              unit={'HKD'}
            />
          </Grid>
        </Grid>
      }
      <Grid container spacing={3}>
        <Grid item xs={12} sm={4}>
          <Typography component={'h4'} className={classes.label}>
            {t('label.outstandingMortgageAmount')}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <FormElement.PrefixNumericInput
            name="outstanding"
            inputProps={{
              placeholder: t('placeholder.typeHere'),
            }}
            control={control}
            unit={'HKD'}
          />
        </Grid>
      </Grid>
    </div>
  );
}

export interface AgreementProps{
  children: JSX.Element;
  title?: string | JSX.Element;
  labelUnderline?: boolean;
  label: string | JSX.Element;
}

export const Agreements: FC<AgreementProps> = ({ labelUnderline=true, label, title, children }) => {
  const { enabled, toggle }: ToggleProps = useToggle(false);
  const classes = useAgreementStyle({ labelUnderline });
  const onCloseAgreement = useCallback(() => {
    toggle();
  }, [toggle]);
  return (
    <React.Fragment>
      <Typography
        component={'span'}
        onClick={() => toggle()}
        className={classes.agreement}
      >{label}</Typography>
      <CommonPopup
        show={enabled}
        afterClose={onCloseAgreement}
        title={title}
      >
        {children}
      </CommonPopup>
    </React.Fragment>
  );
}

export const AddressInputWithEnjoyPopup: FC = () => {
  const { t } = useTranslation('form');
  const { t: Bt } = useTranslation('button');
  const router = useRouter();
  const { enabled, toggle }: ToggleProps = useToggle(false);
  const classes = useEnjoyStyle();
  const {
    control,
  }: StepSolutionContext = useStepSolution();
  const checkValuation = useCallback(() => {
    toggle();
    router.push(StaticRoutes.onlineProperty);
  }, [toggle, router]);
  const title = t('label.enjoyFirstMortgage');
  const subTitle = t('label.preliminaryOffer');
  const description = t('note.enjoyFirstMortgageNote');
  const slogan = t('note.enjoySlogan');
  const subSlogan = t('note.enjoySubSlogan');
  return (
    <React.Fragment>
      <FormElement.TextMultilineInput
        name="address"
        rows={2}
        inputProps={{
          placeholder: t('placeholder.typeHere'),
          onClick: toggle,
        }}
        control={control}
        // register={register}
      />
      <CommonPopup
        show={enabled}
        afterClose={toggle}
        classes={{
          paper: classes.paper,
        }}
      >
        <Box display={'flex'} alignItems={'center'} className={classes.content}>
          <Box display={'flex'} flexDirection={'column'} alignItems={'flex-start'} className={classes.left}>
            <Typography
              component={'h1'}
              className={classes.title}
            >
              {title}
            </Typography>
            <Typography
              component={'h2'}
              className={classes.subTitle}
            >
              {subTitle}
            </Typography>
            <div className={classes.description} >
              {description}
            </div>
            <Box className={classes.control} display={'flex'} alignItems={'center'} justifyContent={'space-between'}>
              <CommonButton
                label={Bt('checkNow')}
                className={css(classes.btn, classes.nowBtn)}
                buttonProps={{
                  startIcon: <LocalImage
                    alt={`${title} ${subTitle}`}
                    src={StaticSources.icon.enjoyCheckNow}
                    className={classes.icon}
                  />
                }}
                onClick={checkValuation}
              />
              <CommonButton
                label={Bt('checkLater')}
                className={css(classes.btn, classes.laterBtn)}
                onClick={toggle}
              />
            </Box>
          </Box>
          <Box display={'flex'} alignItems={'center'} className={classes.right}>
            <div className={classes.banner}>
              <LocalImage
                alt={`${title} ${subTitle}`}
                src={StaticSources.banner.enjoyFirstMortgage}
              />
              <div className={classes.slogan}>
                <Typography
                  component={'h3'}
                  className={classes.sloganText}
                >
                  {slogan}
                </Typography>
                <Typography
                  component={'h3'}
                  className={classes.subSloganText}
                >
                  {subSlogan}
                </Typography>
              </div>
            </div>
          </Box>
        </Box>
      </CommonPopup>
    </React.Fragment>
  );
}

export interface NoteBlockProps {
  content: string;
}
export const NoteBlock: FC<NoteBlockProps> = ({ content }) => {
  const { t } = useTranslation('form');
  const classes = useFormStyles();
  return (
    <div className={classes.hints}>
      <Typography
        component={'h2'}
        className={classes.fact}
      >
        {t('label.keyFacts')}
      </Typography>
      <div className={classes.factContent} dangerouslySetInnerHTML={{ __html: content }}  />
    </div>
  );
}

export const AgreementBlock: FC = () => {
  const { t } = useTranslation('form');
  const classes = useAgreementStyle({ labelUnderline: false });
  const {
    control,
  }: StepSolutionContext = useStepSolution();
  const customerDeclaration: string = `A Declaration Statement is required for all outgoing international shipments. It is a legal certification you provide to Customs affirming that the information on your international forms, regarding your shipment, is true and accurate.<br /><br/>In its simplest form, a declaration statement might read: "I hereby certify that the information on this invoice is true and correct and the contents and value of this shipment are as stated."<br/><br/>For shipments governed by the United States - Mexico - Canada Trade Agreement (USMCA), a statement might read: "I hereby certify that the good covered by this shipment qualifies as an originating good for purposes of preferential tariff treatment under the USMCA."<br/><br/>Your declaration statement can be up to 550 characters in length, and you can save up to 10 customized statements for repeated use.`;
  const disclaimer: string = `Otherwise, what one employee says may be construed as being what the entire company believes, thinks or condones, and this may be very inaccurate and damaging.<br /><br />"Views expressed" disclaimers are seen most often in personal opinion writing by experts or professionals working in the same field of study as their post.<br /><br />For example, a climate change scientist writing an editorial or opinion piece that involves the topic of climate change may include a disclaimer saying that the opinions are his own and not that of his employer.`;
  const privacyPolicy: string = `The privacy policy is one of the most essential legal requirements for websites.<br /><br />
  Even if you just have a small business or a blog with no income at all, you might be surprised to discover that you still need a privacy policy.<br /><br />Basically, if your website collects personal data, you need a privacy policy that informs your users about this according to privacy laws in most jurisdictions, including the EU and the US.<br /><br />Almost all modern websites function with the use of cookies, so chances are high that your website is collecting personal data, for example for statistical, functional or marketing purposes.<br /><br />In this blogpost, we take a look at what constitutes a good privacy policy, how to make a compliant GDPR privacy policy and whether using a privacy policy generator is a good idea.<br /><br />Learn what the privacy policy is and how to get one for your website below.`;
  return (
    <Box display={'flex'} alignItems={'center'} className={classes.root}>
      <FormElement.NativeCheckbox
        name={'agree'}
        control={control}
      />
      <Typography component={'span'} className={classes.note}>
        {t('note.confirmRead')}
      </Typography>
      <Agreements
        title={t('label.customerDeclaration')}
        label={t('label.customerDeclaration')}
      >
        <div dangerouslySetInnerHTML={{ __html: customerDeclaration }} />
      </Agreements>
      <Agreements
        title={t('label.disclaimer')}
        label={t('label.disclaimer')}
      >
        <div dangerouslySetInnerHTML={{ __html: disclaimer }} />
      </Agreements>
      <span>{t('label.and')}</span>
      <Agreements
        title={t('label.privacyPolicy')}
        label={t('label.privacyPolicy')}
      >
        <div dangerouslySetInnerHTML={{ __html: privacyPolicy }} />
      </Agreements>
    </Box>
  );
}

export interface PropertyContactInformationLookupProps {
  label: string;
  field: string;
  key: string;
  inspector?: Function;
};
export interface PropertyContactInformationReviewItemProps extends PropertyContactInformationLookupProps{
  // TODO
}
export interface PropertyContactInformationReviewTableProps {
  lookups: PropertyContactInformationLookupProps[];
  // fieldsHook: string[];
};
export const PropertyContactInformationReviewItem: FC<PropertyContactInformationReviewItemProps> = ({
  label,
  inspector,
  field,
}) => {
  // const { t } = useTranslation('form');
  const classes = useFormStyles();
  const reviewClasses = useReviewFormStyles();
  const {
    watch,
    // register,
  }: StepSolutionContext = useStepSolution();
  // [IMPORTANT] only hook for the current target form field
  const { [field]: formValue } = watch();
  return (
    <Grid container spacing={3} className={reviewClasses.row}>
      <Grid item xs={12} sm={4}>
        <Typography component={'h4'} className={classes.label}>
          {label}
        </Typography>
      </Grid>
      <Grid item xs={12} sm={8} className={classes.reviewValue}>
        {
          !!inspector ? inspector(formValue) : formValue
        }
      </Grid>
    </Grid>
  );
};
export const PropertyContactInformationReviewTable: FC<PropertyContactInformationReviewTableProps> = ({
  lookups=[],
}) => {
  const classes = useFormStyles();
  return (
    <Box display={'flex'} flexDirection={'column'} className={classes.reviewTable}>
      {
        map(lookups, ({ key, ...lookup }) => (
          <PropertyContactInformationReviewItem
            key={key}
            {...lookup}
          />
        ))
      }
    </Box>
  );
}
export const PropertyContactInformationReview: FC = () => {
  const { t } = useTranslation('form');
  const { t:Bt } = useTranslation('button');
  const classes = useFormStyles();
  const reviewClasses = useReviewFormStyles();
  const { verifyCode, verifying, verifyOtpError, verificationSucceed, passVerified, failVerified } = useOTP();
  const { submit, submitting, submitError, beingSubmitted } = useApplicationForm();
  const {
    goNextStep,
    goPrevStep,
    slideTo,
    watch,
    step,
  }: StepSolutionContext = useStepSolution();
  const { isResolvingLoan } = useSolutionRule();
  const formValue = watch();
  const changePrice = useCallback((v: number=0): string => (
    `${Service.currency} ${Numeral(v).format('0,0.00')}`
  ), []);
  const goBack = useCallback(() => {
    goPrevStep();
    slideTo('form');
  }, [goPrevStep]);
  const onGoNext = useCallback(async () => {
    if (step === 1) {
      if (!verificationSucceed) {
        verifyCode(formValue);
      }
    }
  }, [
    verifyCode,
    verificationSucceed, 
    step, 
    goNextStep, 
    slideTo, 
    formValue,
    // submit,
  ]);
  useEffect(() => {
    if (passVerified) {
      submit(formValue);
    }
  }, [passVerified]);
  useEffect(() => {
    if (beingSubmitted && !submitError) {
      slideTo('form');
      goNextStep();
    }
  }, [beingSubmitted]);
  const personalLookups: PropertyContactInformationLookupProps[] = [
    {
      key: 'loanType',
      label: t('label.loanType'),
      field: 'loanType',
      inspector: (v: string='first'): string => {
        const lookup: {
          first: string;
          second: string;
        } = {
          first: t('options.firstMortgageLong'),
          second: t('options.secondMortgageLong'),
        };
        return lookup[v];
      },
    },
    {
      key: 'appliedAmount',
      label: t('label.appliedAmount'),
      field: 'appliedAmount',
      inspector: changePrice,
    },
    {
      key: 'loanTenor',
      label: t('label.loanTenor'),
      field: 'loanTenor',
      inspector: (v: string=''): string => (
       `${v} ${t('label.yearPercent')}`
      ),
    },
    {
      key: 'address',
      label: t('label.addressWithHints'),
      field: 'address',
    },
    {
      key: 'propertyPrice',
      label: t('label.propertyPriceWithHints'),
      field: 'propertyPrice',
      inspector: changePrice,
    },
    ...(
      isResolvingLoan ? [
        {
          key: 'creditLimit',
          label: t('label.resolvingCreditLimit'),
          field: 'creditLimit',
          inspector: changePrice,
        }
      ] : []
    ),
    {
      key: 'outstanding',
      label: t('label.outstandingMortgageAmount'),
      field: 'outstanding',
      inspector: changePrice,
    }
  ];
  const contactLookups: PropertyContactInformationLookupProps[] = [
    {
      key: 'title',
      label: t('label.title'),
      field: 'title',
    },
    {
      key: 'name',
      label: t('label.name'),
      field: 'name',
    },
    {
      key: 'mobileNo',
      label: t('label.mobileNo'),
      field: 'mobileNo',
    },
    {
      key: 'hkId',
      label: t('label.hkidNo'),
      field: 'hkId',
    },
    {
      key: 'email',
      label: t('label.emailAddress'),
      field: 'email',
    },
    {
      key: 'bestContactTime',
      label: t('label.bestContactTime'),
      field: 'bestContactTime',
    },
  ];
  const remindStatement: string = 'Please read the following terms before loan application:<br />(1) Loan application is subject to detailed terms and conditions which may be obtained upon enquiry.<br />(2) Eligible customers for particular loan products will not be required to pay any administration fees and handling charges, and all legal costs to prepare the Mortgage or Second Mortgage will be paid by the Company. Other customers who are not eligible or suitable for a particular loan product being applied for may be referred or recommended to apply for other loan products for which customers are free to make their own final choice and will be bound by different terms and conditions, including terms requiring payment of legal costs, administration fees and/or handling charges.<br />(3) Customers are advised to seek their own independent legal advice whenever they deem it necessary.';
  const facts: string = '1. The minimum and maximum repayment period is 6 months and 84 months respectively.<br />2. The annual percentage rate ranges from 14% to 24%.<br />3. A representative example of total cost for a loan below is for reference only:<br />- Loan amount: HK$1,000,000<br />- Monthly instalment of HK$89,803 for 12 months<br />- Annual percentage rate: 14%<br />- Total repayment amount: HK$1,077,631';
  return (
    <div>
      <Box display={'flex'} alignItems={'flex-end'} justifyContent={'space-between'} className={classes.root}>
        <Typography component={'h1'} className={classes.title}>
          {t('title.reviewInformation')}
        </Typography>
      </Box>
      <Box display={'flex'} justifyContent={'space-between'} className={css(classes.form, reviewClasses.root)}>
        <Box display={'flex'} flexDirection={'column'} className={reviewClasses.left}>
          <Typography component={'h1'} className={css(classes.title, reviewClasses.title)}>
            {t('title.propertyInformation')}
          </Typography>
          <PropertyContactInformationReviewTable lookups={personalLookups} />
        </Box>
        <Box display={'flex'} flexDirection={'column'} className={reviewClasses.right}>
          <Typography component={'h1'} className={css(classes.title, reviewClasses.title)}>
            {t('title.contactInformation')}
          </Typography>
          <PropertyContactInformationReviewTable lookups={contactLookups} />
        </Box>
      </Box>
      <div className={classes.auth}>
        <OTPAuthentication pageType={'mortgage'} />
      </div>
      <div className={css(reviewClasses.footer, classes.footer)}>
        <div className={classes.statementBlock}>
          <StatementBlock content={remindStatement} />
        </div>
        <Snackbar open={failVerified && !!verifyOtpError} autoHideDuration={4000} >
          <Alert severity="error">
            {t('message.verifiedCodeError')}
          </Alert>
        </Snackbar>
        <Snackbar open={!!submitError && beingSubmitted} autoHideDuration={4000} >
          <Alert severity="error">
            {t('message.submitError')}
          </Alert>
        </Snackbar>
        <Box display={'flex'} justifyContent={'space-between'} className={reviewClasses.step}>
          <CommonButton
            onClick={goBack}
            label={Bt('back')}
            className={classes.stepButton}
            buttonProps={{
              disabled: verifying || submitting,
            }}
          />
          <CommonButton
            onClick={onGoNext}
            label={Bt('continueAndApply')}
            className={classes.stepButton}
            buttonProps={{
              disabled: verifying || submitting,
            }}
          />
        </Box>
        <div>
          <NoteBlock content={facts} />
        </div>
      </div>
    </div>
  );
}
export interface PropertyContactValidationProps {
  valid: boolean;
  msg: string[];
};
export interface PropertyContactValidationTargetProps {
  name: string;
  field: string;
};
export const usePropertyContactValidation = () => {
  const { t } = useTranslation('form');
  const required: PropertyContactValidationTargetProps[] = [
    { name: 'loanType', field: t('label.loanType') },
    { name: 'appliedAmount', field: t('label.appliedAmount') },
    { name: 'title', field: t('label.title') },
    { name: 'address', field: t('label.address') },
    { name: 'name', field: t('label.name') },
    { name: 'mobileNo', field: t('label.mobileNo') },
  ];
  const requiredBoolean: PropertyContactValidationTargetProps[] = [
    { name: 'agree', field: t('note.readAndConfirm') },
  ];
  const optionalNumeric = [
    { name: 'loanTenor', field: t('label.loanTenor') },
    { name: 'creditLimit', field: t('label.resolvingCreditLimit') }
  ];
  const iterativeCheck = useCallback((
    values: any,
    set: PropertyContactValidationTargetProps[], 
    message: string,
    inspector: Function,
  ) => {
    return chain(set).reduce(
      (msg: string[], { name, field }: PropertyContactValidationTargetProps) => {
        if (inspector(values[name])) {
          return [
            ...msg,
            t(message, { fieldName: field }),
          ]
        }
        return msg;
    }, []).value();
  }, [t]);
  const isValidLoanType = useCallback((values: any) => {
    const { loanType } = values;
    if (!includes([
      'first',
      'second',
    ], loanType)) {
      return t('message.loanTypeInvalid')
    }
    return null;
  }, [t]);
  const isValidPhone = useCallback((values: any) => {
    const { mobileNo='' } = values;
    if (mobileNo && mobileNo.length > 7) {
      return null;
    }
    return t('message.phoneFormatInvalid');
  }, [t]);
  const isAppliedAmountType = useCallback((values: any) => {
    const { appliedAmount } = values;
    if (validator.isNumeric(appliedAmount || '0') && appliedAmount > 0) {
      return null;
    }
    return t('message.appliedAmountNumeric');
  }, [t]);
  const isOptionalNumeric = useCallback((optional: PropertyContactValidationTargetProps[], values: any) => {
    const errorMsg = iterativeCheck(
      values, 
      optional, 
      'message.numeric', 
      (v: any) => {
        const isNumeric = validator.isNumeric(v || '0');
        return v && (!isNumeric || Number(v) <= 0);
      });
    return errorMsg;
  }, [t]);
  const isRequiredBoolean = useCallback((required: PropertyContactValidationTargetProps[], values: any) => {
    const errorMsg = iterativeCheck(
      values, 
      required, 
      'message.agreement', 
      (v: any) => !v);
    return errorMsg;
  }, [t]);
  const isOptionalEmail = useCallback((values: any) => {
    const { email } = values;
    if (!email || (email && validator.isEmail(email))) {
      return null;
    }
    return t('message.emailFormat');
  }, [t]);
  const validation = useCallback((values: any) => {
    let errorMsg = iterativeCheck(
      values, 
      required, 
      'message.empty', 
      (v: any) => validator.isEmpty(v || '')
    );
    const loanTypeMsg = isValidLoanType(values);
    const phoneMsg = isValidPhone(values);
    const amountTypeMsg = isAppliedAmountType(values);
    const numericMsg = isOptionalNumeric(optionalNumeric, values);
    const emailMsg = isOptionalEmail(values);
    const booleanMsg = isRequiredBoolean(requiredBoolean, values);
    if (phoneMsg) {
      errorMsg = [...errorMsg, phoneMsg];
    }
    if (loanTypeMsg) {
      errorMsg = [...errorMsg, loanTypeMsg];
    }
    if (amountTypeMsg) {
      errorMsg = [...errorMsg, amountTypeMsg];
    }
    if (numericMsg) {
      errorMsg = [...errorMsg, ...numericMsg];
    }
    if (booleanMsg) {
      errorMsg = [...errorMsg, ...booleanMsg];
    }
    if (emailMsg) {
      errorMsg = [...errorMsg, emailMsg];
    }
    return errorMsg;
  }, [
    iterativeCheck,
    isRequiredBoolean,
    isValidLoanType,
    isOptionalEmail,
    isAppliedAmountType,
    isValidPhone,
  ]);

  return {
    validation,
    isAppliedAmountType,
    isValidLoanType,
    isOptionalEmail,
    isOptionalNumeric,
    isValidPhone,
    isRequiredBoolean,
  };
};

export const PropertyContactInformation: FC = () => {
  const { t } = useTranslation('form');
  const { enabled: start, toSwitch, toggle: startToggle }: ToggleProps = useToggle(false);
  // const { enabled: showError, toggle }: ToggleProps = useToggle(false);
  const { t:Bt } = useTranslation('button');
  const classes = useFormStyles();
  const {
    goNextStep,
    watch,
    slideTo,
    step,
  }: StepSolutionContext = useStepSolution();
  const { validation } = usePropertyContactValidation();
  const formValue = watch();
  // const { submit } = useApplicationForm();
  const remindStatement: string = 'Please read the following terms before loan application:<br />(1) Loan application is subject to detailed terms and conditions which may be obtained upon enquiry.<br />(2) Eligible customers for particular loan products will not be required to pay any administration fees and handling charges, and all legal costs to prepare the Mortgage or Second Mortgage will be paid by the Company. Other customers who are not eligible or suitable for a particular loan product being applied for may be referred or recommended to apply for other loan products for which customers are free to make their own final choice and will be bound by different terms and conditions, including terms requiring payment of legal costs, administration fees and/or handling charges.<br />(3) Customers are advised to seek their own independent legal advice whenever they deem it necessary.';
  const facts: string = '1. The minimum and maximum repayment period is 6 months and 84 months respectively.<br />2. The annual percentage rate ranges from 14% to 24%.<br />3. A representative example of total cost for a loan below is for reference only:<br />- Loan amount: HK$1,000,000<br />- Monthly instalment of HK$89,803 for 12 months<br />- Annual percentage rate: 14%<br />- Total repayment amount: HK$1,077,631';
  const onGoNext = useCallback(() => {
    if (step === 0) {
      const error = validation(formValue);
      if (!error || (error && !error.length)) {
        goNextStep();
        // submit(formValue);
      } else {
        // start the form
        toSwitch(true);
        setTimeout(() => {
          toSwitch(false);
        }, 5000);
      }
      slideTo('form');
    } 
  }, [step, goNextStep, slideTo, toSwitch, formValue]);
  
  // useAfterEffect(() => {
  //   const msg = validation(formValue);
  //   console.log(formValue);
  //   if (msg && msg.length) {
  //     setError(msg);
  //   }
  // }, [formValue]);
  const error = validation(formValue);
  // console.log(error);
  return (
    <div>
      {
        start && !!error && error.length > 0 && <Alert className={classes.alert} onClose={(e: any) => startToggle()} severity="error">
          {join(error, ', ')}
        </Alert>
      }
      <Box display={'flex'} justifyContent={'space-between'} className={classes.inlineForm}>
        <div>
          <Box display={'flex'} alignItems={'flex-end'} justifyContent={'space-between'} className={classes.root}>
            <Typography component={'h1'} className={classes.title}>
              {t('title.propertyInformation')}
            </Typography>
            <div className={classes.hints}>
              {t('note.inEnglish')}
            </div>
          </Box>
          <div className={classes.form}>
            <PropertyInformationForm />
          </div>
        </div>
        <div>
          <Box display={'flex'} alignItems={'flex-end'} justifyContent={'space-between'} className={classes.root}>
            <Typography component={'h1'} className={classes.title}>
              {t('title.contactInformation')}
            </Typography>
            <div className={classes.hints}>
              {t('note.inEnglish')}
            </div>
          </Box>
          <div className={classes.form}>
            <ContactInformationForm />
          </div>
        </div>
      </Box>
      <div className={classes.footer}>
        <div className={classes.underBlock}>
          <Typography component={'h4'} className={css(classes.label, classes.remarks, classes.inline)}>
            {t('note.required')}
          </Typography>
        </div>
        <div className={css(classes.statementBlock, classes.statementWithBorder)}>
          <StatementBlock content={remindStatement} />
        </div>
        <div className={classes.statement}>
          <AgreementBlock />
        </div>
        <Box display={'flex'} alignItems={'center'} flexDirection={'column'} className={classes.step}>
          {/* <div>
            <Snackbar open={start && !!error && error.length > 0} autoHideDuration={4000} onClose={(e: any) => startToggle()}>
              <Alert onClose={(e: any) => startToggle()} severity="error">
                This is a success message!
              </Alert>
            </Snackbar>
          </div> */}
          <CommonButton
            onClick={onGoNext}
            label={Bt('nextStep')}
            className={classes.stepButton}
            buttonProps={{
              // disabled: error && !!error.length,
            }}
          />
        </Box>
        <div>
          <NoteBlock content={facts} />
        </div>
      </div>
    </div>
  );
}

export interface StatementBlockProps {
  content: string;
}
export const StatementBlock: FC<StatementBlockProps> = ({ content }) => {
  const classes = useStatementStyles();
  return (
    <div dangerouslySetInnerHTML={{ __html: content }} className={classes.root} />
  );
};
