import { Action } from 'redux'
import APIManager from '@lib/apiManager';

export enum ContentActionTypes {
  FETCH_PERSONA = 'FETCH_PERSONA',
  FETCH_PERSONA_SUCCEED = 'FETCH_PERSONA_SUCCEED',
  FETCH_PERSONA_FAILED = 'FETCH_PERSONA_FAILED',
  FETCH_CALCULATOR_CONFIG = 'FETCH_CALCULATOR_CONFIG',
  FETCH_CALCULATOR_CONFIG_SUCCEED = 'FETCH_CALCULATOR_CONFIG_SUCCEED',
  FETCH_CALCULATOR_CONFIG_FAILED = 'FETCH_CALCULATOR_CONFIG_FAILED',
  FETCH_CALCULATION = 'FETCH_CALCULATION',
  RESET_CALCULATION = 'RESET_CALCULATION',
  FETCH_CALCULATION_SUCCEED = 'FETCH_CALCULATION_SUCCEED',
  FETCH_CALCULATION_FAILED = 'FETCH_CALCULATION_FAILED',
  FETCH_FAQ = 'FETCH_FAQ',
  FETCH_FAQ_SUCCEED = 'FETCH_FAQ_SUCCEED',
  FETCH_FAQ_FAILED = 'FETCH_FAQ_FAILED'
}

export type ContentAction =
  | Action<ContentActionTypes.FETCH_PERSONA>
  | Action<ContentActionTypes.FETCH_PERSONA_SUCCEED>
  | Action<ContentActionTypes.FETCH_PERSONA_FAILED>
  | Action<ContentActionTypes.FETCH_CALCULATOR_CONFIG>
  | Action<ContentActionTypes.FETCH_CALCULATOR_CONFIG_SUCCEED>
  | Action<ContentActionTypes.FETCH_CALCULATOR_CONFIG_FAILED>
  | Action<ContentActionTypes.FETCH_CALCULATION>
  | Action<ContentActionTypes.RESET_CALCULATION>
  | Action<ContentActionTypes.FETCH_CALCULATION_SUCCEED>
  | Action<ContentActionTypes.FETCH_CALCULATION_FAILED>
  | Action<ContentActionTypes.FETCH_FAQ>
  | Action<ContentActionTypes.FETCH_FAQ_SUCCEED>
  | Action<ContentActionTypes.FETCH_FAQ_FAILED>


export const requestPersona = () => ({ type: ContentActionTypes.FETCH_PERSONA });
export const requestPersonaSucceed = (payload) => ({ type: ContentActionTypes.FETCH_PERSONA_SUCCEED, payload });
export const requestPersonaError = (payload) => ({ type: ContentActionTypes.FETCH_PERSONA_FAILED, payload });
export const fetchPersona = isServer => async (dispatch) => {
    dispatch(requestPersona());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'perona',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestPersonaError(error));
      return null;
    } else {
      dispatch(requestPersonaSucceed(data));
    }
    return data;
};

export const requestCalculatorConfig = () => ({ type: ContentActionTypes.FETCH_CALCULATOR_CONFIG });
export const requestCalculatorConfigSucceed = (payload) => ({ type: ContentActionTypes.FETCH_CALCULATOR_CONFIG_SUCCEED, payload });
export const requestCalculatorConfigError = (payload) => ({ type: ContentActionTypes.FETCH_CALCULATOR_CONFIG_FAILED, payload });
export const fetchCalculatorConfig = isServer => async (dispatch) => {
    dispatch(requestCalculatorConfig());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'calculatorConfig',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestCalculatorConfigError(error));
      return null;
    } else {
      dispatch(requestCalculatorConfigSucceed(data));
    }
    return data;
};
export type SectionType = 'interestOnly' | 'principalAndInterest' | 'revolving' | 'banner';

export interface CalculatePayloadProps {
  principal: number;
  rate: number;
  term: number;
}
export interface CalculateParameterProps {
  paymentType: SectionType;
}
export const resetCalculation = () => ({ type: ContentActionTypes.RESET_CALCULATION });
export const requestCalculation = () => ({ type: ContentActionTypes.FETCH_CALCULATION });
export const requestCalculationSucceed = (payload: any, params: CalculateParameterProps) => ({ type: ContentActionTypes.FETCH_CALCULATION_SUCCEED, payload, params });
export const requestCalculationError = (payload) => ({ type: ContentActionTypes.FETCH_CALCULATION_FAILED, payload });
export const calculate = (payload: CalculatePayloadProps, params: CalculateParameterProps) => async (dispatch) => {
    dispatch(requestCalculation());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'calculate',
        data: payload,
        params,
        underAuth: false,
    });
    if (error) {
      dispatch(requestCalculationError(error));
    } else {
      dispatch(requestCalculationSucceed(data, params));
    }
};

export const requestFaq = () => ({ type: ContentActionTypes.FETCH_FAQ });
export const requestFaqSucceed = (payload) => ({ type: ContentActionTypes.FETCH_FAQ_SUCCEED, payload });
export const requestFaqError = (payload) => ({ type: ContentActionTypes.FETCH_FAQ_FAILED, payload });
export const fetchFaq = (isServer: boolean) => async (dispatch) => {
    dispatch(requestFaq());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'faq',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestFaqError(error));
      return null;
    } else {
      dispatch(requestFaqSucceed(data));
    }
};
