import React, { FC, useCallback, useEffect } from "react";
import css from "clsx";
import { map } from "lodash";
import { useRouter } from "next/router";
import Routes from "@config/routes";
import StaticSource from "@config/sources";

import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import StaticSources from "@config/sources";
import Link from "@components/common/Link";
import Fade from "react-reveal/Fade";
import { InnerPage } from "@containers/layout/Heading";
import { useTranslation } from "next-i18next";
import { Typography, Box } from "@material-ui/core";
// import {
//   RefControlProps,
// } from '@types/common/common.d'
import { useStyles, useCreditStyles } from "./styles";

import { HeadingWithSubTitle } from "@containers/section/StepSolution";
import { Agreements } from "@containers/section/FullStepSolution/form";
import { CreditForm } from "./form";
import {
  checkUserInfo,
  checkApplicationOwner,
} from "@containers/section/AccountCommon/context";
export const IncreaseCreditLimit: FC<any> = ({ applicationNo }) => {
  const classes = useStyles();
  const { t } = useTranslation("section");

  /* check user and application here */
  const router = useRouter();
  useEffect(() => {
    checkUserInfo(router);
    checkApplicationOwner(router, applicationNo);
  }, []);
  /* check user and application here */

  return (
    <div className={classes.root}>
      <InnerPage>
        <div className={classes.heading}>
          <HeadingWithSubTitle
            title={t("title.customerService")}
            subTitle={t("title.increaseCreditLimit")}
          />
        </div>
        <CreditForm applicationNo={applicationNo} />
      </InnerPage>
    </div>
  );
};

export default IncreaseCreditLimit;
