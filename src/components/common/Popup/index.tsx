import { FC, useCallback, useMemo } from 'react'
  // useImperativeHandle, useRef, forwardRef, createContext, useContext } from 'react'
import css from 'clsx'
import {
  Dialog,
  DialogTitle,
  DialogContent,
  IconButton,
} from '@material-ui/core';
import {
  useStyles,
} from './styles'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'

import {
  useToggle,
  ToggleProps,
  useAfterEffect,
} from '@hooks/common'

export interface PopupProps {
  show: boolean | undefined;
  afterClose: Function;
  children: JSX.Element;
  classes?: any;
  noCloseBtn?: boolean;
  maxWidth?: 'lg' | 'md' | 'sm' | 'xl' | 'xs' | false;
  title?: string | JSX.Element;
  blockContentScroll?: Boolean;
  otherProps?: any;
};

// export interface PopupContext extends PopupProps {

// };

// export const Context = createContext<Partial<PopupContext>>({});
// export const usePopup = () => useContext(Context) as PopupContext;
const Popup: FC<PopupProps> = ({ blockContentScroll=false, otherProps={}, classes: className={}, noCloseBtn=false, maxWidth='sm', show=false, title, afterClose, children }) => {
  const { enabled, toSwitch, toggle }: ToggleProps = useToggle(show);
  const classes = useStyles();
  const onClose = useCallback(() => {
    toggle();
    if (afterClose) {
      afterClose();
    }
  }, [toggle, afterClose]);
  useAfterEffect(() => {
    toSwitch(show);
  }, [show]);
  
  return useMemo(() => {
    let contentClasses = classes.content;
    if (blockContentScroll) {
      contentClasses = '';
    }
    return (
      <Dialog
        className={css(classes.root)}
        classes={className}
        maxWidth={maxWidth}
        onClose={onClose}
        open={enabled}
        {...otherProps}
      >
        {
          !!title && <DialogTitle id="simple-dialog-title" className={classes.title}>
            {title}
          </DialogTitle>
        }
        <DialogContent>
          <div className={contentClasses}>
            {
              !noCloseBtn && <IconButton className={classes.close} onClick={onClose}>
                <LocalImage src={StaticSources.icon.popClose} alt={'Close'} />
              </IconButton>
            }
            <div className={classes.body}>{children}</div>
          </div>
        </DialogContent>
      </Dialog>
    );
  }, [maxWidth, blockContentScroll, otherProps, enabled, noCloseBtn, children, onClose, title, classes]);
};

// export const CommonPopup: FC<PopupProps> = (props) => {
//   const popRef = useRef();
//   return (
//     <Context.Provider value={{
//       ...props,
//       popRef,
//     }}>
//       <Popup {...props} ref={popRef} />
//     </Context.Provider>
//   );
// }

export default Popup;
