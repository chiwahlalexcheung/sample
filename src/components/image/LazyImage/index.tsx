import React, { FC, useCallback, useState, useEffect } from 'react';
// import PreloadImage from 'react-preload-image';
// import {
//   Fade,
// } from '@material-ui/core';
// import PreLoadImage from 'react-preload-image';
// import Flash from 'react-reveal/Flash';
import { useTranslation } from 'next-i18next'
import css from 'clsx';
import {
  Skeleton,
} from '@material-ui/lab';
import {
  useToggle,
  ToggleProps,
  useAfterEffect,
} from '@hooks/common';
import {
  useStyles,
} from './styles';
import StaticSources from '@config/sources';

interface LazyImageProps {
  src: string;
  alt?: string;
  errorSrc?: string;
  className?: string;
  imageClass?: string;
  fitImage?: boolean;
}

const LazyImage: FC<LazyImageProps> = ({
  src=StaticSources.mainLogo,
  alt='', errorSrc,
  className='',
  fitImage=false,
  imageClass='',
}) => {
  const { t } = useTranslation();
  const { enabled: loading, toSwitch: setLoading }: ToggleProps  = useToggle(true);
  const { enabled: error, toSwitch: setError }: ToggleProps = useToggle(false);
  const [image, setImage] = useState<string | undefined>('');
  const classes = useStyles({ fitImage });

  const onError = useCallback(() => {
    setLoading(false);
    setError(true);
    setImage(errorSrc || StaticSources.mainLogo);
  }, [setLoading, setError, setImage]);
  const onLoaded = useCallback(() => {
    setLoading(false);
    setImage(src);
    setError(false);
  }, [setLoading, setError, setImage, src]);
  const loadImage = useCallback(() => {
    const target = document.createElement("img");
    target.onload = () => {
      onLoaded();
    }
    target.src = src;
  }, [onLoaded, src]);
  useEffect(() => {
    loadImage();
  }, [src]);
  return (
    <div className={css(classes.image, className)}>
      {
        loading && <div>
          <img className={classes.process} src={src} onError={onError} onLoad={onLoaded} />
          <Skeleton className={classes.skeleton} variant={'rect'} width={'100%'} height={'100%'} />
        </div>
      }
      {!loading && !error && !!image && 
        <img src={image} className={css(classes.postImage, imageClass)} alt={alt} />
      }
      {
        error && <div className={classes.error}>
          <img src={image} alt={t('info.appName')} className={classes.errorImage} />
        </div>
      }
    </div>
  );
}

export default LazyImage;
