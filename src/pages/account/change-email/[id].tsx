import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import { storeWrapper } from '@store/store'
import { GetServerSideProps } from 'next'
import ChangeEmail from '@containers/section/ChangeEmail'

const ChangeEmailPage: React.FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation('common');
  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.change-email')}</title>
        </Head>
        < ChangeEmail applicationNo={applicationNo}/>
    </HeadingLayout>
  );
}

export const getServerSideProps: GetServerSideProps = storeWrapper.getServerSideProps(
  async ({ locale, params={} }: any) => {
    return {
      props: {
        applicationNo: params.id,
        ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
      }
    };
  }
)
export default ChangeEmailPage
