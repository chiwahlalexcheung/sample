import {
  TypographyProps,
} from '@interfaces/text'
// export interface LabelValueProps {
//   label: string;
//   value: string;
// }

// declare module "*.list" {
  export interface LabelValueProps {
    value: any;
    label: string;
    isActive?: boolean;
    link: string;
    key: string;
    labelProps?: TypographyProps;
  }
// }

