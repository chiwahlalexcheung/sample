import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useProgressStyles = makeStyles(theme => ({
  root: {
    width: '937px',
    margin: '0 auto',
    marginTop: '53px',
    position: 'relative',
  },
  submitBtn: {
    borderRadius: '20px',
    position: 'absolute',
    right: '-140px',
    top: '-30px',
    width: '120px',
    fontWeight: 'bolder',
  },
  step: {
    position: 'absolute',
    top: '-37px',
    minWidth: '50px',
  },
  step1: {
    left: '30%',
  },
  step2: {
    left: '63.6%',
  },
  step3: {
    left: '96%',
  },
  activeStep: {
    fontWeight: 'bold',
  },
  clickableStep: {
    cursor: 'pointer',
  },
  wrapper: {
    fontSize: '10px',
    textTransform: 'uppercase',
    boxShadow: '0px 3px 5px #00000029',
    padding: '5px',
    borderRadius: '8px',
    background: 'white',
    textAlign: 'center',
    // position: `relative`,
    
    // width: '0px',
    // height: '0px',
    // borderWidth: '15px',
    // borderStyle: 'solid',
    // borderColor: `#333 transparent transparent transparent`,
    // bottom: `-30px`,
    // right: `50px`,
    '&::before': {
      content: "'▼'",
      display: 'block',
      color: 'white',
      fontSize: '13px',
      position: 'absolute',
      bottom: '-11px',
      right: '0',
      left: '0',
      margin: '0 auto',
      textShadow: '0 6px 5px #00000029',
    }
  },
  bar: {
    position: 'relative',
    flex: 1,
  },
  mainBarContent: {
    width: '923px',
    background: theme.palette.text.disabled,
  },
  barContent: {
    
  },
  barBufferContent: {
    background: `transparent linear-gradient(270deg, #EF191B 0%, #EF191B 0%, #780D0E 100%) 0% 0% no-repeat padding-box`,
  },
  label: {
    fontWeight: 'bolder',
    textTransform: 'uppercase',
    position: 'absolute',
    left: '-130px',
  },
}));


const useCardStyles = makeStyles(theme => ({
  root: {
    userSelect: 'none',
    height: '430px',
    width: '303px',
    position: 'relative',
    background: 'transparent linear-gradient(180deg, #FFFFFF 0%, #F7F8F9 100%) 0% 0% no-repeat padding-box',
    boxShadow: '0px 3px 6px #C1C1C1B0',
    transition: "transform 0.15s ease-in-out",
  },
  active: {
    transform: "scale3d(1.15, 1.15, 1)",
    zIndex: 999,
    marginBottom: '32px',
    // transition: `max-height 1s`,
    // height: '493px',
  },
  activeConner: {
    width: '80px',
    height: '80px',
    position: 'absolute',
    display: 'block',
    top: '-10px',
    left: '-10px',
    clip: `rect(auto, 150px, auto, 45px)`,
    transform: `rotate(-135deg)`,
    '&::after': {
      content: "''",
      position: 'absolute',
      top: '10px',
      display: 'block',
      bottom: '10px',
      left: '10px',
      right: '10px',
      background: 'transparent linear-gradient(180deg, #EF191B 0%, #780D0E 100%) 0% 0% no-repeat padding-box',
      transform: 'rotate(-45deg)',
    },
  },
  content: {
    padding: '0 15px 61px 42px !important',
    display: 'flex',
    flex: 1,
    flexDirection: 'column',
    height: '100%',
  },
  heading: {
    marginTop: '10px',
    fontSize: '25px',
    color: 'black',
  },
  offset: {
    color: theme.palette.text.disabled,
    fontSize: '46px',
  },
  offsetActive: {
    color: theme.palette.primary.light,
    fontSize: '53px',
  },
  question: {
    flex: 1,
    // height: '167px',
    // marginBottom: '10px',
  },
  control: {
    // flex: 1,
  },
  title: {
    fontSize: '16px',
    // marginBottom: '10px',
    color: theme.palette.text.secondary,
    width: '100%',
  },
  subTitle: {
    fontSize: '22px',
    color: 'black',
    fontWeight: 'bolder',
  },
}));

const useYesNoStyles = makeStyles({
  root: {
    width: '110px',
    margin: '115px auto 0 auto',
  },
  button: {
    borderRadius: '15px',
    color: '#7E8EAA',
    padding: '5px 10px',
    minWidth: 'fit-content',
    lineHeight: 1.15,
  },
  buttonNonSelected: {
    background: 'transparent !important',
    boxShadow: 'none !important',
    '&:hover': {
      background: 'transparent !important',
      boxShadow: 'none !important',
    },
  },
  buttonSelected: {
    boxShadow: 'none !important',
    background: '#EF191B !important',
    color: 'white !important',
    '&:hover': {
      background: '#EF191B !important',
      boxShadow: 'none !important',
      color: 'white !important',
    },
  },
  yes: {
    boxShadow: '0px 5px 20px #CFECF8B3',
    background: 'white',
    '&:hover': {
      background: 'white',
      boxShadow: '0px 5px 20px #CFECF8B3',
    },
  },
  no: {
    background: 'transparent',
    boxShadow: 'none',
    '&:hover': {
      background: 'transparent',
      boxShadow: 'none',
    },
  },
});

const useCheckListStyles = makeStyles({
  root: {
    margin: '-50px 0 0 30px',
  },
  optionLabel: {
    fontSize: '11px',
    color: 'black',
  },
  optionRoot: {
    marginBottom: '10px',
  },
  radio: {
    padding: '0 3px',
  },
});

const useRootStyles = makeStyles(theme => ({
  root: {
    backgroundImage: `url(${StaticSources.bg.questionMainBg})`,
    padding: '60px 0 121px 0',
    ...FullCoverBgStyles,
  },
  popRoot: {
    margin: '0 136px',
    textAlign: 'center',
    '&:before': {
      content: "' '",
      display: 'block',
      width: 'calc(100% - 5px)',
      height: '290px',
      backgroundImage: `url(${StaticSources.bg.steppop})`,
      backgroundSize: 'cover',
      position: 'absolute',
      left: 0,
      top: 0,
      zIndex: 0,
    },
  },
  popContainer: {
    width: '626px',
    margin: '0 auto',
    position: 'relative',
    paddingBottom: '38px',
    zIndex: 9,
  },
  popMessage: {
    margin: '50px 0 28px 0',
    color: 'white',
    fontSize: '18px',
    textAlign: 'left',
  },
  popHeader: {
    marginBottom: '32px',
    color: 'white',
    fontSize: '26px',
    fontWeight: 700,
    textAlign: 'left',
  },
  listControl: {
    margin: '0 -20px',
  },
  btn: {
    fontWeight: 'bold',
    width: '100%',
    height: '45px',
    borderRadius: '9x',
    boxShadow: '2px 6px 6px #00000029',
    margin: '0 20px',
  },
  learn: {
    background: 'transparent linear-gradient(180deg, #F4AD3B 0%, #624103 100%) 0% 0% no-repeat padding-box',
    '&:hover': {
      background: 'transparent linear-gradient(180deg, #F4AD3B 0%, #624103 100%) 0% 0% no-repeat padding-box',
    },
  },
  popHeaderIcon: {
    width: '37px',
    marginRight: '30px',
  },
  popList: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
    boxShadow: '2px 6px 8px #E5ECF3',
    borderRadius: '15px',
    marginBottom: '43px',
  },
  listItem: {
    marginBottom: '28px',
    fontSize: '16px',
    color: theme.palette.text.primary,
    textAlign: 'left',
  },
  listIcon: {
    width: '52px',
    height: '52px',
    marginRight: '39px',
  },
  popListBox: {
    padding: '47px 75px 34px 75px',
  },
  smallBar: {
    height: '52px',
  },
  largeBar: {
    flex: 1,
  },
  topHeading: {
    marginBottom: '130px',
  },
  heading: {
    position: 'relative',
    '&::before': {
      content: "''",
      display: 'block',
      position: 'absolute',
      height: '6px',
      width: '220px',
      bottom: '-15px',
      background: theme.palette.primary.main,
    },
  },
  title: {
    fontSize: '27px',
    fontWeight: 'normal',
    color: 'black',
  },
  subTitle: {
    fontSize: '29px',
    fontWeight: 'bolder',
    color: 'black',
    textTransform: 'uppercase'
  },
}));

export {
  useCardStyles,
  useYesNoStyles,
  useRootStyles,
  useProgressStyles,
  useCheckListStyles,
};
