import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import Faq from '@containers/section/Faq'
import ContactUs from '@containers/section/ContactUs'
import TitleBanner, { TitleBannerProps } from '@containers/banner/TitleBanner'
import StaticSources from '@config/sources';
import { fetchContact, fetchGeneralContact } from '@store/service/actions';

import { storeWrapper } from '@store/store'
import { fetchSolutionRules } from '@store/common/actions'
import ServiceCentre from '@containers/section/ServiceCentre'

const CustomerCentrePage: React.FC = () => {
    const { t } = useTranslation('common');
    const { t: st } = useTranslation('section');
    return (
        <HeadingLayout>
            <Head>
                <title>{t('menu.cs')}</title>
            </Head>
            <ServiceCentre />
            <ContactUs />
        </HeadingLayout>
    );
}

export const getStaticProps: GetStaticProps = storeWrapper.getStaticProps(
    async ({ store, ...props }: any) => {
        const isServer = true;
        await Promise.all([
            store.dispatch(fetchContact(isServer)),
            store.dispatch(fetchGeneralContact(isServer)),
        ]);
        return {
          props: {
            ...await serverSideTranslations(props.locale, ['common', 'form', 'button', 'footer', 'section']),
          }
        };
    }
)
  
export default CustomerCentrePage
