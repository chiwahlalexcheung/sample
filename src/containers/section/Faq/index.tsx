import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from '@material-ui/core';
import {
    ExpandMore
} from '@material-ui/icons';
import {
  useStyles,
  useBoxStyles,
  useAccordionStyles
} from './styles';
import { useFaq } from '@store/content/context'
import { useLocale, LocaleContextProps } from '@components/common/Locale'

export interface FaqListProps {
    index?: any;
    title: string;
    body: string;
}

export interface FaqListsProps {
  list: FaqListProps[];
}

export const FaqRow: FC<FaqListProps> = ({title, body, index}) => {
    const classes = useAccordionStyles();
    return(
        <Accordion className={classes.root}>
            <AccordionSummary 
                className={classes.summary}
                expandIcon={<ExpandMore />}
            >
                <div dangerouslySetInnerHTML={{__html: `${index}. ${title}`}} />
            </AccordionSummary>
            <AccordionDetails>
                <div dangerouslySetInnerHTML={{__html: body}} />
            </AccordionDetails>
        </Accordion>
    )
}
export const Faq: FC<any> = () => {
    const classes = useBoxStyles();
    const { inspector }: LocaleContextProps = useLocale();
    const { list } = useFaq();
    
    const faqList: FaqListProps[] = map(list, (item)=>({
        title: inspector({
            target: item,
            field: 'question',
            defaultValue: '',
        }),
        body: inspector({
            target: item,
            field: 'answer',
            defaultValue: '',
        })
    }));
    
    return (
        <Box display={'flex'} className={classes.root} flexDirection={'column'} justifyContent={'flex-start'} >
            {map(faqList, (item: FaqListProps, index)=>{
                item.index = (index+1);
                return(
                    <FaqRow {...item} key={index}  />
                )
            })}
        </Box>
    );

}
export default Faq;
