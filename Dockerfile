FROM node:12

# Create app directory
WORKDIR ~/shkc-frontend

# Install app dependencies
COPY package*.json ~/shkc-frontend
COPY yarn.lock ~/shkc-frontend
RUN npm install -g yarn pm2

# Copying source files
COPY . ~/shkc-frontend

# Building app
RUN yarn install

# Running the app
EXPOSE 3000
CMD "yarn" "run" "dev"
