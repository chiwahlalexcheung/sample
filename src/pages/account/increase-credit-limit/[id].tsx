import { useRef } from "react";
import Head from "next/head";
import HeadingLayout from "@containers/layout/Heading";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { useTranslation } from "next-i18next";
// import Fade from 'react-reveal/Fade';
import { GetServerSideProps } from "next";
import { storeWrapper } from "@store/store";
import IncreaseCreditLimit from "@containers/section/IncreseCreditLimit";

const IncreaseCreditLimitPage: React.FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation("common");
  return (
    <HeadingLayout>
      <Head>
        <title>{t("menu.increase-credit-limit")}</title>
      </Head>
      <IncreaseCreditLimit applicationNo={applicationNo} />
    </HeadingLayout>
  );
};

export const getServerSideProps: GetServerSideProps = storeWrapper.getServerSideProps(
  async ({ locale, params = {} }: any) => {
    return {
      props: {
        applicationNo: params.id,
        ...(await serverSideTranslations(locale, [
          "common",
          "form",
          "button",
          "footer",
          "section",
        ])),
      },
    };
  }
);
export default IncreaseCreditLimitPage;
