import TextInput, { InputProps } from './text'
import NumericInput, {
  NumericProps,
  NumericPrefixProps,
  FormPrefixNumeric,
  NumericUnitProps,
  FormUnitNumeric,
} from './number'
import NativeSelect, { NativeSelectProps } from './nativeSelect'
import NativeSlider, { FormSliderProps } from './inputSlider'
import FormMultilineInput, { FormMultilineInputProps } from './textarea'
import NativeSwitcher, {NativeSwitcherProps} from './switch'
import NativeCheckBoxer, {NativeCheckboxesProps as NativeCheckBoxerProps} from './checkbox'
import SearchSelect, { SearchSelectProps } from './searchSelect'
import NativeRadioGroup, { NativeRadioGroupProps } from './radioGroup'


export interface FormElementProps {
  TextInput: React.FC<InputProps>;
  NumericInput: React.FC<NumericProps>;
  NativeSelect: React.FC<NativeSelectProps>;
  PrefixNumericInput: React.FC<NumericPrefixProps>;
  SuffixNumericInput: React.FC<NumericUnitProps>;
  InputSlider: React.FC<FormSliderProps>;
  TextMultilineInput: React.FC<FormMultilineInputProps>;
  NativeSwitcher: React.FC<NativeSwitcherProps>;
  NativeCheckbox: React.FC<NativeCheckBoxerProps>;
  SearchSelect: React.FC<SearchSelectProps>;
  NativeRadioGroup: React.FC<NativeRadioGroupProps>;
}

const FormElement: FormElementProps = {
  TextInput,
  NumericInput,
  NativeSelect,
  PrefixNumericInput: FormPrefixNumeric,
  SuffixNumericInput: FormUnitNumeric,
  InputSlider: NativeSlider,
  NativeSwitcher,
  TextMultilineInput: FormMultilineInput,
  NativeCheckbox: NativeCheckBoxer,
  SearchSelect,
  NativeRadioGroup,
};

export default FormElement;
