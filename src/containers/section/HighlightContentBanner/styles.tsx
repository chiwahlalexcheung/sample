import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '43px 75px',
    backgroundImage: `url(${StaticSources.banner.revolvingHighlightBanner})`,
    ...FullCoverBgStyles,
  },
  left: {
    width: '50%',
  },
  right: {
    flex: 1,
  },
  slider: {
    width: '100%',
    position: 'relative',
  },
  prev: {
    zIndex: 99999,
    left: '-80px',
  },
  next: {
    zIndex: 99999,
    right: '-80px',
  },
}));
const useBoxStyles = makeStyles(theme => ({
    advantagesCard: {
        flex: 1,
        height: '100%',
        backgroundColor: 'white !important',
        // margin: 20,
        padding: 50,
        // borderRadius: 20, 
        boxShadow: '0px 3px 6px #00000029',
    },
    title: {
      fontSize: '26px',
      color: theme.palette.text.primary,
      letterSpacing: '0px',
      fontWeight: 'bolder',
    },
    subTitle: {
      fontSize: '26px',
      textTransform: 'uppercase',
      color: theme.palette.primary.main,
      letterSpacing: '0px',
      fontWeight: 'bolder',
    },
    list: {
      paddingLeft: '20px',
      '@global': {
        'li': {
          marginBottom: '10px',
        },
      },
    },
    li: {
      paddingLeft: '10px',
      color: theme.palette.primary.light,
      position: 'relative',
      listStyle: 'none',
      fontSize: '18px',
      fontStyle: 'italic',
      lineHeight: '21px',
      wordSpacing: '-0.22px',
      '&::before': {
        content: "''",
        width: '20px',
        height: '20px',
        left: '-20px',
        top: '3px',
        display: 'block',
        position: 'absolute',
        backgroundImage: `url(${StaticSources.icon.advantagesCheck})`,
      },
    },
}))

const useCardStyles = makeStyles(theme => ({
    root: {
        background: '#FFFFFF 0% 0% no-repeat padding-box',
        boxShadow: '2px 10px 6px #00000029',
        borderRadius: '8px',
        width: 317,
        height: '100%',
        margin: 10,
        padding: 15,
        opacity: 1,
        backdropFilter: 'blur(50px)',
        justifyContent: 'center'

    },
    content: {
        padding: '0',
        height: '100%',
        alignSelf: 'center'
    },
    title: {
        fontStyle: 'bold',
        fontSize: '22px',
        letterSpacing: '0px',
        color: theme.palette.text.primary,
    },
    subTitle: {
        fontStyle: 'bold',
        fontSize: '22px',
        letterSpacing: '0px',
        color: theme.palette.text.primary,

    },
    image: {
        display: 'flex',
        flex : 1
    },
    img: {
    },
    button: {
    },
    btn: {
    },
    wrapper: {
        padding: '10px',
        height: '400px'
    }

}))
export {
  useStyles,
  useBoxStyles,
  useCardStyles
};
