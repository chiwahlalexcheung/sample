import { parse } from 'url'
import path from 'path';
import express from 'express'
import next from 'next'

const port = process.env.PORT ? parseInt(process.env.PORT, 10) : 3000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

const startApp = async () => {
  await app.prepare()
  const server = express()
  const staticPath = path.join(__dirname, './../public/images')
  server.use('/images', express.static(staticPath, {
    maxAge: '30d',
    immutable: true
  }));
  server.get('*', (req, res) => {
    return handle(req, res, parse(req.url || '', true))
  })
  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`)
  })
}

startApp()
