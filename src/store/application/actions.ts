import { Action } from 'redux'
import { get, map } from 'lodash'
import Config from '@config/service';
import APIManager from '@lib/apiManager';

export enum ApplicationActionTypes {
  SUBMIT_APPLICATION = 'SUBMIT_APPLICATION',
  SUBMIT_APPLICATION_SUCCEED = 'SUBMIT_APPLICATION_SUCCEED',
  SUBMIT_APPLICATION_FAILED = 'SUBMIT_APPLICATION_FAILED',
  SEND_OTP = 'SEND_OTP',
  SEND_OTP_SUCCEED = 'SEND_OTP_SUCCEED',
  SEND_OTP_FAILED = 'SEND_OTP_FAILED',
  VERIFY_OTP = 'VERIFY_OTP',
  VERIFY_OTP_SUCCEED = 'VERIFY_OTP_SUCCEED',
  VERIFY_OTP_FAILED = 'VERIFY_OTP_FAILED',
  FETCH_EVALUATION = 'FETCH_EVALUATION',
  FETCH_EVALUATION_SUCCEED = 'FETCH_EVALUATION_SUCCEED',
  FETCH_EVALUATION_FAILED = 'FETCH_EVALUATION_FAILED',
}

export type ApplicationAction =
  | Action<ApplicationActionTypes.SUBMIT_APPLICATION>
  | Action<ApplicationActionTypes.SUBMIT_APPLICATION_SUCCEED>
  | Action<ApplicationActionTypes.SUBMIT_APPLICATION_FAILED>
  | Action<ApplicationActionTypes.SEND_OTP>
  | Action<ApplicationActionTypes.SEND_OTP_SUCCEED>
  | Action<ApplicationActionTypes.SEND_OTP_FAILED>
  | Action<ApplicationActionTypes.VERIFY_OTP>
  | Action<ApplicationActionTypes.VERIFY_OTP_SUCCEED>
  | Action<ApplicationActionTypes.VERIFY_OTP_FAILED>
  | Action<ApplicationActionTypes.FETCH_EVALUATION>
  | Action<ApplicationActionTypes.FETCH_EVALUATION_SUCCEED>
  | Action<ApplicationActionTypes.FETCH_EVALUATION_FAILED>

  export const requestVerifyOtp = ()  => ({ type: ApplicationActionTypes.VERIFY_OTP });
  export const requestVerifyOtpSucceed = (payload)  => ({ type: ApplicationActionTypes.VERIFY_OTP_SUCCEED, payload });
  export const requestVerifyOtpError = (payload) => ({ type: ApplicationActionTypes.VERIFY_OTP_FAILED, payload });
  export const verifyOtp = (info: any) => async (dispatch) => {
    dispatch(requestVerifyOtp());
    const { data, error } = await APIManager.post({
        dispatch,
        url: 'verifyOtp',
        data: info,
        underAuth: false,
    });
    if (error) {
      dispatch(requestVerifyOtpError(error));
      return null;
    } else if (data && !data.success) {
      dispatch(requestVerifyOtpError('message.verifyOtpError'));
      return null;
    } else {
      dispatch(requestVerifyOtpSucceed(data));
    }
    return data;
  };

export const requestApplication = ()  => ({ type: ApplicationActionTypes.SUBMIT_APPLICATION });
export const requestApplicationSucceed = (payload)  => ({ type: ApplicationActionTypes.SUBMIT_APPLICATION_SUCCEED, payload });
export const requestApplicationError = (payload) => ({ type: ApplicationActionTypes.SUBMIT_APPLICATION_FAILED, payload });
export const submitApplication = (info: any={}) => async (dispatch) => {
  // TODO: workaround for demo, use authed user token
  APIManager.loginToken = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF90eXAiOiJJIiwiaXNzX2N0cnkiOiJIS0ciLCJpZF9ubyI6IkExMjM0NTYzIiwiY3VzdF9pZF9jb21iIjoiSEtHSUExMjM0NTYzIiwibW9iaWxlIjoiOTQ1NTkyNjYiLCJjb2RlIjoiKzg1MiIsImlhdCI6MTYxODgyMDYzNSwiZXhwIjoxNjUwMzU2NjM1fQ.1uLIiRNOpszw8Et2greXfomUGxlHh-zsRD_cG9fDjwY';
  dispatch(requestApplication());
  const { data, error } = await APIManager.post({
      dispatch,
      url: 'createApplication',
      data: info,
      noFormData: true,
  });
  const response: any = get(data, 'LOS.BODY') || {};
  if (response.rtn_msg) {
    const { rtn_msg } = response;
    dispatch(requestApplicationError(rtn_msg));
    return { error: rtn_msg }
  } else {
    dispatch(requestApplicationSucceed(response));
  }
  return { info: response };
};

export const requestSendOTP = ()  => ({ type: ApplicationActionTypes.SEND_OTP });
export const requestSendOTPSucceed = (payload)  => ({ type: ApplicationActionTypes.SEND_OTP_SUCCEED, payload });
export const requestSendOTPError = (payload) => ({ type: ApplicationActionTypes.SEND_OTP_FAILED, payload });
export const sendOTP = (info: any) => async (dispatch) => {
  dispatch(requestSendOTP());
  const { data, error } = await APIManager.post({
      dispatch,
      url: 'sendOtp',
      data: {
        ...info,
        live: Config.smsLive,
      },
      underAuth: false,
  });
  if (error) {
    dispatch(requestSendOTPError(error));
    return null;
  } else if (data && !data.success) {
    dispatch(requestSendOTPError('message.verifyCodeError'));
    return null;
  } else {
    dispatch(requestSendOTPSucceed(data));
  }
  return data;
};

export const requestEvaluation = ()  => ({ type: ApplicationActionTypes.FETCH_EVALUATION });
export const requestEvaluationSucceed = (infoType, payload)  => ({ type: ApplicationActionTypes.FETCH_EVALUATION_SUCCEED, infoType, payload });
export const requestEvaluationError = (payload) => ({ type: ApplicationActionTypes.FETCH_EVALUATION_FAILED, payload });
export const fetchEvaluation = (isServer: boolean, type: string, info: any) => async (dispatch) => {
  dispatch(requestEvaluation());
  const { data, error } = await APIManager.get({
      dispatch,
      url: `evaluation.${type}`,
      isServer, 
      data: {
        ...info
      },
      underAuth: false,
  });
  
  if (error) {
    dispatch(requestEvaluationError(error));
    return null;
  } else {
    const normalizedData = map(get(data, 'data', []), (value, key)=>({
        key: key,
        label: value,
        value: key,
        link: ''
    }))
    dispatch(requestEvaluationSucceed(type, normalizedData));
  }
  return data;
};