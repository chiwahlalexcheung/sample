import Cookies from 'universal-cookie';
import Config from '@config/service';
import { get } from 'lodash';

class CookieHelper {
    clientCookie: any;
    serverCookie: any;
    constructor() {
        this.clientCookie = new Cookies();
        this.serverCookie = null;
    }

    isServer() {
        return typeof window === 'undefined' && !!this.serverCookie;
    }

    setReq(req: any) {
        this.serverCookie = req;
    }

    getObject(key: string | number, id: string) {
        let target;
        try {
            target = this.get(key);
            if (!!target) {
                // console.log('>>>>>>>> ', target, typeof target);
                target = JSON.parse(target);
            }
        } catch (err) {
            console.log('>>> Get Object Error: ', err);
        }
        if (!!id) {
            target = get(target, id);
        }
        return target;
    }

    setObject(key: string | number, info: any[]=[]) {
        try {
            let target = this.get(key);
            if (!!target) {
                target = { ...target, ...info };
            } else {
                target = info;
            }
            console.log(target);
            this.set(key, JSON.stringify(target));
        } catch (err) {
            console.log('>>> Set Object Error: ', err);
        }
    }

    get(key: string | number) {
        if (this.isServer()) {
            return this.serverCookie.get(key);
        } else {
            return this.clientCookie.get(key);
        }
    }

    set(key: string | number, data: any, options: any={}) {
        const opt = { ...Config.cookieOptions, ...options };
        if (this.isServer()) {
            this.serverCookie.set(key, data, opt);
        } else {
            this.clientCookie.set(key, data, opt);
        }
    }

    remove(key: string | number, options: any={}) {
        const opt = { ...Config.cookieOptions, ...options };
        if (this.isServer()) {
            this.serverCookie.remove(key, opt);
        } else {
            this.clientCookie.remove(key, opt);
        }
    }
}

export default new CookieHelper();