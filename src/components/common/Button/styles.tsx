import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    borderRadius: '10px',
    color: 'white',
    background: `transparent linear-gradient(181deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.main} 100%) 0% 0% no-repeat padding-box`,
    '&:hover': {
      background: `transparent linear-gradient(181deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.main} 100%) 0% 0% no-repeat padding-box`,
    }
  },
  secondary: {
    background: 'transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box !important',
    '&:hover': {
      background: `transparent linear-gradient(180deg, #565B70 0%, #121314 100%) 0% 0% no-repeat padding-box !important`,
    }
  },
  icon: {
    height: '16px',
    width: '16px',
    marginRight: '10px',
    resizeMode: 'contain'
  }
  
}));

export {
  useStyles,
};
