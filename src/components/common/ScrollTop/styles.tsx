import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    position: 'fixed',
    bottom: '-88px',
    right: '20px',
    zIndex: 9999,
    cursor: 'pointer',
  },
  hide: {
    transition: 'bottom 0.3s',
    bottom: '-88px',
  },
  show: {
    transition: 'bottom 0.3s',
    bottom: '0px',
    opacity: 0.5,
    '&:hover':{
      opacity: 1,
    }
  },
  wrapper: {
    position: 'relative',
    width: '58px',
    height: '58px',
    // borderRadius: '50%',
    // background: '#FFFFFF 0% 0% no-repeat padding-box',
    // boxShadow: '0px 5px 20px #CFECF8B3',
    // padding: '10px 7px',
    // fontWeight: 'bolder',
  }
}));

export {
  useStyles,
};
