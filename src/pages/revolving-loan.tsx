import { useRef, useCallback } from 'react'
import { get } from 'lodash'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
// import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetStaticProps } from 'next'
import StaticSources from '@config/sources'
import MainStepSolution from '@containers/section/StepSolution'
import RevolvingBanner from '@containers/section/RevolvingBanner'
import HighlightContentBanner from '@containers/section/HighlightContentBanner'
import Footnotes from '@containers/section/Footnotes'

/**
 * Revolving Loan Page
 */
const RevolvingLoanPage: React.FC = () => {
  const banner = useRef<any>();
  const highlightContentRef = useRef<any>();
  const refs = {
    banner,
    highlightContentRef
  };
  // const {t} = useTranslation();
  const slide = useCallback((target: string) => {
    const ref = get(refs, target, {});
    if (ref.current) {
      ref.current.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
  }, []);
  const bannerInfo = {
    title: 'Unlimited Re-draw of Fund',
    subTitle: 'Revolving Loan',
    description: 'Unleash the POWER of your property! Sun Hung Kai Credit, your best mortgage choice, offers you flexible mortgage, which can help you save interest expenses and ready when needed funding to achieve your life goals such as home purchase, investment, startup, business expansion or planning overseas study for your children!',
    tips: '3-STEP SIMPLE APPLICATION, APPLY NOW!',
    banner: StaticSources.banner.revolvingLoanBanner,
    buttons: [
        {
            label: 'FIRST MORTAGE LOAN',
            link: '/',
            width: '100%'
        },
        {
            label: 'SECOND MORTAGE LOAN',
            link: '/'
        }
    ]
  }
  return (
    <HeadingLayout>
      <RevolvingBanner bannerInfo={bannerInfo} />
      <HighlightContentBanner />
      <Footnotes />
    </HeadingLayout>
  );
}

export const getStaticProps: GetStaticProps = async ({ locale={} }: any) => ({
  props: {
    ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
  }
})
export default RevolvingLoanPage
