import _ from 'lodash'
import { CommonAction, CommonActionTypes } from './actions'
import { ApplicationActionTypes } from './../application/actions'
import { BasicInfoStateProps, BasicListStateProps } from '@interfaces/state'
import { CalculationInfoPayloadProps } from './actions';
export interface CommonState {
  readonly config: BasicInfoStateProps;
  readonly ruleQuestions: BasicListStateProps;
  readonly applyRule: BasicInfoStateProps;
  readonly applyCalculation: CalculationInfoPayloadProps;
}

const initialCommonState = {
  config: {
    info: null,
    loading: false,
    error: null,
  },
  ruleQuestions: {
    list: [],
    loading: false,
    error: null,
  },
  applyRule: {
    info: null,
    loading: false,
    error: null,
  },
  applyCalculation: {
    loanAmount: 0,
    loanType: 'first',
    calculateType: 'interestOnly',
  },
}

/**
 * Config Reducer
 */
export const commonReducer = (
  state: CommonState = initialCommonState,
  action: any
): CommonState => {
  const { config, ruleQuestions, applyRule } = state;
  switch (action.type) {
    case CommonActionTypes.APPLY_CALCULATION:
      return {
        ...state,
        applyCalculation: action.payload,
      }
    case CommonActionTypes.FETCH_CONFIG:
      return {
        ...state,
        config: {
          ...config,
          loading: true,
        },
      }
    case CommonActionTypes.FETCH_CONFIG_SUCCEED:
      return {
        ...state,
        config: {
          ...config,
          loading: false,
          info: action.payload,
        },
      }
    case CommonActionTypes.FETCH_CONFIG_FAILED:
      return {
        ...state,
        config: {
          ...config,
          loading: false,
          error: action.payload,
        },
      }

    case CommonActionTypes.FETCH_SOLUTION_RULES:
      return {
        ...state,
        ruleQuestions: {
          ...ruleQuestions,
          loading: true,
        },
      }
    case CommonActionTypes.FETCH_SOLUTION_RULES_SUCCEED:
      const { payload=[] } = action;
      // // normalize to { 'rule_road': { target, rule } }
      // payload = _.chain(payload).reduce((a: any[], d: any) => ([
      //   ...d,
      //   {
      //     rule: `${d.step1}-${d.step3}-${d.step4}`,
      //     target: d.target, // target popup type
      //   },
      // ]), []).keyBy('rule').value();
      return {
        ...state,
        ruleQuestions: {
          ...ruleQuestions,
          loading: false,
          list: payload,
        },
      }
    case CommonActionTypes.FETCH_SOLUTION_RULES_FAILED:
      return {
        ...state,
        ruleQuestions: {
          ...ruleQuestions,
          loading: false,
          error: action.payload,
        },
      }

    case CommonActionTypes.APPLY_SOLUTION_RULES:
      return {
        ...state,
        applyRule: {
          ...applyRule,
          loading: true,
        },
      }
    case CommonActionTypes.APPLY_SOLUTION_RULES_SUCCEED:
      return {
        ...state,
        applyRule: {
          ...applyRule,
          loading: false,
          info: action.payload,
        },
      }
    case CommonActionTypes.APPLY_SOLUTION_RULES_FAILED:
      return {
        ...state,
        applyRule: {
          ...applyRule,
          loading: false,
          error: action.payload,
        },
      }
    // [IMPORTANT] Once the application submitted, clean the applied rule for 3 question on landing
    case CommonActionTypes.REMOVE_APPLIED_CALCULATION:
      return {
        ...state,
        applyCalculation: initialCommonState.applyCalculation,
      }
    case ApplicationActionTypes.SUBMIT_APPLICATION_SUCCEED:
      return {
        ...state,
        applyRule: initialCommonState.applyRule,
        applyCalculation: initialCommonState.applyCalculation,
      }
      
    default:
      return state
  }
}
