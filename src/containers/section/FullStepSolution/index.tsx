import React, { FC, forwardRef, useCallback } from 'react'
import { map, times} from 'lodash'
import css from 'clsx'
import Routes from '@config/routes'
import { useRouter } from 'next/router'
// import Zoom from 'react-reveal/Zoom'
// import Fade from 'react-reveal/Fade'
// import Shake from 'react-reveal/Shake'
// import Slide from 'react-reveal/Slide'
import StaticSources from '@config/sources';
import LocalImage from '@components/image/LocalImage'
import CommonButton from '@components/common/Button'
import LinearProgress from '@material-ui/core/LinearProgress'
import FormElement from '@components/form'
import { useTranslation } from 'next-i18next'
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  Typography,
  Box,
} from '@material-ui/core';

import {
  PropertyContactInformation,
  PropertyContactInformationReview,
} from './form';

import {
  CompletionInformation,
} from './complete';

import {
  useBarItemStyles,
  useBarStyles,
  useStyles,
  useProgressStyles,
  useButtonStyles,
} from './styles';
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
// import {
//   useProgressStyles
// } from '@containers/section/StepSolution/styles'

export interface StepListItemProps {
  onIcon: string;
  label: string;
  offIcon: string;
  key: string;
  component: JSX.Element;
}

export interface StepBarProps {
  list: StepListItemProps[];
};
export interface StepBarItemProps extends StepListItemProps {
  onClick: Function;
  active: Boolean;
  offset: number;
}
export const StepBarItem: FC<StepBarItemProps> = ({ offset, active, onClick, onIcon, offIcon, label }) => {
  const classes = useBarItemStyles();
  const {
    step,
    isLastStep,
    maxStep,
  }: StepSolutionContext = useStepSolution();
  const onPress = useCallback(() => {
    if (!active) {
      onClick(offset);
    }
  }, [active, onClick, offset]);
  let addOn = {};
  let styles = classes.root;
  let iconStyles = classes.icon;
  let titleStyles = classes.title;
  if (!active) {
    if (offset <= step && offset !== step && !isLastStep) {
      addOn = { onClick: onPress };
    }
    styles = css(styles, classes.inActive);
    iconStyles = css(iconStyles, classes.inActiveIcon);
    titleStyles = css(titleStyles, classes.inActiveTitle);
  } else {
    iconStyles = css(iconStyles, classes.activeIcon);
    styles = css(styles, classes.active);
    titleStyles = css(titleStyles, classes.activeTitle);
  }
  return (
    <Box display={'flex'} alignItems={'center'} className={styles} {...addOn}>
      <LocalImage src={active ? onIcon : offIcon} alt={label} className={iconStyles} />
      <Typography component={'h1'} className={titleStyles}>
        {label}
      </Typography>
    </Box>
  );
}

export const MortgageSwitch: FC = () => {
  const { t } = useTranslation('section');
  const {
    control,
  }: StepSolutionContext = useStepSolution();
  const classes = useButtonStyles();
  return (
    <FormElement.NativeSwitcher
      control={control}
      name='type'
      className={classes.switchLabel}
      offLabel={t('label.first-mortgage')}
      onLabel={t('label.second-mortgage')}
    />
  );
}

export interface BigBoxButtonProps {
  label: string;
  className?: string;
  onClick: Function;
}

export const BigBoxButton: FC<BigBoxButtonProps> = ({ onClick, className='', label }) => {
  const classes = useButtonStyles();
  return (
    <CommonButton
      className={css(classes.root, className)}
      label={label}
      onClick={onClick}
    />
  );
};

export const BigBoxButtons: FC = () => {
  const { t } = useTranslation('section');
  const classes = useButtonStyles();
  const router = useRouter();
  const onCalculator = useCallback(() => {
    router.push(Routes.loanCalculator);
  }, [router]);
  const onValuation = useCallback(() => {
    router.push(Routes.valuation);
  }, [router]);
  return (
    <React.Fragment>
      <BigBoxButton
        onClick={onCalculator}
        label={t('useful-tools.mortgageLine')}
      />
      <BigBoxButton
        onClick={onValuation}
        label={t('useful-tools.valuationServiceLine')}
        className={classes.yellow}
      />
    </React.Fragment>
  );
};

export const ProgressBar: FC = () => {
  const { t } = useTranslation('section');
  const classes = useProgressStyles();
  const {
    maxStep,
    goStep,
    step,
    stepValue,
    isLastStep,
  }: StepSolutionContext = useStepSolution();
  const onPress = useCallback((offset: number) => {
    goStep(offset);
  }, [goStep]);
  return (
    <Box display={'flex'} alignItems={'center'} justifyContent={'center'} className={classes.root}>
      <Box display={'flex'} className={classes.bar}>
        <LinearProgress
          variant="determinate"
          classes={{
            bar: classes.barContent,
            colorPrimary: classes.mainBarContent,
            barColorPrimary: classes.barBufferContent,
          }}
          value={stepValue}
        />
        {
          times(maxStep, (s: number) => {
            let styles = css(classes.step, classes[`step${s + 1}`]);
            let wrapperStyles = classes.wrapper;
            let addOn = {};
            if (s <= step) {
              styles = css(styles, classes.activeStep);
            }
            if (s <= step && s !== step && !isLastStep) {
              addOn = { onClick: () => onPress(s) };
              styles = css(styles, classes.clickableStep);
            }
            if (s < step) {
              wrapperStyles = css(wrapperStyles, classes.finished);
            }
            return (
              <Box display={'flex'}
                key={s}
                {...addOn}
                className={styles}>
                <div className={wrapperStyles}>
                  {t('label.step', { step: s + 1 })}
                </div>
              </Box>
            );
          })
        }
      </Box>
    </Box>
  );
}

export const StepBar: FC<StepBarProps> = ({ list }) => {
  const classes = useBarStyles();
  const {
    step,
    goStep,
  }: StepSolutionContext = useStepSolution();
  return (
    <div className={classes.root}>
      <Box display={'flex'} className={classes.step} justifyContent={'space-between'} alignItems={'center'}>
        {
          map(list, ({ key, ...l }: StepListItemProps, index: number) => (
            <StepBarItem
              key={key}
              {...l}
              active={index <= step}
              offset={index}
              onClick={goStep}
            />
          ))
        }
      </Box>
      <ProgressBar />
    </div>
  );
}

export const StepContent: FC<StepBarProps> = ({ list }) => {
  const {
    step,
  }: StepSolutionContext = useStepSolution();
  return (
    <div>
      {
        map(list, ({ key, ...l }: StepListItemProps, index: number) => {
          let styles = { display: 'none' };
          if (index === step) {
            styles = { display: 'block' };
          }
          return (
            <div key={key} style={styles}>
              {l.component}
            </div>
          );
        })
      }
    </div>
  );
}

export const FullStepSolution: FC<any> = forwardRef(({ slide, initialType='first' }, r) => {
  const { t } = useTranslation('section');
  const classes = useStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);
  const steps: StepListItemProps[] = [
    {
      key: 'pi',
      label: t('label.propertyContactInformation'),
      onIcon: StaticSources.icon.stepOn,
      offIcon: StaticSources.icon.stepOff,
      component: <PropertyContactInformation />
    },
    {
      key: 'rc',
      label: t('label.reviewConfirmation'),
      onIcon: StaticSources.icon.stepOn,
      offIcon: StaticSources.icon.stepOff,
      component: <PropertyContactInformationReview />
    },
    {
      key: 'cp',
      label: t('label.completion'),
      onIcon: StaticSources.icon.stepOn,
      offIcon: StaticSources.icon.stepOff,
      component: <CompletionInformation />
    },
  ];
  return (
    <div ref={r as React.RefObject<HTMLDivElement>}>
      <StepSolution values={{
        loanType: initialType,
        sms: ["", "", "", "", "", ""],
      }} slide={slide} maxStep={steps.length} onSubmit={onSubmit}>
        <div className={classes.root}>
            <InnerPage>
              <div className={classes.wrapper}>
                <HeadingWithSubTitle
                  title={t('title.stepHeader')}
                  subTitle={t('title.stepSubTitle')}
                />
                <div className={classes.content}>
                  <StepBar list={steps} />
                </div>
                <Box display={'flex'} className={classes.tools} flexDirection={'column'} alignItems={'flex-end'}>
                  <Box display={'flex'} className={classes.tool} justifyContent={'space-between'}>
                    <BigBoxButtons />
                  </Box>
                  <div className={classes.switch}>
                    <MortgageSwitch />
                  </div>
                </Box>
                <div className={classes.form}>
                  <StepContent list={steps} />
                </div>
              </div>
            </InnerPage>
        </div>
      </StepSolution>
    </div>
  );
});

export default FullStepSolution;
