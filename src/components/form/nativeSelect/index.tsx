import React, { FC } from 'react'
import { Controller } from "react-hook-form";
import { map } from 'lodash'
import css from 'clsx'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'
import {
  Select,
} from '@material-ui/core'
import {
  FormRenderElementProps,
  FormInputProps,
} from '@interfaces/form'
import {
  LabelValueProps,
} from '@interfaces/list'
import {
  useStyles,
} from './styles'
import {
  useStyles as useInputStyles,
} from './../text/styles'

export interface NativeSelectProps extends FormRenderElementProps{
  // backgroundColor?: string;
  options: LabelValueProps[];
  specialClass?: string;
}

export interface NativeSelectOptionProps {
  onChange: Function;
  options: LabelValueProps[];
  value: any;
  inputProps?: any;
  specialClass: string;
};

const NativeSelectOption: FC<NativeSelectOptionProps> = ({ inputProps={}, onChange, options, value, specialClass }) => {
  const classes = useStyles();
  let dropIcon: string = StaticSources.icon.selectDrop;
  if(specialClass=='documentUpload') {
      dropIcon = StaticSources.icon.selectDropDark;
  }
  return (
    <Select
      native
      onChange={(e: any) => onChange(e)}
      value={value}
      IconComponent={() => (
        <div>
          <LocalImage src={dropIcon} alt={inputProps.name} />
        </div>
      )}
      classes={{
        // select: classes.select,
        select: css(classes.select, classes[specialClass])
      }}
      className={classes.root}
      inputProps={inputProps}
    >
      {
        map(options, ({ value, label }: LabelValueProps) => (
          <option value={value} key={value}>{label}</option>
        ))
      }
    </Select>
  );
}

const NativeSelector: FC<NativeSelectProps> = ({
  control,
  name,
  specialClass='interest',
  // backgroundColor,
  render,
  options=[],
  inputProps={},
}) => {
  const classes = useStyles();
  const inputClasses = useInputStyles();
  let addOn = {
    render: ({ onChange, value }: any) => (
      <NativeSelectOption 
        onChange={(r: any) => onChange(r)}
        value={value}
        inputProps={{
          ...inputProps,
          name,
        }}
        options={options}
        specialClass={specialClass}
      />
    ),
  };
  if (!!render) {
    addOn = { render: (payload: any) => render(payload) };
  }
  return (
    <div className={css(inputClasses.root, classes.root, classes[specialClass])}>
      <div>
        <Controller
          name={name}
          control={control}
          {...addOn}
        />
      </div>
    </div>
  );
}

export default NativeSelector;