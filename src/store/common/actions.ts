import { Action } from 'redux'
import APIManager from '@lib/apiManager';
import { isArray } from 'lodash';
import { SectionType } from './../content/actions';
export enum CommonActionTypes {
  FETCH_CONFIG = 'FETCH_CONFIG',
  FETCH_CONFIG_SUCCEED = 'FETCH_CONFIG_SUCCEED',
  FETCH_CONFIG_FAILED = 'FETCH_CONFIG_FAILED',

  FETCH_SOLUTION_RULES = 'FETCH_SOLUTION_RULES',
  FETCH_SOLUTION_RULES_SUCCEED = 'FETCH_SOLUTION_RULES_SUCCEED',
  FETCH_SOLUTION_RULES_FAILED = 'FETCH_SOLUTION_RULES_FAILED',

  APPLY_SOLUTION_RULES = 'APPLY_SOLUTION_RULES',
  APPLY_SOLUTION_RULES_SUCCEED = 'APPLY_SOLUTION_RULES_SUCCEED',
  APPLY_SOLUTION_RULES_FAILED = 'APPLY_SOLUTION_RULES_FAILED',

  APPLY_CALCULATION = 'APPLY_CALCULATION',
  REMOVE_APPLIED_CALCULATION = 'REMOVE_APPLIED_CALCULATION',
  // APPLY_CALCULATION_FAILED = 'APPLY_CALCULATION_FAILED',
}

export type CommonAction =
  | Action<CommonActionTypes.FETCH_CONFIG>
  | Action<CommonActionTypes.FETCH_CONFIG_SUCCEED>
  | Action<CommonActionTypes.FETCH_CONFIG_FAILED>
  | Action<CommonActionTypes.FETCH_SOLUTION_RULES>
  | Action<CommonActionTypes.FETCH_SOLUTION_RULES_SUCCEED>
  | Action<CommonActionTypes.FETCH_SOLUTION_RULES_FAILED>
  | Action<CommonActionTypes.APPLY_CALCULATION>
  | Action<CommonActionTypes.REMOVE_APPLIED_CALCULATION>

export const requestConfig = () => ({ type: CommonActionTypes.FETCH_CONFIG });
export const requestConfigSucceed = (payload) => ({ type: CommonActionTypes.FETCH_CONFIG_SUCCEED, payload });
export const requestConfigError = (payload) => ({ type: CommonActionTypes.FETCH_CONFIG_FAILED, payload });
export const fetchConfig = () => async (dispatch) => {
    dispatch(requestConfig());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'config',
        underAuth: false,
    });
    if (error) {
      dispatch(requestConfigError(error));
    } else {
      dispatch(requestConfigSucceed(data));
    }
};

export const requestSolutionRules = () => ({ type: CommonActionTypes.FETCH_SOLUTION_RULES });
export const requestSolutionRulesSucceed = (payload) => ({ type: CommonActionTypes.FETCH_SOLUTION_RULES_SUCCEED, payload });
export const requestSolutionRulesError = (payload) => ({ type: CommonActionTypes.FETCH_SOLUTION_RULES_FAILED, payload });
export const fetchSolutionRules = (isServer: boolean) => async (dispatch) => {
    dispatch(requestSolutionRules());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'rules',
        isServer,
        underAuth: false,
    });
    if (error) {
      dispatch(requestSolutionRulesError(error));
      return null;
    }
    const { questions } = data;
    // const target = isArray(questions) ? questions[0] : null;
    dispatch(requestSolutionRulesSucceed(questions));
    return questions;
};

export const normalizationProduct = (data: any): void => {
  switch (data.product) {
    case '1st-mtg':
      data.product = 'first';
      break;
    case '2nd-mtg':
      data.product = 'second';
      break;
    default:
      // TODO
  }
}
// [IMPORTANT] api logic changed, can keep this and directly call 
export const requestApplyRule = () => ({ type: CommonActionTypes.APPLY_SOLUTION_RULES });
export const requestApplyRuleSucceed = (payload) => ({ type: CommonActionTypes.APPLY_SOLUTION_RULES_SUCCEED, payload });
export const requestApplyRuleError = (payload) => ({ type: CommonActionTypes.APPLY_SOLUTION_RULES_FAILED, payload });
export const applyRule = (data: any) => async (dispatch) => {
  dispatch(requestApplyRule());
  // const { data, error } = await APIManager.post({
  //     dispatch,
  //     url: 'rules',
  //     data: payload,
  //     underAuth: false,
  // });
  // if (error) {
  //   dispatch(requestApplyRuleError(error));
  // } else {
  normalizationProduct(data);
  setTimeout(() => {
    dispatch(requestApplyRuleSucceed(data));
  }, 500);
  
  //  }
};

export interface CalculationInfoPayloadProps {
  loanAmount: number;
  loanType: string;
  calculateType: string;
}

export const applyCalculation = (payload: CalculationInfoPayloadProps) => ({
  type: CommonActionTypes.APPLY_CALCULATION,
  payload,
});
export const removeCalculation = () => ({
  type: CommonActionTypes.REMOVE_APPLIED_CALCULATION,
});

