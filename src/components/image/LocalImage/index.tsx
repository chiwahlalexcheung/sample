import React, { FC } from 'react';
import css from 'clsx';
import {
  useStyles,
} from './styles';
import PreLoadImage from 'react-preload-image'
import StaticSources from '@config/sources';
import {
  Box,
} from '@material-ui/core'

interface LocalImageProps {
  src: string;
  alt?: string;
  className?: string;
}

const LocalImage: FC<LocalImageProps> = ({
  src=StaticSources.mainLogo,
  alt='',
  className='',
}) => {
  const classes = useStyles();
  return (
    <Box display={'flex'} alignItems={'center'} className={css(classes.image, className)}>
      <img src={src} className={classes.source} alt={alt} />
    </Box>
  );
};

export default LocalImage;
