import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from '@material-ui/core';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';
import { useContact } from '@store/service/context'

export const Map: FC = () => {
    // TODO
    const center = {
        lat: 22.317803122111826, 
        lng: 114.16940233299819
    };
    return(
        <LoadScript
          googleMapsApiKey="AIzaSyCPsseBX9Jc8LFk0rCt_OoUEHfW9pfal7A"
        >
          <GoogleMap
            mapContainerStyle={{
                display: 'flex',
                flex: 1
            }}
            center={center}
            zoom={17}
          >
            <>
                {   // TODO
                }
                <Marker
                    position={{
                        lat: 22.317803122111826, 
                        lng: 114.16940233299819
                    }}
                />
            </>
          </GoogleMap>
        </LoadScript>
    )
}