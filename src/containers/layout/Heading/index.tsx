import React, { FC, useContext, useEffect, useMemo, createContext } from 'react'
import { map } from 'lodash'
// import ScrollAnimation from 'react-animate-on-scroll';
import Sticky from 'react-stickynode';
import dynamic from 'next/dynamic'
import { useRouter } from 'next/router'
import { useTranslation } from 'next-i18next'
import MainLayout from '@containers/layout/Layout'
import Link from '@components/common/Link'
import css from 'clsx';
import { useForm } from 'react-hook-form';
import MainMenu, {
  // MainBannerProps,
  MenuList,
} from '@components/common/MainMenu'
import MainFooter from '@components/common/Footer'
import RouteSources from '@config/routes';
// import MenuListItemProps from '@interfaces/menu'
import {
  Grid,
  Container,
  AppBar,
  Box,
  ListItem,
  // ListItemText,
  ListItemIcon,
  Typography,
  Slide,
  // Fade,
  // ButtonBase,
} from '@material-ui/core'
import {useGetUser} from '@hooks/user'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'
import {
  useStyles,
  useRootStyles,
  useFormStyles,
  useLanguageStyles,
  useInnerLayoutStyles,
} from './styles'

const BackToTop = dynamic(() => import('@components/common/ScrollTop'), { ssr: false })

export type ContextProps = { 
  // TODO
};

const LanguageSelector: FC<{}> = () => {
  const { t, i18n } = useTranslation();
  const classes = useLanguageStyles();
  return useMemo(() => {
    const list = [
      {
        key: 'en',
        label: t('language.short.en'),
      },
      {
        key: 'hk',
        label: t('language.short.hk'),
      },
      {
        key: 'cn',
        label: t('language.short.cn'),
      },
    ];

    return (
      <Box>
        {
          map(list, ({ key, label }, index) => {
            let styles = classes.link;
            let addOn = {};
            if (key !== i18n.language) {
              styles = css(styles, classes.clickable);
            }
            return (
              <span className={styles} {...addOn} key={key}>
                {label}
                {
                  index < list.length - 1 && <span className={classes.delimiter}>
                    |
                  </span>
                }
              </span>
            );
          })
        }
      </Box>
    );
  }, [i18n.language, t]);
};

const SearchBar: FC<{}> = () => {
  const { register, handleSubmit, errors } = useForm();
  const classes = useFormStyles();
  const { t } = useTranslation('form');
  const onSubmit = (data) => {
    console.log(data);
  };
  return (
    <form className={classes.form} onSubmit={handleSubmit(onSubmit)}>
      <ListItem className={classes.styledInput}>
        <ListItemIcon className={classes.icon}>
          <LocalImage
            alt={t('placeholder.searchFor')}
            src={StaticSources.icon.search}
          />
        </ListItemIcon>
        <input name="k" placeholder={t('placeholder.searchFor')} ref={register} className={classes.input} />
      </ListItem>
      <input type="submit" className={classes.hideSubmit} />
    </form>
  );
};

const Navigator: FC<{}> = () => {
  const classes = useStyles();
  const { t } = useTranslation();
  const { user } = useGetUser();
  const labelProps = {
    component: 'h2',
    className: classes.barItem,
  };
  const navItems = useMemo(() => ([
    {
      label: t('menu.aboutUs'),
      key: 'about',
      link: RouteSources.aboutUs,
      labelProps,
    },
    {
      label: t('menu.cs'),
      key: 'cs',
      link: RouteSources.customerCentre,
      labelProps,
    },
    {
      label: t('menu.contactUs'),
      key: 'cu',
      link: RouteSources.contactUs,
      labelProps,
    },
  ]), [t]);
  return (
    <div className={classes.bar}>
      <Container maxWidth={'lg'}>
        <Box display={'flex'} alignItems={'center'} justifyContent={'flex-end'}>
          <Box className={classes.barMenu}>
            <MenuList items={navItems} />
          </Box>
          <Box display={'flex'}>
            <ListItem className={classes.listLabel}>
              <ListItemIcon className={classes.csIcon}>
                <LocalImage
                  alt={'2996 2688'}
                  src={StaticSources.icon.cs}
                />
              </ListItemIcon>
              <Typography className={classes.cs}>{'2996 2688'}</Typography>
            </ListItem>
            {user ? (
            <ListItem button className={classes.listButton}>
                <ListItemIcon className={classes.csIcon}>
                  <LocalImage
                    alt={t('menu.customer-profile')}
                    src={StaticSources.icon.logon}
                  />
                </ListItemIcon>
                <Link href={RouteSources.accountProfile} rel={'start'}>
                  <Typography className={css(classes.cs, classes.upper)}>
                    {t('menu.customer-profile')}
                  </Typography>  
                </Link>
            </ListItem>
            ) : (
            <ListItem button className={classes.listButton}>
                <ListItemIcon className={classes.csIcon}>
                  <LocalImage
                    alt={t('menu.customer-login')}
                    src={StaticSources.icon.logon}
                  />
                </ListItemIcon>
                <Link href={RouteSources.accountLogon} rel={'start'}>
                  <Typography className={css(classes.cs, classes.upper)}>
                    {t('menu.customer-login')}
                  </Typography>  
                </Link>
            </ListItem>
            )}
          </Box>
        </Box>
      </Container>
    </div>
  );
};

export const InnerPage: FC = ({ children }) => {
  const classes = useInnerLayoutStyles();
  return (
    <Container>
      <div className={classes.root}>
        {children}
      </div>
    </Container>
  );
};

const Header: FC<{}> = () => {
  const { t } = useTranslation('common');
  const classes = useStyles();
  return (
      <Slide direction={"down"} in timeout={300}>
        <AppBar color={'transparent'} position="static" className={classes.root}>
          <Navigator />
          <Container maxWidth={'lg'}>
            <Grid container>
              <Grid item>
                <Link className={classes.logo} href={RouteSources.landing} rel={'start'}>
                  <LocalImage alt={t('info.appName')} src={StaticSources.mainLogo} />
                </Link>
              </Grid>
              <Grid item xs={12} sm container>
                <Box
                  display={'flex'}
                  flexGrow={1}
                  alignItems={'center'}
                  justifyContent={'space-between'}
                >
                  <MainMenu />
                  <div className={classes.search}>
                    <SearchBar />
                  </div>
                  <LanguageSelector />
                </Box>
              </Grid>
            </Grid>
          </Container>
        </AppBar>
      </Slide>
  );
};

export const HeadingContext = createContext<Partial<ContextProps>>({});
export const useHeading = () => useContext(HeadingContext) as ContextProps;
const HeadingLayout: FC<{}> = ({ children }) => {
  const classes = useRootStyles();
  const { query } = useRouter();
  useEffect(() => {
    // always scroll to top when page changed
    if (!query.r) {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
  }, []);
  return (
    <MainLayout>
      <HeadingContext.Provider value={{}}>
        <Sticky enabled innerZ={999} enableTransforms activeClass={classes.active}>
          <Header />
        </Sticky>
        <BackToTop />
        {children}
        <MainFooter />
      </HeadingContext.Provider>
    </MainLayout>
  );
}

export default HeadingLayout;
