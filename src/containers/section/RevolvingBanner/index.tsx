import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import { useRouter } from 'next/router'
import StaticSources from '@config/sources'
import Fade from 'react-reveal/Fade'
// import Routes from '@config/routes'
import LocalImage from '@components/image/LocalImage'
import LazyImage from '@components/image/LazyImage';
import CommonButton from '@components/common/Button'
import Link from '@components/common/Link'
import ServiceConfig from '@config/service'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
} from '@material-ui/core';
// import {
//   RefControlProps,
// } from '@interfaces/common'
import {
  useStyles,
  useBoxStyles,
  useRootStyles
} from './styles';
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'

export interface ButtonProps {
    label: string;
    link: string;
}

export interface BannerBoxInfoProps {
  title: string;
  subTitle: string;
  description?: string;
  tips?: string;
  banner: string;
  buttons?: ButtonProps[];
}
export interface BannerBoxProps {
  info: BannerBoxInfoProps;
}

export const BannerBox: FC<BannerBoxProps> = ({ info }) => {
  const boxClasses = useBoxStyles({width:'50%'});
  const classes = useStyles();
  // const { t } = useTranslation('button');
  const router = useRouter();
  const {
    title,
    subTitle,
    description,
    tips,
    banner,
    buttons
  }: BannerBoxInfoProps = info;

  return (
    <Box display={'flex'}>
      <Box className={boxClasses.left} display={'flex'} flexDirection={'column'}>
        <div>
            <HeadingWithSubTitle
                title={title}
                subTitle={subTitle}
            />
        </div>
        {
          !!description && <div dangerouslySetInnerHTML={{
            __html: description
          }} className={boxClasses.description} />
        }
        {
            !!tips && <Typography component={'h1'} className={boxClasses.tips}>
                {tips}
            </Typography>
        }
        {
          (buttons.length > 0) && <Box display={'flex'} flexDirection={'column'} justifyContent={'space-between'}>
              {
                map(buttons, (item)=>(
                    <CommonButton
                      className={boxClasses.button}
                      label={item.label} onClick={
                        (e: any)=>{ router.push(item.link); }
                      }/>
                ))
              }
          </Box>
        }
      </Box>
    </Box>
  );
}

export const RevolvingBanner: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const classes = useRootStyles();
  
  const {
    bannerInfo
  } = props;
  
  const bgStyle = {
    backgroundImage: `url(${bannerInfo.banner})`,
  };

  return (
    <div className={classes.root} style={bgStyle}>
        <InnerPage>
            <Fade left>
                <BannerBox
                    info={bannerInfo}
                />
            </Fade>
        </InnerPage>
    </div>
  );
});

export default RevolvingBanner;
