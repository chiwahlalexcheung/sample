import React, { FC, useMemo, useCallback } from 'react'
import { ButtonProps as MuiButtonProps } from "@material-ui/core/Button";
import css from 'clsx'
import {
  Button,
} from '@material-ui/core'
import {
  useStyles,
} from './styles'
import LocalImage from '@components/image/LocalImage'

interface ButtonProps {
  onClick?: Function;
  label: string;
  className?: string;
  secondary?: boolean;
  buttonProps?: MuiButtonProps;
  icon?: string;
}

const CommonButton: FC<ButtonProps> = ({
  onClick,
  label,
  icon,
  buttonProps={},
  className='',
  secondary,
}) => { 
  const classes = useStyles();
  let styles = classes.root;
  const onPress = useCallback(() => {
    if (onClick) {
      onClick();
    }
  }, [onClick]);
  if (secondary) {
    styles = css(styles, classes.secondary)
  }
  return useMemo(() => (
    <Button
      onClick={(e: any) => onPress()}
      variant="contained"
      className={css(styles, className)}
      {...buttonProps}
    >
        {!!icon && <LocalImage
            className={classes.icon}
            src={icon}
        />}
        {!!label && label}
    </Button>
  ), [
    onPress,
    label,
    className,
    buttonProps,
  ]);
}

export default CommonButton;
