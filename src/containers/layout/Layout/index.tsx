import React, { FC, useContext, createContext } from 'react'
import Link from 'next/link'
import {
  Grid,
} from '@material-ui/core'
import {
  useStyles,
} from './styles'

export type ContextProps = { 
  authenticated: boolean,
};

export const LayoutContext = createContext<Partial<ContextProps>>({});
export const useLayout = () => useContext(LayoutContext) as ContextProps;
const MainLayout: FC<{}> = ({ children }) => {
  return (
    <LayoutContext.Provider value={{}}>
      {children}
    </LayoutContext.Provider>
  );
}
export default MainLayout;
