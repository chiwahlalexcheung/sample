import React, { FC, useCallback, useEffect } from "react";
import css from "clsx";
import { map } from "lodash";
import { useRouter } from "next/router";
import Routes from "@config/routes";
import StaticSource from "@config/sources";

import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import StaticSources from "@config/sources";
import Link from "@components/common/Link";
import Fade from "react-reveal/Fade";
import { InnerPage } from "@containers/layout/Heading";
import { useTranslation } from "next-i18next";
import { Typography, Box } from "@material-ui/core";
// import {
//   RefControlProps,
// } from '@types/common/common.d'
import { useStyles, useLoginStyles } from "./styles";

import { HeadingWithSubTitle } from "@containers/section/StepSolution";
import { Agreements } from "@containers/section/FullStepSolution/form";
import { LoginForm } from "./form";
import { apiGet } from "@containers/section/AccountCommon/context";
import RouteSources from "@config/routes";

export const AccountLogon: FC = () => {
  const classes = useStyles();
  const { t } = useTranslation("section");
  /* check user here */
  const router = useRouter();
  // logonOn page, checkUserInfo is different
  const checkUserInfo = async (router) => {
    const userInfo = await apiGet("userInfo");
    if (userInfo !== undefined) {
      if ("user" in userInfo["data"]) {
        router.push(RouteSources.accountProfile, RouteSources.accountProfile);
      }
    }
  };
  useEffect(() => {
    checkUserInfo(router);
  }, []);
  /* check user here */

  return (
    <div className={classes.root}>
      <InnerPage>
        <div className={classes.heading}>
          <HeadingWithSubTitle
            title={t("title.customerService")}
            subTitle={t("title.accountLogon")}
          />
        </div>
        <LoginForm />
      </InnerPage>
    </div>
  );
};

export default AccountLogon;
