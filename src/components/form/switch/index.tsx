import React, { FC } from 'react'
import { Controller } from "react-hook-form";
// import { map } from 'lodash'
// import css from 'clsx'
// import LocalImage from '@components/image/LocalImage'
// import StaticSources from '@config/sources'
import {
  Switch,
  FormControlLabel,
} from '@material-ui/core'
import {
  FormRenderElementProps,
  FormInputProps,
} from '@interfaces/form'
// import {
//   LabelValueProps,
// } from '@interfaces/list'
// import {
//   useStyles,
// } from './styles'
// import {
//   useStyles as useInputStyles,
// } from './../text/styles'
export type MaterialColor = 'primary' | 'secondary'; 
export interface NativeSwitcherProps extends FormRenderElementProps{
  // backgroundColor?: string;
  onLabel?: string;
  offLabel?: string;
  color?: MaterialColor;
  className?: string;
}

export interface NativeSwitchProps {
  onChange: Function;
  value: any;
  onLabel?: string;
  offLabel?: string;
  inputProps?: FormInputProps;
  className?: string;
  color?: MaterialColor;
};

const NativeSwitch: FC<NativeSwitchProps> = ({ className='', color='secondary', inputProps={}, onChange, onLabel, offLabel, value }) => (
  <FormControlLabel
    control={<Switch
      color={color}
      className={className}
      onChange={(r: any, checked: boolean) => onChange(checked)}
      inputProps={inputProps}
    />}
    label={!!value ? onLabel : offLabel}
  />
);

const NativeSwitcher: FC<NativeSwitcherProps> = ({
  control,
  name,
  render,
  onLabel,
  offLabel,
  className='',
  color,
  inputProps={defaultValue: false},
}) => {
  let addOn = {
    render: ({ onChange, value }: any) => (
      <NativeSwitch 
        onChange={onChange}
        value={value}
        onLabel={onLabel}
        className={className}
        offLabel={offLabel}
        color={color}
        inputProps={{
          ...inputProps,
          name,
        }}
      />
    ),
  };
  if (!!render) {
    addOn = { render: (payload: any) => render(payload) };
  }
  return (
    <Controller
      name={name}
      control={control}
      {...addOn}
    />
  );
}

export default NativeSwitcher;