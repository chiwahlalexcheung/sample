import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';
import {
  Colors,
} from '@theme/global';
const{
  lightGrey
} = Colors;

const useStyles = makeStyles(theme => ({
  root: {
    background: 'white',
  },
}));

const useBoxStyles = makeStyles(theme => ({
    root: {
        padding: '30px 0 30px 0'
    },
    left: {
        width: '45%',
        paddingLeft: '0',
        paddingRight: '30px',
    },
    right: {

    },
    heading: {
        paddingBottom: '70px',
    },
    description: {
        paddingLeft: '50px',
        // backgroundColor: '#0f0'
    },
    banner: {
        width: '587px',
        height: '395px',
        backgroundImage: `url(${StaticSources.banner.whyChooseUsBanner})`,
        // ...FullCoverBgStyles,
    },
    bannerBottomCover: {
        width: '100%',
        height: '171px',
        alignSelf: 'flex-end',
        padding: '30px 50px 30px 50px',
        backgroundColor: '#ffffffe6',
        borderWidth: '1px'
    },
    buttons: {
		gap: '5px',
        width: '100%',
        // backgroundColor:''
    },
    button: {
        borderRadius: '5px',
        flex: 1,
        fontWeight: 700,
    },
    // buttonDark: {
    //     borderRadius: '5px',
    //     flex: 1,
    //     fontWeight: 700,
    //     borderRadius: '5px',
    //     color
    // },
    title: {
      color: theme.palette.primary.light,
      fontSize: '26px',
      textTransform: 'uppercase',
      fontWeight: 'bolder',
    },
    affordHeader: {
    //   margin: '38px 0 32px 0',
      display: 'flex',
      flex: 1,
      alignItems: 'center',
    },
    icon: {
      width: '47px',
      height: '33px',
      marginRight: '18px',
    },
}));

const useHighLightStyles = makeStyles(theme => ({
    root: {
        padding: '70px 0 70px 0',
    },
    title: {
      color: theme.palette.primary.light,
      fontSize: '26px',
      textTransform: 'uppercase',
      fontWeight: 'bolder',
    },
    phoneIcon: {
      width: '240px',
      height: '347px',
    },
    highLights: {
        width: '850px',
        justifyContent: 'flex-start', 
        flexWrap: 'wrap',
    },
    highLight: {
        width: '50%',
        height: '50%',
    },
    highLightIcon: {
        width: '150px',
        height: '150px',
        borderRadius: '75px',
        backgroundColor: '#fff'
    },
    highLightLabel: {
        padding: '18px',
        display: 'flex',
        flex: 1,
        fontSize: '18px',
        fontWeight: 'bolder'

    }
    
}));

const useFootnotesStyles = makeStyles(theme => ({
    root: {
        padding: '50px 0 50px 0',
        '&::before': {
            content: "' '",
            display: 'block',
            // position: 'absolute',
            width: '136px',
            height: '2px',
            background: theme.palette.primary.light,
            top: '-15px',
            left: 0,
          },
    },
    footnote: {
        paddingTop: '10px',
        width: '850px',
        color: theme.palette.text.secondary,
        // backgroundColor: 'red',
        fontSize: '16px',
        wordSpacing: '0.4px',
        lineHeight: '22px'
    },
    title: {
      color: theme.palette.primary.light,
      fontSize: '26px',
      textTransform: 'uppercase',
      fontWeight: 'bolder',
    },
    phoneIcon: {
      width: '240px',
      height: '347px',
    },
    highLights: {
        flex: 1, 
        justifyContent: 'flex-start', 
        flexWrap: 'wrap',
    },
    highLight: {
        width: '50%',
        height: '50%',
    },
    highLightIcon: {
        width: '150px',
        height: '150px',
        borderRadius: '75px',
        backgroundColor: '#fff'
    },
    highLightLabel: {
        padding: '18px',
        display: 'flex',
        flex: 1,
        fontSize: '18px',
        fontWeight: 'bolder'
    }
}));


export {
  useStyles,
  useBoxStyles,
  useFootnotesStyles,
  useHighLightStyles
};
