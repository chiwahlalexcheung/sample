import React, { FC, forwardRef, useCallback, useState, useEffect, useContext ,createContext } from 'react'

import {
    OnlineValuationForm
} from './form';

export const OnlineValuation: FC<any> = forwardRef((props, r) => {
  // const { t: Bt } = useTranslation('button');
    return (
        <div ref={r as React.RefObject<HTMLDivElement>}>
            <OnlineValuationForm />
        </div>
    );
});

export default OnlineValuation;
