import React, {
  FC,
  forwardRef,
  ReactNode,
  useCallback,
  useState,
  useEffect,
  useContext,
  createContext,
} from "react";
import { map, isUndefined } from "lodash";
import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import CommonPopup from "@components/common/Popup";
import StaticSources from "@config/sources";
import FormElement from "@components/form";
import { useTranslation } from "next-i18next";
import css from "clsx";
import Link from "@components/common/Link";
import RouteSources from "@config/routes";
import { useOTP } from "@store/application/context";
import APIManager from "@lib/apiManager";
import { useRouter } from "next/router";
import {
  Typography,
  Grid,
  //   Container,
  Box,
  //   List,
  //   ListItem,
  //   ListItemText,
  //   ListItemIcon,
  //   Card,
  //   CardHeader,
  //   CardContent,
  Radio,
  RadioGroup,
  FormControlLabel,
} from "@material-ui/core";
import {
  OTPAuthentication,
  SMSBlock,
} from "@containers/section/FullStepSolution/sms";
import { ReCaptchaBlock } from "@containers/section/ContactUs";
import { useStyles, useProfileStyles } from "./styles";
import { ContactlessOutlined, LabelImportantTwoTone } from "@material-ui/icons";
import {
  StepSolution,
  useStepSolution,
  StepSolutionContext,
  HeadingWithSubTitle,
} from "@containers/section/StepSolution";
import {
  // KeyPairHandleProps,
  // useKeyPairValue,
  KeyPairProps,
  useKeyPairFormValue,
  KeyPairFormHandleProps,
  useToggle,
  ToggleProps,
} from "@hooks/common";
import {
  Agreements,
  StatementBlock,
  StatementBlockProps,
} from "@containers/section/FullStepSolution/form";
import ServiceConfig from "@config/service";
const {
  googleReCaptcha: { siteKey: reCaptchaKey },
} = ServiceConfig;
import {
  GoogleReCaptchaProvider,
  useGoogleReCaptcha,
} from "react-google-recaptcha-v3";
import apiManager from "@lib/apiManager";
import RadioTable from "@components/form/radioTable";
import {
  formatDate,
  formatMoney,
  apiGet,
  apiPost,
} from "@containers/section/AccountCommon/context";

export interface ContextProps extends KeyPairFormHandleProps {
  // TODO
  info: KeyPairProps;
  onChange: Function;
  control: any;
}
export interface ProfileProps {
  children: JSX.Element;
  values: KeyPairProps;
}

export const ProfileContext = createContext<Partial<ContextProps>>({});
export const useProfile = () => useContext(ProfileContext) as ContextProps;
export const ProfileProvider: FC</*LogonProps*/ any> = ({
  values = {},
  children,
}) => {
  const {
    control,
    register,
    changeValue,
    setValue,
    watch,
    getValue,
    getValues,
    value,
    reset,
  }: KeyPairFormHandleProps = useKeyPairFormValue(values);
  return (
    <ProfileContext.Provider
      value={{
        info: value,
        changeValue,
        getValue,
        setValue,
        onChange: changeValue,
        register,
        control,
        getValues,
        watch,
        reset,
      }}
    >
      <form>{children}</form>
    </ProfileContext.Provider>
  );
};

export interface ProfileItemProps {
  key: string;
  component: JSX.Element;
}
export interface ProfileBarProps {
  list: ProfileItemProps[];
}

export const ApplicationList = ({
  setLoan,
  loanList,
  loanFilteredList,
  lastLogon,
  detailVisited,
}) => {
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
    getValues,
  }: ContextProps = useProfile();
  const { goNextStep }: StepSolutionContext = useStepSolution();
  const classes = useProfileStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const router = useRouter();

  /* if the number exists, and not from the detail page
     go to next step.
  */
  if ("number" in router.query && !detailVisited) {
    goNextStep();
  }

  /* P146 after login in, show the application list  */

  const [selected, setSelected] = useState("");
  const onClickContinue = () => {
    if (!(selected === "")) {
      const obj = JSON.parse(selected);
      if (
        "acct_no" in obj &&
        "loan_typ" in obj &&
        "status" in obj &&
        "acct_date" in obj
      ) {
        const found = loanList.find(
          (e) =>
            e["acct_no"] === obj["acct_no"] &&
            e["loan_typ"] === obj["loan_typ"] &&
            e["status"] === obj["status"] &&
            e["acct_date"] === obj["acct_date"]
        );
        setLoan(found);
        goNextStep();
      }
    }
  };

  const onClickLogout = async () => {
    await apiGet("signOut", {});
    router.push(RouteSources.accountLogon, RouteSources.accountLogon);
  };

  /* write the application list ui */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={onClickLogout}
        />
      </Box>

      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Typography className={classes.title}>
          {ft("label.selectLoanAccount")}
        </Typography>

        <Box
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          <RadioTable
            data={loanFilteredList}
            selected={selected}
            setSelected={setSelected}
          />

          <Grid
            container
            spacing={3}
            className={css(classes.row, classes.bottomLine)}
          ></Grid>

          <CommonButton
            className={classes.button}
            label={bt("continue")}
            onClick={onClickContinue}
          />
        </Box>

        <Box width="100%" display={"flex"}>
          <Box flexGrow="1">
            <Typography className={classes.remarkRequired}>
              {ft("note.required")}
            </Typography>
          </Box>
          <Box flexGrow="1" display="flex" justifyContent="center">
            <Typography className={classes.remarkDate}>
              {ft("note.lastLoginDate") + ": " + lastLogon}
            </Typography>
          </Box>
          <Box flexGrow="1">
            <Typography className={classes.remarkTransparent}>
              {ft("note.required")}
            </Typography>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

const applicationInfo = ({ loan }) => {
  const classes = useProfileStyles();
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const isInstalment = loan["revv_loan"] === "N";

  let tenor = loan["org_tenor"];
  if (tenor === 1) {
    tenor += " Month";
  } else {
    tenor += " Months";
  }

  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        width="100%"
        className={classes.otpSMSBlock}
      >
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-start"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.loanAccountApplicationNo")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.typeOfAccount")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.appliedLoanAmount")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.appliedLoanTenor")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.loanApplicationStatus")}
          </Typography>
        </Box>
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-end"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["acct_no"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["loan_typ"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatMoney(loan["loan_ledg_bal"])}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {tenor}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["status"]}
          </Typography>
        </Box>
      </Box>
      <Box
        display={"flex"}
        justifyContent={"center"}
        className={classes.operationButtons}
        width="100%"
      >
        {isInstalment ? (
          <Link
            href={RouteSources.account.documentsUpload + "/" + loan["acct_no"]}
            rel={"start"}
          >
            <CommonButton
              className={classes.operationButton}
              label={bt("uploadDocuments")}
            />
          </Link>
        ) : (
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {ft("label.uploadDocumentInInstalment")}
          </Typography>
        )}
      </Box>
    </>
  );
};

const instalmentInfo = ({ loan }) => {
  const classes = useProfileStyles();
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");

  const [holidayDisplay, setHolidayDisplay] = useState("none");

  const verifyPaymentHoliday = useCallback(async () => {
    const data = await apiPost(
      "paymentHoliday",
      { acct_no: loan["acct_no"], action: "V" },
      {}
    );
    if (data) {
      const code = data["data"]["LOS"]["BODY"]["rtn_cde"];
      if (code === "000") {
        setHolidayDisplay("inline");
      }
    }
  }, [loan]);
  verifyPaymentHoliday();

  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        width="100%"
        className={classes.otpSMSBlock}
      >
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-start"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.loanAccountApplicationNo")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.typeOfAccount")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.accountOutstandingBalance")}
          </Typography>
        </Box>
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-end"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["acct_no"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["loan_typ"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["loan_ledg_bal"]}
          </Typography>
        </Box>
      </Box>
      <Box
        display={"flex"}
        justifyContent={"center"}
        className={classes.operationButtons}
        width="100%"
      >
        <Link
          href={RouteSources.account.documentsUpload + "/" + loan["acct_no"]}
          rel={"start"}
        >
          <CommonButton
            className={classes.operationButton}
            label={bt("uploadDocuments")}
          />
        </Link>

        <Link
          href={
            RouteSources.account.paymentHolidayRequest + "/" + loan["acct_no"]
          }
          rel={"start"}
        >
          <Box display={holidayDisplay}>
            <CommonButton
              className={classes.operationButton}
              label={bt("paymentHolidayRequest")}
            />
          </Box>
        </Link>

        <Link
          href={RouteSources.account.changeEmail + "/" + loan["acct_no"]}
          rel={"start"}
        >
          <CommonButton
            secondary={true}
            className={classes.operationButton}
            label={bt("changeOrAddEmail")}
          />
        </Link>
      </Box>
    </>
  );
};

const revolvingInfo = ({ loan }) => {
  const classes = useProfileStyles();
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const [statementDisplay, setStatementDisplay] = useState("none");

  const verifyStatement = useCallback(async () => {
    const data = await apiGet(
      "statementDate",
      { acct_no: loan["acct_no"] },
      {}
    );
    if (data) {
      const dates = data["data"]["LOS"]["BODY"]["dates"];
      if (dates && dates.length > 0) {
        setStatementDisplay("inline");
      }
    }
  }, [loan]);
  verifyStatement();

  return (
    <>
      <Box
        display="flex"
        justifyContent="space-between"
        width="100%"
        className={classes.otpSMSBlock}
      >
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-start"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.loanAccountApplicationNo")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.typeOfAccount")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.accountOutstandingBalance")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.availableCreditLimit")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.minimumPaymentAmount")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.paymentDueDate")}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoKey}>
            {ft("label.lastPaymentDate")}
          </Typography>
        </Box>
        <Box
          flexGrow="1"
          display="flex"
          flexDirection="column"
          alignItems="flex-end"
          className={classes.accountInfoColumn}
        >
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["acct_no"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {loan["loan_typ"]}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatMoney(loan["loan_ledg_bal"])}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatMoney(loan["avl_cr_limit"])}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatMoney(loan["min_pay"])}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatDate(loan["due_date"])}
          </Typography>
          <Typography component={"h4"} className={classes.accountInfoValue}>
            {formatDate(loan["last_rpy_date"])}
          </Typography>
        </Box>
      </Box>
      <Box
        display={"flex"}
        justifyContent={"center"}
        className={classes.operationButtons}
        width="100%"
      >
        <Box display="flex">
          <Link
            href={RouteSources.account.documentsUpload + "/" + loan["acct_no"]}
            rel={"start"}
          >
            <CommonButton
              className={classes.operationButtonBig}
              label={bt("uploadDocuments")}
            />
          </Link>
          <Box display="flex" flexDirection="column">
            <Box display="flex">
              <Link
                href={RouteSources.account.fundTransfer + "/" + loan["acct_no"]}
                rel={"start"}
              >
                <CommonButton
                  className={classes.operationButton}
                  label={bt("fundTransfer")}
                />
              </Link>
              <Link
                href={
                  RouteSources.account.increaseCreditLimit +
                  "/" +
                  loan["acct_no"]
                }
                rel={"start"}
              >
                <CommonButton
                  className={classes.operationButton}
                  label={bt("increaseCreditLimit")}
                />
              </Link>
            </Box>
            <Box display="flex">
              <Link
                href={RouteSources.account.changeEmail + "/" + loan["acct_no"]}
                rel={"start"}
              >
                <CommonButton
                  secondary={true}
                  className={classes.operationButton}
                  label={bt("changeOrAddEmail")}
                />
              </Link>
              <Link
                href={
                  RouteSources.account.onlineEStatement + "/" + loan["acct_no"]
                }
                rel="start"
              >
                <Box display={statementDisplay}>
                  <CommonButton
                    secondary={true}
                    className={classes.operationButton}
                    label={bt("onlineEStatement")}
                  />
                </Box>
              </Link>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export const ApplicationDetail = ({ loan, lastLogon, setDetailVisited }) => {
  const classes = useProfileStyles();
  const { t } = useTranslation("section");
  const { t: ft } = useTranslation("form");
  const { t: bt } = useTranslation("button");
  const [mountCount, setMountCount] = useState<number>(0);
  const {
    control,
    watch,
    setValue,
    register,
    reset,
    info,
  }: ContextProps = useProfile();
  const {
    // register,
    // control,
    // watch,
    step,
    goPrevStep,
  }: StepSolutionContext = useStepSolution();
  /* 
    my code start here 
  */
  setDetailVisited(true);
  const router = useRouter();
  const onClickLogout = async () => {
    await apiGet("signOut", {});
    router.push(RouteSources.accountLogon, RouteSources.accountLogon);
  };
  let TypeInfo = null; // it is application of loan
  if (loan) {
    TypeInfo = applicationInfo;
    if (loan["cat"] === "L") {
      TypeInfo = instalmentInfo; // it is instalment loan
      if (loan["revv_loan"] === "Y") {
        TypeInfo = revolvingInfo; // it is revolving loan
      }
    }
  }

  /* the jsx */
  return (
    <Box display={"flex"} flexDirection={"column"} className={classes.root}>
      <Box display={"flex"} alignItems={"flex-end"} justifyContent={"flex-end"}>
        <Link href={RouteSources.landing} rel={"start"}>
          <CommonButton
            className={classes.TopButtonHome}
            label={t("label.home")}
          />
        </Link>
        <CommonButton
          className={classes.TopButtonLogout}
          label={t("label.logout")}
          onClick={onClickLogout}
        />
      </Box>
      <Box
        display={"flex"}
        alignItems={"flex-start"}
        justifyContent={"center"}
        flexDirection={"column"}
        className={classes.form}
      >
        <Box
          display={"flex"}
          alignItems={"flex-start"}
          justifyContent={"flex-start"}
          flexDirection={"column"}
          className={classes.formBlock}
        >
          {TypeInfo && <TypeInfo loan={loan} />}
          <Box
            display={"flex"}
            justifyContent={"center"}
            className={classes.optButtons}
            width="100%"
          >
            <CommonButton
              className={classes.otpButton}
              label={bt("back")}
              onClick={goPrevStep}
            />
          </Box>
        </Box>

        <Box width="100%" display={"flex"} justifyContent="center">
          <Typography className={classes.remarkDate}>
            {ft("note.lastLoginDate") + ": " + lastLogon}
          </Typography>
        </Box>
      </Box>
    </Box>
  );
};

export const ProfileContent: FC<ProfileBarProps> = ({ list }) => {
  const { step }: StepSolutionContext = useStepSolution();
  return (
    <div>
      {map(list, ({ key, ...l }: ProfileItemProps, index: number) => {
        let styles = { display: "none" };
        if (index === step) {
          styles = { display: "block" };
        }
        return (
          <div key={key} style={styles}>
            {l.component}
          </div>
        );
      })}
    </div>
  );
};

export const ProfileForm: FC = () => {
  const { t } = useTranslation("section");
  const classes = useProfileStyles();
  const onSubmit = useCallback((v: any) => {
    console.log(v);
  }, []);

  /* for both loan-list and loan-details */
  const [loan, setLoan] = useState({});
  const [loanList, setLoanList] = useState([]);
  const [loanFilteredList, setLoanFilteredList] = useState({});
  const [lastLogon, setLastLogon] = useState("");
  // if we visit the detail, the value will become true
  // then the list page know that we come back via the "BACK" button
  const [detailVisited, setDetailVisited] = useState(false);
  const router = useRouter();

  /* fetch and pre-process the data */
  const fetchData = useCallback(async () => {
    const result = await apiGet("custAppLoan", {});
    if (!("error" in result)) {
      const loanListTemp = result["data"]["LOS"]["BODY"]["appl_loan_list"];
      setLoanList(loanListTemp);
      const loanFilteredListTemp = loanListTemp.map(
        ({ acct_no, loan_typ, status, acct_date }) => ({
          acct_no,
          loan_typ,
          status,
          acct_date,
        })
      );
      setLoanFilteredList(loanFilteredListTemp);
      const timestamp = result["data"]["LOS"]["BODY"]["last_logon_timestamp"];
      const lastLogonTemp =
        timestamp.slice(0, 4) +
        "-" +
        timestamp.slice(4, 6) +
        "-" +
        timestamp.slice(6, 8);
      setLastLogon(lastLogonTemp);
      /* go back from operation, with the number */
      // the code in the use effect, exec in the end.
      if ("number" in router.query) {
        const number = router.query["number"];
        const found = loanListTemp.find(
          (e) =>
            e["acct_no"] === number &&
            !(e["cat"] === "A" && e["revv_loan"] === "Y")
        );
        setLoan(found);
      }
      /* go back from operation, with the number */
    }
  }, []);
  useEffect(() => {
    fetchData();
  }, []);

  const steps: ProfileItemProps[] = [
    {
      key: "applicationList",
      component: (
        <ApplicationList
          setLoan={setLoan}
          loanList={loanList}
          loanFilteredList={loanFilteredList}
          lastLogon={lastLogon}
          detailVisited={detailVisited}
        />
      ),
    },
    {
      key: "applicationDetail",
      component: (
        <ApplicationDetail
          loan={loan}
          lastLogon={lastLogon}
          setDetailVisited={setDetailVisited}
        />
      ),
    },
  ];

  return (
    <GoogleReCaptchaProvider reCaptchaKey={reCaptchaKey}>
      <ProfileProvider values={{}}>
        <StepSolution maxStep={steps.length} onSubmit={onSubmit}>
          <ProfileContent list={steps} />
        </StepSolution>
      </ProfileProvider>
    </GoogleReCaptchaProvider>
  );
};
