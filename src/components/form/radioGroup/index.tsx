import React, { FC, useCallback } from 'react'
import { Controller } from "react-hook-form";
// import { map } from 'lodash'
import css from 'clsx'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'
import {
    Radio,
    Checkbox,
    RadioGroup,
    FormControlLabel
} from '@material-ui/core'
import {
  FormRenderElementProps,
  FormInputProps,
} from '@interfaces/form'
import {
  LabelValueProps,
} from '@interfaces/list'
import {
  useCheckboxStyles,
} from './styles'
import { map } from 'lodash'
// import {
//   useStyles as useInputStyles,
// } from './../text/styles'
export type MaterialColor = 'default' | 'primary' | 'secondary'; 
// export interface NativeCheckBoxerProps extends FormRenderElementProps{
//   // backgroundColor?: string;
//   label?: string | JSX.Element;
// }
export interface NativeRadioProps {

}

export interface NativeRadioGroupProps  extends FormRenderElementProps {
//   selectedValue?: string;
  setValue: Function;
  options: LabelValueProps[];
};

const NativeRadioGroup: FC<NativeRadioGroupProps> = ({
  control,
  name,
  render,
//   selectedValue,
  setValue,
  options,
  inputProps={defaultValue: false},
}) => {
    const classes = useCheckboxStyles();
    const handleChange = useCallback(({ target: e }: any) => {
        const { value: v } = e;
        setValue(name, v);
    }, [name, setValue]);

    let addOn = {
        render: ({ onChange, value }: any) => (
            <RadioGroup row name={name} value={value} onChange={(e: any) => handleChange(e)}>
                {map(options, (item: LabelValueProps)=>(
                    <FormControlLabel 
                        key={item.value}
                        value={item.value} 
                        label={item.label} 
                        control={<Radio 
                            checkedIcon={
                                <span className={classes.checkbox}>
                                    <LocalImage
                                        className={classes.icon}
                                        src={StaticSources.icon.calculator.checked}
                                        alt={'Check'}
                                    />
                                </span>
                            }
                            icon={
                                <span className={classes.checkbox} />
                            }
                        />} 
                    />
                ))}
            </RadioGroup>
        ),
    };
    if (!!render) {
        addOn = { render: (payload: any) => render(payload) };
    }
    return (
        <Controller
        name={name}
        control={control}
        {...addOn}
        />
    );
}

export default NativeRadioGroup;