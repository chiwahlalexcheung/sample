import { useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { get } from 'lodash'
import {
  fetchPersona,
  fetchCalculatorConfig,
  calculate,
  SectionType,
  CalculateParameterProps,
  CalculatePayloadProps,
  fetchFaq
} from './actions'
import { AppState } from './../store'
// import { MediaState } from './reducers'

export const usePersona = () => {
  const dispatch = useDispatch();
  const { list, loading, error } = useSelector(({ content }: AppState) => content.persona);
  
  const fetch = useCallback(() => {
    const isServer = false;
    dispatch(fetchPersona(isServer))
  }, [dispatch]);
  return {
    fetch,
    loading,
    list,
    error,
  };
}

export const useCalculatorConfig = () => {
  // paymentType: SectionType) => {
  const dispatch = useDispatch();
  const { info, loading } = useSelector(({ content }: AppState) => content.calculatorConfig);
  const fetch = useCallback(() => {
    const isServer = false;
    dispatch(fetchCalculatorConfig(isServer))
  }, [dispatch]);
  return {
    fetch,
    loading,
    config: info,
    // get(info, paymentType),
  };
}

export interface CalculateInfoProps {
  loanAmount: number;
  repaymentTerms: number;
  interestRate: number;
}
export type CustomizedSectionType = 'interest' | 'principal' | 'revolving';
export const CustomizedSectionLookup: any = {
  interest: 'interestOnly',
  principal: 'principalAndInterest',
  revolving: 'revolving'
}
export const useCalculate = (section: CustomizedSectionType) => {
  const dispatch = useDispatch();
  const { info, loading } = useSelector(({ content }: AppState) => content.calculation);
  const paymentType = CustomizedSectionLookup[section] || section;
  const normalization = useCallback((info: CalculateInfoProps) => ({
    principal: info.loanAmount,
    rate: info.interestRate,
    term: info.repaymentTerms,
  }), []);
  const execute = useCallback((
    info: CalculateInfoProps, 
    paymentType: CustomizedSectionType,
  ) => {
    dispatch(calculate(normalization(info), {
      paymentType: CustomizedSectionLookup[paymentType] || paymentType
    }));
  }, [dispatch]);
  // console.log(info, paymentType);
  return {
    execute,
    loading,
    currentAmount: info?.current || 0,
    amount: get(info, paymentType) || 0,
  };
}
export const useFaq = () => {
    const dispatch = useDispatch();
    
    const { list, loading, error } = useSelector(({ content }: AppState) => content.faq);
    
    const fetch = useCallback(() => {
      dispatch(fetchFaq(false));
    }, [dispatch]);

    return {
        fetch,
        list,
        loading,
        error
    };
}
