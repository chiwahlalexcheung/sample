import { makeStyles } from '@material-ui/core/styles';
// import StaticSources from '@config/sources';
// import {
//   FullCoverBgStyles,
//   // RelativeBefore,
// } from '@theme/global';

const useStyles = makeStyles(theme => ({
  root: {
    padding: '153px 0',
    background: 'transparent linear-gradient(181deg, #FFFFFF 0%, #E5E6EC 40%, #FFFFFF 100%) 0% 0% no-repeat padding-box',
  },
  left: {
    width: '50%',
  },
  right: {
    flex: 1,
  },
  slider: {
    width: '100%',
    position: 'relative',
  },
  prev: {
    zIndex: 99999,
    left: '-80px',
  },
  next: {
    zIndex: 99999,
    right: '-80px',
  },
}));

const useSlideStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    marginRight: '0',
    height: '296px',
    width: '190px',
    overflow: 'hidden',
    boxShadow: '0px 3px 6px #00000029',
    transition: "transform 0.15s ease-in-out",
  },
  block: {
    position: 'absolute',
    margin: '0 auto',
    left: 0,
    right: 0,
    bottom: '60px',
    zIndex: 2,
    textAlign: 'center',
  },
  title: {
    fontSize: '9px',
    textTransform: 'uppercase',
    color: theme.palette.text.primary,
  },
  activeTitle: {
    fontSize: '12px',
    fontWeight: 'bolder',
  },
  subTitle: {
    fontSize: '5px',
    color: theme.palette.text.secondary,
  },
  activeSubTitle: {
    fontSize: '8px',
    color: theme.palette.text.primary,
  },
  button: {
    background: 'transparent linear-gradient(180deg, #EF191B 0%, #780D0E 100%) 0% 0% no-repeat padding-box',
    marginTop: '30px',
    borderRadius: '5px',
    fontSize: '8px',
    padding: '2px 15px',
  },
  overlay: {
    background: 'transparent linear-gradient(180deg, #FFFFFF00 0%, #FAFAFA 36%, #FFFFFF 100%) 0% 0% no-repeat padding-box',
    opacity: 0.89,
    position: 'absolute',
    bottom: 0,
    height: '250px',
    width: '100%',
    zIndex: 1,
  },
  image: {
    width: 'auto !important',
    objectFit: 'cover',
  },
  thumbnail: {
    width: '130%',
    height: '130%',
    cursor: 'pointer',
    position: 'absolute',
    left: '-20px',
    top: '-10px',
    margin: 'auto',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
  },
  active: {
    transform: "scale3d(1.3, 1.3, 1)",
    zIndex: 9999,
  },
  inActive: {
    zIndex: (props: any) => props.zIndex,
  },
  skeleton: {
    boxShadow: '0px 5px 10px #00000033',
  },
}));

export {
  useStyles,
  useSlideStyles,
};
