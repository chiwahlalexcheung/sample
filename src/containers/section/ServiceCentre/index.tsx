import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from '@material-ui/core';
import {
    ExpandMore
} from '@material-ui/icons';
import {
  useStyles,
  useBoxStyles,
  useAccordionStyles,
  useInfoStyles
} from './styles';
import { useContact } from '@store/service/context'
import { useLocale, LocaleContextProps } from '@components/common/Locale'

import {
    HeadingWithSubTitle,
} from '@containers/section/StepSolution'

import {
    Map
} from './map'

export const BranchBox: FC<any> = ({item}) => {
    const classes = useInfoStyles();
    const { inspector }: LocaleContextProps = useLocale();
    return (
        <div className={classes.infoBlock}>
            <Typography className={classes.infoBody}>{inspector({ target: item, field: 'name', defaultValue: '',})}</Typography>
            <Typography className={classes.infoBody}>{inspector({ target: item, field: 'address', defaultValue: '',})}</Typography>
        </div>
    )
}
export const ServiceCentreBox: FC<any> = ({item}) => {
    const classes = useInfoStyles();
    const { t } = useTranslation('section');
    const { inspector }: LocaleContextProps = useLocale();
    const branchList = map(item.addresses, (address, index)=> (
        <BranchBox item={address} key={index} />
    ));

    const telList = map(item.numbers, (tel, index)=>(
        <Typography key={index} className={classes.infoBody}>{inspector({ target: tel, field: 'name', defaultValue: '',})}: {tel.number}</Typography>
    ));

    return(
       <Box display={'flex'} flexDirection={'row'} className={classes.root}>
            <LocalImage src={StaticSources.icon.addressInfo} className={classes.infoIcon} />
            <Box display={'flex'} flexDirection={'column'}>
                <Typography className={classes.infoTitle}>{inspector({ target: item, field: 'region', defaultValue: '',})}</Typography>
                
                <div className={classes.infoBlock}>{branchList}</div>

                <div className={classes.infoBlock}>
                    <Typography className={classes.infoBody}>{t('label.contact.businessHours')}</Typography>
                    <Typography className={classes.infoBody}>{inspector({ target: item, field: 'businessHour', defaultValue: '',})}</Typography>
                </div>

                <div className={classes.infoBlock}>{telList}</div>
            </Box>
        </Box>
    )
}

export const ServiceCentre: FC<any> = () => {
    const classes = useStyles();
    const { inspector }: LocaleContextProps = useLocale();
    const { t } = useTranslation('section');
    const { list } = useContact();
    const serviceCentreList = map(list , (centre: any)=> (
        <ServiceCentreBox item={centre} />
    ))
    return (
        <Box display={'flex'} flexDirection={'row'} className={classes.root} >
            <Box display={'flex'} flexDirection={'column'} className={classes.left}>
                <div className={classes.heading}>
                    <HeadingWithSubTitle
                        title={t('label.findUsEasily')}
                        subTitle={t('label.cs')}
                    />
                </div>
                { serviceCentreList }
            </Box>
            <Box display={'flex'} className={classes.right} >
                <Map />
            </Box>
        </Box>
    );

}
export default ServiceCentre;
