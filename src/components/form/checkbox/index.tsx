import React, { FC } from 'react'
import { Controller } from "react-hook-form";
// import { map } from 'lodash'
import css from 'clsx'
import LocalImage from '@components/image/LocalImage'
import StaticSources from '@config/sources'
import {
  Checkbox,
  FormControlLabel,
} from '@material-ui/core'
import {
  FormRenderElementProps,
  FormInputProps,
} from '@interfaces/form'
// import {
//   LabelValueProps,
// } from '@interfaces/list'
import {
  useCheckboxStyles,
} from './styles'
// import {
//   useStyles as useInputStyles,
// } from './../text/styles'
export type MaterialColor = 'default' | 'primary' | 'secondary'; 
export interface NativeCheckboxesProps extends FormRenderElementProps{
  // backgroundColor?: string;
  label?: string | JSX.Element;
  color?: MaterialColor;
  className?: string;
}

export interface NativeCheckboxProps {
  onChange: Function;
  value: any;
  label?: string | JSX.Element;
  inputProps?: FormInputProps;
  className?: string;
  color?: MaterialColor;
};

const NativeCheckbox: FC<NativeCheckboxProps> = ({ className='', color='default', inputProps={}, onChange, label, value }) => {
  const classes = useCheckboxStyles();
  return (
    <FormControlLabel
      control={
        <Checkbox
          color={color}
          className={css(classes.root, className)}
          checkedIcon={
            <LocalImage
              src={StaticSources.icon.calculator.checked}
              alt={'Check'}
            />
          }
          icon={<span className={classes.icon} />}
          onChange={(e: any, checked: boolean) => onChange(checked)}
          inputProps={inputProps}
        />
      }
      label={label}
    />
  );
}

const NativeCheckBoxer: FC<NativeCheckboxesProps> = ({
  control,
  name,
  render,
  label,
  className='',
  color,
  inputProps={defaultValue: false},
}) => {
  let addOn = {
    render: ({ onChange, value }: any) => (
      <NativeCheckbox 
        onChange={onChange}
        value={value}
        label={label}
        className={className}
        color={color}
        inputProps={{
          ...inputProps,
          name,
        }}
      />
    ),
  };
  if (!!render) {
    addOn = { render: (payload: any) => render(payload) };
  }
  return (
    <Controller
      name={name}
      control={control}
      {...addOn}
    />
  );
}

export default NativeCheckBoxer;