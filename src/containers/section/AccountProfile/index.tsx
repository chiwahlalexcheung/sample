import React, { FC, useCallback, useEffect } from "react";
import css from "clsx";
import { map } from "lodash";
import { useRouter } from "next/router";
import Routes from "@config/routes";
import StaticSource from "@config/sources";

import LocalImage from "@components/image/LocalImage";
import CommonButton from "@components/common/Button";
import StaticSources from "@config/sources";
import Link from "@components/common/Link";
import Fade from "react-reveal/Fade";
import { InnerPage } from "@containers/layout/Heading";
import { useTranslation } from "next-i18next";
import { Typography, Box } from "@material-ui/core";
// import {
//   RefControlProps,
// } from '@types/common/common.d'
import { useStyles, useProfileStyles } from "./styles";

import { HeadingWithSubTitle } from "@containers/section/StepSolution";
import { Agreements } from "@containers/section/FullStepSolution/form";
import { ProfileForm } from "./form";
import {
  checkUserInfo,
  checkApplicationOwner,
} from "@containers/section/AccountCommon/context";

export const AccountProfile: FC<any> = ({ query }) => {
  const classes = useStyles();
  const { t } = useTranslation("section");

  /* check user here */
  const router = useRouter();
  useEffect(() => {
    checkUserInfo(router);
  }, []);
  /* check user here */

  /* operation to profile, check the number  */
  let number: any = null;
  if ("number" in router.query) {
    number = router.query["number"];
    checkApplicationOwner(router, number);
  }


  console.log("####### operation done.")
  /* operation to profile, check the number  */

  return (
    <div className={classes.root}>
      <InnerPage>
        <div className={classes.heading}>
          <HeadingWithSubTitle
            title={t("title.customerService")}
            subTitle={t("title.accountProfile")}
          />
        </div>
        <ProfileForm />
      </InnerPage>
    </div>
  );
};

export default AccountProfile;
