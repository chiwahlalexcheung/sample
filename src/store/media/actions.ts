import { Action } from 'redux'
import APIManager from '@lib/apiManager';

export enum MediaActionTypes {
  FETCH_BANNERS = 'FETCH_BANNERS',
  FETCH_BANNERS_SUCCEED = 'FETCH_BANNERS_SUCCEED',
  FETCH_BANNERS_FAILED = 'FETCH_BANNERS_FAILED',
}

export type MediaAction =
  | Action<MediaActionTypes.FETCH_BANNERS>
  | Action<MediaActionTypes.FETCH_BANNERS_SUCCEED>
  | Action<MediaActionTypes.FETCH_BANNERS_FAILED>

export const requestBanners = ()  => ({ type: MediaActionTypes.FETCH_BANNERS });
export const requestBannersSucceed = (payload)  => ({ type: MediaActionTypes.FETCH_BANNERS_SUCCEED, payload });
export const requestBannersError = (payload) => ({ type: MediaActionTypes.FETCH_BANNERS_FAILED, payload });
export const fetchMainBanners = (isServer: boolean) => async (dispatch) => {
    dispatch(requestBanners());
    const { data, error } = await APIManager.get({
        dispatch,
        url: 'banners',
        isServer,
        underAuth: false,
    });
    
    if (error) {
      dispatch(requestBannersError(error));
      return null;
    } else {
      dispatch(requestBannersSucceed(data));
    }
};
