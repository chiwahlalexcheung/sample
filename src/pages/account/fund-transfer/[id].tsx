import { useRef } from 'react';
import Head from 'next/head'
import HeadingLayout from '@containers/layout/Heading'
import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { useTranslation } from 'next-i18next'
// import Fade from 'react-reveal/Fade';
import { GetServerSideProps } from 'next'
import FundTransfer from '@containers/section/FundTransfer'
import { storeWrapper } from '@store/store'

const FundTransferPage: React.FC<any> = ({ applicationNo }) => {
  const { t } = useTranslation('common');
  return (
    <HeadingLayout>
        <Head>
            <title>{t('menu.fund-transfer')}</title>
        </Head>
        < FundTransfer applicationNo={applicationNo} />
    </HeadingLayout>
  );
}


export const getServerSideProps: GetServerSideProps = storeWrapper.getServerSideProps(
  async ({ locale, params={} }: any) => {
    return {
      props: {
        applicationNo: params.id,
        ...await serverSideTranslations(locale, ['common', 'form', 'button', 'footer', 'section']),
      }
    };
  }
)
export default FundTransferPage
