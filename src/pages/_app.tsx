import React, { FC, useEffect } from 'react'
import Head from 'next/head'
// import { GoogleReCaptchaProvider } from 'react-google-recaptcha-v3'
import { AppProps } from 'next/app'
import { withRouter } from 'next/router';
import { appWithTranslation } from 'next-i18next'
import { ThemeProvider } from "@material-ui/styles"
import { LocalizationProvider } from '@components/common/Locale'
import CssBaseline from "@material-ui/core/CssBaseline"
import { storeWrapper } from '@store/store'
import MainTheme from '@theme/main'
import nextI18NextConfig from './../../next-i18next.config.js'
import config from 'react-reveal/globals';

import '@common/css/layout.scss'
// import "animate.css/animate.min.css"
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";

config({ ssrFadeout: true });

/**
 * withRedux and appWithTranslation HOC
 * NextJS wrapper for Redux
 */
const MainApp: FC<AppProps> = ({ Component, pageProps }) => {
  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);
  return (
    <ThemeProvider theme={MainTheme}>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <link rel="stylesheet" href="/styles/common.css"/>
      </Head>
      <CssBaseline />
      <LocalizationProvider ns={'common'}>
        <Component {...pageProps} />
      </LocalizationProvider>
    </ThemeProvider>
  );
}
// @ts-ignore
export default storeWrapper.withRedux(
  withRouter(appWithTranslation(MainApp))
)
