import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';
import {
  FullCoverBgStyles,
  // RelativeBefore,
} from '@theme/global';



const useStyles = makeStyles(theme => ({
  root: {
    position: 'relative',
    padding: '120px 0',
    '&::before': {
      content: "''",
      width: '55%',
      height: '100%',
      top: 0,
      right: 0,
      position: 'absolute',
      display: 'block',
      backgroundImage: `url(${StaticSources.bg.usefulToolBg})`,
      ...FullCoverBgStyles,
      backgroundSize: 'cover',
      background: 'white',
      backgroundPosition: 'center right',
    }
  },
  panel: {
    gap: '30px',
  },
  left: {
    width: '35%',
  },
  right: {
    width: '65%',
  },
  sentence: {
    fontSize: '20px',
    color: theme.palette.text.primary,
    margin: '90px 0 70px 0',
  },
  list: {
    fontSize: '20px',
    fontWeight: 700,
    paddingLeft: '20px',
    '@global': {
      'li': {
        marginBottom: '10px',
      },
    },
  },
}));

const useCardStyles = makeStyles(theme => ({
  root: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
    boxShadow: '2px 10px 6px #00000029',
    borderRadius: '8px',
    width: '370px',
    height: '560px',
    opacity: 1,
    backdropFilter: 'blur(50px)',
  },
  content: {
    padding: '0',
    height: '100%',
  },
  wrapper: {
    padding: '40px 30px 10px 30px',
    height: '100%',
  },
  title: {
    fontSize: '23px',
    color: 'black',
    fontWeight: 'bolder',
    height: '70px',
    textTransform: 'capitalize',
    display: 'block',
  },
  image: {
    flex: 1,
  },
  button: {
    height: '60px',
  },
  btn: {
    width: '195px',
    height: '60px',
    borderRadius: '10px',
    fontWeight: 'bolder',
  },
}));

export {
  useStyles,
  useCardStyles,
};
