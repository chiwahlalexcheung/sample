import {  ContentActionTypes } from './actions'
import { ApplicationActionTypes } from './../application/actions'
import { BasicListStateProps, BasicInfoStateProps } from '@interfaces/state'
export interface ContentState {
  readonly persona: BasicListStateProps
  readonly calculatorConfig: BasicInfoStateProps
  readonly calculation: BasicInfoStateProps
  readonly faq: BasicListStateProps
}
// export interface CalculatorResultStateProps {
//   interestOnly: number;
//   principalAndInterest: number;
//   revolving: number;
// }
const initialContentState = {
  persona: {
    list: [],
    loading: false,
    error: null,
  },
  calculatorConfig: {
    info: {
      banner: {},
      interestOnly: {},
      principalAndInterest: {},
      revolving: {},
    },
    loading: false,
    error: null,
  },
  calculation: {
    info: {
      interestOnly: 0,
      principalAndInterest: 0,
      revolving: 0,
      current: 0,
    },
    loading: false,
    error: null,
  },
  faq: {
    list: [],
    loading: false,
    error: null,
  },
}

/**
 * Content Reducer
 */
export const contentReducer = (
  state: ContentState = initialContentState,
  action: any
): ContentState => {
  const { persona, calculatorConfig, calculation, faq } = state;
  switch (action.type) {
    case ContentActionTypes.FETCH_PERSONA:
      return {
        ...state,
        persona: {
          ...persona,
          loading: true,
        },
      }
    case ContentActionTypes.FETCH_PERSONA_SUCCEED:
      return {
        ...state,
        persona: {
          ...persona,
          loading: false,
          list: action.payload,
        },
      }
    case ContentActionTypes.FETCH_PERSONA_FAILED:
      return {
        ...state,
        persona: {
          ...persona,
          loading: false,
          error: action.payload,
        },
      }
    case ContentActionTypes.FETCH_CALCULATOR_CONFIG:
      return {
        ...state,
        calculatorConfig: {
          ...calculatorConfig,
          loading: true,
        },
      }
    case ContentActionTypes.FETCH_CALCULATOR_CONFIG_SUCCEED:
      return {
        ...state,
        calculatorConfig: {
          ...calculatorConfig,
          loading: false,
          info: action.payload,
        },
      }
    case ContentActionTypes.FETCH_CALCULATOR_CONFIG_FAILED:
      return {
        ...state,
        calculatorConfig: {
          ...calculatorConfig,
          loading: false,
          error: action.payload,
        },
      }
    case ContentActionTypes.RESET_CALCULATION:
      return {
        ...state,
        calculation: initialContentState.calculation,
      }
    case ContentActionTypes.FETCH_CALCULATION:
      return {
        ...state,
        calculation: {
          ...calculation,
          loading: true,
        },
      }
    case ContentActionTypes.FETCH_CALCULATION_SUCCEED:
      const { payload={}, params={} } = action;
      const { paymentType } = params;
      return {
        ...state,
        calculation: {
          ...calculation,
          loading: false,
          info: {
            ...calculation.info,
            [paymentType]: payload.amount || 0,
            current: payload.amount || 0,
          },
        },
      }
    case ContentActionTypes.FETCH_CALCULATION_FAILED:
      return {
        ...state,
        calculation: {
          ...calculation,
          loading: false,
          error: action.payload,
        },
      }
    case ContentActionTypes.FETCH_FAQ:
        return {
            ...state,
            faq: {
            ...faq,
            loading: true,
            },
        }
    case ContentActionTypes.FETCH_FAQ_SUCCEED:
        return {
            ...state,
            faq: {
            ...faq,
            loading: false,
            list: action.payload,
            },
        }
    case ContentActionTypes.FETCH_FAQ_FAILED:
        return {
            ...state,
            faq: {
            ...faq,
            loading: false,
            error: action.payload,
            },
        }
    default:
      return state
  }
}
