import { makeStyles } from '@material-ui/core/styles';
import StaticSources from '@config/sources';

const useLanguageStyles = makeStyles(theme => ({
  link: {
    fontSize: '11px',
    color: theme.palette.text.primary,
  },
  delimiter: {
    margin: '0 5px',
  },
  clickable: {
    cursor: 'pointer',
    fontWeight: 'bolder',
  },
}));

const useRootStyles = makeStyles({
  active: {
    '@global': {
      'header': {
        opacity: '0.85',
        '&:hover': {
          opacity: 1,
        },
      }
    },
  },
});

const useInnerLayoutStyles = makeStyles({
  root: {
    width: '1234px',
    margin: '0 auto',
  }
});

const useFormStyles = makeStyles({
  form: {
    width: '100%',
  },
  input: {
    border: 0,
    fontSize: '10px',
    outline: 'none',
    '&:hover': {
      outline: 'none',
    }
  },
  styledInput: {
    background: '#FFFFFF 0% 0% no-repeat padding-box',
    boxShadow: '0px 5px 10px #00000033',
    width: '100%',
  },
  icon: {
    minWidth: 'initial',
    marginRight: '17px',
    width: '8px',
  },
  hideSubmit: {
    display: 'none',
  },
});

const useStyles = makeStyles(theme => ({
  root: {
    padding: '0 0 37px 0',
    background: 'white',
    boxShadow: '0px 2px 2px -1px rgb(0 0 0 / 5%), 0px 4px 5px 0px rgb(0 0 0 / 5%), 0px 1px 10px 0px rgb(0 0 0 / 1%)',
  },
  logo: {
    width: '166px',
    marginRight: '40px',
  },
  bar: {
    height: '37px',
    marginBottom: '19px',
    backgroundImage: `url("${StaticSources.navigationBg}")`,
    backgroundSize: 'cover',
    backgroundPosition: 'center right',
    background: 'white', 
    // paddingRight: '130px',
  },
  barMenu: {
    marginRight: '36px',
  },
  search: {
    width: '195px',
  },
  barItem: {
    color: 'white',
    fontSize: '14px',
    fontWeight: 'bold',
    marginLeft: '52px',
    '&:hover': {
      color: 'white',
    }
  },
  cs: {
    color: 'white',
    fontSize: '13px',
    fontWeight: 'bold',
  },
  csIcon: {
    minWidth: 'initial',
    marginRight: '11px',
    width: '12px',
  },
  upper: {
    textTransform: 'uppercase',
  },
  listLabel: {
    width: 'fit-content',
  },
  listButton: {
    width: 'fit-content',
    padding: '0 14px',
    height: '37px',
    marginLeft: '12px',
    borderBottomLeftRadius: '10px',
    borderBottomRightRadius: '10px',
    background: `transparent linear-gradient(181deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.main} 100%) 0% 0% no-repeat padding-box`,
    '&:hover': {
      background: `transparent linear-gradient(181deg, ${theme.palette.primary.light} 0%, ${theme.palette.primary.main} 100%) 0% 0% no-repeat padding-box`,
    }
  },
}));

export {
  useStyles,
  useFormStyles,
  useRootStyles,
  useInnerLayoutStyles,
  useLanguageStyles,
};
