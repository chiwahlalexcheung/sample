import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    border: `1px solid ${theme.palette.text.secondary}`,
    background: 'white',
    padding: '10px 12px',
    // minHeight: '150px',
  },
  hide: {
    display: 'none',
  },
  textarea: {
    border: 0,
    fontSize: '13px',
    width: '100%',
    background: 'transparent',
    padding: '0 !important',
    outline: 'none',
    resize: 'none',
    '&::placeholder': {
      color: theme.palette.text.disabled,
    },
  },
}));

export {
  useStyles,
};
