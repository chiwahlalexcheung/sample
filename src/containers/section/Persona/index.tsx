import React, { FC, useMemo, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import { useLocale, LocaleContextProps } from '@components/common/Locale'
import {
  InnerPage,
} from '@containers/layout/Heading'
import {
  HeadingWithSubTitle,
} from '@containers/section/StepSolution'
// import Slide from 'react-reveal/Slide'
import { useTranslation } from 'next-i18next'
import { usePersona } from '@store/content/context'
import {
  Typography,
  Container,
  Box,
  // Card,
  // CardContent,
} from '@material-ui/core';
import {
  useStyles,
  useSlideStyles,
} from './styles';
import {
  BannerListItem,
} from '@interfaces/banner'
import {
  useStepper,
  StepHandleProps,
} from '@hooks/common';
import {
  SlidePrevPageArrow,
  SlideNextPageArrow,
  MainSlideProps,
} from '@containers/banner/MainSlider'

// const Slider = dynamic(() => import('react-slick'), { ssr: false });

export interface ToolProps {
  label: string;
  icon: string;
  link: string;
  buttonLabel: string;
  id: string;
};

export interface UseFulCardProps extends ToolProps {
  // todo
};
export interface PersonaSliderProps {
  list: BannerListItem[];
};

export interface PersonaSlideProps extends MainSlideProps {
  active?: boolean;
  zIndex: number;
}

export const PersonaSlide: FC<PersonaSlideProps> = ({ item, zIndex, onChanged, offset, active }) => {
  const classes = useSlideStyles({ zIndex });
  const { t } = useTranslation('button');
  const { label, title, thumbnail }: BannerListItem = item;
  let styles = classes.root;
  let titleStyles = classes.title;
  let subTitleStyles = classes.subTitle;
  const bgStyle = {
    backgroundImage: `url(${thumbnail})`,
  };
  if (active) {
    styles = css(styles, classes.active);
    subTitleStyles = css(subTitleStyles, classes.activeSubTitle);
    titleStyles = css(titleStyles, classes.activeTitle);
  } else {
    styles = css(styles, classes.inActive);
  }
  return (
    <Box className={styles} onClick={() => onChanged(offset)}>
      <div className={classes.thumbnail} style={bgStyle} />
      <div className={classes.block}>
        <Typography className={titleStyles}>{label}</Typography>
        <Typography className={subTitleStyles}>{title}</Typography>
        {
          active && <CommonButton label={t('learnMore')} className={classes.button} />
        }
      </div>
      <div className={classes.overlay} />
    </Box>
  );
};

export const PersonaSlider: FC<PersonaSliderProps> = ({ list }) => {
  const classes = useStyles();
  const {
    setStep,
    goPrevStep,
    goNextStep,
    step,
  }: StepHandleProps = useStepper({ maxStep: list.length, step: 0 });
  const onChanged = useCallback((index: number) => {
    setStep(index);
}, [setStep]);
  const targetList: BannerListItem[] = list.slice(0, 3);
  const totalLength: number = targetList.length;
  // let prevArrowStyle = classes.prev;
  // let nextArrowStyle = classes.next;
  // if (step === 0) {
  //   nextArrowStyle = css(classes.next, classes.minor);
  //   prevArrowStyle = classes.prev;
  // } else if (step === totalLength - 1) {
  //   nextArrowStyle = classes.next;
  //   prevArrowStyle = css(classes.prev, classes.minor);
  // }
  // const settings = {
  //   className: "slider2 variable-width",
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   speed: 100,
  //   afterChange: onChanged,
  //   dots: false,
  //   autoplay: true,
  //   initialSlide: 0,
  //   variableWidth: true,
  //   autoplaySpeed: 10000,
  //   prevArrow: (<SlidePrevPageArrow />),
  //   nextArrow: (<SlideNextPageArrow />),
  //   // infinite: false,
  // };
  return (
    <Box display={'flex'} alignItems={'center'} justifyContent={'flex-start'} className={classes.slider}>
      <SlidePrevPageArrow className={classes.prev} onClick={goPrevStep} />
      {
        map(targetList, (l: BannerListItem, offset: number) => (
          <PersonaSlide
            zIndex={totalLength - offset}
            onChanged={onChanged}
            item={l}
            key={offset}
            offset={offset}
            active={step === offset}
          />
        ))
      }
      <SlideNextPageArrow className={classes.next} onClick={goNextStep} />
    </Box>
  );
}

export const Persona: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const { inspector }: LocaleContextProps = useLocale();
  const classes = useStyles();
  const { list }: any = usePersona();
  const bannerLists: BannerListItem[] = useMemo(() => 
    map(list, ({ thumbnail, ...item }: BannerListItem) => ({
      thumbnail,
      label: inspector({ target: item, field: 'name' }),
      title: inspector({ target: item, field: 'businessTitle' }),
      buttonLink: '/',
    }))
  , [list, inspector]);
  // const bannerLists: BannerListItem[] = [
  //   {
  //     label: 'Person 1',
  //     thumbnail: StaticSources.banner.persona1,
  //     title: 'Co-Founder & CTO',
  //     buttonLink: '/',
  //   },
  //   {
  //     label: 'Person 2',
  //     thumbnail: StaticSources.banner.persona2,
  //     title: 'Co-Founder & CEO',
  //     buttonLink: '/',
  //   },
  //   {
  //     label: 'Persona 3',
  //     thumbnail: StaticSources.banner.persona3,
  //     title: 'Co-Founder & COO',
  //     buttonLink: '/',
  //   },
  // ];
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <Box display={'flex'} justifyContent={'space-between'}>
          <Box display={'flex'} className={classes.left} alignItems={'center'} justifyContent={'flex-start'} >
            <Fade left>
              <HeadingWithSubTitle
                subTitle={t('title.personaHeader')}
              />
            </Fade>
          </Box>   
          <Box display={'flex'} className={classes.right} alignItems={'center'} justifyContent={'center'} >
            <Fade bottom>
              <PersonaSlider list={bannerLists} />
            </Fade>
          </Box> 
        </Box>
      </InnerPage>
    </div>
  );
});

export default Persona;
