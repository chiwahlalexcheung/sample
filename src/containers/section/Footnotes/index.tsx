import React, { FC, forwardRef, useCallback } from 'react'
import { map } from 'lodash'
import css from 'clsx';
// import dynamic from 'next/dynamic'
import Fade from 'react-reveal/Fade'
import StaticSources from '@config/sources'
// import { useRouter } from 'next/router'
// import LazyImage from '@components/image/LazyImage'
import CommonButton from '@components/common/Button'
import LocalImage from '@components/image/LocalImage'
import {
  InnerPage,
} from '@containers/layout/Heading'
import { useTranslation } from 'next-i18next'
import {
  Typography,
  Container,
  Box,
  Card,
  CardContent,
  // CardContent,
} from '@material-ui/core';
import {
  useStyles,
  useBoxStyles,
  useCardStyles
} from './styles';
import {
  useStepper,
  StepHandleProps,
} from '@hooks/common';


export interface FootnotesListProps {
  type: string;
  label: string;
}

export interface FootnotesListsProps {
  data: FootnotesListProps[];
}

export const FootnotesList: FC<FootnotesListsProps> = ({ data }) => {
    const classes = useBoxStyles();
    return (
        <Box display={'flex'} className={classes.root} flexDirection={'column'} justifyContent={'flex-start'} >
            {map(data, (item: FootnotesListProps)=>{
                switch(item.type)
                {
                    case "text":
                    default:
                        return (
                            <Typography className={classes.footnote}>{item.label}</Typography>
                        );
                        break;
                }
            })}
            {/* {map(item, (FootnotesListProps: ))} */}
        </Box>
    );
}

export const Footnotes: FC<any> = forwardRef((props, r) => {
  const { t } = useTranslation('section');
  const classes = useStyles();
  const footnotesInfo: FootnotesListProps[] = [
    {
      type: "text",
      label: "* According to the survey done by our company dated as of Aug 12, 2020, this Mortgage Loan Promotion Product is first ever launched amongst major money lenders providers in Hong Kong. "
    },
    {
      type: "text",
      label: "# New customers can use Payment Holiday at any time after the mortgage loan is successfully approved. "
    },
    {
      type: "text", // html?
      label: "Please call 2996 2688 for enquiries. Click here to view the terms and conditions of the Payment Holiday."
    }
  ];
  return (
    <div className={classes.root} ref={r as React.RefObject<HTMLDivElement>}>
      <InnerPage>
        <FootnotesList data={footnotesInfo} />
      </InnerPage>
    </div>
  );
});

export default Footnotes;
