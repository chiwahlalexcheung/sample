import { RefObject } from 'react'

// declare module "*.form" {
  export interface FormInputProps {
    placeholder?: string;
    value?: any;
    name?: string;
    [propName: string]: any;
  }

  export interface FormNumberProps extends FormInputProps {
    max?: number;
    min?: number;
  }

  export interface FormElementProps {
    name: string;
    register: any;
    inputProps?: FormInputProps;
  }

  export interface FormRenderElementProps {
    name: string;
    control: any;
    children?: JSX.Element;
    inputProps?: FormInputProps;
    render?: Function;
  }

  // the element which can control the form value
  export interface FormRenderWithControlElementProps extends FormRenderElementProps {
    setValue: Function;
  }
// }


