const path = require('path')

module.exports = {
    i18n: {
      defaultLocale: 'en',
      locales: ['en'],
      ns: ['common', 'footer', 'button', 'form', 'section'],
      localePath: path.resolve('./public/locales')
    },
    // localeSubPaths: {
    //   hk: 'hk',
    //   en: 'en',
    //   cn: 'cn',
    // }
}

